JML Machine Learning Toolkit V0.80a

JML is a python library for machine learning, especially reinforcement learning.
It contains general classification and regression tools along with reinforcement learning framework. Regression tools can be easily adopted as a function approximation under the RL framework. 

It is first built in 2011 as Jake's machine learning/reinforcement learning research goes on. 
Currently it is maintained by Jake (lemin). 

### How to Install ###

This package uses distuils, the default way of installing python packages. To install in your home directory, use:

  python setup.py install --home=~

To install forl all users on Linux:

  python setup.py build
  sudo python setup.py install

### Development ###

You can check the latest sources with the command:

  git clone https://lemin@bitbucket.org/lemin/jml.git

or if you have write privileges:

  git clone git@bitbucket.org:lemin/jml.git


### Contribution ###

Currently it is managed as a private project. 
If you are interested in contributing the project, please contact Jake (lemin@cs.colostate.edu).
