""" Base model for regression and classification models
    to provide basic functions such as standardization

"""
import numpy as np
from util.exp import make_standardize


class BaseModel():
    """ Base class of regression and classification models
    """

    def __init__(self, ):
        self.standardizeX = None
        self.unstandardizeX = None
        self.standardizeT = None
        self.unstandardizeT = None

    def make_normalize(self, X, T, ts=False):
        """ standardize X and T """
        
        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        if not isinstance(T, np.ndarray):
            X = np.asanyarray(T)

        self.standardizeX, self.unstandardizeX = make_standardize(X)
        X = self.standardizeX(X)
        if ts:
            self.standardizeT, self.unstandardizeT = make_standardize(T)
            T = self.standardizeT(T)
        return X, T
