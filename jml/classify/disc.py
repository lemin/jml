""" Classification Models 

    for reference, please take a look at lecture notes
    from CS545 (http://www.cs.colostate.edu/~cs545) and
    Bishop's book (Pattern Recognition and Machine Learning, Springer)

                                    by lemin (Minwoo Jake Lee)

    Contains:
        classes QDA, LDA, LogisticRegression, NonlinearLogisticRegression

    history:
        10/27/2011
        modify linear regression into class LinearRegression

    late modified: Oct 31, 2011
        add nonlinear logistic regression
"""
import numpy as np
from ..base import BaseModel
import math


class QDA(BaseModel):
    """ Quadratic Discriminant Analysis
    """

    def __init__(self):
        BaseModel.__init__(self)
        self._prob = []

    def disc_function(self, X, mu, sigma, prior):
        """ QDA discriminant function """
        
        _, n_dim = X.shape
        if not isinstance(mu, np.ndarray) or len(mu.shape) != 2:
            mu = np.asarray(mu).reshape((1, -1))
        if not isinstance(sigma, np.ndarray) or len(sigma.shape) != 2:
            sigma = sigma * np.eye(n_dim)
        sigma_inv = np.linalg.inv(sigma)
        diffv = X - mu
        return -0.5 * np.log(np.linalg.det(sigma))\
                - 0.5 * np.sum(np.dot(diffv, sigma_inv) * diffv, axis=1)\
                + np.log(prior)

    def train(self, X, T, **params):
        """ create a QDA model

        Parameters
        ----------
        X       array. shape=[n_samples, n_dim]
                source data set
        T       array. shape=[n_samples]
                target data set (label)
        stdT    boolean
                option for standardizing the target
        """

        stdT = params.pop('stdTarget', False)
        X, T = self.make_normalize(X, T, stdT)

        n_samples, n_dim = X.shape
        self.classes = np.unique(T)
        n_classes = len(self.classes)
        prior = params.pop('prior', [1. / n_classes] * n_classes)
        if isinstance(prior, int):
            prior = [prior] * n_classes
        elif len(prior) != n_classes:
            raise ValueError("wrong prior prob. info.!")

        for i, c in enumerate(self.classes):
            ci = np.where(T == c)[0]
            mu, cov = np.mean(X[ci], axis=0), np.cov(X[ci].T)
            self._prob.append((mu, cov, prior[i]))

    def use(self, X, **params):
        """ apply a linear regression model
        Parameters
        ----------
        X        data set to apply a linear model for prediction
        """

        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        X = self.standardizeX(X)
        ds = np.vstack(map(lambda p: self.disc_function(X, *p), self._prob)).T
        predicted = self.classes[np.argmax(ds, axis=1)]
        return predicted, ds


class LDA(QDA):

    def disc_function(self, X, mu, sigma, prior):
        """ LDA discriminant function
            redefined for plotting
            the result will be same w/ QDA disc_function
        """
        
        _, n_dim = X.shape
        if not isinstance(mu, np.ndarray) or len(mu.shape) != 2:
            mu = np.asarray(mu).reshape((1, -1))
        if not isinstance(sigma, np.ndarray) or len(sigma.shape) != 2:
            sigma = sigma * np.eye(n_dim)
        sigma_inv = np.linalg.inv(sigma)
        return np.sum(np.dot(X, sigma_inv) * mu \
                - 0.5 * np.dot(mu, sigma_inv) * mu \
                + np.log(prior), axis=1).flat


    def use(self, X, **params):
        """ apply a linear regression model
        Parameters
        ----------
        X        data set to apply a linear model for prediction
        """

        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        X = self.standardizeX(X)
        cov = np.add.reduce(map(lambda p: p[1] * p[2], self._prob))
        ds = np.vstack(map(lambda p: self.disc_function(X, p[0], cov, p[2]), self._prob)).T
        predicted = self.classes[np.argmax(ds, axis=1)]
        return predicted, ds
