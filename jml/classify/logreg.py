""" Classification Models 

    for reference, please take a look at lecture notes
    from CS545 (http://www.cs.colostate.edu/~cs545) and
    Bishop's book (Pattern Recognition and Machine Learning, Springer)

                                    by lemin (Minwoo Jake Lee)

    Contains:
        classes QDA, LDA, LogisticRegression, NonlinearLogisticRegression

    history:
        10/27/2011
        modify linear regression into class LinearRegression

    late modified: Oct 31, 2011
        add nonlinear logistic regression
"""
import numpy as np
from ..base import BaseModel
from ..util.exp import make_indicator_vars
from ..util.grad import scg
from copy import deepcopy as copy
from ..regress.nnet import NeuralNet
import math


class LogisticRegression(BaseModel):
    """ Linear Logistic Regression
    """

    def __init__(self):
        BaseModel.__init__(self)
        self.beta = None
        self.bias = True

    def _g(self, X):
        """ function for posterior probability
        Parameters
        ----------
        X:      array, shape=[n_samples, n_dim]

        Notes
        -----
                g_k(x) = p(C=k|x) 
                        = f(x, beta_k) / sum_m f(x, beta_m)

            assuming f(x, beta_k) = 1  when k=K
                g_k(x) = f(x, beta_k) / 1 + sum_m f(x, beta_m), k<K
                         1 / 1 + sum_m f(x, beta_m), k=K

            f is defined as
                f(x, beta_k) = e^(X beta_k), k < K
                                1 , k=K
        """
        fs = np.exp(np.dot(X, self.beta))  # N * K-1
        denom = 1 + np.cumsum(fs, axis=1)[:, -1:]
        gs = fs / denom
        return np.hstack((gs, 1 / denom))

    def train(self, X, T, **params):
        """ create a linear logistic regression model

        Parameters
        ----------
        X       array. shape=[n_samples, n_dim]
                source data set
        T       array. shape=[n_samples]
                target data set (label)
        """
        self.bias = params.pop('bias', True)  # inductive bias
        #parameters for scg
        niter = params.pop('niter', 10000)
        wprecision = params.pop('wprecision', 1e-10)
        fprecision = params.pop('fprecision', 1e-10)
        wtracep = params.pop('wtracep', False)
        ftracep = params.pop('ftracep', False)

        self.classes = np.unique(T)
        n_samples, n_dim = X.shape
        X, T = self.make_normalize(X, T)
        if self.bias:
            X = np.hstack((np.ones((n_samples, 1)), X))
        T = make_indicator_vars(T)
        self.beta = np.zeros((n_dim + 1, T.shape[1] - 1))

        def gradF(beta):
            """ gradient ascent to maximize log-likelihood"""
            self.beta[:] = beta.reshape(self.beta.shape)
            Y = self._g(X)
            return -np.dot(X.T, T[:, :-1] - Y[:, :-1]).flatten()

        def objectiveF(beta):
            """ log likelihood
                sum_n x_n (t_n,j - g_j(x_n))
            """
            self.beta[:] = beta.reshape(self.beta.shape)
            Y = self._g(X)
            #return -math.exp(np.sum(T * np.log(Y)) / n_samples)
            return -np.sum(T * np.log(Y))

        result = scg(copy(self.beta.flatten()), gradF, objectiveF,
                                    wPrecision=wprecision, fPrecision=fprecision, 
                                    nIterations=niter,
                                    wtracep=wtracep, ftracep=ftracep,
                                    verbose=True)
        self.beta[:] = result['w'].reshape(self.beta.shape)
        self.f = result['f']

    def use(self, X, **params):
        """ apply a linear regression model
        Parameters
        ----------
        X        data set to apply a linear model for prediction
        """
        X = self.standardizeX(X)
        if self.bias:
            X = np.hstack((np.ones((X.shape[0], 1)), X))
        Y = self._g(X)
        predicted = self.classes[np.argmax(Y, axis=1)]
        return predicted, Y


class NonlinearLogisticRegression(NeuralNet):
    """ Nonlinear Logistic Regression
    """

    def __init__(self, nunits):
        NeuralNet.__init__(self, nunits)
        self.stdTarget = False

    def forward(self, X):
        Y, Z = NeuralNet.forward(self, X)
        fs = np.exp(Y)
        denom = 1 + fs.cumsum(axis=1)[:, -1:]
        Y = np.hstack((fs / denom, 1. / denom))
        return Y, Z

    def backward(self, error, Z, T, lmb=0):
        return NeuralNet.backward(self, error, Z, T, lmb)

    def _errorf(self, T, Y):
        return T[:, :-1] - Y[:, :-1]

    def _objectf(self, T, Y, wpenalty):
        return -np.sum(T * np.log(Y)) + wpenalty

    def train(self, X, T, **params):
        self.classes = np.unique(T)
        T = make_indicator_vars(T)
        NeuralNet.train(self, X, T, **params)

    def use(self, X):
        Y = NeuralNet.use(self, X)
        predicted = self.classes[np.argmax(Y, axis=1)]
        return predicted, Y
