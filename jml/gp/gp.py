""" Gaussian Processes for regression

                                    by lemin (Minwoo Jake Lee)

    references
    ----------
    [1]
    
"""
import numpy as np
#from ..base import BaseModel
from jml.base import BaseModel
from jml.svm.svmdata import SVMData


class GaussianProcess(BaseModel):
    """ Gaussian Processes for regression

        GP(mu(x), K)

        parameters
        -----------
        kernel      string
                    kernel function option for covariance matrix

        Usage
        -----
            model = LinearRegression()
            model.train(Xtrain, Ttrain, 0.01)
            prediction = model.use(Xtest)
    """

    def __init__(self):
        BaseModel.__init__(self)
        self._weights = None

        self.data = None

    # TODO: add online train

    def train(self, X, T, lmb=0., **params):
        """ build a GP model over the observed data points
            when t_n = y_n + e_n and 
                 e_n = N (0, beta^{-1})
                 y_n = N (m(x_n), K)

            so, the model GP (m, K)
                m = y_n = y(x_n)
                K = 

        Parameters
        ----------
        X       array, shape=[n_samples, n_dim]
                source data set
        T       array
                target data set (label)
        lmb     float
                lambda for regularization parameter
        stdT    boolean
                option for standardizing the target
        """

        stdT = params.pop('stdTarget', False)
        beta0 = params.pop('beta', 1.)
        batch_size = params.pop('batch', 0)
        n_samples, n_dim = X.shape

        Xs, Ts = self.make_normalize(X, T, stdT)

        # kernel setting
        if self.data is None:
            self.data = SVMData(Xs, Ts.astype(float))
        else:
            self.data.set_data(Xs)
            self.data.set_label(Ts)

        self.data.set_kernel(params.pop('kernel', 'rbf'), **params)
        kernelf_ = self.data.kernelf

        if batch_size == 0: # all at once
            K = kernelf_(Xs)   # basis for future use

            self._C = K
            self._T = Ts
            self._beta = beta0

            self._m = self._T
            """
        else:
            # gradually increase the size (online)
            N = len(Xs)
            szs = range[0, N, batch_size] # + [N]
            for bs in szs:
                xs =  Xs[bs:bs+batch_size]
                self._C
            """

        return self._m, self._C


    def use(self, X, std=True):
        """ apply a linear regression model
        Parameters
        ----------
        X       array, shape=[n_samples, n_dim]
                data set to apply a linear model for prediction

        Return
        ------
        Y       array
                prediction result
        """

        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        n_samples, n_dim = X.shape
        if std:
            Xs = self.standardizeX(X)
        else:
            Xs = X

        k = self.data.kernelf(Xs).T
        k_tt = self.data.kernelf(Xs, Xs)
        c = k_tt + 1. / self._beta
        C_inv = np.linalg.solve(self._C, np.eye(self._C.shape[0]))
        m = np.dot(k.T, np.dot(C_inv, self._T))
        sigma = c - np.dot(k.T, np.dot(C_inv, k))

        """
        import pdb; pdb.set_trace()
        k_t = self.data.kernelf(Xs).T
        k_tt = self.data.kernelf(Xs, Xs)

        c = k_tt + 1. / self._beta

        m = np.dot(k_t.T, np.linalg.solve(k_tt, self._T))
        sigma = self._C - np.dot(k_t.T, np.linalg.solve(k_tt, k_t))
        """

        if self.unstandardizeT is None:
            return m, sigma
        else:
            return self.unstandardizeT(m), sigma

if __name__=='__main__':
    
    import matplotlib.pyplot as plt

    def f(x):
        return x ** 2 + x * 4 + 3

    # observed samples
    X = np.array([2,4,7,9]).reshape((-1, 1))
    #X = np.array([3,7]).reshape((-1, 1))
    T = f(X)
    model = GaussianProcess()
    m, s = model.train(X, T)

    # plot trained model
    #np.random.multivariate_normal(model._m
    #plt.

    x = np.arange(10).reshape((-1,1))
    y = f(x)
    prediction = model.use(x)

    # plot prediction results

    plt.plot(x, prediction[0], 'r-')
    # samples with prediction
    rand = np.random.multivariate_normal(prediction[0].ravel(), prediction[1], 10)
    plt.plot(rand.T, 'y-', alpha=0.8)
    plt.plot(X, T, "bo")
    plt.plot(x, y, "g-")
    plt.show()





