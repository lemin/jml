""" linRegress module implements a linear regression model

    for reference, please take a look at lecture notes
    from CS545 (http://www.cs.colostate.edu/~cs545) and
    Bishop's book (Pattern Recognition and Machine Learning, Springer)

                                    by lemin (Minwoo Jake Lee)

    history:
        9/19/2011
            create two functions makeLLS and useLLS
    late modified: Oct 27, 2011
        modify linear regression into class LinearRegression
"""
import numpy as np
from ..base import BaseModel


class LinearRegression(BaseModel):
    """ Least square Linear Regression

        Usage
        -----
            model = LinearRegression()
            model.train(Xtrain, Ttrain, 0.01)
            prediction = model.use(Xtest)
    """

    def __init__(self):
        BaseModel.__init__(self)
        self._weights = None

    def train(self, X, T, lmb=0., **params):
        """ create a linear regression model
            w = (X'X + lambda I)^-1 X' T

        Parameters
        ----------
        X       array, shape=[n_samples, n_dim]
                source data set
        T       array
                target data set (label)
        lmb     float
                lambda for regularization parameter
        stdT    boolean
                option for standardizing the target
        """

        stdT = params.pop('stdTarget', False)
        n_samples, n_dim = X.shape
        X, T = self.make_normalize(X, T, stdT)

        X = np.hstack((np.ones((n_samples, 1)), X))  # bias column
        penalty = lmb * np.eye(n_dim + 1)
        penalty[0, 0] = 0  # no penalty for bias
        self._weights = np.linalg.solve(np.dot(X.T, X) +\
                        penalty, np.dot(X.T, T))

    def use(self, X):
        """ apply a linear regression model
        Parameters
        ----------
        X       array, shape=[n_samples, n_dim]
                data set to apply a linear model for prediction

        Return
        ------
        Y       array
                prediction result
        """

        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        n_samples, n_dim = X.shape
        Xs = self.standardizeX(X)
        Xs = np.hstack((np.ones((n_samples, 1)), Xs))
        if self.unstandardizeT is None:
            return np.dot(Xs, self._weights)
        else:
            return self.unstandardizeT(np.dot(Xs, self._weights))

if __name__=='__main__':
    
    X = np.arange(10).reshape((-1,1))
    T = X * 4 + 3
    model = LinearRegression()
    model.train(X, T)
    prediction = model.use(X)
    print prediction
