""" Neural Network for SARSA

                                by Jake Lee (lemin)

    last modified: 3/7/2012
        a seperate inherited class for SARSA func approx
"""
import numpy as np
#from ..base import BaseModel
from ..util.exp import Standardizer
from ..util.grad import scg, steepest
from nnet import NeuralNet
from copy import copy
#to test other optim methods
#from scipy.optimize import minimize

#to test netlab scg
#from ..util.grad import steepest
#from netlab import scg, foptions


class NeuralNetQ(NeuralNet):
    """ Neural Network Function Approximation (SARSA)

        parameters
        ==========
        algo    string
                'TD' for general TD learning update
                'TDC' for TD with Correction in batch
    """

    def __init__(self, nunits, stateRange=None):
        NeuralNet.__init__(self, nunits)
        if stateRange is not None:
            self.stdX = Standardizer(stateRange)
            #self.stdX.set_sigma(1) # set sigma to standard
        self.stdTarget = False
        self.algo = 'TD'

    def backward(self, error, Z, T, lmb=0):
    
        if self.algo == 'TD':
            # eligibility trace
            if self.et_lambda > 0. and self.et_lambda < 1.:
                return self.et_backward(error, Z, T, lmb)
            else:
                return NeuralNet.backward(self, error, Z, T, lmb)
        elif self.algo == 'TDC':
            return self.td_backward(error, Z, T, lmb)
            

    def td_backward(self, error, Z, T, lmb=0):
        # GTD2/TDC batch update - for 2 layer only
        if self._nLayers != 2:
            raise AssertionError('td_backward() is only for 2 layer network')
        #print "check weight-------------------------------------"
        #print self._weights
        #print self._W
        #print "--------------------------------------------------"
        N = T.size

        psi_v = self.psi

        X1 = self.add_ones(Z[0])
        # v
        dZ = 1 - Z[1]**2
        phi_v = np.dot(X1.T, self._W[1][1:, :].T * dZ)
        d_phi_v = -2. * np.dot((X1 ** 2).T, self._W[1][1:, :].T * Z[1] * dZ)
        mean_err = np.mean(error)
        d_phi_psi = d_phi_v * psi_v
        h_k = mean_err * d_phi_psi - phi_v * psi_v * d_phi_psi
        #h_k = np.dot(d_phi_v, (error - np.dot(phi_v.T, psi_v))) * psi_v

        rh = float(self.rho[0]) / N 
        delta = np.dot(error, self._W[1][1:, :].T) * dZ
        dv = -rh * (np.dot(X1.T, delta) - h_k)

        # w
        lmbterm = lmb * np.vstack((np.zeros((1, self._W[1].shape[1])),
                    self._W[1][1:,]))
        rh = float(self.rho[1]) / N
        dw = -rh * (np.dot(self.add_ones(Z[1]).T, error) + lmbterm)

        #dpsi = np.dot(phi_v, (error - np.dot(phi_v.T, psi_v)))
        dpsi = np.dot(phi_v, (mean_err - np.dot(phi_v.T, psi_v)))

        return self.pack([dv, dw, dpsi])


    def et_backward(self, error, Z, T, lmb=0):
        if self._nLayers != 2:
            raise AssertionError('et_backward() is only for 2 layer network')

        delta = 1 # error
        N = T.size
        dws = []
        
        rl = self.et_lambda * self.gamma
        rl = np.power(rl, np.arange(N)[::-1].reshape((-1, 1)))

        np.set_printoptions(precision=3)
        # et is NOT TESTED for more than two hidden units
        if True:
            for i in xrange(self._nLayers - 1, -1, -1):
                rh = float(self.rho[i]) / N
                #if i==0:
                #    lmbterm = 0
                #else:
                #    lmbterm = lmb * np.vstack((np.zeros((1, self._W[i].shape[1])),
                #                self._W[i][1:,]))

                # delta_w Q 
                #Zi = self.add_ones(rl * Z[i])
                Zi = rl * self.add_ones(Z[i])
                if isinstance(delta, np.ndarray):
                    dw = np.array([Zi[:, ci] * delta.T
                                    for ci in xrange(Zi.shape[1])])
                    #import pdb; pdb.set_trace()
                    # flatten 
                    dw = np.array([dw[:, :, ni].flatten()
                                    for ni in xrange(dw.shape[2])])
                else:
                    dw = Zi
                #print dw

                # cumulative sum for eligibility trace
                dw = np.cumsum(dw, 0)
                # compensate rl for each step

                rl[rl==0] = 1
                dw = (1. / rl) * error * dw
                # avg. over multiple samples
                dw = -rh * np.sum(dw, 0)
                dws.insert(0, dw)

                #dw = -rh * np.outer(delta, np.cumsum(rl * Z[i], axis=0)) + lmbterm
                
                if i != 0:
                    # for multiple this should be modified
                    # this is based on assumption that _W[i] is weight for
                    # 1 output unit
                    delta = delta * self._W[i][1:, :].T *(1 - Z[i]**2)
        else:
            # dW
            #import pdb; pdb.set_trace()
            zeroidx = np.where(rl==0)[0]
            dw = rl * self.add_ones(Z[1])
            dw = np.cumsum(dw, 0) / rl

            if len(zeroidx) > 0:
                dw[zeroidx, :] = 0
            dws.insert(0, -np.sum(error * dw, 0) / N)

            # dV
            dv = None
            X1 = self.add_ones(Z[0])
            for ci in xrange(X1.shape[1]):
                dw = X1[:, ci] * (rl * self._W[1][1:,:].T * (1-Z[1]**2)).T
                dw = error * np.cumsum(dw.T, 0) / rl
                if len(zeroidx) > 0:
                    dw[zeroidx, :] = 0
                dw = -np.sum(dw, 0) / N

                if dv is None:
                    dv = dw
                else:
                    dv = np.vstack((dv, dw))
            dws.insert(0, dv)

        #print "---------------------------------------------------------------"
        #print NeuralNet.backward(self, error, Z, T, lmb)
        #print "==============================================================="
        #print self.pack(dws)
            
        return self.pack(dws)


    def _errorf(self, T, Y, mat=False):
        if self._AI is not None:
            Ybest = Y[xrange(Y.shape[0]), self._AI.flat]
            Ybest.shape = T.shape
            error = T - Ybest
            if mat:
                return error
            else:
                error_mat = np.zeros(Y.shape)
                error_mat[xrange(Y.shape[0]), self._AI.flat] = error.flatten()
                return error_mat
        return T - Y

    def _objectf(self, T, Y, wpenalty):
        error = self._errorf(T, Y, True)
        return .5 * np.mean(np.square(error)) + wpenalty

    def unpack(self, weights):
        if self.algo == 'TDC':
            self._weights[:] = weights[:-self.psi.size]
            self.psi[:] = weights[-self.psi.size:].reshape(self.psi.shape)
        else:
            NeuralNet.unpack(self, weights)
            
    def cp_weight(self):
        if self.algo == 'TDC':
            return np.hstack((self._weights, np.zeros(self.psi.size)))
        else:
            return copy(self._weights)

    def train(self, X, Q, R, Qn, AI=None, **params):
        """ train Q neural network
            
            gamma       float  [0,1]
                        discounting factor
            et_lambda   float  [0,1]
                        eligibility trace 
            algo        string 
                        learning algorithm (TD/TDC)
        """
        self._AI = AI
        self.gamma = params.pop('gamma', 0.9)
        self.et_lambda = params.pop('et_lambda', 0.)

        self.algo = params.pop('algo', 'TD') 

        if self.algo == 'TDC':
            self.psi = np.zeros(self._W[0].shape)
        else:
            self.psi = None

        # SARSA
#        if self.et_lambda > 0. and self.et_lambda <= 1.:
#            T = R + self.gamma * Qn
#            N = X.shape[0]
#            et = np.arange(N)[::-1].reshape((N, 1))
#            et = np.power(self.gamma * self.et_lambda, et)
#            T = T * et
#        else:
        T = R + self.gamma * Qn

        # SARSA(lambda)
        # alpha * ( R + gamma * Qn - Y) * et
        # et = gamma * lambda * et_1 + gradient
        # in JML,  error becomes
        #       error * et ; et = gamma * lambda * et_old + gradient
        # CHECK: how to represent/compute the gradient 
        NeuralNet.train(self, X, T, **params)

    def search_action(self, S, A, wprec=1e-12, fprec=1e-12, nIter=10000, optim='steepest'):

        DA = A.shape[1]
        #DS = S.shape[1]

        def gradf(act):
            SA = np.hstack((S,np.asarray(act).reshape((1, -1))))
            SA = self.stdX.standardize(SA)

            #print "gradf", SA

            [Y,Z] = self.forward(SA)
                
            delta = np.identity(self._W[self._nLayers-1].shape[1])
            for i in xrange(self._nLayers-1,0,-1):
                delta = np.dot(delta, self._W[i][1:,:].T) * (1 - Z[i]*Z[i])
            last = self._W[0].shape[0]
            grad = np.dot(-delta, self._W[0][(last-DA):,:].T)
            grad.shape=grad.size,

            #print "gradf:grad", grad
            return grad #, False

        def qfunc(act):
            SA = np.hstack((S,np.asarray(act).reshape((1, -1))))
            SA = self.stdX.standardize(SA)
            #print "qfunc:", SA
            [Y,Z] = self.forward(SA)
            #print -Y
            if isinstance(Y, np.ndarray):
                return -Y.item()  # maximize Q
            else:
                return -Y

        if optim=='steepest':
            result = steepest(A.flatten(), gradf, qfunc,
                                wtracep=True, ftracep=True,
                                wPrecision=wprec, fPrecision=fprec, nIterations=nIter)
        elif optim=='scg':
            if True:
                """ JML SCG"""
                result = scg(A.flatten(), gradf, qfunc,
                             wtracep=True, ftracep=True,
                             wPrecision=wprec, fPrecision=fprec, nIterations=nIter)
            else:
                """ Netlab SCG for test"""
                options = foptions()
                res = scg(qfunc, A.flatten(), options, gradf,
                             returnFlog=True,
                             returnPoint=True)
                result = {'w': res[0], 'wtrace': res[2], 'ftrace': res[1]}
        else:
            res= minimize(qfunc, A, method=optim, jac=gradf, 
                          options={'gtol': 1e-6, 'disp':False})
            result = {'w': res['x']}

        # FOR DEBUG, plot f and w traces
        if False and (optim=='steepest' or optim=='scg'):
            import matplotlib.pyplot as plt
            plt.ioff()
            plt.figure(21)
            plt.subplot(1,2, 1)
            plt.plot(np.around(np.asarray(result['ftrace']), 5).flat)
            plt.subplot(1,2, 2)
            plt.plot(np.around(np.asarray(result['wtrace']), 4).flat)
            print np.asarray(result['ftrace']).flatten()
            plt.show()
        #import pdb; pdb.set_trace()
        #print result['f']
        #print result['reason']
        return result['w']
