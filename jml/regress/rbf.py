""" RBFtiling function approximation

   references:
       A. Barreto and C. Anderson (2007)
         http://dl.acm.org/citation.cfm?id=1332315
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html


                            by lemin (Minwoo Jake Lee)

    Currently RBF have
        makeLLS(X, T, lmb) to create a linear regression model
        useLLS(X, model) to apply the model to new data

    ### NOT COMPLETE: need more test!!! ######################################

    late modified: Oct 19, 2011
        - different gradient for bias
    
    history:
        10/17/2011
        - prevent width shrinkage over the threshold
        - division by sample size and output dimension on
          gradient for center and sigma

        10/14/2011
        - removed _localRBF function 
        - change _forward function to use matrix operation 
          (removed loops)
        - ignore data samples on sigma and center update 
          if maxtheta is less than tau

        10/07/2011
        - add RGD original (online version - only for RL) 
        - change previous RGD to RGDBatch
        
        10/02/2011
        - removed option attribute
"""


from scipy import random, exp, zeros, dot, sqrt
from scipy.linalg import norm, pinv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from ..util.grad import scg, steepest
from math import ceil, log
from ..util.exp import Standardizer
import sys
from copy import deepcopy as copy


XX = []
SIGMA, SIGMA_ONLY = range(1, 3)
debug = None


# Search the border indices from a sorted list and return the list of indices
#
# @T    input list
def get_border_indices(T):
    """ a function to search the border indices
        from a list and return the list of indices

        Usage:
            a = [0,0,1,1,1,2,2,2]
            border_i = get_border_indices(a)
    """
    return map(lambda t: np.min(np.argwhere(T == t)), np.sort(np.unique(T)))


class RBF:
    """ Fixed RBF tiling function approximation
        - directly solve for W w/o gradient descent
          http://www.rueckstiess.net/research/snippets/show/72d2363e
    """

    def __init__(self, indim, numCenters, outdim):
        self.indim = indim
        self.outdim = outdim
        self.numCenters = numCenters
        self.centers = [random.uniform(-1, 1, indim)
                        for i in xrange(numCenters)]
        self.beta = 8
        self.W = random.random((self.numCenters, self.outdim))

    def _basisfunc(self, c, d):
        assert len(d) == self.indim
        return exp(-self.beta * norm(c - d) ** 2)

    def _calcAct(self, X):
        # calculate activations of RBFs
        G = zeros((X.shape[0], self.numCenters), float)
        for ci, c in enumerate(self.centers):
            for xi, x in enumerate(X):
                G[xi, ci] = self._basisfunc(c, x)
        return G

    def train(self, X, Y):
        """ train with data and label
            X: matrix of dimensions n x indim
            y: column vector of dimension n x 1 """

        # choose random center vectors from training set
        rnd_idx = random.permutation(X.shape[0])[:self.numCenters]
        self.centers = [X[i, :] for i in rnd_idx]

        # calculate activations of RBFs
        G = self._calcAct(X)

        # calculate output weights (pseudoinverse)
        self.W = dot(pinv(G), Y)

    def test(self, X):
        """ apply the trained model
            X: matrix of dimensions n x indim """
        G = self._calcAct(X)
        Y = dot(G, self.W)
        return Y


class RGD:
    """ Restricted Gradient Descent with SCG/Steepest Descent
        
        online version of RGD for reinforcement learning
            by A. Barreto and C. Anderson (2007)
            http://dl.acm.org/citation.cfm?id=1332315
    """

    ##########################################################################
    # object interface
    ##########################################################################
    def __init__(self, ni, ntiles, no=1, **params):
        """ create RGD object and set initial values. """

        self.ni, self.no = ni, no
        self.ntiles = ntiles   # the number of RBF tiles
        self.sigma = params.pop('sigma', 8)  # control the generalization
        self._gamma = params.pop('gamma', 0.9)
        self._lambda = params.pop('Lambda', 0)
        self._alpha = params.pop('alpha', 0.01)
        beta = params.pop('beta', [self._alpha * 0.1] * 2)
        self._beta_c, self._beta_s =\
            beta if isinstance(beta, list) else [beta] * 2
        self.verbose = params.pop('verbose', False)
        centers = params.pop('centers', None)
        weights = params.pop('weights', None)
        self.bias = params.pop('bias', 0)
        # 20111017 - width shrinkage limit
        self.shrink_limit = params.pop('shrinklimit', 0.99)
        self.shrink_limit = sqrt(1 / -2 * log(self.shrink_limit))

        if weights is None:  # +1 bias
            self.W = random.uniform(-.1, .1, (self.ntiles + self.bias, self.no))
        else:
            self.W = weights
        if debug >= SIGMA:  # for debugging
            self.W[0] = 0
            self.W[1] = 1
        if centers is None:
            self.C = np.array([random.uniform(-1, 1, self.ni)
                    for i in xrange(self.ntiles)])
        else:
            self.C = centers
        if debug >= SIGMA_ONLY:  # for debugging
            self.C = np.array([0])
        self.sigmas = [self.sigma] * self.ntiles

    ##########################################################################
    # protected methods for RBF computation
    ##########################################################################
    def _forward(self, X):
        """ process feed-forward and return the output along with
            intermediate hidden layer outputs. """

        Z = np.asarray(map(lambda c: -np.sum((X - c) ** 2, axis=1), self.C)).T
        Z = exp(Z / np.asarray(self.sigmas) ** 2)

        if self.bias == 1:
            Z1 = np.hstack((np.ones((Z.shape[0], 1)), Z))
        else: 
            Z1 = Z
        return dot(Z1, self.W), Z

    ##########################################################################
    # Train and Test interface
    ##########################################################################
    def train(self, X, T, R=np.array([]), Q=np.array([]), Ai=np.array([]), **param ):
        # gradient
        tau = param.pop('threshold', 0.5)
        stop = param.pop('stop', 5)

        while True:
            old_c = copy(self.C)
            old_w = copy(self.W)
            old_s = np.asarray(copy(self.sigmas))

            Y, Z = self._forward(X)
            error = R + self._gamma * Q - Y
            dwE = np.dot(Z.T, error) / X.shape[0]

            # update weights for center and sigma update
            self.W = self.W + self._alpha * dwE

            nzr = Z.shape[0]
            maxI = np.argmax(Z, 1).reshape(nzr, 1)
            ri = np.arange(nzr).reshape(nzr, 1)
            maxtheta = Z[ri, maxI]

            dc = np.zeros(self.C.shape)
            ds = np.zeros(self.C.shape[0])
            weighted_error = np.dot(error, self.W.T)
            errdotprod = weighted_error[ri, maxI]
            numer = errdotprod * 4 * maxtheta
            dist_center = X - self.C[maxI.flatten(), :]

            pos_error_i = np.where(errdotprod > 0)[0]
            neg_error_i = np.where(errdotprod <= 0)[0]
            dci = maxI[pos_error_i]
            dsi = maxI[neg_error_i]

            sigmas = np.asarray(self.sigmas)[maxI]
            gd_c = self._beta_c * (numer[pos_error_i] *
                    dist_center[pos_error_i, :] / sigmas[pos_error_i] ** 2)
            nrm = np.asarray(map(norm, dist_center[neg_error_i, :]))
            gd_s = self._beta_s * (numer.flat[neg_error_i] *
                    nrm ** 2 / sigmas.flat[neg_error_i] ** 3)

            argsort_dci = np.argsort(dci.flat)
            argsort_dsi = np.argsort(dsi.flat)
            sort_dci = np.unique(dci.flat[argsort_dci])
            sort_dsi = np.unique(dsi.flat[argsort_dsi])

            if len(sort_dci) > 0:
                t_dc = np.add.reduceat(gd_c[argsort_dci],
                                        get_border_indices(sort_dci))
            else:
                t_dc = 0
            dc[sort_dci, :] = t_dc
            if len(sort_dci) > 0:
                t_ds = np.add.reduceat(gd_s[argsort_dsi],
                                        get_border_indices(sort_dsi))
            else:
                t_ds = 0
            ds[sort_dsi] = t_ds

            self.C -= dc
            self.sigmas = list(np.array(self.sigmas) - ds)

            if False:
                for j in xrange(len(X)):
                    Y, Z = self._forward(X[slice(j, j+1), :])
                    error = R[j] + self._gamma * Q[j] - Y
                    self.W += self._alpha * np.dot(Z.T, error)

                    # max_j \theta_j(s)
                    i = np.argmax(Z, 1)[0]
                    thetai = Z[0, i]  # only one row
                    
                    if thetai < tau:
                        # allocate new hidden unit
                        self.W = np.vstack((self.W, [0] * self.W.shape[1]))
                        self.C = np.vstack((self.C, X[j, :]))
                        tsigma= sqrt(-norm(X[j, :] - self.C[i]) ** 2 /\
                                (2 * log(tau)))
                        self.sigmas.append(tsigma)
                        self.ntiles += 1
                    else:
                        numer = np.dot(error, self.W[i, :].T) * 4 * thetai
                        dist_c = X[j, :] - self.C[i]
                        if np.dot(error,self.W[i, :]) > 0:
                            # update ci
                            dci = numer * dist_c / (self.sigmas[i] ** 2)
                            self.C[i] -= self._beta_c * dci
                        else:
                            # update sigma_i
                            dsigmai = numer * norm(dist_c) ** 2 /\
                                        (self.sigmas[i] ** 3)
                            self.sigmas[i] -= self._beta_s * dsigmai[0]

            if len(old_c) == len(self.C):
                a = (np.linalg.norm(old_c - self.C),
                        np.linalg.norm(old_w - self.W),
                        np.linalg.norm(old_s - np.asarray(self.sigmas)))
                #print a
                if max(a) < 0.1: break
            #else:
            #    print len(self.C)
            # check stopping criteria
            if len(self.C) >= stop: break

    def use(self, X):
        Y, _ = self._forward(X)
        return Y


class RGDBatch(RGD):
    """ Restricted Gradient Descent with SCG/Steepest Descent

        Variation from orignal RGD:
            - instead of maintaining weights for each action,
              we assume that action is part of input for the state
            - extended RGD for regression
            - RGD supports batch algorithm

        sigma shrinks only, so having initial large sigma value is recommended.

        Usage:
            rbf = RGDBatch(ni, ntiles, no, alpha=test_alpha, beta=test_beta)
            rbf.train(X, T,
                        option='normal',
                        stop=15,
                        threshold=0.5,
                        showprog=True,
                        nIterations=n_iter)
            Y = rbf.use(X)
    """

    ##########################################################################
    # object interface
    ##########################################################################
    def __init__(self, ni, ntiles, no=1, **params):
        """ create RGDBatch object and set initial values. """

        RGD.__init__(self, ni, ntiles, no, bias=1, **params)
        self.stdX = None
        self.stdT = None

    ##########################################################################
    # Plotting interface
    ##########################################################################
    def plot_hunits_results(self, Z, X, T, **params):
        """ plot radial basis functions for each center and
            the final output. """

        plt.clf()
        opt = params.pop('option', '1d')
        bstd = params.pop('standardize', False)
        if bstd:
            Xs = self.stdX.standardize(X)
            Ts = self.stdT.standardize(T)
        else:
            Xs, Ts = X, T

        if opt == '1d':
            plt.plot(Xs, Ts, "ro")
            xval = np.arange(-1.5, 1.5, 0.1)
            yval, Z = self._forward(xval)
            plt.plot(xval, Z, "--")
            plt.plot(xval, yval, "-")
        elif opt == '2x1':
            fig = plt.figure(1)
            plt.clf()
            nrplt, ncplt = int(ceil(self.ntiles / 2.)) + 1, 2

            pos = np.linspace(-2., 2., 41)
            xs, ys = np.meshgrid(pos, pos)
            xval = np.vstack((xs.flat, ys.flat)).T
            yval, Z = self._forward(xval)

            # plot hidden units
            for i in xrange(self.ntiles):
                rect = fig.add_subplot(nrplt, ncplt, i + 1).get_position()
                ax = Axes3D(fig, rect)
                #ax.plot_surface(xs, ys, Z[:, i].reshape(xs.shape))
                ax.contourf(xs, ys, Z[:, i].reshape(xs.shape))

            ridx = np.where(Ts[:, 0] > 0.5)[0]
            bidx = np.where(Ts[:, 0] <= 0.5)[0]

            lastidx = (nrplt - 1) * ncplt
            rect = fig.add_subplot(nrplt, ncplt, lastidx + 1).get_position()
            ax = Axes3D(fig, rect)
            ax.plot_surface(xs, ys, yval[:, 0].reshape(xs.shape))

            #ax.scatter(Xs[:, 0], Xs[:, 1], Ts[:, 0], c='r', marker='o')
            # for Marble test
            if len(ridx) > 0:
                ax.scatter(Xs[ridx, 0], Xs[ridx, 1], Ts[ridx, 0], c='r', marker='^')
            if len(bidx) > 0:
                ax.scatter(Xs[bidx, 0], Xs[bidx, 1], Ts[bidx, 0], c='g', marker='o')

            rect = fig.add_subplot(nrplt, ncplt, lastidx + 2).get_position()
            if False:
                ax.plot_surface(xs, ys, yval[:, 1].reshape(xs.shape))
                ax.scatter(Xs[:, 0], Xs[:, 1], Ts[:, 1], c='r', marker='o')
            else:
                cs = plt.contourf(xs, ys, yval[:, 0].reshape(xs.shape))
                plt.colorbar(cs)
                #plt.plot(Xs, Ts, 'go')  # for XOR normal plot
                #plt.plot(Xs[:,0], Xs[:,1], 'go')  # for XOR RL plot
                # for Marble test
                plt.plot(Xs[bidx,0], Xs[bidx,1], 'go')  
                plt.plot(Xs[ridx,0], Xs[ridx,1], 'r^')  
                #ax.scatter(Xs[:,0],Xs[:,1],Ts[:,1],c='r', marker='o')

    ##########################################################################
    # Train and Test interface
    ##########################################################################
    def train(self, X, T, R=np.array([]), Q=np.array([]),
            Ai=np.array([]), **param):
        """ train a RGDBatch network with given data. """

        self._gamma = param.pop('gamma', self._gamma)

        # gradient
        tau = param.pop('threshold', 0.5)
        stop = param.pop('stop', 5)
        option = param.pop('option', 'TD')
        optim = param.pop('optim', 'scg')
        # 20111017 - set learning rate to if optim = 'scg'
        if optim == 'scg':
            self._alpha, self._beta_c, self._beta_s = 1, 1, 1

        #parameters for scg
        niter = param.pop('nIterations', 1000)
        wprecision = param.pop('wprecision', 1e-10)
        fprecision = param.pop('fprecision', 1e-10)
        wtracep = param.pop('wtracep', False)
        ftracep = param.pop('ftracep', False)
        #bwmin = param.pop('bwmin', False)
        #wmin = param.pop('wmin', -1)
        #bwmax = param.pop('bwmax', False)
        #wmax = param.pop('wmax', 1)

        #regression option
        bnormtarget = param.pop('normtarget', True)

        #progress visualization
        show_progress = param.pop('showprog', False)
        showprog = '1d'
        if show_progress:
            showprog = ''.join((str(X.shape[1]), 'x', str(T.shape[1])))
            plt.ion()

        #debug option
        debugopt = param.pop('freeze', None)
        if debugopt is None:
            debug_freezeweight = False
            debug_freezesigma = False
            debug_freezecenter = False
        else:
            debug_freezeweight = 'weight' in debugopt
            debug_freezesigma = 'sigma' in debugopt
            debug_freezecenter = 'center' in debugopt

        #if stop <= self.ntiles:
        #    print "Stop condition (",stop,") is not properly selected!"
        #    sys.exit(1)

        if self.stdX == None:
            self.stdX = Standardizer(X)
        Xs = self.stdX.standardize(X)
        # for DEBUG ############################################
        stdc = param.pop('stdC', False)
        if stdc:
            print "Standardize C"
            self.C = self.stdX.standardize(self.C)
        ########################################################3
        if option == 'normal' and bnormtarget and self.stdT == None:
            self.stdT = Standardizer(T)
            Ts = self.stdT.standardize(T)
        else:
            Ts = T

        def pack(dw, dc, dsigma):
            return np.hstack((dw.flat, dc.flat, dsigma))

        def unpack(w):
            lw, lc = len(self.W.flat), self.C.size
            shpw, shpc = self.W.shape, self.C.shape
            self.W = w[:lw].reshape(shpw)
            self.C = w[lw:lw + lc].reshape(shpc)
            #self.sigmas[:] = w[lw + lc:]
            # 20111017 - shrinkage limit
            newsigma = w[lw + lc:]
            if True:
                shrinklimit_i =\
                    np.where(newsigma < self.shrink_limit)[0]
                if len(shrinklimit_i) > 0:
                    newsigma[shrinklimit_i] = np.asarray(self.sigmas)[shrinklimit_i]
            self.sigmas[:] = newsigma[:]

        def errorf():
            Y, _ = self._forward(Xs)

            if option == 'normal':
                error = (Ts - Y)
            else:
                if Y.shape[1] != 1:
                    nSamples = Y.shape[0]
                    Ybest = np.zeros((nSamples, 1))
                    for i in xrange(nSamples):
                        Ybest[i, 0] = Y[i, Ai[i, 0]]
                    error = R + self._gamma * Q - Ybest
                else:
                    error = R + self._gamma * Q - Y
            return error

        def gradientf(weights, *fargs):
            unpack(weights)
            error = errorf()

            Y, Z = self._forward(Xs)
            if debug_freezeweight:
                dwE = np.zeros(self.W.shape)
            else:
                Z1 = np.hstack((np.ones((Z.shape[0], 1)), Z))
                dwE = np.dot(Z1.T, error) / Xs.shape[0]
                # 20111019
                dwE[0] = np.sum(error) / Xs.shape[0]
            #print np.dot(Z1.T, error)
            #print self.W.flatten()
            #print "$\\sigma$",self.sigmas

            # update weights for center and sigma update
            #self.W = self.W + self._alpha * dwE  # 20111014-dwE 

            #Y, Z = self._forward(Xs)  # 20111014-dwE 
            nzr = Z.shape[0]
            maxI = np.argmax(Z, 1).reshape(nzr, 1)
            ri = np.arange(nzr).reshape(nzr, 1)
            maxtheta = Z[ri, maxI]

            ignore_i, _ = np.where(maxtheta < tau)  # 20111014-Ignore

            dc = np.zeros(self.C.shape)
            ds = np.zeros(self.C.shape[0])
            weighted_error = np.dot(error, self.W[1:, :].T)
            errdotprod = weighted_error[ri, maxI]
            numer = errdotprod * 4 * maxtheta
            dist_center = Xs - self.C[maxI.flatten(), :]

            pos_error_i = np.where(errdotprod > 0)[0]
            neg_error_i = np.where(errdotprod <= 0)[0]

            # 20111014-Ignore
            if len(ignore_i) > 0:
                pos_error_i = set(pos_error_i).difference(ignore_i)
                neg_error_i = set(neg_error_i).difference(ignore_i)
                pos_error_i = np.asarray(list(pos_error_i))
                neg_error_i = np.asarray(list(neg_error_i))

            sigmas = np.asarray(self.sigmas)[maxI]
            if len(pos_error_i) > 0:
                dci = maxI[pos_error_i]
                gd_c = self._beta_c * (numer[pos_error_i] *
                        dist_center[pos_error_i, :] / sigmas[pos_error_i] ** 2)
            else:
                dci = np.array([])
            if len(neg_error_i) > 0:
                dsi = maxI[neg_error_i]
                nrm = np.asarray(map(norm, dist_center[neg_error_i, :]))
                gd_s = self._beta_s * (numer.flat[neg_error_i] *
                        nrm ** 2 / sigmas.flat[neg_error_i] ** 3)
            else:
                dsi = np.array([])

            argsort_dci = np.argsort(dci.flat)
            argsort_dsi = np.argsort(dsi.flat)
            sort_dci = np.unique(dci.flat[argsort_dci])
            sort_dsi = np.unique(dsi.flat[argsort_dsi])

            if len(sort_dci) > 0:
                t_dc = np.add.reduceat(gd_c[argsort_dci],
                                        get_border_indices(sort_dci))
                dc[sort_dci, :] = t_dc / Xs.shape[0]  # 20111017
            if len(sort_dsi) > 0:
                t_ds = np.add.reduceat(gd_s[argsort_dsi],
                                        get_border_indices(sort_dsi))
                ds[sort_dsi] = t_ds / Xs.shape[0]  # 20111017

            print ds, dc
            # to avoid doubly updating
            #if not debug_freezeweight:
            # 20111014-dwE 
            #    Y, Z = self._forward(Xs)
            #    Z1 = np.hstack((np.ones((Z.shape[0], 1)), Z))
            #    dwE = self._alpha * np.dot(Z1.T, error) /\
            #            (Xs.shape[0] * Ts.shape[1])
            if debug_freezesigma:
                ds[:] = 0
            if debug_freezecenter:
                dc[:] = 0

            # show the progress or movement of RBF
            if show_progress:
                if option == 'normal':
                    self.plot_hunits_results(Z, Xs, Ts, option=showprog)
                else:
                    self.plot_hunits_results(Z, Xs, R + self._gamma * Q, option=showprog)
                plt.draw()

            # 20111017 - shrinkage limit
            if True:
                shrinklimit_i =\
                    np.where(np.asarray(self.sigmas) < self.shrink_limit)[0]
                if len(shrinklimit_i) > 0:
                    ds[shrinklimit_i] = 0

            return pack(-dwE, -dc, -ds)

        # optimization target function : MSE / LL / TD / Q
        def optimtargetf(weights):
            unpack(weights)
            Y, _ = self._forward(Xs)
            wpenalty = self._lambda * np.dot(self.W.flat, self.W.flat)
            if option == 'normal':
                return (0.5 * np.mean(np.square(Ts - Y)) + wpenalty)
            else:
                if Y.shape[1] != 1:
                    nSamples = Y.shape[0]
                    Ybest = np.zeros((nSamples, 1))
                    for i in xrange(nSamples):
                        Ybest[i, 0] = Y[i, Ai[i, 0]]
                    return 0.5 * np.mean((R + self._gamma * Q - Ybest) ** 2)\
                            + wpenalty
                return 0.5 * np.mean(np.square(R + self._gamma * Q - Y))\
                            + wpenalty

        ### RGD Training - 20110909
        while True:
            # update weights, centers and sigmas, given fixed # of centers
            if optim == 'scg':
                scgResult = scg(pack(self.W, self.C, self.sigmas),
                                    gradientf, optimtargetf,
                                    wPrecision=wprecision,
                                    fPrecision=fprecision,
                                    nIterations=niter,
                                    wtracep=wtracep, ftracep=ftracep,
                                    verbose=True)
            else:
                scgResult = steepest(pack(self.W, self.C, self.sigmas),
                                    gradientf, optimtargetf,
                                    xPrecision=wprecision,
                                    fPrecision=fprecision,
                                    nIterations=niter, stepsize=.1)
            if self.verbose:
                print scgResult['reason']

            # now, update centers with optimized weights, centers, and sigmas
            Y, Z = self._forward(Xs)
            nzr = Z.shape[0]
            maxI = np.argmax(Z, 1).reshape(nzr, 1)
            ri = np.arange(nzr).reshape(nzr, 1)
            maxtheta = Z[ri, maxI]
            if np.any(maxtheta < tau) and self.ntiles <= stop and\
                not debug_freezecenter:
                if self.verbose:
                    print "adding new center"
                j = np.argmin(maxtheta)  # choose the min instead of the first
                i = maxI[j, 0]
                #self.W = np.vstack((self.W, [0] * self.W.shape[1]))
                # 20111018 - initiate new weight with TD error
                self.W = np.vstack((self.W,
                            R[j] + self._gamma * Q[j] - Y[j]))
                self.C = np.vstack((self.C, Xs[j, :]))  # new center to current
                tsigma = sqrt(-norm(Xs[j, :] - self.C[i, :]) ** 2 /
                        (2 * log(tau)))
                self.sigmas.append(tsigma)
                self.ntiles += 1
            else:  # no need to add centers => stop the loop
                break
        
        if show_progress:
            plt.ioff()

        unpack(scgResult['w'])
        self.f = scgResult['f']
        if ftracep:
            self.ftrace = scgResult['ftrace']
        return self.W

    def use(self, X, **params):
        """ apply the trained RGDBatch network on new test data. """

        bhunit = params.pop('hunit', False)
        if self.stdX:
            Xs = self.stdX.standardize(X)
        else:
            Xs = X
        Y, Z = self._forward(Xs)
        if self.stdT:
            Y = self.stdT.destandardize(Y)
        if bhunit:
            return Y, Z
        else:
            return Y


# Parameter search experiment
#
# @X        test data
# @T        test label
# @params   test options
def param_search(X, T, **params):
    """ run a batch experiment to find a good parameters for
        a RGD network with some data and labels. """

    alpha_sets = params.pop('alphas', [.01, 0.1, 1, 10])
    beta_sets = params.pop('betas', [.01, 0.1, 1, 10])
    n_repeat = params.pop('repeat', 10)
    n_iter = params.pop('niter', 1e6)

    plt.ion()
    rmses = {}
    for test_alpha in alpha_sets:
        for test_beta in beta_sets:
            rmse = []
            for rep in xrange(n_repeat):
                print (">>>>>>>>>>>>>>>>>>>>>>> ", test_alpha, " ", test_beta,
                        " <<<<<<<<<<<<<<<<<<<<<<<<")
                ni, no, ntiles = X.shape[1], T.shape[1], 1
                #rbf = RGDBatch(ni, ntiles, no, alpha=1.0, beta=0.1, sigma=1)
                rbf = RGDBatch(ni, ntiles, no, alpha=test_alpha, beta=test_beta)
                rbf.train(X, T,
                            option='normal',
                            #optim='steepest',
                            stop=15,  # stop at number of tiles
                            threshold=0.5,
                            showprog=True,
                            nIterations=n_iter)
                Y = rbf.use(X)
                print "====== result ======="
                print np.hstack((Y, T))
                print "====================="
                print "center=", rbf.C
                print "sigma=", rbf.sigmas
                print "w=", rbf.W
                print "RMSE = ", np.sqrt(np.mean((T - Y) ** 2))
                print "====================="
                rmse.append(np.sqrt(np.mean((T - Y) ** 2)))
            rmses[(test_alpha, test_beta)] = np.mean(rmse)

    if len(alpha_sets) + len(beta_sets) > 2:
        plt.clf()
        plt.plot(rmses.values(), "o-")
        plt.xticks(range(len(rmses)), rmses.keys())
        plt.xlabel("($\\alpha,\\beta$)")
        plt.ylabel("RMSE")
        plt.show()


# Simple test function
#
# @X        test data
# @T        test label
# @params   test options
def testf(X, T, **params):
    """ Run a simiple test with given options and data. """

    alpha = params.pop('alpha', .3)
    beta = params.pop('beta', .5)
    opt = params.pop('option', 'normal')
    stop = params.pop('stop', 15)
    tau = params.pop('threshold', 0.5)
    showprog = params.pop('showprog', False)
    n_iter = params.pop('niter', 1e6)
    showpred = params.pop('showpred', True)
    n_tiles = params.pop('ntiles', 1)
    bnormtarget = params.pop('normtarget', True)

    ni, no = X.shape[1], T.shape[1]
    rbf = RGDBatch(ni, n_tiles, no, alpha=alpha, beta=beta)
    rbf.train(X, T, option=opt, stop=stop, threshold=tau,
                normtarget=bnormtarget,
                showprog=showprog, nIterations=n_iter)
    Y = rbf.use(X)

    if showpred:
        plt.clf()
        plt.plot(T, 'b-', linewidth=3)
        plt.plot(Y, 'r-', linewidth=3)
        plt.show()


if __name__ == '__main__':

    ###########################################################################
    # near normal data test
    ###########################################################################
    if False:
        X = np.array([1, 2, 3]).reshape(3, 1)
        #T = np.array([1, 2, 1]).reshape(3, 1)
        T = np.array([1, 0, 1]).reshape(3, 1)
        """ param_search(X, T)
           steepest descent test works with alpha = 0.1 and beta = 1.5
           scg works best with alpha = 0.3 and beta = 0.5
        """
        testf(X, T, alpha=0.3, beta=0.5, stop=15, normtarget=False,
                threshold=0.5, showprog=True, showpred=True)

    ###########################################################################
    # another simple data test
    ###########################################################################
    if False:
        X = np.arange(-5, 15)
        T = (X - 5) ** 2 + 5
        X.resize((len(X), 1))
        T.resize((len(T), 1))

        #param_search(X, T)
        testf(X, T, alpha=0.1, beta=0.01, stop=15, ntiles=1,
                threshold=0.5, showprog=True, showpred=False)

    ###########################################################################
    # XOR data test
    ###########################################################################
    if True:
        X = np.array([0, 0, 1, 0, 0, 1, 1, 1]).reshape(4, 2)
        T = np.array([0, 1, 1, 0]).reshape(4, 1)
        #T = 3 * X[:, slice(0, 1)] + 0.4 * X[:,slice(1, 2)] ** 3
        #T = np.array([0, 1, 1, 0, 1, 0, 0, 1]).reshape(4, 2)

        #param_search(X, T, repeat=1)
        if True:
            alpha = 1
            beta_c = 1.
            beta_s = 1.
            stop = 15
            n_tiles = 2
            tau = 0.5
            n_iter = 1e6
            ni, no = X.shape[1], T.shape[1]
            #test_centers = np.array([[1, -1],[-1, 1]])
            #test_centers = np.array([[1, 1],[-1, 0]])
            #test_centers = np.array([[1, 1],[-1, -1]])  # hardest
            test_centers = None
            #test_weights = np.ones((n_tiles+1, no))
            #test_weights[0, :] = 0
            test_weights = None
            showprog = True
            if showprog:
                plt.ion()
            #rbf = RGDBatch(ni, n_tiles, no, alpha=alpha, beta=beta, sigma=2)
            # debugging center movement and sigmas
            rbf = RGDBatch(ni, n_tiles, no,
                            alpha=alpha, beta=[beta_c, beta_s], sigma=2,
                            weights=test_weights, centers=test_centers,
                            verbose=True)
            rbf.train(X, T, option='normal', stop=stop, threshold=tau,
                        showprog=showprog, nIterations=n_iter,
                        wprecision=1e-8, fprecision=1e-8)
                        #freeze=['weight'])
                        #freeze=['sigma','weight'])
            Y, Z = rbf.use(X, hunit=True)
            if showprog:
                plt.ioff()

            print np.hstack((T, Y))
            print "---------------weights----------------"
            print rbf.W
            rbf.plot_hunits_results(Z, X, T, option='2x1', standardize=True)
            plt.show()
