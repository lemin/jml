""" Reinforcement learning experimental framework with AOSVR

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

    ### NOT COMPLETE: need more test!!! ######################################
"""
import numpy as np
import matplotlib.pyplot as plt
from ..svm.aosvrq import AOSVRQ
from ..svm.svmdata import SVMData
from base import RLFrame


class RLtrainSVR(RLFrame):
    """ Reinforcement learning framework with Support Vector Machines

    """
    def make(self, *args, **params):
        if len(args) > 3:
            raise TypeError("make(): wrong number of arguments, pass gamma, C, eps in order")
        elif len(args) == 3:
            self._func = AOSVRQ(gamma=args[0], C=args[1], eps=args[2])
        else:
            self._func = AOSVRQ()

    def pretrain_SVs(self, X, A):
        """ collecting samples by running the model with epsilong greedy. """
        n_samples = X.shape[0]

        R = np.zeros((n_samples, 1))

        for step in range(n_samples):
            s = X[step]
            a = A[step]

            # test action on model and retrieve reward for Q value estimation
            s1 = self.model.next(s, a) 
            r1 = self.model.get_reward(s, s1, a)

            # Collect
            R[step, 0] = r1
            
        return R


    def pretrain(self, *args):
        """ support vector transfer 
            
            transfer the parameters like C, eps, tol
            along with support vectors and theta weights
        """
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        master = args[0]

        # do not transfer weights since it is quite relevant to domain
        # instead, transfer only support vectors,
        # which are considered as meaningful moves
        # for this, set weights to zeros and initiate the sv's as R
        # so, pretrained SV's are set to Remaining Set (R) and 
        # R matrix does not need to be set
        # again do not transfer bias b 
        if True:
            #self._func.theta = np.zeros(len(master.S)) #master.theta[master.S]

            # general approach (SV instance transfer)
            SV = master.data.X[master.S]
            X = SV[:self.model.ndim]  # state
            A = SV[self.model.ndim:]  # action

            R = self.pretrain_SVs(X, A)
            # ignore 2nd arg, assume Q is equivalent to R initially
            self._func.train(X, Q, R, R, **rlparams)
            
        else:
            self._func.theta = master.theta[master.S]
            self._func.b = master.b
            self._func.S = range(len(master.S))
            self._func.Rmat = master.Rmat

            # transfer support vectors
            self._func.data = SVMData(master.data.X[master.S],
                                      master.data.label[master.S])
            self._func.data.set_kernel(master.data.kernel,
                                       gamma=master.data.gamma,
                                       coef=master.data.coef0,
                                       degree=master.data.degree)
        self.tol = master.tol
        self.eps = master.eps
        self.C = master.C

    def train(self, N=None, **rlparams):
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, _ = \
                    RLFrame.train(self, N, **rlparams)
        # gamma has been passed now. update aosvr gamma
        self._func.set_gamma(gamma)

        # AOSVR option
        max_sample = rlparams.pop('maxsample', None)

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA, smplAI, orgX =\
                self.get_samples(N[reps],epsilon)

            ## Update the Q neural network.
            #import time
            #cur = time.time()
            self._func.train(smplX, smplY, smplR, smplQ, 
                                maxsample=max_sample, **rlparams)
            #print time.time() - cur
            #print ">>", self._func.data.n_samples, len(self._func.R)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, smplX[:step+1, :],
                                 extonlineplot, n_extonlineplot)

        if bplot: plt.ioff()

        return (rtrace, epsilontrace)

# TODO for transfer analysis
#   check which samples are transfered 
#   and which samples are remained/removed in the end
#   --- how theta values are changed?

#  for transfer in XOR
#   transfering only SV's might be better since theta is predicting the 
#   state changes
#   but by transfering only SV's, it can reduce the possibility of neg. transfer
#   but, how to utilize SV's on RL training is critical point to think about
