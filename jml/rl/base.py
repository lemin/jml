""" Reinforcement learning experimental framework

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       [1] Sutton and Barto (1998)
           Reinforcement Learning: An Introduction
           http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)
"""
import numpy as np
import random 
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
from jml.util.exp import make_indicator_vars, unique_rows
from collections import Iterable


def convert_to_npmatrix(a):
    am = np.array(a)
    if am.ndim==1: am.shape = (1,len(am))
    elif am.ndim==0: am.shape = (1,1)
    return am


class RLModel:
    """ virtual class for reinforce learning agent model """
    nnNI=0

    def __init__(self):
        self.action_stddev = 1
        self.action_stddev_dec = 0.
        self.action_stddev_min = 0.01
        self.disc_act_learning = False
        self.episodic = False

    def init(self, start=None):
        pass
    def get_random_action(self):
        pass
    def get_action_len(self):
        return 1
    def get_bound_act(self,a):
        return a
    def next(self,s,a) :
        pass
    def get_reward(self,s,s1,a):
        pass
    def get_state_range(self):
        pass
    def get_actions(self):
        pass
    def set_actions(self, actions):
        pass
    def get_search_act_start(self):
        return [self.get_random_action()]
    def set_search_act_start(self, actions):
        pass
    def get_action_index(self, action):
        return None
    def draw_trajectory(self, smplX):
        pass
    def is_episodic(self):
        return self.episodic
    def check_early_stop(self, s, R):
        return False
    def transform_state(self, s):
        return s
    def transform_action(self, a):
        # critical change (treat a as 2-dimensional in epsilon greedy!!)
        if isinstance(a, Iterable):
            return a
        else:
            return [a]
    def check_search_cond(self, rtr):
        return False
    def action_reshape(self, a):
        return a
    def search_act_store(self, R):
        pass
    def get_episodic_penalty(self, step, maxstep):
        return 0
    def set_search_action(self, enable):
        self.search_action = enable
    def get_search_action(self):
        return self.search_action
    def get_action_stddev(self):
        return self.action_stddev
    def set_action_stddev(self, std):
        self.action_stddev = std
    def decrease_action_stddev(self):
        self.action_stddev -= self.action_stddev_dec
        if self.action_stddev < self.action_stddev_min:
            self.action_stddev = self.action_stddev_min
    def plot_Q(self, rlframe, n_plots=-1):
        pass
    def debug_progress(self, rlframe, reps):
        pass

    def Qtitle(self, rlframe):
        if rlframe.__class__.__name__ == 'RLtrainRVM':
            RV = rlframe._func.get_RVs()
            if RV is not None:
                return ''.join(["Max Q (", str(len(RV)), " RVs)"])
        return "Max Q"

    def animate(self, states):
        import matplotlib.animation as animation
        def ani_init():
            pass
        #def ani_step():
        #    pass
        def ani_run():
            pass
        fig = plt.figure()
        ani = animation.FuncAnimation(fig, ani_run,
                                      frames=states.shape[0],
                                      init_func=ani_init,
                                      interval=25,
                                      blit=True,
                                      repeat=True)


class RLFrame:
    """ Reinforcement learning framework
        
        Parameters
        ----------

        model:  RLModel
            The reinforcement learning problem to perform an experiment

        _func: 
            Function approximation class that has train() and use()

        _updact_wpre: float
            The parameter for searching action, wprecision

        _updact_fpre: float
            The parameter for searching action, fprecision

        _updact_fpre: int
            The parameter for searching action, the number of iteration
            
        type_fa:    string ('Q' / 'value')
                    function approximator type: Q or Value function

        Methods
        -------

        Attributes
        ----------

        Notes
        -----
    """

    def __init__(self, model, **params):
        self.model = model 
        self.set_search_action(self.model.get_search_action())
        self._func = None
        self.disc_action_learning = False
        self.pdf = None
        self.trace = None

        self.type_fa = 'Q'

        self.progress = False
        self.progress_filen = "/tmp/.progress"
        self._progress = {}
    
    def set_search_action(self, enable=True, update_params=True, **params):
        """ set search action parameters. """
        self.search_act = enable
        self.search_act_gaussian = params.pop('gaussian', False)
        self.model.set_search_action(enable)
        if enable:
            self.disc_action_learning = False
        if update_params:
            self._updact_wpre = params.pop('wpre', 1e-8)
            self._updact_fpre = params.pop('wpre', 1e-8)
            self._updact_niter= params.pop('niter', 10000)
            self._updact_optim= params.pop('optim', 'scg')
            self.search_act_cond = params.pop('sa_cond', 'none') # n/n-thres/thres/2perm/none
            self.search_act_stop = params.pop('sa_stop', False) # n/n-thres/thres/2perm/

    ######################################################################
    # Interface for sample collection and epsilon policy handling
    ######################################################################

    def get_eps_policy(self, nReps, **rlparams):
        """ return epsilon policy based on parameters. 

            parameters
            ==========
            epsilonType     String  
                            expDecrease: exponentially decrease (finalEpsilon)
                            fixed:       fixed epsilon value
                            -tanh:       neg. tanh shape (slope, start and end)
                            epslist:     predefine epsilon values in a list
            finalEpsilon    float (expDecrease)
            epsilonSlope    float (-tanh)
            epsilonStart    float (-tanh)
            epsilonEnd      float (-tanh)
            fixedEpsilon    float (fixed)
        """

        epsilonType = rlparams.pop("epsilonType", 'expDecrease') 
        epsilonSlope = rlparams.pop("epsilonSlope", 6)
        epsilonStart = rlparams.pop("epsilonStart", 0)
        epsilonEnd = rlparams.pop("epsilonEnd", -1)
        fixedEpsilon = rlparams.pop("fixedEpsilon", 0.5)
        finalEpsilon = rlparams.pop("finalEpsilon", 0.001)
        epsilons = None
        epsilon = rlparams.pop("initEpsilon", 1.)
        epsilonRate = 1

        if epsilonType == 'fixed': 
            epsilon = fixedEpsilon
        elif epsilonType == '-tanh':
            if epsilonEnd < 0 or epsilonEnd > nReps: 
                epsilonEnd = nReps
            sigmoidRange = epsilonEnd - epsilonStart

            step= (2. * epsilonSlope) / sigmoidRange
            epsx = list(np.arange(-epsilonSlope, epsilonSlope, step))
            epsilons = [1] * epsilonStart + list((-np.tanh(epsx) + 1) / 2)\
                        + [0] * (nReps - epsilonEnd)

        elif epsilonType == 'expDecrease': # 'expDecrease'
            epsilonRate = np.exp(np.log(finalEpsilon) / nReps)
            #print "epsilon decay rate = %f" % epsilonRate
        else: # 'epslist' set epsilon list
            epsilons = rlparams.pop('epsilonList', None)

        return (epsilonType, epsilonRate, epsilons, epsilon)

    def get_cur_epsilon(self, eps, epsType, epsRate, epsilons, reps):
        """ return computed current epsilon """

        ### record progress info
        if self.progress:
            self._progress['cur'].append(time.time())
            with open(self.progress_filen, "a") as f:
                # print elapsed time for the run
                f.write("%d/%d %f %f\n" % (reps+1, self._progress['nReps'], 
                          self._progress['cur'][-1],  
                          self._progress['cur'][-1] - self._progress['start']))

        if self.softmax:
            #return eps - self.temp_dec
            return self.temp / (1. + self.temp_dec * (reps + 1))

        if epsType=='expDecrease': 
            return eps * epsRate
        elif epsType=='-tanh' or epsType=='epslist' :
            return  epsilons[reps]
        elif epsType =='fixed':
            return eps

    def epsilon_greedy(self, s, epsilon, **params):
        """ apply epsilon greedy with given epsilon
            searching action using backpropagation is executed
            if configured

            parameters
            ==========
            qlearning   boolean - deprecated
                        Q-learning option (compute all and return qs)

            TODO/CHECK:
            =====
            current 'softmax + search action' extends qs with continuous actions
            this can results in different ai index return value.
            this experimental codes are necessary to be checked before it is used!
            for now, to prevent possible error, it is set to None
        """

        # option for Q learning - max
        #qlearning = params.pop('qlearn', False)

        sm = convert_to_npmatrix(self.model.transform_state(s))
        actions = self.get_actions()
        ams = convert_to_npmatrix([self.model.transform_action(t_a)\
                for t_a in actions])

        if self.disc_action_learning or self.disc_action_indicator:
            if self.disc_action_learning:
                qs = self._func.use(sm).reshape((-1, 1))
            elif self.disc_action_indicator:
                ams = make_indicator_vars(np.arange(len(actions)))
                qs = self._func.use(np.hstack((
                                        np.tile(sm, (ams.shape[0], 1)),
                                        ams)))

            if self.softmax:
                temp = epsilon
                exp_qs = np.exp(qs / temp)
                prob_act = exp_qs / np.sum(exp_qs)

                #Kitagawa sampling
                #print prob_act.flatten()
                ai = np.sum(np.cumsum(prob_act) < random.random())
            else:
                if random.uniform(0,1) < epsilon:
                    #ai = random.randrange(len(actions))
                    ai = random.randrange(ams.shape[0])
                else:
                    ai = np.argmax(qs)
            a = actions[ai]
            Q = qs[ai, 0]
        else:
            a = self.model.get_random_action()
            am = convert_to_npmatrix(self.model.transform_action(a))

            if am.shape[1] == 1:
                ams = ams.reshape((-1, 1))
            # precompute qs to handle qlearning case as well
            qs = self._func.use(np.hstack((
                                    #np.tile(sm, (len(actions), 1)),
                                    np.tile(sm, (ams.shape[0], 1)),
                                    ams)))
            # DEBUG: check shape of qs : should be a vector/list
            if self.softmax:
                temp = epsilon

                if self.search_act:
                    acts = self.model.get_search_act_start()  #default: one random point

                    for a in acts:
                        am = convert_to_npmatrix(self.model.transform_action(a))
                        a_opt = self._func.search_action(
                                        sm, am,
                                        self._updact_wpre,
                                        self._updact_fpre,
                                        self._updact_niter,
                                        self._updact_optim)

                        # push the found into a boundary
                        a_opt = self.model.get_bound_act(a_opt)
                        am = convert_to_npmatrix(self.model.transform_action(a_opt))
                        q = float(self._func.use(np.hstack((sm,am))))

                        if not a_opt in actions:
                            #for ai in xrange(len(actions)):
                            for ai in xrange(ams.shape[0]):
                                if a_opt < actions[ai]:
                                    break
                                ai += 1

                            # insert q to qs 
                            qs.insert(ai, q)
                                
                            # insert a into actions
                            actions.insert(ai, a_opt)
                   
                ## common part for both discrete and continuous search ##
                ## softmax: choose an action based on prob_a

                exp_qs = np.exp(np.asarray(qs) / temp)
                prob_act = exp_qs / np.sum(exp_qs)

                #Kitagawa sampling
                #print prob_act.flatten()
                ai = np.sum(np.cumsum(prob_act) < random.random())

                a = actions[ai]
                Q = qs[ai]

                if self.search_act:
                    ai = None
            elif random.uniform(0,1) < epsilon:  #epsilon greedy
                Q = float(self._func.use(np.hstack((sm,am))))
                #print "<base> Q:", Q
                if self.search_act:
                    ai = None
                else:
                    ai = self.model.get_action_index(a)
            else:
                if self.search_act:
                    # search the best action 
                    Q = None
                    cur_a = None
                    cur_q = None

                    # default search_act_start() returns a random action
                    # but this function gives a possibility to more than one
                    # starting points
                    acts = self.model.get_search_act_start() 
                    for a in acts:
                        am = convert_to_npmatrix(self.model.transform_action(a))
                        a = self._func.search_action(
                                        sm, am,
                                        #convert_to_npmatrix(self.model.get_random_action()),
                                        self._updact_wpre,
                                        self._updact_fpre,
                                        self._updact_niter,
                                        self._updact_optim)
                        a = self.model.get_bound_act(a)
                        am = convert_to_npmatrix(self.model.transform_action(a))
                        q = float(self._func.use(np.hstack((sm,am))))

                        if cur_q is None:
                            cur_q = q
                            cur_a = a
                        elif q > cur_q:
                            cur_q = q
                            cur_a = a

                    q = cur_q
                    a = cur_a

                    # Gaussian noise for exploration (test with fixed variance)
                    if self.search_act_gaussian:
                        cur_a = np.random.normal(a, self.model.get_action_stddev())

                        a = self.model.get_bound_act(cur_a)
                        am = convert_to_npmatrix(self.model.transform_action(a))
                        q = float(self._func.use(np.hstack((sm,am))))

                    
                    ####################################################################
                    """ DEBUG: plot Q to see if search works fine
                    """
                    if False:
                        q_actions = np.arange(-90, 91, 1)
                        pts = [10, 50, 90, 130, 170]
                        xs, ys = np.meshgrid(pts, pts)
                        #states = np.hstack((xs.reshape((-1,1)), ys.reshape((-1,1))[::-1]))

                        plt.ioff()
                        plt.figure(13)
                        das = acts
                        stin = np.hstack((np.tile(sm, (len(q_actions), 1)),
                                          q_actions.reshape((-1, 1))))
                        qs = self._func.use(stin)
                        #idx = [np.where(q_actions==da)[0][0] for da in das]
                        idx = [np.where(abs(q_actions-da) < 1e-8)[0][0] for da in das]

                        plt.plot(q_actions,qs)
                        plt.plot(das, qs[idx], "go")
                        plt.vlines(das, min(qs), max(qs), 'g', 'dashed')
                        plt.title(str(sm))

                        #print "base:366> ",cur_a, cur_q
                        plt.plot(cur_a, cur_q, 'r*')
                        plt.axvline(cur_a, color='r')
                        plt.show()
                    ####################################################################

                    if Q==None or q > Q:
                        Q = q 
                        reta= a
                    a = reta 
                    ai = None
                else:  # remove loop 20110107
                    # add random in case of multiple max values
                    maxq = np.max(qs)
                    i_qs = np.where(qs == maxq)[0]
                    if len(i_qs) == 0:
                        raise ValueError("invalid Q values")
                    ai = int(np.random.choice(i_qs))
                    a = actions[ai]
                    Q = qs[ai, 0]

        #if qlearning:
        return (Q, a, ai, qs)
        #else:
        #    return (Q, a, ai)

    def get_actions(self):
        """ retrieve model actions """
        if self.__class__.__name__ == 'RLtrainRVM' and self.search_act:
            ### return actions from RVs
            RVs = self._func.get_RVs()
            if RVs is None or len(RVs) < 3:
                return self.model.get_actions()
            # remove duplicate actions!!!
            actions = unique_rows(RVs[:, self.model.n_state:])

            return [self.model.action_reshape(a) for a in actions]
        else:
            return self.model.get_actions()

    def get_samples(self, numSamples, epsilon, earlyStop=None, startSt=None):
        """ collecting samples by running the model with epsilong greedy. """

        if self.model.is_episodic():
            self._step_trace = []
        else:
            self._step_trace = 0

        if earlyStop is None:
            earlyStop = self._early_stop

        na = self.model.get_action_len()
        Xd = np.zeros((numSamples, self.model.nnNI))
        R = np.zeros((numSamples, 1))
        if self.disc_action_learning:
            Qn = np.zeros((numSamples, len(self.get_actions())))
        else:
            Qn = np.zeros((numSamples, 1))
        Q = np.zeros((numSamples, 1))
        A = np.zeros((numSamples, na))
        AI = np.zeros((numSamples, 1))

        s = self.model.init(startSt)
        q, a, ai, qs = self.epsilon_greedy(s, epsilon)

        X = np.zeros((numSamples, len(s)+na))

        #plt.figure(10)

        for step in range(numSamples):
            s1 = self.model.next(s, a) 
            r1 = self.model.get_reward(s, s1, a)
            q1, a1, ai1, qs = self.epsilon_greedy(s1, epsilon)

            # Collect
            ts = self.model.transform_state(s)
            ta = self.model.transform_action(a)
            Xd[step, 0:len(ts)], Xd[step, len(ts):] = ts, ta
            X[step, 0:len(s)], X[step, len(s):] = s, a.flatten()
            R[step, 0], Q[step, 0] = r1, q
            if self.disc_action_learning:
                Qn[step, :] = qs.flat
            else:
                if self._qlearn: # Q-learning (maxq)
                    Qn[step, 0] = np.max(qs)
                else:  # SARSA
                    Qn[step, 0] = q1
            A[step, :] = a.flatten()
            if ai is None or AI is None:
                AI = None
            else:
                AI[step, :] = ai

            # to debug model
            #plt.clf()
            #self.model.draw_trajectory(X[:step+1, :])
            #plt.draw()

            if self.model.check_early_stop(s1, r1):
                ### record steps
                if self.model.is_episodic():
                    self._step_trace.append(self.model.n_steps)

                ### options for keep collecting samples
                if earlyStop:
                    break

                # restart
                s = self.model.init()
                q, a, ai, qs = self.epsilon_greedy(s, epsilon)
            else:    
                # next state
                s, a, ai, q = s1, a1, ai1, q1

        R[step, :] += self.model.get_episodic_penalty(step, numSamples)

        # for episodic samples, set Qn[step,:] to zero
        if step + 1 < numSamples:
            Qn[step, :] = 0
        if not self.model.is_episodic():
            self._step_trace = step

        # after sample collection, decrease stddev for gaussian search
        if self.search_act_gaussian:
            self.model.decrease_action_stddev()

        return (Xd[:step+1, :], R[:step+1, :], Qn[:step+1, :],
                Q[:step+1, :], A[:step+1, :],
                AI[:step+1, :] if AI is not None else None,
                X[:step+1, :])

    ######################################################################
    # Function approximation management
    ######################################################################

    def make(self, *args, **params):
        """ interface for creating function approximation """
        pass

    def pretrain(self, *args, **params):
        """ interface for pre-trainingfunction approximation """
        pass

    ######################################################################
    # train reinforcment learning
    ######################################################################

    def train(self, N=None, extonlineplot=None, **rlparams):
        """ interface for training function approximation 
            with common parameter reading"""
        # disc_action_learning
        self.disc_action_learning = rlparams.pop('discAct', False)
        self.disc_action_indicator = rlparams.pop('discActIndicator', False)

        # RL params
        gamma = rlparams.pop("Gamma", 0.9)
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)
        et_lambda = rlparams.pop("et_lambda", 1.)

        if N==None: N=[nSteps] * nReps

        self.softmax = rlparams.pop('softmax', False)
        self.temp = rlparams.pop('temp0', 3.)
        self.temp_dec = rlparams.pop('temp_rate', 0.0001)

        ### Initial epsilon is 1, for fully random action selection
        if self.softmax:
            # for softmax, epsilon replace tau
            epsilon = self.temp
            epsilonType = epsilonRate = epsilons = None
        else:
            epsilon = 1
            epsilonType, epsilonRate, epsilons, epsilon =\
                        self.get_eps_policy(nReps, **rlparams)

        # plot option
        bplot = rlparams.pop("showplot", False)
        plot_intvl = rlparams.pop("plot_interval", 1)
        extonlineplot = rlparams.pop("onlineplot", None)
        n_extonlineplot = rlparams.pop("Nonlineplot", 0)

        # the number of batch training
        n_batch = rlparams.pop('nbatch', 1)

        # algorithm to update (for now, only w/ nn)
        algo = rlparams.pop('algo', 'TD')

        self.debug = rlparams.pop('debug', 0) # default debug option = false

        # trace option - additional trace for parameter analysis
        self.btrace = rlparams.pop('btrace', False)

        # init tracer
        self._trace = {'actual_randact': []}

        self._progress['nReps'] = nReps
        self._progress['start'] = time.time()
        self._progress['cur'] = []

        self._qlearn = rlparams.pop('qlearn', False)

        # option to stop collecting samples when it reaches a goal
        self._early_stop = rlparams.pop('earlyStop', False)
       
        return gamma, et_lambda, nReps, nSteps, N, algo,\
               epsilonType, epsilonRate, epsilons, epsilon,\
               bplot, plot_intvl, extonlineplot, n_extonlineplot, n_batch

    ######################################################################
    # plotting functions
    ######################################################################

    def online_plot(self, rtrace, epsilontrace, smplX,
                    extplot, n_extplot=0, ftrace=None):
        #plt.gcf().set_size_inches(25, 10)
        n_plots = 6 + n_extplot + (1 if ftrace else 0)
        if 'err' in rtrace.keys():
            n_plots += 1

        if self.type_fa == 'Q':
            n_plots += 1  # plot Q
            if "plot_policy" in dir(self.model):
                n_plots += 1  # plot policy

        if n_plots > 7:
            npltRow = 3
            npltCol = int(np.ceil(n_plots / 3.))
        else:
            npltRow = 2
            npltCol = int(np.ceil(n_plots / 2.))

        fig = plt.figure(1, figsize=(5*npltCol, 5*npltRow))
            
        plotidx = 1
        plt.clf()
        fig.add_subplot(npltRow,npltCol,plotidx)
        plt.plot(rtrace['mean'],'-') 
        plt.xlabel("episodes") 
        plt.ylabel("avg. rewards")

        plotidx += 1
        fig.add_subplot(npltRow,npltCol,plotidx)
        plt.plot(rtrace['sum'],'-') 
        plt.xlabel("episodes") 
        plt.ylabel("sum rewards")

        if 'err' in rtrace.keys():
            plotidx += 1
            fig.add_subplot(npltRow,npltCol,plotidx)
            plt.plot(rtrace['err'],'-') 
            plt.xlabel("episodes") 
            plt.ylabel("error")

        plotidx += 1
        fig.add_subplot(npltRow,npltCol,plotidx)
        plt.plot(rtrace['steps'],'-') 
        plt.xlabel("episodes") 
        plt.ylabel("steps")
        
        plotidx += 1
        fig.add_subplot(npltRow,npltCol,plotidx)
        plt.plot(epsilontrace)
        plt.xlabel("episodes") 
        if self.softmax:
            plt.ylabel("$\\tau$")
        else:
            plt.ylabel("$\\epsilon$")

        if ftrace is not None:
            plotidx += 1
            fig.add_subplot(npltRow,npltCol,plotidx)
            plt.plot(ftrace)
            #plt.xlabel("# rep") 
            plt.ylabel("TD error")

        # searching action plot
        #plt.subplot(npltRow,npltCol,4)
        #plt.plot()
        #plt.xlabel("actions") 
        #plt.ylabel("Q")

        # test trajectory
        plotidx += 1
        fig.add_subplot(npltRow,npltCol,plotidx)
        self.model.draw_trajectory(smplX)
        #if reps==nReps-1:
        #    plt.show()
        #else:

        # Q plot
        if self.type_fa == 'Q':
            plotidx += 1
            fig.add_subplot(npltRow,npltCol,plotidx)
            self.model.plot_Q(self, 1)
            # plot SV's or RV's
            if self.__class__.__name__ == 'RLtrainRVM':
                RV = self._func.get_RVs()
                if RV is not None:
                    plt.plot(RV[:, 0], RV[:, -1], 'kx', linewidth=2)
            elif self.__class__.__name__ == 'RLtrainSVR':
                plt.plot(self._func.data.X[self._func.S, 0],
                         self._func.data.X[self._func.S, -1],
                         'kx', linewidth=2)

            if "plot_policy" in dir(self.model):
                plotidx += 1
                fig.add_subplot(npltRow,npltCol,plotidx)
                self.model.plot_policy(self)

        if extplot is not None:
            extplot(npltRow, npltCol, plotidx)

        plt.draw()        
        if self.pdf is not None:
            self.pdf.savefig(fig)

    def draw_Qplot(self, xs, ys, st_inputs, opt=None, add_func=None):
        """ draw contour plot of max Q values for state input """

        qs = self._func.use(st_inputs)
        if self.disc_action_learning:
            qsmax = qs.T
        else:
            qsmax = np.max(qs,axis=1).reshape(xs.shape)
        cs = plt.contourf(xs, ys, qsmax)
        plt.colorbar(cs)

        if add_func is not None:
            add_func()

        if opt=='3d':
            fig = plt.figure()
            #ax = fig.gca(projection='3d')  # for old matplotlib
            ax = Axes3D(fig)
            surf = ax.plot_surface(xs, ys, qsmax,
                                   rstride=1, cstride=1, cmap=cm.jet,
                                   linewidth=0, antialiased=False)
            fig.colorbar(surf)
