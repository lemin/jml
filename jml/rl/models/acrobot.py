""" Acrobot model for reinforcement learning and animation
"""
import numpy as np
import matplotlib.pyplot as plt
#import random
from copy import deepcopy as copy
#from ..base import RLModel
from jml.rl import RLModel
import collections
#from math import cos
import matplotlib.animation as animation
from matplotlib.patches import Circle


class Animate:

    def __init__(self, ax, states, params):
        self.states = states
        self.params = params
        self.ax = ax

        self.ax.set_xlim(-2, 2)
        self.ax.set_ylim(-2, 2)
        self.ax.grid(True)
        self.line, = ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = {:.1f}s'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

        theta1 = self.states[:, 0]
        theta2 = self.states[:, 2]

        self.x1 = self.params['l1'] * np.sin(theta1)
        self.y1 = -self.params['l1'] * np.cos(theta1)
        self.x2 = self.x1 + self.params['l2'] * np.sin(theta1 + theta2)
        self.y2 = self.y1 - self.params['l2'] * np.cos(theta1 + theta2)

    def init(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def __call__(self, i):

        self.line.set_data([0., self.x1[i], self.x2[i]],
                           [0., self.y1[i], self.y2[i]])
        self.time_text.set_text(self.time_template.format(i * self.params['dt']))

        return self.line, self.time_text


class Acrobot(RLModel):
    """ Acrobot problem (p270 Sutton and Barto)

        states: theta1, dtheta1, theta2, dtheta2
        action: action [-1,1]

        xbound: [0, 2 pi]
        theta1 dot bound: [-4 pi, 4 pi]
        theta2 dot bound: [-9 pi, 9 pi]


                     |
                     |
     Goal:           |
      over this______|_________
                     |
                     |
                     |
                     |
        -----------------------
            theta1  /
                   /
                  /
                 /
                 \
           theta2 \
                   \
                    \
    """

    def __init__(self, goal=1.0,
                 **params):
        RLModel.__init__(self)
        self._angle_state = params.pop('anglestate', True)
        self.episodic = params.pop('episodic', True)

        if self._angle_state: 
            self.n_state = 4
        else:
            self.n_state = 6
        self.n_action = 1
        self.Goal = goal

        self.rewardf = params.pop('rfunc', 0)

        self.disc_act_learning = params.pop('disc_act_learning', False)
        #self.xbound = params.pop('xbound', [0, 2*np.pi])  # [-np.pi, np.pi])
        self.xbound = params.pop('xbound', [-np.pi, np.pi])
        self.xdotbound1 = params.pop('xdotbound1', [-4. * np.pi, 4. * np.pi])
        self.xdotbound2 = params.pop('xdotbound2', [-9. * np.pi, 9. * np.pi])

        self.nnNI = self.n_state
        self._st_range = np.array([self.xbound, self.xdotbound1,
                                  self.xbound, self.xdotbound2]).T
        rng = [-1, 1]
        self._pst_range = np.array([rng, rng, self.xdotbound1,
                                    rng, rng, self.xdotbound2]).T
        if not self.disc_act_learning:
            self.nnNI += 1
            self._st_range = np.hstack((self._st_range, np.array([[-1], [1]])))
            self._pst_range = np.hstack((self._pst_range, np.array([[-1], [1]])))

        self._actions = np.array([-1, 0, 1])

        self.params = {'l1': 1., 'l2': 1.,  # length of links
                       'm1': 1., 'm2': 1.,  # masses of the links
                       'lc1': 0.5, 'lc2': 0.5,  # lengthts to center link mass
                       'I1': 1, 'I2': 1,  # moments of inertial of links
                       'g': 9.8,        # gravity
                       'force': 2.,    # force to be multiplied to action
                       'simsteps': 4,    # force to be multiplied to action
                       'dt': 0.05        # time step
                       }

    def set_actions(self, actions):
        self._actions = copy(np.asarray(actions))

    def init(self, start=None):
        self.n_steps = 0
        #return [random.uniform(self.xbound[0], self.xbound[1]), 0.,
        #        random.uniform(self.xbound[0], self.xbound[1]), 0.]
        if start is not None:
            return start
        return np.array([0., 0., 0., 0.])

    def get_random_action(self):
        return np.random.choice(self._actions)
        #return float(np.random.randint(3) -1) # discrete action
        #return random.uniform(-1,1) # for continuous action

    def get_bound_act(self, a):
        if a[0] > 1:
            return 1
        elif a[0] < -1:
            return -1
        else:
            return a[0]

    def angle_clip(self, angle):
        if angle > np.pi:
            angle -= 2 * np.pi
        elif angle < -np.pi:
            angle += 2 * np.pi
        return angle

    def next(self, s, a):
        stps = self.params['simsteps']
        for st in range(stps):
            s1 = self._next(s, a)
            s = s1
        self.n_steps += 1
        return s1

    def _next(self, s, a):
        if isinstance(a, collections.Iterable):
            a = a[0] * self.params['force']

        m1 = self.params['m1']
        m2 = self.params['m2']
        l1 = self.params['l1']
        #l2 = self.params['l2']
        lc1 = self.params['lc1']
        lc2 = self.params['lc2']

        d1 = m1 * lc1 ** 2 +\
            m2 * (l1 ** 2 + lc2 ** 2 + 2 * l1 * lc2 * np.cos(s[2])) +\
            self.params['I1'] + self.params['I2']
        d2 = m2 * (lc2 ** 2 + l1 * lc2 * np.cos(s[2])) + self.params['I1']

        phi2 = m2 * lc2 * self.params['g'] * np.cos(s[0] + s[2] - np.pi/2.)
        phi1 = -m2 * l1 * lc2 * s[3]**2 * np.sin(s[2]) -\
            2 * m2 * l1 * lc2 * s[3] * s[1] * np.sin(s[2]) +\
            (m1 * lc1 + m2 * l1) * self.params['g'] * np.cos(s[0] - np.pi/2.)\
            + phi2

        dd2 = (a + d2 * phi1 / d1 -
               m2 * l1 * lc2 * s[1]**2 * np.sin(s[2]) - phi2) /\
            (m2 * lc2**2 + self.params['I2'] - d2**2 / d1)
        dd1 = -(d2 * dd2 + phi1) / d1

        s1 = copy(s)
        dT = self.params['dt']
        s1[[0, 2]] += dT * s[[1, 3]]
        s1[1] += dT * dd1
        s1[3] += dT * dd2

        # clip only the speed
        s1[[1, 3]] = np.clip(s1, self._st_range[0, :4],
                             self._st_range[1, :4])[[1, 3]]
        # put angle in xbound
        s1[0] = self.angle_clip(s1[0])
        s1[2] = self.angle_clip(s1[2])

        return s1

    def get_heights(self, s):
        h1 = -self.params['l1'] * np.sin(np.pi/2. + s[0])
        h2 = -self.params['l2'] * np.sin(np.pi/2. + (s[0] + s[2]))
        return h1, h1+h2

    def get_reward(self, s, s1, a):
        elbow, hand = self.get_heights(s1)
        if self.rewardf == 0:
            return 1 if hand > self.Goal else 0
        elif self.rewardf == -1:
            return 0 if hand > self.Goal else -1

    def get_state_range(self):
        if self._angle_state:
            return self._st_range
        else:
            return self._pst_range

    def get_actions(self):
        return self._actions

    def get_action_index(self, action):
        return np.where(self._actions == action)[0][0]
        #return np.where(np.array([-1, 0, 1]) == action)[0][0]

    def draw_trajectory(self, smplX):
        # over state space (velocity exluded)
        if self._angle_state:
            #plt.plot(smplX[:, 0], smplX[:, 2])  # theta1 against theta2
            #plt.axis([self.xbound[0], self.xbound[1],
            #         self.xbound[0], self.xbound[1]])
            #plt.plot(smplX[0, 0], smplX[0, 2], 'go')  # starting pos
            #plt.plot(smplX[-1, 0], smplX[-1, 2], 0, 'ro')
            plt.plot(np.cos(smplX[:, 0]), np.sin(smplX[:, 0]), 'b')
            plt.plot(np.cos(smplX[:, 2]), np.sin(smplX[:, 2]), 'r')
            plt.xlabel("$\\cos(\\theta)$")
            plt.ylabel("$\\sin(\\theta)$")
        else:
            plt.plot(smplX[:, 0], smplX[:, 1], 'b')
            plt.plot(2 * smplX[:, 3], 2 * smplX[:, 4], 'r')
            plt.xlabel("$\\cos(\\theta)$")
            plt.ylabel("$\\sin(\\theta)$")

    def check_early_stop(self, s, R):
        elbow, hand = self.get_heights(s)
        if hand > self.Goal:
            return True
        return False

    def set_search_action(self, enable=True):
        self.search_action = enable

    def transform_state(self, s):
        if self._angle_state:
            return s
        return np.array([np.cos(s[0]), np.sin(s[0]), s[1],
                         np.cos(s[2]), np.sin(s[2]), s[3]])

    def plot_Q(self, rlframe, n_plots=-1):
        pass
        #xpos = np.linspace(self.xbound[0], self.xbound[1], 20)
        #actions = self.get_actions()
        #xs, ys = np.meshgrid(xpos, actions)
        #states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        #rlframe.draw_Qplot(xs, ys, states)
        #plt.xlabel("s")
        #plt.ylabel("a")
        #plt.title(self.Qtitle(rlframe))

    def animate(self, states, bsave=False):
        fig = plt.figure()
        ax = fig.add_subplot(111, xlim=(-2, 2), ylim=(-2, 2), autoscale_on=False)
        ani = Animate(ax, states, self.params)
        anm = animation.FuncAnimation(fig, ani,
                                frames=np.arange(states.shape[0]),
                                init_func=ani.init,
                                interval=250,
                                blit=True,
                                repeat=True,
                                repeat_delay=1000)
        if bsave:
            ani.save('acrobots.mp4', fps=15)
        plt.show()


if __name__ == '__main__':

    acro = Acrobot()
    acro.params['dt'] = .05 #1
    acro.params['force'] = 1.
    actions = acro.get_actions()
    s = acro.init()
    states = s

    for i in xrange(1000):
        a = np.random.choice(actions)
        s1 = acro.next(s, a)

        states = np.vstack((states, s1))
        s = s1

        if acro.check_early_stop(s1, None):
            break

    acro.animate(states)
