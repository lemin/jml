import numpy as np
import matplotlib.pyplot as plt
#import random
from copy import deepcopy as copy
from ..base import RLModel
#import collections
from math import copysign


class Bicycle(RLModel):
    """ Bicycle problem 
            - first shown in Randlov and Alstrom (1998)
            - followed the version by Wilson and Fern (2014)
        
        states: [omega, domega, theta, dtheta]
        action: torque on handle bar T \in [-1,0, 1] N
                dispalcement of center mass d \in [-2, 0, 2] cm

        xbound: [0, 2 pi] 
        theta1 dot bound: [-4 pi, 4 pi]
        theta2 dot bound: [-9 pi, 9 pi]

     Goal: balancing the bike and following the preset route

          \    (|)    /
           \    |    /
            \   |   /
             \  |--/ ----> max angle is 12 degrees not to fall
              \ | /       omega is angle of the bike upright
               \|/          so, if omega > 12 degrees, it fails.


          path      bike
              \    /
               \--/  ---> the angular deviation of bike from straightforward 
                \/         is represented by theta
                 \
                  \
    """

    def __init__(self, goal=[0., 1000., 100.], **params):
        RLModel.__init__(self)
        self.episodic = params.pop('episodic', True)

        self.n_state = 4
        self.n_action= 2
        self.Goal = goal[:2]
        self.Goal_r = goal[2]

        self._option = params.pop('task', 'balancing')

        deg = np.radians(12 + 3)
        self.xbound1 = params.pop('xbound1', [-deg, deg]) # for omega
        self.xbound2 = params.pop('xbound2', [-np.pi/2., np.pi/2.]) # for theta

        vel = np.radians(2)
        self.xdotbound1 = params.pop('xdotbound1', [-vel, vel]) # for domega
        self.xdotbound2 = params.pop('xdotbound2', [-vel, vel]) # for dtheta
        self.disc_act_learning = params.pop('disc_act_learning', False)
        if self.disc_act_learning:
            self.nnNI = self.n_state
            self._st_range = np.array([self.xbound1, self.xdotbound1,
                                       self.xbound2, self.xdotbound2]).T
        else:
            self.nnNI = self.n_state + self.n_action
            self._st_range = np.array([self.xbound1, self.xdotbound1,
                                       self.xbound2, self.xdotbound2,
                                      [-1, 1], [-2, 2]]).T

        T = np.array([-1, 0, 1])
        d = np.array([-2, 0, 2])
        self._actions = np.array([np.tile(T, len(d)), np.repeat(d, len(T))]).T

        self.params = {'c': .66,    # horizontal distance 66cm
                       #'CM': ,     # the center of mass
                       'dcm': .30,  # the vertical distance between the CM
                                    # for the bike and for the cyclist
                       'h': .94,    # height of the CM voer the ground
                       'l': 1.11,   # distance between front and rear tyre
                       'Mc': 15,    # mass of the bicycle
                       'Md': 1.7,   # mass of a tyre
                       'Mp': 60,    # mass of a cyclist
                       'r': .34,    # radius of a tyre
                       'v': 10./3.6,# the velocity of the bicycle  m/s
                       'g': 9.8,
                       'dt': 0.01
                      }
        # the angular velocity of a tyre
        self.params['dsigma'] = self.params['v'] / self.params['r']
        self.params['M'] = self.params['Mc'] + self.params['Mp']
        self.params['Idc'] = self.params['Md'] * self.params['r']**2
        self.params['Idv'] = 3./ 2. * self.params['Idc']
        self.params['Idl'] = 1./ 2. * self.params['Idc']

        self._bstop = False

    def set_actions(self, actions):
        self._actions = copy(np.asarray(actions))

    def init(self, start=None):
        self.n_steps = 0
        if start is not None:
            return start
        #return [random.uniform(self.xbound[0], self.xbound[1]), 0.,
        #        random.uniform(self.xbound[0], self.xbound[1]), 0.]

        # internal state for position of two tires
        self._pos = np.array([0., self.params['l'], 0., 0.])
        self._poslog = copy(self._pos)
        self._anglog = [0.]
        self._anglog1 = [0.]
        self._bstop = False
        return np.array([0., 0., 0., 0.])
       
    def get_random_action(self):
        i = np.random.randint(self._actions.shape[0])
        return self._actions[i]

    def get_bound_act(self, a):
        return np.asarray(a).clip(np.min(self._actions, 0),
                                  np.max(self._actions, 0))

    def next(self,s,a) :
        
        h = self.params['h']
        r = self.params['r']
        l = self.params['l']
        c = self.params['c']
        M = self.params['M']
        Md = self.params['Md']
        Idc = self.params['Idc']
        Idv = self.params['Idv']
        Idl = self.params['Idl']
        g = self.params['g']
        v = self.params['v']
        dsigma = self.params['dsigma']
        dT = self.params['dt']
        
        psi = s[0] + np.arctan(a[1] / h)

        # intertia
        I = 13. / 3. * self.params['Mc'] * h**2 +\
            self.params['Mp'] * (h + self.params['dcm'])**2

        # radius
        if s[2] == 0.:
            r_f = r_b = r_cm = 1e8
        else:
            r_f = l / abs(np.sin(s[2]))
            r_b = l / abs(np.tan(s[2]))
            r_cm = np.sqrt((l - c)**2 + l**2 / np.tan(s[2])**2)
        
        #import pdb; pdb.set_trace()
        # acceleration
        ddomega = 1. / I * (M * h * g * np.sin(psi) -\
                            np.cos(psi) * ((Idc * dsigma * s[3]) +\
                            copysign(1, s[2]) * v**2 *\
                            (Md * r / r_f + Md * r / r_b + M * h / r_cm)))
        ddtheta = (a[0] - Idv * dsigma * s[1]) / Idl

        # front tire
        vdT = v * dT
        pt = psi + s[2]
        ang = pt + copysign(1, pt) * np.arcsin(vdT/(2*r_f))
        self._pos[0] += vdT * -np.sin(ang)  # x
        self._pos[1] += vdT * np.cos(ang)   # y
        # back tire
        ang = psi + copysign(1, psi) * np.arcsin(vdT/(2*r_b))
        self._pos[2] += vdT * -np.sin(ang)  # x
        self._pos[3] += vdT * np.cos(ang)   # y

        # adjust rear-wheel to the length l to be constant
        delta_x = self._pos[0]-self._pos[2]
        delta_y = self._pos[1]-self._pos[3]
        dist = np.sqrt(delta_x**2 + delta_y**2)
        if abs(dist - l) > 0.01:
            ratio = (l - dist) / dist
            self._pos[2] += delta_x * ratio
            self._pos[3] += delta_y * ratio

        s1 = copy(s)
        s1[[0,2]] += dT * s[[1,3]]  
        s1[1] += dT * ddomega
        s1[3] += dT * ddtheta

        # clip only the speed
        s1[[1,3]] = np.clip(s1, self._st_range[0, :4],
                            self._st_range[1, :4])[[1,3]]
        # put angle in xbound
        s1[0] = self.angle_clip(s1[0])
        s1[2] = self.angle_clip(s1[2])

        # collect log for trajectory
        self._poslog = np.vstack((self._poslog, self._pos))
        self._anglog.append(np.degrees(s1[0]))
        self._anglog1.append(np.degrees(s1[2]))

        self.n_steps += 1
        return s1

    def angle_clip(self, angle):
        return np.clip(angle, -np.pi, np.pi)

    def get_reward(self,s,s1,a):
        if self._option == 'balancing':
            # Wilsons' reward for balancing
            if abs(np.degrees(s1[0])) > 12:  # fall
                self._bstop = True
                #print -10
                return -10
            else:
                #return 0  #(s1[0] - s[0])**2   
                #print -(s1[0] - s[0])**2   
                return (np.degrees(s1[0]) - np.degrees(s[0]))**2   
        else:
            # Randlov to Goal region
            if abs(np.degrees(s1[0])) > 12:  # fall
                self._bstop = True
                return -1
            else:
                # within 10m radius
                if np.linalg.norm(self._pos[:2] - np.asarray(self.Goal)) < self.Goal_r:
                    self._bstop = True
                    return 0.01
                else:
                    # angle to the goal
                    ang = self.angle(self.Goal, self._pos[:2] - self._pos[2:])
                    return (4. - ang**2) * 0.00004
    
    def angle(v1, v2):
        # http://newtonexcelbach.wordpress.com/2014/03/01/the-angle-between-two-vectors-python-version/
        cosang = np.dot(v1, v2)
        sinang = np.linalg.norm(np.cross(v1, v2))
        return np.arctan2(sinang, cosang)

    def get_state_range(self):
        return self._st_range

    def get_actions(self):
        return self._actions

    def get_action_len(self):
        return 2

    def get_action_index(self, action):
        return np.where(self._actions == action)[0][0]
        #return np.where(np.array([-1, 0, 1]) == action)[0][0]

    def draw_trajectory(self, smplX):
        # over state space (velocity exluded)
        """
        if self._option == 'balancing':
            plt.plot(self._anglog)  # traces of omega
            plt.xlabel("steps")
            plt.ylabel("$\\omega$")
        else:
            plt.plot(self._poslog[:,0],self._poslog[:,1])  # traces of front wheel
            plt.plot(self._poslog[0,0],self._poslog[0,2],'go')  # starting pos

            plt.Circle(self.Goal, self.Goal_r, color='r', fill=False)
            plt.xlabel("$x$")
            plt.ylabel("$y$")
        """
        ax = plt.gca()
        rect = ax.get_position()

        plt.plot(self._poslog[:,2],self._poslog[:,3])  # traces of rear wheel
        plt.plot(self._poslog[0,2],self._poslog[0,3],'go')  # starting pos

        if self._option == 'navigating':
            plt.plot(self.Goal[0], self.Goal[1], 'ro')
            #print self.Goal, self.Goal_r
            #cir = plt.Circle(self.Goal, self.Goal_r, color='r') #, fill=False)
            #ax.add_artist(cir)
        plt.xlabel("$x$ (" + str(self._poslog.shape[0]) + " steps)")
        plt.ylabel("$y$")

        rect = ax.get_position()
        sz = rect.width * 0.4
        plt.sca(ax)
        x = rect.x0 + rect.width * 0.1
        y = rect.y0 + rect.height * 0.7
        a = plt.axes([x, y, sz, sz])
        plt.plot(self._anglog)  # traces of omega
        #plt.xlabel("steps")
        #plt.ylabel("$\\omega$")
        plt.title("$\\omega$")
        plt.setp(a, xticks=[]) #, yticks=[])

        x = rect.x0 + rect.width * 0.6
        a = plt.axes([x, y, sz, sz])
        plt.plot(self._anglog1)  # traces of omega
        #plt.xlabel("steps")
        #plt.ylabel("$\\theta$")
        plt.title("$\\theta$")
        plt.setp(a, xticks=[]) #, yticks=[])

    def check_early_stop(self, s, R):
        return self._bstop

    def set_search_action(self, enable=True):
        self.search_action = enable

    def plot_Q(self, rlframe, n_plots=-1):
        pass
        #xpos = np.linspace(self.xbound[0], self.xbound[1], 20)
        #actions = self.get_actions()
        #xs, ys = np.meshgrid(xpos, actions)
        #states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        #rlframe.draw_Qplot(xs, ys, states)
        #plt.xlabel("s")
        #plt.ylabel("a")
        #plt.title(self.Qtitle(rlframe))
