import numpy as np
import matplotlib.pyplot as plt
import random
from copy import deepcopy as copy
from ..base import RLModel
import math
import collections


BOAT_RIVER_SIZE=200
class Boat(RLModel):
    """ boat problem in Lazaric et al. 

        states: x, y, angle, dangle, speed
        action: the desired direction U [-90, 90] degrees

        |       |       |      |    |        |
        |       v       v      v    v        |
        |                                    |______________
        |              \\                    |              |
        |               \\                   |-> Goal(Zs)   | Zv
        |                                    l______________|
        |                                    |

        fixed params
        ------------
        fc:     1.25
        I:      0.1
        smax:   2.5
        sd:     1.75
        p:      0.9
        Zs:     0.2
        Zv:     20
    """

    def __init__(self, river = [BOAT_RIVER_SIZE , BOAT_RIVER_SIZE],
                 goal=None, **params):

        RLModel.__init__(self)
        self.episodic = params.pop('episodic', True)
        # len(state) + action (1)
        self.river = river
        if goal is None:
            goal = [self.river[0], self.river[1] * .55]
        self.Goal = goal

        self.n_state = 5
        self.n_action = 1
        disc_act_learning = params.pop('disc_act_learning', False)
        if disc_act_learning:
            self.nnNI = self.n_state
        else:
            self.nnNI = self.n_state + 1
        self.disc_act_learning = disc_act_learning

        self.params = {'fc': 1.25, 'I':0.1, 'smax':2.5, 'sd':1.75,
                        'p': 0.9, 'Zs':self.river[1] / 100,
                        'Zv': self.river[1] / 10}
        self.lrtr = []
        self.search_action = False

        # as in Jouffe's
        self._disc_actions = [-90, -75, -60, -45, -30, -15, 0, 15, 45, 75, 90]
        self._sa_start = -1 #range(-75, 90, 30)

        self.sum_rewards = 0

        # stdard deviation for gaussian action exploration (15 degree)
        self.action_stddev = 15 
        self.action_stddev_dec = 0.01

    def init(self, start=None):
        self.n_steps = 0
        if start is not None:
            return start
        #self.is_done = False
        self.sum_rewards = 0
        x = 0 #random.sample([0,50,100,150], 1)[0]
        cand = np.linspace(self.river[1] * .05, self.river[1] * .95, 7)
        y = random.sample(cand, 1)[0]
        return [x, y, 0, 0, 0] #self.params['sd']]

    def get_random_action(self):
        if self.search_action:
            return random.uniform(-90,90) # for continuous action
        else:
            return random.sample(self.get_actions(), 1)

    def get_bound_act(self, a):
        if isinstance(a, collections.Iterable): a = a[0]
        if a > 90:
            return 90 
        elif a < -90:
            return -90
        else:
            return a

    def next(self,s,a) :
        s1 = copy(s)

        x, y, angle, dangle, speed = s

        # rudder angle
        if isinstance(a, collections.Iterable): a = a[0]
        w1 = min(max(self.params['p'] * (a - angle), -45.), 45.)

        # speed
        s1[4] = speed1 = speed + (self.params['sd'] - speed) * self.params['I']

        # angle and angle speed
        s1[3] = dangle1 = dangle + (w1 - dangle) * speed1 / self.params['smax']
        s1[2] = angle1 = angle + self.params['I'] * dangle1

        # x1
        rad_a1 = math.radians(angle1)
        s1[0] = x1 = min(self.river[0], max(0., x + speed1 * math.cos(rad_a1)))
        E = self.params['fc'] * (x1 / 50 - (x1 / 100) ** 2)

        # y1
        s1[1] = min(self.river[1], max(0, y - speed1 * math.sin(rad_a1) - E))
        
        #np.set_printoptions(precision=3)
        #print np.array(s), a
        #print "--", np.array(s1)

        self.n_steps += 1
        return s1

    def get_reward(self,s,s1,a):
        """
          +10       __
                   /  \
                  /    \
          -10 ___/      \____
        """
        if s1[0] < self.river[0]:
            if s1[1] >= self.river[1] or s1[1] <= 0.:
                return -.1
            else:
                return 0.
        else:
            #self.is_done = True
            zv = self.params['Zv'] / 2.
            zs = self.params['Zs'] / 2.

            dev_goal = s1[1] - self.Goal[1]
            if abs(dev_goal) < zs:
                #print "in Goal[",s1[1],"]: 10"
                return 10
            elif abs(dev_goal) < zv:
                if dev_goal < 0:
                    # linearly decreasing reward from 10 to -10
                    slope = 20 / (zv - zs)
                    #print "slope: ", slope
                    #print "in margin[",s1[1],"]: ", slope * s1[1] - 10 - slope * (self.Goal[1] - zv)
                    return slope * s1[1] - 10 - slope * (self.Goal[1] - zv)
                else:
                    # linearly decreasing reward from 10 to -10
                    slope = -20 / (zv - zs)
                    #print "slope: ", slope
                    #print "in margin[",s1[1],"]: ", slope * s1[1] - 10 - slope * (self.Goal[1] + zv)
                    return slope * s1[1] - 10 - slope * (self.Goal[1] + zv)
            else:
                return -10

    def get_state_range(self):
        if self.disc_act_learning:
            return np.array([[0, self.river[0]], [0, self.river[1]],
                             [-90, 90], [-45, 45],[0, 2.5]]).T
        else:
            return np.array([[0, self.river[0]], [0, self.river[1]],
                             [-90, 90], [-45, 45],
                             [0, 2.5], [-90, 90]]).T
                         #[-90, 90]]).T

    def get_actions(self):
        return self._disc_actions

    def set_actions(self, actions):
        self._disc_actions = actions

    def get_search_act_start(self):
        if self._sa_start == -1:
            return [self.get_random_action()]  # 1 random starting point will be better
        else:
            return self._sa_start

    def set_search_act_start(self, actions):
        self._sa_start = actions

    def get_action_index(self, action):
        return np.where(np.array(self._disc_actions) == action)[0][0]
        # for multiple dim
        #return np.where((np.array(self._disc_actions) == action).all(axis=1))[0][0]

    def draw_trajectory(self, smplX):
        plt.plot(smplX[:,0],smplX[:,1])
        plt.axis([0, self.river[0], 0, self.river[1]])

        self.draw_goal()

    def draw_goal(self):
        # draw marginal goal regions
        zv = self.params['Zv'] / 2.
        zs = self.params['Zs'] / 2.
        
        # zv
        plt.plot([self.Goal[0], self.Goal[0]],
                 [self.Goal[1] - zv, self.Goal[1] + zv],
                 'y-', linewidth=5)  # zv
        # zs
        plt.plot([self.Goal[0], self.Goal[0]],
                 [self.Goal[1] - zs, self.Goal[1] + zs],
                 'g-', linewidth=8)  # zv
        plt.plot(self.Goal[0], self.Goal[1], 'ro')

        plt.xlabel("x") 
        plt.ylabel("y")

    def check_search_cond(self, rtr):
        
        # if the boat reaches near goal area several times
        # CHECK: rtr should not be avg
        if len(self.lrtr) >= 5 and\
            np.all(np.array(self.lrtr[len(self.lrtr)-5:]) > 0.):
            return True
        return False

    def search_act_store(self, R):
        self.lrtr.append(R[-1])

    def check_early_stop(self, s, R):
        """ check if the episode is over """

        self.sum_rewards += R
        #if self.is_done:
        if s[0] >= self.river[0]:
            return True
        elif self.sum_rewards < -10:
            return True
        return False

    def get_episodic_penalty(self, step, maxstep):
        if (step+1) == maxstep:
            return -10
        else:
            return 0

    #def transform_state(self, s):
    #    ts = copy(s[:-1])
    #    return np.round(np.array(ts) / 10) * 10
        #ts[:2] = np.round(np.array(ts[:2]) / 10) * 10
        #return ts

    def set_search_action(self, enable=True):
        self.search_action = enable

    def plot_Q(self, rlframe, n_plots=-1):
        posx = [10, 30, 100, 180, 190]
        posy = [10, 100, 110, 120, 190]
        xs, ys = np.meshgrid(posx, posy)
        states = np.vstack((xs.flat, ys.flat)).T
        actions = np.asarray(self.get_actions())

        i = 1
        if n_plots == 1:
            states = states[len(states)/2, None]
        for st in states:
            plt.subplot(5, 5, i)
            stin = np.hstack((np.tile(st, (len(actions), 1)),
                             actions.reshape((-1, 1))))
            rlframe.draw_Qplot(xs, ys, stin)
            plt.xlabel("x")
            plt.ylabel("a")
            plt.title("State: " + str(st))
            i += 1
        plt.suptitle("Max Q")
