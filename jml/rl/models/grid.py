"""  Grid world
        discrete world with walls.
        Agent takes five actions, moving four directions or staying.

                                    Jake Lee (lemin)
"""
import numpy as np
import matplotlib.pyplot as plt
#import random
#from copy import deepcopy as copy
from ..base import RLModel
import collections
#from jml.util.exp import make_indicator_vars
import warnings


class GridWorld(RLModel):
    """ Grid World environment
            there are four actions (left, right, up, and down) to move an agent
            In a grid, if it reaches a goal, it get 30 points of reward.
            If it falls in a hole or moves out of the grid world, it gets -5.
            Each step costs -1 point.

            states: x
            action: action [-1,1]

        to test GridWorld, run the following sample codes:

            env = GridWorld('grid.txt')

            env.print_map()
            print [2,3], env.check_state([2,3])
            print [0,0], env.check_state([0,0])
            print [3,4], env.check_state([3,4])
            print [10,3], env.check_state([10,3])

            env.init([0,0])
            print env.next(1)  # right
            print env.next(3)  # down
            print env.next(0)  # left
            print env.next(2)  # up
            print env.next(2)  # up

        map file format:
            * for walls, S for starting position, G for Goal position
            if S or G does not exist, they are randomly selected

            ie.
            *****
            *  G*
            * ***
            *  S*
            *****

    """
    def __init__(self, fn):
        # read a map from a file
        RLModel.__init__(self)
        self.episodic = params.pop('episodic', True)
        self.n_state = 2
        self.n_action = 1
        self.nnNI = self.n_state + self.n_action
        self._map = self.read_map(fn)
        self._size = np.asarray(self._map.shape)
        self.Goal = np.asarray(np.where(self._map == 'G')).flatten()
        if len(self.Goal) == 0:  # no 'G'
            self.Goal = np.array([np.random.randint(1, self._size[0]),
                                  np.random.randint(1, self._size[1])])
        self.Start = np.asarray(np.where(self._map == 'S')).flatten()
        self._st_range = np.array([[1, self._size[0]-2],
                                   [1, self._size[1]-2],
                                   [0, 3]]).T

        # definition of actions (left, right, up, and down repectively)
        self._actions = [[0, -1], [0, 1], [-1, 0], [1, 0]]
        self._s = None
        self.len_acts = len(self._actions)
        self._acts = np.arange(self.len_acts)

    def get_cur_state(self):
        return self._s

    def get_size(self):
        return self._size

    def read_map(self, fn):
        grid = []
        with open(fn) as f:
            for line in f:
                grid.append(list(line.strip()))
        return np.asarray(grid)

    def print_map(self):
        print self._map

    def check_state(self, s):
        if isinstance(s, collections.Iterable) and len(s) == 2:
            if s[0] < 0 or s[1] < 0 or\
               s[0] >= self._size[0] or s[1] >= self._size[1] or\
               self._map[tuple(s)] == '*':
                return -1
            return 0
        else:
            return -2

    def init(self, start=None):
        self.n_steps = 0
        if start is not None:
            return start
        s = list(self.Start)
        if len(s) == 0: # no 'S' -> random start
            s = [np.random.randint(1, self._size[0]),
                 np.random.randint(1, self._size[1])]

        if self.check_state(s) == 0:
            self._s = np.asarray(s)
        else:
            raise ValueError("Invalid state for init")
        return self._s

    def next(self, s, a):
        self._s = s
        s1 = self._s + self._actions[a]
        if self.check_state(s1) == 0:
            self._s = s1
        self.n_steps += 1
        return self._s

    def get_reward(self, s, s1, a):
        if np.all(s1 == self.Goal):
            return 30
        else:
            return -1

    def get_actions(self):
        return self._acts

    def get_random_action(self):
        #return self._actions[np.random.randint(len(self._actions))]
        return np.random.randint(self.len_acts)

    def get_bound_act(self, a):
        return np.clip(a, 0, self.len_acts-1)

    def get_state_range(self):
        return self._st_range

    def get_action_index(self, action):
        return np.where(self._acts == action)[0][0]

    def draw_trajectory(self, smplX):
        if smplX.shape[1] == 1:
            return
        plt.plot(smplX[:, 0], smplX[:, 1])
        plt.axis(self._st_range.T[:-1].flatten())
        plt.plot(smplX[0, 0], smplX[0, 1], 'go')
        plt.plot(self.Goal, 'ro')
        # draw a goal region
        plt.xlabel("x")
        plt.ylabel("y")

    def check_early_stop(self, s, R):
        if np.all(s == self.Goal):
            return True
        return False

    def set_search_action(self, enable=True):
        self.search_action = enable
