import numpy as np
import matplotlib.pyplot as plt
import random
from copy import deepcopy as copy
from ..base import RLModel
import collections
from jml.util.exp import make_indicator_vars


class Marble(RLModel):
    """ 1d marble problem
        
        states: x, dx
        action: action [-1,1]


        |            ___                     |
        |___________|///|____G_______________|
                    <- ->
    """

    def __init__(self,goal=5, **params):
        RLModel.__init__(self)

        # len(state) + action (1)
        self.n_state = 2
        self.n_action = 1
        self.Goal = goal
        self.bound = params.pop('bound', [0, 10, -5, 5])
        if len(self.bound) != 4:
            self.bound = self.bound[:2] + [-5, 5]
        self.disc_act_learning = params.pop('disc_act_learning', False)
        self.disc_act_indicator = params.pop('disc_act_indicator', False)
        if self.disc_act_learning:
            self.nnNI = self.n_state
            self._st_range = np.array([self.bound[:2], self.bound[2:]]).T
        elif self.disc_act_indicator:
            self._st_range = np.array([self.bound[:2], self.bound[2:], [0, 1], [0, 1], [0, 1]]).T
            self.nnNI = self.n_state + 3
        else:
            self._st_range = np.array([self.bound[:2], self.bound[2:], [-1, 1]]).T
            self.nnNI = self.n_state + 1
        self.goal_width = 1

    def init(self, start=None):
        if start is not None:
            return start
        return [random.randint(self.bound[0], self.bound[1]), 0.]
       
    def get_random_action(self):
        return float(np.random.randint(3) -1) # discrete action
        #return random.uniform(-1,1) # for continuous action

    def get_bound_act(self, a):
        if a[0] > 1:
            return 1
        elif a[0] < -1:
            return -1
        else:
            return a[0]

    def next(self,s,a) :
        if isinstance(a, collections.Iterable):
            a = a[0]
        s1 = copy(s)
        dT = 0.1
        s1[0] += dT * s[1]  
        s1[1] += dT * ( 2*a - 0.2 * s[1] )

        # adjust velocity when outside of the track
        if s1[0] < self.bound[0]:
            #s1[:]  = [1 - (s[0]-1)*0.8, s[1] * 0.8]
            s1[:]  = [self.bound[0], 0]
        elif s1[0] > self.bound[1] :
            #s1[:] = [10 - (s[0]-10)*0.8, s[1] * -0.8]
            s1[:] = [self.bound[1], 0]
        # clipping the velocity
        s1[1] = np.clip(s1[1], self._st_range[0, 1],
                               self._st_range[1, 1])

        #appxPos = round(s[0])
        #sys.stdout.write("\r|__________|")
        #sys.stdout.write("\r|")
        #for i in xrange(10):
        #    if i==appxPos-1: 
        #        sys.stdout.write("o")
        #    else:
        #        sys.stdout.write("_")
        #sys.stdout.write("|")
        return s1

    def get_reward(self,s,s1,a):
        return 1 if abs(s1[0] - self.Goal) < self.goal_width else 0

    def get_state_range(self):
        return self._st_range

    def get_actions(self):
        return [-1., 0., 1.]

    def get_action_index(self, action):
        return np.where(np.array([-1, 0, 1]) == action)[0][0]

    def draw_trajectory(self, smplX):
        if smplX.shape[1] == 1: return
        plt.plot(smplX[:,0],smplX[:,1])
        plt.axis([self.bound[0], self.bound[1],-5,5])
        plt.plot(smplX[0,0],smplX[0,1],'go')
        plt.plot(self.Goal,0,'ro')
        # draw a goal region
        plt.fill_between([self.Goal-self.goal_width, self.Goal+self.goal_width],
                         [-5,-5], [5,5],
                         color="red", alpha=0.3)
        plt.xlabel("s") 
        plt.ylabel("s dot")

    def check_search_cond(self, rtr):
        if len(rtr) >= 5 and\
            np.all(np.array(rtr[len(rtr)-5:]) > 0.7):
            return True
        return False

    def set_search_action(self, enable=True):
        self.search_action = enable

    def _get_input_states(self, rlframe):
        xpos = np.arange(self.bound[0], self.bound[1]+.1)
        actions = self.get_actions()
        xs, ys = np.meshgrid(xpos, actions)
        if rlframe.disc_action_learning:
            states = np.vstack((xpos, np.zeros(len(xpos)))).T
        elif rlframe.disc_action_indicator:
            states = np.vstack((xs.flat, np.zeros(xs.size))).T
            a_ind = make_indicator_vars(ys.reshape((-1, 1)))
            states = np.hstack((states, a_ind))
        else:
            states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        return states, xs, ys, len(xpos)

    def plot_Q(self, rlframe, n_plots=-1):
        states, xs, ys, _ = self._get_input_states(rlframe)
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("s")
        plt.ylabel("a")
        plt.title(self.Qtitle(rlframe))

    def plot_policy(self, rlframe):
        states, xs, ys, dim = self._get_input_states(rlframe)
        qs = rlframe._func.use(states)
        if not rlframe.disc_action_learning:
            qs = qs.reshape((-1, dim)).T

        str_acts = ["L", "0", "R"]
        act_i = np.argmax(qs, axis=1)
        for i, a_i in enumerate(act_i):
            plt.text(i, 0, str_acts[a_i])
        plt.xlim(self.bound[0]-1, self.bound[1]+1)
        plt.ylim(-1, 1)
        plt.axis("off")

    def print_policy(self, rlframe, temp=None):
        width = self.bound[1] - self.bound[0] + 50
        np.set_printoptions(precision=3, suppress=True, linewidth=width)
        states, xs, ys, dim = self._get_input_states(rlframe)
        qs = rlframe._func.use(states)
        if not rlframe.disc_action_learning:
            qs = qs.reshape((-1, dim))
        else:
            qs = qs.T

        if np.all(qs == 0.):
            print "ALL ZEROS"
            return

        str_acts = ["L", "0", "R"]
        act_i = np.argmax(qs, axis=0)
        print "<Greedy>>>>>>>>>"
        for i, a_i in enumerate(act_i):
            print str_acts[a_i],
        print
        if temp is not None:
            print "<SOFTMAX>>>>>>>>>"
            exp_qs = np.exp(qs / temp)
            prob_act = exp_qs / np.sum(exp_qs, 0)

            if dim > 20:
                unit_size = 20
                line_max = dim / unit_size
            else:
                unit_size = dim
                line_max = 1
            for i in xrange(line_max):
                print "\t", i*unit_size, ":", (i+1)*unit_size
                print prob_act[:, i*unit_size:(i+1)*unit_size]

    def print_Q(self, rlframe):
        #width = self.bound[1] - self.bound[0] + 50
        #np.set_printoptions(precision=3, suppress=True, linewidth=width)
        states, xs, ys, dim = self._get_input_states(rlframe)
        qs = rlframe._func.use(states)  #reshape((-1, dim)).T
        if not rlframe.disc_action_learning:
            qs = qs.reshape((-1, dim))
        else:
            qs = qs.T

        print "<<Q>>>>>>>>>>>>"
        if np.all(qs == 0.):
            print "ALL ZEROS"
            return

        if dim > 20:
            unit_size = 20
            line_max = dim / unit_size
        else:
            unit_size = dim
            line_max = 1
        for i in xrange(line_max):
            print "\t", i*unit_size, ":", (i+1)*unit_size
            print np.vstack((qs, xs[0, :]))[:, i*unit_size:(i+1)*unit_size]


class DMarble(Marble):  # RLT.RLModel):
    """ Reinforcement learning Model for Discrete Marble. 
    """

    def __init__(self,goal=5, **params):
        Marble.__init__(self, goal, **params)
        # len(state) + action (1)
        self.nnNI -= 1
        self.Goal = goal 
        self.act_bound = [-1, 1]
        self.bound = self.bound[:2]
        self._st_range = np.delete(self._st_range, 1, 1)

    def init(self):
        return [random.randint(self.bound[0], self.bound[1])]
        #return [random.randint(0,10)]
        #return [0.]
       
    def next(self,s,a):
        s1 = copy(s)
        s1[0] += a
        if s1[0] < self.bound[0]:
            s1[0] = self.bound[0]
        elif s1[0] > self.bound[1]:
            s1[0] = self.bound[1] 
        return s1

    def get_state_range(self):
        return self._st_range
        #return np.array([self.bound, self.act_bound]).T

    def _get_input_states(self, rlframe):
        xpos = np.arange(self.bound[0], self.bound[1]+.1)
        actions = self.get_actions()
        xs, ys = np.meshgrid(xpos, actions)
        if rlframe.disc_action_learning:
            states = xpos.reshape((-1, 1))
        elif rlframe.disc_action_indicator:
            a_ind = make_indicator_vars(ys.reshape((-1, 1)))
            states = np.hstack((xs.reshape((-1, 1)), a_ind))
        else:
            states = np.vstack((xs.flat, ys.flat)).T
        return states, xs, ys, len(xpos)
