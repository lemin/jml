import numpy as np
import matplotlib.pyplot as plt
import random
from copy import deepcopy as copy
from ..base import RLModel
import collections
from math import cos


class MtCar(RLModel):
    """ Mountain car problem
        
        states: x, dx
        action: action [-1,1]

        xbound: [-1.2, 0.5]
        xdotboudn: [-0.1, 0.1]

                         _______G
                        /
                       /
        \             /
         \           /
          \         /
           \       /
            \_|//|/
             <-  ->
    """

    def __init__(self,goal=0.5, **params):
        RLModel.__init__(self)
        self.episodic = params.pop('episodic', True)

        self.n_state = 2
        self.n_action = 1
        self.Goal = goal
        self.xbound = params.pop('xbound', [-1.2, goal])
        self.xdotbound = params.pop('xdotbound', [-.1, .1])
        self.disc_act_learning = params.pop('disc_act_learning', False)
        if self.disc_act_learning:
            self.nnNI = self.n_state
            self._st_range = np.array([self.xbound, self.xdotbound]).T
        else:
            self.nnNI = self.n_state + 1
            self._st_range = np.array([self.xbound, self.xdotbound, [-1, 1]]).T
        # len(state) + action (1)

        self._actions = np.array([-1, 0, 1])

    def set_actions(self, actions):
        self._actions = copy(np.asarray(actions))

    def init(self, start=None):
        self.n_steps = 0
        if start is not None:
            return start
        return [random.uniform(self.xbound[0], self.xbound[1]), 0.]
       
    def get_random_action(self):
        return np.random.choice(self._actions)
        #return float(np.random.randint(3) -1) # discrete action
        #return random.uniform(-1,1) # for continuous action

    def get_bound_act(self, a):
        if a[0] > 1:
            return 1
        elif a[0] < -1:
            return -1
        else:
            return a[0]

    def next(self,s,a) :
        if isinstance(a, collections.Iterable):
            a = a[0]
        s1 = copy(s)
        dT = 0.1
        mass = 0.2
        force = 0.2
        s1[0] += dT * s[1]  
        s1[1] += dT * (-9.8 * mass * cos(3 * s[0]) +\
                       a * force / mass - 0.5 * s[1])

        # adjust velocity to zero on top of left hill
        if s[0] < self.xbound[0]:
            s1[:]  = [self.xbound[0], 0.]
        self.n_steps += 1
        return s1

    def get_reward(self,s,s1,a):
        return -1 if s1[0] < self.Goal else 0
        """
        if s1[0] >= self.Goal:
            return 10
        elif s1[0] < -0.5 or s1[0] > 0.3:
            return -0.1
        else:
            return -1
        """
        """
        if s1[0] < 0:
            return -1
        elif s1[0] < self.Goal:
            return -0.1
        else:
            return 0
        """

    def get_state_range(self):
        return self._st_range

    def get_actions(self):
        return self._actions
        #return [-1., 0., 1.]

    def get_action_index(self, action):
        return np.where(self._actions == action)[0][0]
        #return np.where(np.array([-1, 0, 1]) == action)[0][0]

    def draw_trajectory(self, smplX):
        if not hasattr(self, '_traj'):
            self._traj = []
        self._traj.extend(smplX[:, 0])
        plt.plot(self._traj)
        plt.ylabel("s") 
        """
        plt.plot(smplX[:,0],smplX[:,1])
        plt.axis([-1.3,0.6,-2,2])
        plt.plot(smplX[0,0],smplX[0,1],'go')
        plt.plot(self.Goal,0,'ro')
        plt.xlabel("s") 
        plt.ylabel("s dot")
        """

    def check_early_stop(self, s, R):
        if s[0] >= self.Goal:
            return True
        return False

    def set_search_action(self, enable=True):
        self.search_action = enable

    def plot_Q(self, rlframe, n_plots=-1):
        xpos = np.linspace(self.xbound[0], self.xbound[1], 20)
        actions = self.get_actions()
        xs, ys = np.meshgrid(xpos, actions)
        states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("s")
        plt.ylabel("a")
        plt.title(self.Qtitle(rlframe))
