"""
    Multi-state model for reinforcement learnig with
        examplar Dual-state and Tri-state
"""
import numpy as np
import math
import collections
from ..base import RLModel
import warnings
import matplotlib.pyplot as plt


class MultiState(RLModel):
    """ Multi-State RL Model

        State: [1,... n_state]
        Actions: [1,... n_action]

        nonlinera Reward functions are defined as follows:
            R(s,a) = (s + a)^2
            R(s,a) = sin(s + a)
            R(s,a) = sin(s) + cos(a)
            ...
    """

    def __init__(self, n_state, n_action=None, reward='square'):
        RLModel.__init__(self)
        self.nnNI = 2
        self.ndim = 1
        self.r_opt = reward
        self.n_state = n_state
        if n_action is None:
            self.n_action = n_state
        else:
            self.n_action = n_action

        # empty transition table
        self.tTable = {}

    def init(self, start=None):
        if start is not None:
            return start
        if len(self.tTable) == 0:
            warnings.warn("Transition table is not set.")
        return [np.random.randint(self.n_state) + 1]
       
    def get_random_action(self):
        return np.random.randint(self.n_action) + 1

    def next(self,s,a) :
        if isinstance(s, collections.Iterable):
            s = s[0]
        return [self.tTable[(s,a)]]

    def get_reward(self,s,s1,a):
        if isinstance(s, collections.Iterable):
            s = s[0]
        if isinstance(a, collections.Iterable):
            a = a[0]

        # square
        if self.r_opt == 'square':
            return (s + a)**2 / 100.
        elif self.r_opt == 'xor':
            return (s + a)#^4
        elif self.r_opt == 'linear':
            #return (2*s - a)
            #return (2* s - a)
            return a
        elif self.r_opt == 'cross':
            return int(a==s) + 1
        elif self.r_opt == 'sin':
            return math.sin(s + a)
        elif self.r_opt == 'sin_cos':
            return math.sin(s) + math.cos(a)
        elif self.r_opt == 'gauss':
            std = np.std(range(1, self.n_state+1))
            return math.exp(-((s-2.)**2/std + (a-2.)**2/std))
        else:
            return 0

    def get_state_range(self):
        return np.array([[1, self.n_state], [1, self.n_action]]).T

    def get_actions(self):
        return range(1, self.n_action+1)

    def draw_trajectory(self, smplX):
        # plot states
        plt.plot(smplX[:,0], 'b-')
        plt.axis([0,smplX.shape[0],0,10])
        # plot actions
        plt.plot(smplX[:,1]+self.n_action+1, 'r-')
        plt.xlabel("reps") 
        plt.ylabel("st a")

    def plot_Q(self, rlframe, n_plots=-1):
        xpos = np.arange(self.n_state) + 1
        ypos = np.arange(self.n_action) + 1
        xs, ys = np.meshgrid(xpos, ypos)
        states = np.vstack((xs.flat, ys.flat)).T
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("s")
        plt.ylabel("a")
        plt.title(self.Qtitle(rlframe))

    def debug_progress(self, rlframe, reps):
        ss = np.arange(self.n_state) + 1
        acs = np.arange(self.n_action) + 1
        xs, ys = np.meshgrid(ss, acs)
        stin = np.vstack((xs.flat, ys.flat)).T
        o = rlframe._func.use(stin)

        headstr = ''.join(['action ' + str(i+1) for i in xrange(self.n_action)])
        cols = ''.join(['{:8.2f} ']  * self.n_action)
        rowstr = ''.join([' '.join(['state', str(i+1), cols, '\n'])
                            for i in xrange(self.n_state)])
        print ''.join(['[{:03d}]{:7s} ', headstr, '\n', rowstr]).format(
                       reps, "", *o[:, 0])


class DualState(MultiState):
    """ Dual-State RL Model

        State: [1, 2]
        Actions: [1, 2]
    """

    def __init__(self, reward='square'):
        MultiState.__init__(self, 2, reward)

        # transition table
        if True:
            self.tTable = { (1,1): 2, (1,2): 1, 
                            (2,1): 1, (2,2): 2 }
        else:
            self.tTable = { (1,1): 1, (1,2): 2, 
                            (2,1): 1, (2,2): 2 }


class TriState(MultiState):
    """ Tri-State RL Model

        State: [1, 2, 3]
        Actions: [1, 2, 3]
    """

    def __init__(self, reward='square'):
        MultiState.__init__(self, 3, reward)

        # transition table
        if True:
            # trap in st(1), opt policy can be (2,2)<->(3,1)
            self.tTable = { (1,1): 3, (1,2): 3, (1,3): 1,
                            (2,1): 1, (2,2): 3, (2,3): 2,
                            (3,1): 2, (3,2): 1, (3,3): 3 }
        else:
            # opt policy can be circular (2,2)->(3,2)->(1,2)->(2,2)
            self.tTable = { (1,1): 3, (1,2): 2, (1,3): 1,
                            (2,1): 2, (2,2): 3, (2,3): 1,
                            (3,1): 2, (3,2): 1, (3,3): 3 }


class Chain(MultiState):
    """ Chain problem (Strens 2000; Dearden et al. 1998)

        State: [1, 2, 3, 4, 5]
        Actions: [a, b] --> [1, 2]

      b,2 /\    a,0     a,0    a,0    a,0    /\ a,10
          \-> 1 --->  2 ---> 3 ---> 4 ---> 5<-/ 
             |<-----b,2
             |<------------b,2
             |<-------------------b,2
             |<--------------------------b,2
    """

    def __init__(self, reward='square'):
        MultiState.__init__(self, 5, 2, reward)

        # transition table
        self.tTable = { (1,1): 2, (1,2): 1,
                        (2,1): 3, (2,2): 1,
                        (3,1): 4, (3,2): 1,
                        (4,1): 5, (4,2): 1,
                        (5,1): 5, (5,2): 1}

    def get_reward(self,s,s1,a):
        if a == 1:
            if s == 5:
                return 10
            else:
                return 0
        elif a == 2:
            return 2
