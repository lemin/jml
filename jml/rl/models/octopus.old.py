""" Reinforcement Learning with Ocotopus Arm 
    based on connection with RL Simulator from RL Competition

    Reference
    ---------
    Engel
    Engel
    RL Competition
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import random 
import pdb
import socket
import sys
from jml.rl.base import RLModel
from jml.rl.nn import RLtrainNN
from jml.regress.nnet import NeuralNet
from scipy.spatial.distance import cdist
import itertools
from time import strftime, localtime


LINE_SEPARATOR = '\n'
BUF_SIZE = 4096 #in bytes

class Octopus(RLModel):

    ndim = 1 
    State= None
    search_action = False
    _point_state = True #False  # default: state in length representation

    def __init__(self, host, port, **args):
        RLModel.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect(host,port)

        self.sendStr('GET_TASK')
        data = self.receive(2)
        self.state_dim = int(data[0])
        self.action_dim = int(data[1])
        #print "action dim: ", self.action_dim
        self.ncomp= self.action_dim/3

        tmp = self.transform_state([0]*self.state_dim) 
        self.nnNI = len(tmp) + self.action_dim

        # Engel's
#        base_act = [0, 0]
        half = int(self.ncomp * 0.6)
        rest = self.ncomp - half
#        actions = []
#        for b in [[0, 0], [0, 1], [1, 0]]:
#            actions.append(np.array([base_act + [1, 0, 0] * self.ncomp,
#                                base_act + [0, 1, 0] * self.ncomp, 
#                                base_act + [1, 1, 1] * half + [0, 0, 0] * rest,
#                                base_act + [1, 0, 0] * rest + [0, 1, 0] * half,
#                                base_act + [0, 1, 0] * rest + [1, 0, 0] * half,
#                                base_act + [0, 0, 0] * self.ncomp]))
#        self._disc_actions = np.vstack((actions))
        self._disc_actions = np.vstack((np.array([[1, 0, 0] * self.ncomp,
                                        [0, 1, 0] * self.ncomp, 
                                        [1, 1, 1] * half + [0, 0, 0] * rest,
                                        [1, 0, 0] * rest + [0, 1, 0] * half,
                                        [0, 1, 0] * rest + [1, 0, 0] * half,
                                        [0, 0, 0] * self.ncomp])))

        # TODO: preset initial states
        #       base angle, base angle speed, 
        #       vx, xy, vxd, xyd, ... 
        #       dx, dy, dxd, dyd, ...
        #self._disc_states = 

    def init(self, start=None):
        self.sendStr('START')
        data = self.receive(2+self.state_dim)
        self.termFlag = int(data[0])
        state = map(float,data[2:])
        #state[0] = self.limit_angle(state[0])

        #self.sendStr('SET_STATE')
        #self.sendStr(str(self.state_dim))
        #self.sendState(self._disc_states)
        return state

    def get_random_action(self):
        if self.search_action:
            a = np.random.random(self.action_dim)
            #a[:2] = [0 ,1]
            return a
        else: # from discrete actions set
            return random.sample(self.get_actions(), 1)[0]

    def bound_action(self, a):
        if a>1: return 1
        elif a<0: return 0 
        else: return a

    def get_bound_act(self,a):
        return map(self.bound_action, a)

    def next(self,s,a) :
        self.sendStr('STEP')
        self.sendStr(str(self.action_dim))
        self.sendAction(a)

        data = self.receive(3+self.state_dim)
        self.curReward = float(data[0])
        self.termFlag = int(data[1])
        state = map(float, data[3:])
        #print state
        #print self.limit_angle(state[0])
        #state[0] = self.limit_angle(state[0])
        return state

    def get_action_len(self):
        return self.action_dim

    def is_episodic(self):
        return True

    def check_early_stop(self, s, R):
        return self.termFlag

    # since OctopusEnv returns reward, 
    # getReard() returns stored reward from next() 
    def get_reward(self,s,s1,a):
        return self.curReward

    def get_state_range(self):
        baseRange = np.hstack((np.array([[-1, 1]]).T, np.array([[-1, 1]]).T,
                               np.array([[-0.3,0.3]]).T))
        if self._point_state:
            #baseRange = np.hstack((np.array([[-math.pi, math.pi]]).T, np.array([[-0.3,0.3]]).T))
            # positional state range
            rng_musc = [self.ncomp * -1.2, self.ncomp * 1.2]
            compRange = np.tile(np.array([rng_musc,rng_musc,[-0.05,0.05],[-0.05,0.05]]).T, (1,self.ncomp*2))
            #FIXME: change this to length representation and check range!!!
        else: # transformed length state
            # angle state is decomposed in to cos and sin
            compRange = np.tile(np.array([[0.5, 1.5], [0.5, 1.5],
                                          [0.5, 1.5], [0.5, 1.5],
                                          [-0.05,0.05], [-0.05,0.05],
                                          [-0.05,0.05]]).T,
                                (1, self.ncomp-1))
                                #(1, self.ncomp)) # there no base pos info. 

        actRange = np.tile(np.array([[0,1]]).T, (1, self.action_dim))
        return np.hstack((baseRange,compRange,actRange))

    # use searchAction for octopus ... b/c the large action space
    def get_actions(self):
        if self.ncomp > 1:
            # return some fixed number of actions for efficiency
            return self._disc_actions   
        else:
            #da = np.arange(11) / 10.
            da = np.array([0.2, 0.8])
            return np.asarray(list(itertools.product(da, repeat=5)))

    def limit_angle(self, theta):
        return math.radians(math.degrees(theta)%360)-math.pi
        #if abs(theta) > math.pi: 
        #    return math.copysign(2*math.pi-abs(theta), -theta)
        #else:
        #    return theta
   
    def transform_state(self, state):
        """ Transform state for training 
            points state representation will be changed into length
            org. state   [ theta, dtheta, Vx, Vy, dVx, dVy, Dx, Dy, dDx, dDy]
            trans. state [ sin(theta), cos(theta), dsin(theta), dcos(theta), 
                           lV, lD, lT, lX, dlV, dlD, dlT]
                where lV: ventral len  lD: dorsal len,  lT: transversal len,
                      lX: diagonal len in a compartment
                      df means the velocity of f
        """
        if self._point_state:
            #state[0] = self.limit_angle(state[0])
            #return state
            return np.hstack((math.cos(state[0]), math.sin(state[0]), state[1:]))

        if not isinstance(state, np.ndarray):
            state = np.asarray(state)
        n_state = len(state)
        n_vent = (n_state - 2)/ 2
        vent = state[2:2+n_vent].reshape((-1, 4))#[:, :2]
        dors = state[2+n_vent:].reshape((-1, 4))#[:, :2]

        v = vent[:, :2]
        dv = vent[:, 2:]
        d = dors[:, :2]
        dd = dors[:, 2:]


        # 1 shifted diagonal for distance
        lV = np.diag(cdist(v, v)[:-1, 1:])
        lD = np.diag(cdist(d, d)[:-1, 1:])
        # for transversal, pick diagonal
        lT = np.diag(cdist(v, d))[1:]

        # 1 shifted below from diagonal for diagonal distance
        lX = np.diag(cdist(v, d)[1:, :-1])

        # with velocity compute the delta length
        v_n = v + dv
        d_n = d + dd
        lV_n = np.diag(cdist(v_n, v_n)[:-1, 1:])
        lD_n = np.diag(cdist(d_n, d_n)[:-1, 1:])
        lT_n = np.diag(cdist(v_n, d_n))[1:]

        dlV = lV - lV_n
        dlD = lD - lD_n
        dlT = lT - lT_n

        # FIXME: decompose the angular velocity?  currently nope 
        # (omega cross r) ref:: http://en.wikipedia.org/wiki/Circular_motion
        return np.hstack((math.cos(state[0]), math.sin(state[0]), state[1],
                          np.hstack((lV, lD, lT, lX, dlV, dlD, dlT)).flat
                        ))
        return state

    ###################################################################
    # Copy of functions in agent_handler
    def connect(self, host, port):
        try:
            self.sock.connect((host, port))
        except socket.error:
            print 'Unable to contact environment at the given host/port.'
            sys.exit(1)
            
    def sendStr(self, s):
        self.sock.send(s + LINE_SEPARATOR)
        
    def receive(self, numTokens):
        data = ['']
        while len(data) <= numTokens:
            rawData = data[-1] + self.sock.recv(BUF_SIZE)
            del data[-1]
            data = data + rawData.split(LINE_SEPARATOR)
            
        del data[-1]
        return data
        
    def sendAction(self, action, act_dim=None):
        if act_dim is None:
            act_dim = self.action_dim
        if not (act_dim == len(action)):
            print 'Error, trying to send more than the dimension of the action space'
            sys.exit(1)

        self.sendStr(LINE_SEPARATOR.join(map(str, a)))
        #for a in action:
        #    self.sendStr(str(a))

    def sendState(self, state, state_dim=None):
        if state_dim is None:
            state_dim = self.state_dim
        if not (state_dim == len(state)):
            print 'Error, trying to send more than the dimension of the state space'
            sys.exit(1)

        self.sendStr(LINE_SEPARATOR.join(map(str, s)))
        #for s in state:
        #    self.sendStr(str(s))


    ######################################################################
    # draw functions
    # TODO!!!!!
    """
    def draw_trajectory(self, smplX):
        plt.plot(smplX[:,0],smplX[:,1])
        plt.axis([0,11,-5,5])
        plt.plot(smplX[0,0],smplX[0,1],'go')
        plt.plot(self.Goal,0,'ro')
        plt.xlabel("x") 
        plt.ylabel("x dot")

     def plot_Q(self, rlframe):
        xpos = np.arange(10) + 1
        actions = self.get_actions()
        xs, ys = np.meshgrid(xpos, actions)
        states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("x")
        plt.ylabel("a")
        plt.title("Max Q")
    """
