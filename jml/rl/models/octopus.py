""" Reinforcement Learning with Ocotopus Arm
    based on connection with RL Simulator from RL Competition
    socket communication to run simulation  (Octopus)

    For faster run, python version of rl-competition octopus arm simulator
    is added (OctArm)

    Reference
    ---------
    Engel
    Engel
    RL Competition
"""
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import random
import pdb
import socket
import sys
from jml.rl.base import RLModel
from scipy.spatial.distance import cdist
import itertools
from copy import deepcopy as copy
#from sympy.geometry import Polygon, Circle, intersection
import collections

np_version = list(map(int, np.version.version.split('.')))

class OctArm(RLModel):

    def __init__(self, n_comp, **args):
        self.ncomp = n_comp
        self.n_state = (n_comp + 1) * 8
        self.n_action = n_comp * 3
        self._point_state = args.pop('pointState', True)
        self.search_action = args.pop('searchAct', False)
        self.episodic = args.pop('episodic', True)

        tmp = self.transform_state(
                np.array([0] * self.n_state).reshape((n_comp+1, 8)))
        self.nnNI = len(tmp) + self.n_action

        self.n_comp = n_comp

        self.dT = 0.05

        self.g = -9.8 * 0.02
        self.b = 8.8 * 0.02

        self.kActive = 0.3
        self.kPassive = 0.04
        self.kLength = 1.0
        self.kFriction = 10.0
        self.kPressure = 10.0
        self.kRepulsion = 0.3  # repulsion constant
        self.pRepulsion = 1.0   # repulsion power
        self.tRepulsion = 0.7   # repulsion threshold

        masses = [0.675, 0.675, 0.669, 0.663, 0.656, 0.650,
                  0.644, 0.638, 0.632, 0.626, 0.620]
        self.mass = np.asarray(masses[:n_comp+1]).reshape((-1, 1))

        Dpos = [0.002,0.899,1.797,2.694,3.591,4.489,5.386,6.283,7.180,8.078,8.975,
                0.35,0.34,0.33,0.32,0.31,0.30,0.29,0.28,0.27,0.26,0.27]
                #0.430,0.424,0.418,0.412,0.406,0.399,0.393,0.387,0.381,0.375,0.368]
        dpos = np.asarray(Dpos).reshape((2, -1)).T
        Vpos = [0.002,0.899,1.797,2.694,3.591,4.489,5.386,6.283,7.180,8.078,8.975,
               -0.35,-0.34,-0.33,-0.32,-0.31,-0.30,-0.29,-0.28,-0.27,-0.26,-0.27]
               #-0.430,-0.424,-0.418,-0.412,-0.406,-0.399,-0.393,-0.387,-0.381,-0.375,-0.368]
        vpos = np.asarray(Vpos).reshape((2, -1)).T

        # 4 for v and  d pos and 4 for their velocity
        self.state = np.zeros((n_comp+1, 8))
        self.state[:, :2] = dpos[:n_comp+1]
        self.state[:, 2:4] = vpos[:n_comp+1]

        self.init_state = copy(self.state)

        if np_version[0] >= 1 and np_version[1] >= 10:
            # length in rest state
            self.length_rest = { 'D': np.linalg.norm(np.diff(self.state[:, :2], axis=0), axis=1, keepdims=True) * 0.2,
                                 'V': np.linalg.norm(np.diff(self.state[:, 2:4], axis=0), axis=1, keepdims=True) * 0.2,
                                 'T': np.linalg.norm((self.state[:, :2] - self.state[:, 2:4]), axis=1, keepdims=True) * 0.2 }
        else:
            self.length_rest = { 'D': np.linalg.norm(np.diff(self.state[:, :2], axis=0), axis=1).reshape((-1, 1)) * 0.2,
                                 'V': np.linalg.norm(np.diff(self.state[:, 2:4], axis=0), axis=1).reshape((-1, 1)) * 0.2,
                                 'T': np.linalg.norm((self.state[:, :2] - self.state[:, 2:4]), axis=1).reshape((-1, 1)) * 0.2 }
        self.desired_areas = self.calc_areas(self.state)

        # goal
        self.goal = args.pop('goal', (4, 2, .25))
        #self.goal = Circle(goalparams[:2], goalparams[2])

        ### predefined discrete actions (Engel's)
        ca = 1.  # action value for contraction
        ra = 0.  # action value for release
        ta = 0.  # action value for trans

        actions = np.zeros((6, n_comp, 3))
        actions[1, ...] = np.tile([ra, ca, ta], (n_comp, 1))
        actions[2, ...] = np.tile([ca, ra, ta], (n_comp, 1))
        divn = round(n_comp/2.)
        actions[3, ...] = np.vstack((np.tile([0.5, 0.5, 0.5], (divn, 1)),
                                    np.tile([0.3, 0.3, 0.3], (n_comp-divn, 1))))
        divn = round(n_comp * 0.6)
        actions[4, ...] = np.vstack((np.tile([ra, ca, ta], (divn, 1)),
                                    np.tile([ca, ra, ta], (n_comp-divn, 1))))
        divn = round(n_comp * 0.6)
        actions[5, ...] = np.vstack((np.tile([ca, ra, ta], (divn, 1)),
                                    np.tile([ra, ca, ta], (n_comp-divn, 1))))
        self._disc_actions = actions

    def init(self, start=None):
        self.n_steps = 0
        if start is not None:
            return start
        self.state[:] = self.init_state
        self.termFlag = False
        return copy(self.state.flatten())

    def get_random_action(self):
        if self.search_action:
            a = np.random.random(self.n_action).reshape((self.n_comp, 3))
            return a
        else: # from discrete actions set
            return random.sample(self.get_actions(), 1)[0]

    def bound_action(self, a):
        if a>1: return 1
        elif a<0: return 0 
        else: return a

    def get_bound_act(self,a):
        return map(self.bound_action, a)

    def next(self,s,a, nstep=10) :
        #state = copy(self.state)  #backup
        dT = self.dT

        for i in range(nstep):
            accel = self.calc_accel(a)
            self.state[:, :2] += dT * self.state[:, 4:6]
            self.state[:, 2:4] += dT * self.state[:, 6:]
            self.state[:, 4:6] += dT * accel['D']
            self.state[:, 6:] += dT * accel['V']

        # freeze base
        self.state[0, :] = self.init_state[0, :]
        self.n_steps += 1
        return copy(self.state.flatten())


    def get_action_len(self):
        return self.n_action

    def check_early_stop(self, s, R):
        return self.termFlag

    def get_reward(self,s,s1,a):
        if self.check_goal():
            self.termFlag = True
            return 10
        return -0.01

    def check_goal(self):
        vecs = self.state[:, :4] - self.goal[:2] * 2
        distD = np.linalg.norm(vecs[:, :2], axis=1)
        distV = np.linalg.norm(vecs[:, 2:], axis=1)

        if np.any(distD <= self.goal[2]) or np.any(distV <= self.goal[2]):
            return True
        return False

        """ old slow check
        for i in range(self.ncomp):
            poly = Polygon(self.state[i, :2], self.state[i, 2:4],
                           self.state[i+1, 2:4], self.state[i+1, :2])
            # the arm intersects with a goal
            if len(intersection(poly, self.goal)) > 0:
                return True
            # the arm contains the goal
            if poly.encloses(self.goal):
                return True
        return False
        """

    def action_reshape(self, a):
        return a.reshape((-1, 3))

    def get_state_range(self):
        if self._point_state:
            # positional state range
            #rng_musc = [self.ncomp * -1.2, self.ncomp * 1.2]
            hrng_musc = [-2., 1.2 * self.ncomp]
            vrng_musc = [-5., 5.]
            speed_musc = [-0.5, 0.5]
            compRange = np.tile(np.array([hrng_musc,vrng_musc,hrng_musc,vrng_musc,
                                          speed_musc, speed_musc, speed_musc, speed_musc]).T,
                                (1,self.ncomp+1))
            #FIXME: change this to length representation and check range!!!
        else: # transformed length state
            # angle state is decomposed in to cos and sin
            compRange = np.tile(np.array([[0.5, 1.5], [0.5, 1.5],
                                          [0.5, 1.5], [0.5, 1.5],
                                          [-0.05,0.05], [-0.05,0.05],
                                          [-0.05,0.05]]).T,
                                (1, self.ncomp-1))
                                #(1, self.ncomp)) # there no base pos info. 

        actRange = np.tile(np.array([[0,1]]).T, (1, self.n_action))
        return np.hstack((compRange,actRange))

    # use searchAction for octopus ... b/c the large action space
    def get_actions(self):
        # too slow!!!
        #da = np.array([0.2, 0.8])
        #da = np.array([0., 1.])
        #return np.asarray(list(itertools.product(da, repeat=self.n_action)))
        return self._disc_actions


    def transform_state(self, state):
        """ Transform state for training 
            parameter
            =========
            state       nd.array (n_comp, 8)
                        x,y points and point-velocities
                        
            return
            ======
            flattened state  if self._point_state option is True
            [base angle, lD, lV, lT, lX, dlV, dlD, dlT]
                where lV: ventral len  lD: dorsal len,  lT: transversal len,
                      lX: diagonal len in a compartment
                      df means the velocity of f
        """
        if self._point_state:
            return state.flatten()

        n_state = len(state)
        n_vent = (n_state - 2)/ 2
        vent = state[2:2+n_vent].reshape((-1, 4))#[:, :2]
        dors = state[2+n_vent:].reshape((-1, 4))#[:, :2]

        v = state[:, 2:4]
        dv = state[:, 6:8]
        d = state[:, :2]
        dd = state[:, 4:6]


        # 1 shifted diagonal for distance
        lV = np.diag(cdist(v, v)[:-1, 1:])
        lD = np.diag(cdist(d, d)[:-1, 1:])
        # for transversal, pick diagonal
        lT = np.diag(cdist(v, d))[1:]

        # 1 shifted below from diagonal for diagonal distance
        lX = np.diag(cdist(v, d)[1:, :-1])

        # with velocity compute the delta length
        v_n = v + dv
        d_n = d + dd
        lV_n = np.diag(cdist(v_n, v_n)[:-1, 1:])
        lD_n = np.diag(cdist(d_n, d_n)[:-1, 1:])
        lT_n = np.diag(cdist(v_n, d_n))[1:]

        dlV = lV - lV_n
        dlD = lD - lD_n
        dlT = lT - lT_n

        if False:  # set to zero (b/c frozen base)
            base_angle = 0.
            return np.hstack((base_angle,
                              np.hstack((lV, lD, lT, lX, dlV, dlD, dlT)).flat
                            ))
        else:
            return np.hstack((lD, lV, lT, lX, dlD, dlV, dlT)).flat

    def transform_action(self, a):
        return a.flatten()

    ######################################################################3
    # octopus arm simulation
    def rotate_down(self, vecs):
        return np.hstack((vecs[:, 1, None], -vecs[:, 0, None]))

    def rotate_up(self, vecs):
        return np.hstack((-vecs[:, 1, None], vecs[:, 0, None]))

    def vec_lengths(self, vecs):
        if np_version[0] >= 1 and np_version[1] >= 10:
            return np.linalg.norm(vecs, axis=1, keepdims=True)  #np.sqrt(rowSums(vecs^2))
        else:
            return np.linalg.norm(vecs, axis=1).reshape((-1, 1))  #np.sqrt(rowSums(vecs^2))


    def calc_areas(self, s):
        if s is None:
            s = self.state
        """ calcuate areas of each compartment in state s """
        x1 = s[:-1, 2, None]  #v
        x2 = s[1:, 2, None]   #v
        x3 = s[1:, 0, None]   #d
        x4 = s[:-1, 0, None]  #d
        y1 = s[:-1, 3, None]  #v
        y2 = s[1:, 3, None]   #v
        y3 = s[1:, 1, None]   #d
        y4 = s[:-1, 1, None]  #d
        
        return np.abs(0.5 * (x1*y2-y1*x2 + x2*y3-y2*x3 + x3*y4 - y3*x4 + x4*y1 - y4*x1))


    def calc_accel(self, a, debug=False):
        """ calcuate acceleration in state s of model by applying action a """

        s = self.get_state()
        g = self.get_gravity()
        b = self.get_buoyancy()
        mass =self.mass

        kActive = self.kActive
        kPassive = self.kPassive
        kLength = self.kLength
        kFriction = self.kFriction
        kPressure = self.kPressure

        kRepulsion = self.kRepulsion
        pRepulsion = self.pRepulsion
        tRepulsion = self.tRepulsion

        desired_areas = self.desired_areas
        length_rest = self.length_rest

        ## ######################################
        ## Gravity (-9.8) and Buoyancy (4)

        N = s.shape[0]  #  the number of points (n_comp + 1)

        gravityBuoyancyD = np.tile([0, g+b], (N, 1))
        gravityBuoyancyV = gravityBuoyancyD

        ## ######################################
        ## Friction
        ##  f_f = -k_f ||v||^2 v / ||v|| = -k_f ||v|| v
        if np_version[0] >= 1 and np_version[1] >= 10:
            frictionD = -kFriction * s[:, 4:6] * np.linalg.norm(s[:, 4:6], axis=1, keepdims=True)
            frictionV = -kFriction * s[:, 6:8] * np.linalg.norm(s[:, 6:8], axis=1, keepdims=True)
        else:
            frictionD = -kFriction * s[:, 4:6] * np.linalg.norm(s[:, 4:6], axis=1).reshape((-1, 1))
            frictionV = -kFriction * s[:, 6:8] * np.linalg.norm(s[:, 6:8], axis=1).reshape((-1, 1))

        ## ######################################
        ## Inter-particl repulsion force
        ##  f_r = k_r / ||r - r'||^p_r (r - r') / ||r - r'||  if ||r - r'|| < T_r 
        ##        0   otherwise

        # find || r - r' || for all combinations of nodes
        fRepulsionD = np.zeros((N, 2))
        fRepulsionV = np.zeros((N, 2))
        for node in range(N):
            # distance from dorsal node
            diffD = (s[:, :4] - np.tile(s[node, :2], (N,2)))
            diffD_D = diffD[:, :2]
            diffD_V = diffD[:, 2:4]
            if np_version[0] >= 1 and np_version[1] >= 10:
                distD_D = np.linalg.norm(diffD_D, axis=1, keepdims=True)
                distD_V = np.linalg.norm(diffD_V, axis=1, keepdims=True)
            else:
                distD_D = np.linalg.norm(diffD_D, axis=1).reshape((-1, 1))
                distD_V = np.linalg.norm(diffD_V, axis=1).reshape((-1, 1))
            distD_D[node] = np.inf
            frD_D = kRepulsion * np.divide(diffD_D, (distD_D ** (pRepulsion + 1)))
            frD_V = kRepulsion * np.divide(diffD_V, (distD_V ** (pRepulsion + 1)))
            frD_D[distD_D.flatten() > tRepulsion, :] = 0
            frD_V[distD_V.flatten() > tRepulsion, :] = 0

            diffV = (s[:, :4] - np.tile(s[node, 2:4], (N,2)))
            diffV_D = diffV[:, :2]
            diffV_V = diffV[:, 2:4]
            if np_version[0] >= 1 and np_version[1] >= 10:
                distV_D = np.linalg.norm(diffV_D, axis=1, keepdims=True)
                distV_V = np.linalg.norm(diffV_V, axis=1, keepdims=True)
            else:
                distV_D = np.linalg.norm(diffV_D, axis=1).reshape((-1, 1))
                distV_V = np.linalg.norm(diffV_V, axis=1).reshape((-1, 1))
            distV_V[node] = np.inf
            frV_D = kRepulsion * np.divide(diffV_D, (distV_D ** (pRepulsion + 1)))
            frV_V = kRepulsion * np.divide(diffV_V, (distV_V ** (pRepulsion + 1)))
            frV_D[distV_D.flatten() > tRepulsion] = 0
            frV_V[distV_V.flatten() > tRepulsion] = 0

            fRepulsionD += (frD_D + frV_D)
            fRepulsionV += (frD_V + frV_V)

        ## ######################################
        ## Muscles, longitudinal
        ## f \propto L/L_rest where L is length of connecting segment
        ## Must calculate for outward force (from base) and inward force.

        ## Dorsal
        vecs = s[1:, :2] - s[:-1, :2]
        if np_version[0] >= 1 and np_version[1] >= 10:
            lengths = np.sum(vecs ** 2, axis=1, keepdims=True)  #rowSums(vecs^2)
        else:
            lengths = np.sum(vecs ** 2, axis=1).reshape((-1, 1))
        vecsUnit = vecs / lengths
        l_rate = np.sqrt(lengths) / length_rest['D'] - kLength
        l_rate[l_rate < 0] = 0
        fOutD = (a[:, 0, None] * kActive + kPassive) * l_rate * vecsUnit
        fD = np.zeros((N, fOutD.shape[1])) 
        fD[0, :] = fOutD[0, :]
        fD[-1, :] = -fOutD[-1, :]
        fD[1:-1, :] = fOutD[1:, :] - fOutD[:-1, :]

        ## Ventral
        vecs = s[1:, 2:4] - s[:-1, 2:4]
        if np_version[0] >= 1 and np_version[1] >= 10:
            lengths = np.sum(vecs ** 2, axis=1, keepdims=True)  #rowSums(vecs^2)
        else:
            lengths = np.sum(vecs ** 2, axis=1).reshape((-1, 1))
        vecsUnit = vecs / lengths  ## rep
        l_rate = np.sqrt(lengths) / length_rest['V'] - kLength
        l_rate[l_rate < 0] = 0
        fOutV = (a[:, 1, None] * kActive + kPassive) * l_rate * vecsUnit
        fV = np.zeros((N, fOutV.shape[1])) 
        fV[0, :] = fOutV[0, :]
        fV[-1, :] = -fOutV[-1, :]
        fV[1:-1, :] = fOutV[1:, :] - fOutV[:-1, :]

        ## Transverse
        ##  NOT DONE: must handle base rotation
        vecs = s[:, 2:4] - s[:, :2]
        if np_version[0] >= 1 and np_version[1] >= 10:
            lengths = np.sum(vecs ** 2, axis=1, keepdims=True)  #rowSums(vecs^2)
        else:
            lengths = np.sum(vecs ** 2, axis=1).reshape((-1, 1))
        vecsUnit = vecs / lengths  ## rep
        l_rate = np.sqrt(lengths) / length_rest['T'] - kLength
        l_rate[l_rate < 0] = 0
        ##### Ignore base ######
        aT = np.zeros((N, 1))
        aT[1:] = a[:, 2, None]
        fTD = (aT * kActive + kPassive) * l_rate * vecsUnit
        #fTD = np.zeros((N, fTD_woB.shape[1]))
        #fTD[1:, :] = fTD_woB
        fTV = -fTD

        musclesD = fD + fTD
        musclesV = fV + fTV

        ## ######################################
        ## Pressure forces
        ## 
        ##
        ## a---c---e---g 
        ## |   |   |   |
        ## b---d---f---h                                                   
        ##  
        ## a---c---e---g 
        ##   |   |   |
        ##   V   V   V
        ##
        ##   ^   ^   ^
        ##   |   |   |
        ## b---d---f---h                                                   
        ###
        ## a---c---e---g 
        ## |-> |-> |-> |->
        ## b---d---f---h                                                   

        vecsD = self.rotate_down(s[1:, :2] - s[:-1, :2])
        vecsDLengths = self.vec_lengths(vecsD)
        vecsV = self.rotate_up(s[1:, 2:4] - s[:-1, 2:4])
        vecsVLengths = self.vec_lengths(vecsV)
        vecsT = self.rotate_down(s[:, :2] - s[:, 2:4])
        vecsTLengths = self.vec_lengths(vecsT)

        areasDiff = self.calc_areas(s) - desired_areas

        leftD = areasDiff * (vecsD + vecsT[:-1, :]) / (vecsDLengths + vecsTLengths[:-1, :]) ## rep
        rightD = areasDiff * (vecsD - vecsT[1:, :]) / (vecsDLengths + vecsTLengths[1:]) ## rep
        rightV = areasDiff * (vecsV - vecsT[1:, :]) / (vecsVLengths + vecsTLengths[1:]) ## rep
        leftV = areasDiff * (vecsV + vecsT[:-1, :]) / (vecsVLengths + vecsTLengths[:-1, :]) ## rep
        Dsum = np.zeros((N, leftD.shape[1]))
        Dsum[0, :] = leftD[0, :]
        Dsum[-1, :] = rightD[-1, :]
        Dsum[1:-1, :] = leftD[1:, :] + rightD[:-1, :]
        pressureD = kPressure * (Dsum)
        Vsum = np.zeros((N, leftV.shape[1]))
        Vsum[0, :] = leftV[0, :]
        Vsum[-1, :] = rightV[-1, :]
        Vsum[1:-1, :] = leftV[1:, :] + rightV[:-1, :]
        pressureV = kPressure * (Vsum)

        ## ######################################
        ## Sum up all forces and calculate accelerations

        forcesD = musclesD + pressureD + frictionD + gravityBuoyancyD * mass + fRepulsionD
        forcesV = musclesV + pressureV + frictionV + gravityBuoyancyV * mass + fRepulsionV

        ##image(t(cbind(musclesD,pressureD,frictionD,gravityBuoyancyD)))
        ##system("sleep 0.1")

        if debug:
            np.set_printoptions(precision=7, suppress=True)
            print (np.hstack((forcesD, forcesV)))
            import pdb; pdb.set_trace()
        if False:
            with open('comp_py.txt', 'a+') as f:
                f.write(str(np.hstack((forcesD, forcesV))))
                f.write("\n")
        ### acceleration, dorsal and ventral
        return {'D':forcesD/mass, 'V':forcesV/mass}


    ######################################################################3
    # setters
    def set_state(self, state):
        """ set current state with (n_comp, 8) matrix"""
        self.state = state

    def set_params(self, **params):
        self.g = params['g']
        self.b = params['b']

        self.kActive = params['kActive']
        self.kPassive = params['kPassive']
        self.kLength = params['kLength']
        self.kFriction = params['kFriction']
        self.kPressure = params['kPressure']
        self.kRepulsion = params['kRepulsion']
        self.pRepulsion = params['pRepulsion']
        self.tRepulsion = params['tRepulsion']

    ######################################################################3
    # getters

    def get_state(self):
        return self.state

    def get_gravity(self):
        return self.g

    def get_buoyancy(self):
        return self.b


##################################################################################
## communication throuth rl-competition java simulator
LINE_SEPARATOR = '\n'
BUF_SIZE = 4096 #in bytes

class Octopus(RLModel):

    ndim = 1 
    State= None
    search_action = False
    _point_state = True #False  # default: state in length representation

    def __init__(self, host, port, **args):
        RLModel.__init__(self)
        self.episodic = args.pop('episodic', True)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect(host,port)

        self.sendStr('GET_TASK')
        data = self.receive(2)
        self.n_state = self.state_dim = int(data[0])
        self.n_action = self.action_dim = int(data[1])
        #print "action dim: ", self.action_dim
        self.ncomp= self.action_dim/3

        tmp = self.transform_state([0]*self.state_dim) 
        self.nnNI = len(tmp) + self.action_dim

        # Engel's
#        base_act = [0, 0]
        half = int(self.ncomp * 0.6)
        rest = self.ncomp - half
#        actions = []
#        for b in [[0, 0], [0, 1], [1, 0]]:
#            actions.append(np.array([base_act + [1, 0, 0] * self.ncomp,
#                                base_act + [0, 1, 0] * self.ncomp, 
#                                base_act + [1, 1, 1] * half + [0, 0, 0] * rest,
#                                base_act + [1, 0, 0] * rest + [0, 1, 0] * half,
#                                base_act + [0, 1, 0] * rest + [1, 0, 0] * half,
#                                base_act + [0, 0, 0] * self.ncomp]))
#        self._disc_actions = np.vstack((actions))
        self._disc_actions = np.vstack((np.array([[1, 0, 0] * self.ncomp,
                                        [0, 1, 0] * self.ncomp, 
                                        [1, 1, 1] * half + [0, 0, 0] * rest,
                                        [1, 0, 0] * rest + [0, 1, 0] * half,
                                        [0, 1, 0] * rest + [1, 0, 0] * half,
                                        [0, 0, 0] * self.ncomp])))

        # TODO: preset initial states
        #       base angle, base angle speed, 
        #       vx, xy, vxd, xyd, ... 
        #       dx, dy, dxd, dyd, ...
        #self._disc_states = 

    def init(self):
        self.n_steps = 0
        self.sendStr('START')
        data = self.receive(2+self.state_dim)
        self.termFlag = int(data[0])
        state = map(float,data[2:])
        #state[0] = self.limit_angle(state[0])

        #self.sendStr('SET_STATE')
        #self.sendStr(str(self.state_dim))
        #self.sendState(self._disc_states)
        return state

    def get_random_action(self):
        if self.search_action:
            a = np.random.random(self.action_dim)
            #a[:2] = [0 ,1]
            return a
        else: # from discrete actions set
            return random.sample(self.get_actions(), 1)[0]

    def bound_action(self, a):
        if a>1: return 1
        elif a<0: return 0 
        else: return a

    def get_bound_act(self,a):
        return map(self.bound_action, a)

    def next(self,s,a) :
        self.sendStr('STEP')
        self.sendStr(str(self.action_dim))
        self.sendAction(a)

        data = self.receive(3+self.state_dim)
        self.curReward = float(data[0])
        self.termFlag = int(data[1])
        state = map(float, data[3:])
        #print state
        #print self.limit_angle(state[0])
        #state[0] = self.limit_angle(state[0])
        self.n_steps += 1
        return state.flatten()

    def get_action_len(self):
        return self.action_dim

    def check_early_stop(self, s, R):
        return self.termFlag

    # since OctopusEnv returns reward, 
    # getReard() returns stored reward from next() 
    def get_reward(self,s,s1,a):
        return self.curReward

    def get_state_range(self):
        baseRange = np.hstack((np.array([[-1, 1]]).T, np.array([[-1, 1]]).T,
                               np.array([[-0.3,0.3]]).T))
        if self._point_state:
            #baseRange = np.hstack((np.array([[-math.pi, math.pi]]).T, np.array([[-0.3,0.3]]).T))
            # positional state range
            rng_musc = [self.ncomp * -1.2, self.ncomp * 1.2]
            compRange = np.tile(np.array([rng_musc,rng_musc,[-0.05,0.05],[-0.05,0.05]]).T, (1,self.ncomp*2))
            #FIXME: change this to length representation and check range!!!
        else: # transformed length state
            # angle state is decomposed in to cos and sin
            compRange = np.tile(np.array([[0.5, 1.5], [0.5, 1.5],
                                          [0.5, 1.5], [0.5, 1.5],
                                          [-0.05,0.05], [-0.05,0.05],
                                          [-0.05,0.05]]).T,
                                (1, self.ncomp-1))
                                #(1, self.ncomp)) # there no base pos info. 

        actRange = np.tile(np.array([[0,1]]).T, (1, self.action_dim))
        return np.hstack((baseRange,compRange,actRange))

    # use searchAction for octopus ... b/c the large action space
    def get_actions(self):
        if self.ncomp > 1:
            # return some fixed number of actions for efficiency
            return self._disc_actions   
        else:
            #da = np.arange(11) / 10.
            da = np.array([0.2, 0.8])
            return np.asarray(list(itertools.product(da, repeat=5)))

    def limit_angle(self, theta):
        return math.radians(math.degrees(theta)%360)-math.pi
        #if abs(theta) > math.pi: 
        #    return math.copysign(2*math.pi-abs(theta), -theta)
        #else:
        #    return theta
   
    def transform_state(self, state):
        """ Transform state for training 
            points state representation will be changed into length
            org. state   [ theta, dtheta, Vx, Vy, dVx, dVy, Dx, Dy, dDx, dDy]
            trans. state [ sin(theta), cos(theta), dsin(theta), dcos(theta), 
                           lV, lD, lT, lX, dlV, dlD, dlT]
                where lV: ventral len  lD: dorsal len,  lT: transversal len,
                      lX: diagonal len in a compartment
                      df means the velocity of f
        """
        if self._point_state:
            #state[0] = self.limit_angle(state[0])
            #return state
            return np.hstack((math.cos(state[0]), math.sin(state[0]), state[1:]))

        if not isinstance(state, np.ndarray):
            state = np.asarray(state)
        n_state = len(state)
        n_vent = (n_state - 2)/ 2
        vent = state[2:2+n_vent].reshape((-1, 4))#[:, :2]
        dors = state[2+n_vent:].reshape((-1, 4))#[:, :2]

        v = vent[:, :2]
        dv = vent[:, 2:]
        d = dors[:, :2]
        dd = dors[:, 2:]


        # 1 shifted diagonal for distance
        lV = np.diag(cdist(v, v)[:-1, 1:])
        lD = np.diag(cdist(d, d)[:-1, 1:])
        # for transversal, pick diagonal
        lT = np.diag(cdist(v, d))[1:]

        # 1 shifted below from diagonal for diagonal distance
        lX = np.diag(cdist(v, d)[1:, :-1])

        # with velocity compute the delta length
        v_n = v + dv
        d_n = d + dd
        lV_n = np.diag(cdist(v_n, v_n)[:-1, 1:])
        lD_n = np.diag(cdist(d_n, d_n)[:-1, 1:])
        lT_n = np.diag(cdist(v_n, d_n))[1:]

        dlV = lV - lV_n
        dlD = lD - lD_n
        dlT = lT - lT_n

        # FIXME: decompose the angular velocity?  currently nope 
        # (omega cross r) ref:: http://en.wikipedia.org/wiki/Circular_motion
        return np.hstack((math.cos(state[0]), math.sin(state[0]), state[1],
                          np.hstack((lV, lD, lT, lX, dlV, dlD, dlT)).flat
                        ))
        return state

    ###################################################################
    # Copy of functions in agent_handler
    def connect(self, host, port):
        try:
            self.sock.connect((host, port))
        except socket.error:
            print ('Unable to contact environment at the given host/port.')
            sys.exit(1)
            
    def sendStr(self, s):
        self.sock.send(s + LINE_SEPARATOR)
        
    def receive(self, numTokens):
        data = ['']
        while len(data) <= numTokens:
            rawData = data[-1] + self.sock.recv(BUF_SIZE)
            del data[-1]
            data = data + rawData.split(LINE_SEPARATOR)
            
        del data[-1]
        return data
        
    def sendAction(self, action, act_dim=None):
        if act_dim is None:
            act_dim = self.action_dim
        if not (act_dim == len(action)):
            print ('Error, trying to send more than the dimension of the action space')
            sys.exit(1)

        self.sendStr(LINE_SEPARATOR.join(map(str, a)))
        #for a in action:
        #    self.sendStr(str(a))

    def sendState(self, state, state_dim=None):
        if state_dim is None:
            state_dim = self.state_dim
        if not (state_dim == len(state)):
            print ('Error, trying to send more than the dimension of the state space')
            sys.exit(1)

        self.sendStr(LINE_SEPARATOR.join(map(str, s)))
        #for s in state:
        #    self.sendStr(str(s))


    ######################################################################
    # draw functions
    # TODO!!!!!
    """
    def draw_trajectory(self, smplX):
        plt.plot(smplX[:,0],smplX[:,1])
        plt.axis([0,11,-5,5])
        plt.plot(smplX[0,0],smplX[0,1],'go')
        plt.plot(self.Goal,0,'ro')
        plt.xlabel("x") 
        plt.ylabel("x dot")

     def plot_Q(self, rlframe):
        xpos = np.arange(10) + 1
        actions = self.get_actions()
        xs, ys = np.meshgrid(xpos, actions)
        states = np.vstack((xs.flat, np.zeros(xs.size), ys.flat)).T
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("x")
        plt.ylabel("a")
        plt.title("Max Q")
    """


if __name__ == '__main__':
    """ test sample routines for OctArm  """
    # delta time
    ncomp = 10
    nsteps = 10000
    pause = False
    reset = False

    # octopus model for store state
    oct = OctArm(ncomp)
    dT = oct.dT

    action = np.zeros((10, 3))
    action[:, 0] = 1

    if True:
        # collect samples
        state = oct.get_state()
        y = [copy(state)]
        yg = [0]
        for i in range(nsteps):
            #if i >= 500:
            #    action[4:, 2] = 1
            #   debug = True
            #else:
            #    debug = False
            #if i >= 1000:
            #    action[:, 0] = 1
            #if i >= 5000:
            #    action[:, 0] = 0

            oct.next(oct.state, action)

            #import time
            #starttime = time.time()
            r = oct.get_reward(oct.state,oct.state,action)
            #elapsed = time.time() - starttime
            #print ("elapsed: ", elapsed)
            #import pdb; pdb.set_trace()
            if r >= 10:
                print (i+1 ,"steps")
                break

            if False:
                print(state[:, :2])
                print(state[:, 2:4])
                print(state[:, 4:6])
                print(state[:, 6:])
                import pdb; pdb.set_trace()

            # collect states
            y.append(copy(oct.get_state()))
            yg.append(r)


    nsteps = len(y)
    fig = plt.figure()
    ax = fig.add_subplot(111, autoscale_on=False, xlim=(-2, 12), ylim=(-5, 5))
    ax.grid()

    lineD, = ax.plot([], [], 'k-', lw=1)
    lineV, = ax.plot([], [], 'k-', lw=1)
    #goalCircle = plt.Circle([oct.goal.center.x, oct.goal.center.y], oct.goal.radius, color='r')
    goalCircle = plt.Circle(oct.goal[:2], oct.goal[2], color='r')
    ax.add_artist(goalCircle)

    lineT = []
    for i in range(ncomp):
        lT, = ax.plot([], [], 'k-', lw=1)
        lineT.append(lT)
    time_template = 'time = {:.1f}s {:s}'
    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

    def unpack():
        return tuple(j for i in [time_text, lineD, lineV, lineT]
                        for j in (i if isinstance(i, collections.Iterable)
                                else (i,)))

    def init():
        lineD.set_data([], [])
        lineV.set_data([], [])
        for i in range(ncomp):
            lineT[i].set_data([], [])
        time_text.set_text('')
        return unpack()

    # step generator
    def anistep():
        i = 0
        while i < nsteps:
            if reset:
                i = 0
            if not pause:
                i += 1
                if i >= nsteps:
                    i = nsteps-1
            yield i

    def animate(i, gather=True):

        goalreached = False
        if gather:
            state = y[i-1]
            if yg[i-1] > 0:
                goalreached = True
        else:
            state = oct.next(oct.state, action) #, nsteps=5)
            r = oct.get_reward(oct.state,oct.state,action)
            if r > 0:
                goalreached = True
        xv = state[:, 0]
        yv = state[:, 1]
        xd = state[:, 2]
        yd = state[:, 3]
        xt = np.vstack((xv[1:], xd[1:]))
        yt = np.vstack((yv[1:], yd[1:]))

        lineD.set_data(xd, yd)
        lineV.set_data(xv, yv)
        #for t in range(ncomp):
        #    lineT[t].set_data(xt[:, t], yt[:, t])
        t = -1
        lineT[t].set_data(xt[:, t], yt[:, t])
        time_text.set_text(time_template.format(i*dT, "GOAL!" if goalreached else ""))
        return unpack()

    def onClick(event):
        global pause
        global reset
        if event.button == 3:
            reset ^= True
        pause ^= True

    fig.canvas.mpl_connect('button_press_event', onClick)
    ani = animation.FuncAnimation(fig, animate,
                                #frames=len(y),
                                frames=anistep,
                                init_func=init,
                                interval=25, blit=True,
                                repeat=True)
                                #repeat=False)
    plt.show()



