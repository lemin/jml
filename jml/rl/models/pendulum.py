#!/usr/bin/python 

#### Pendulum Base Model

#import pdb
import math
import random
import numpy as np
from ..base import RLModel
import matplotlib.pyplot as plt
from copy import deepcopy as copy


class CartPole: 

    def getTimeStep(self):  # return in millisecond
        return 1 #int(1000*self.tau)

    def __init__(self, pos, brand=False):
        self.cartMass = 1. #0.2
        self.poleMass = 0.1 #2.0
        self.poleLength = 1 # 5.0
        self.forceMag = 10.0
        self.tau = 0.005 #0.0005 # time step 
        self.fricCart = 0.00005 # 0.5
        self.fricPole = 0.005 #0.4

        self.state = [ pos,0,math.pi,0 ]  
        #self.state = [ pos, 0, 0, 0 ]  
        #self.strng = np.array([[1,10],[-10,10], [-math.pi,math.pi], [-10,10], [-1,1]]).T
        self.strng = np.array([[1,10],[-3.0,3.0], [-math.pi,math.pi], [-8,8], [-1,1]]).T

        # randomly position the stick 
        if brand:
            self.state[2] = math.pi +  math.radians(random.randint(-90,90))
        self.action = 0

        self.totalMass = self.cartMass + self.poleMass 
        self.halfPole = 0.5 * self.poleLength 
        self.poleMassLength = self.halfPole * self.poleMass

    def isBalanced(self, s1=None):
        #if s1==None: s1=self.state # ignore s1
        curdeg = math.degrees(self.state[2])%360
        #print curdeg, self.state
        if abs(curdeg-180) < 60:
            return True
        else: return False

    def getScreenPos(self,start,end):
        unit = (end-start) / 9.
        center = (self.state[0]-1)*unit
        multPole = unit * 2
        return ( center + start, 
                    (self.poleLength*math.sin(self.state[2])*multPole, 
                     self.poleLength*math.cos(self.state[2])*multPole))

    def add(self, x,y): return x+y 

    def getState(self): # 20110516 - to sin-cos
        #return [ self.state[0], self.state[1], math.cos(self.state[2]), math.sin(self.state[2]), self.state[3] ]
        return self.state

    def setAction(self, action):
        self.action = action

    def getAction(self) : return self.action

    def move(self):
        (pos, posDot, angle, angleDot) = self.state

        force = self.forceMag * self.action
        angleDotSq = angleDot * angleDot 
        sinangle = math.sin(angle)
        cosangle = math.cos(angle)
        common = (force + self.poleMassLength * angleDotSq * sinangle - \
                    self.fricCart * math.copysign(1,posDot)) / self.totalMass
        angleDDot = (-9.8 * sinangle - cosangle * common - self.fricPole * angleDot / self.poleMassLength) / \
                        (self.halfPole * (4.0/3.0 - self.poleMass * cosangle * cosangle / self.totalMass))
        posDDot = common - self.poleMassLength * angleDDot * cosangle / self.totalMass

        t = self.tau
        delta = [ posDot*t, posDDot*t, angleDot*t, angleDDot*t ] 
        self.state = map(self.add, self.state, delta) 

        self.state[2] = self.state[2] #% (2*math.pi) - mod make the curve discrete

        #if self.state[2]<0: pdb.set_trace()

        # adjust velocity when outside of the track
        if self.state[0] < 1:
            self.state[:2] = [1, 0]
        elif self.state[0] > 10:
            self.state[:2] = [10, 0]
        # apply clip on angles
        self.state[2] = self.angle_clip(self.state[2])
        self.state[3] = np.clip(self.state[3], self.strng[0, 3], self.strng[1, 3])

    def angle_clip(self, angle):
        if angle > np.pi:
            angle -= 2 * np.pi
        elif angle < -np.pi:
            angle += 2 * np.pi
        return angle


class RlCartPole(RLModel):

    def __init__(self,initpos, brand=False, draw=False):
        RLModel.__init__(self)
        self.cart = CartPole(initpos,brand)
        self.initState = copy(self.cart.state)
        self.initst = self.cart.getState()
        # len(state) + action (1)
        self.nnNI = len(self.initst) + 1
        self.n_state = self.ndim = len(self.initst)
        self.n_action = 1

        if draw:
            from pendulum import BaseWindow
            self.disp = BaseWindow(False)
            self.disp.simscr.cart = self.cart
            self.disp.start()
        else: 
            self.disp = False

        self._actions = np.array([-1, 0, 1])

    def init(self, start=None):
        if start is not None:
            self.cart.state[:] = start[:]
        else:
            self.cart.state[:] = self.initState[:]
        if self.disp: self.disp.draw()
        return self.cart.getState()
       
    def get_random_action(self):
        return float(np.random.randint(3) -1) # discrete action

    def get_action_index(self, action):
        return np.where(self._actions == action)[0][0]

    def get_bound_act(self, a):
        if a[0]>1: return 1
        elif a[0]<-1: return -1
        else: return a[0]

    def next(self,s,a) :
        self.cart.setAction(a) 
        self.cart.move()
        if self.disp: self.disp.draw()
        return self.cart.getState()

    def get_reward(self,s,s1,a):
        # failure -1
        #if self.cart.state[0]<= 1 or self.cart.state[0] >= 10:
        if s1[0]<= 1 or s1[0] >= 10:
            return 0
        #elif self.cart.isBalanced():
        elif self.cart.isBalanced(s1):
            return 1
        else :
            return 0

    def get_state_range(self):
        return self.cart.strng
        #return np.array([[1,10],[-10,10], [0,2*math.pi], [-10,10], [-1,1]]).T

    def get_actions(self):
        return self._actions
        #return [-1, -0.5, 0, 0.5, 1]

    def draw_trajectory(self, smplX):
        if not hasattr(self, '_traj1'):
            self._traj1 = []
            self._traj2 = []
        self._traj1.extend(smplX[:, 0])
        self._traj2.extend(smplX[:, 2])
        plt.plot(self._traj1, 'r-')
        plt.plot(self._traj2, 'b-')
        plt.ylabel("angle and pos") 
        """
        plt.plot(smplX[:,0],smplX[:,1],'b-')
        plt.plot(smplX[:,2]%(2*math.pi),smplX[:,3],'r-')
        plt.axis([0,11,-10,10])
        plt.xlabel("s") 
        plt.ylabel("s dot")
        """

    def plot_Q(self, rlframe, n_plots=-1):
        posx = np.linspace(0, 10, 11)
        posy = np.linspace(0, 2*math.pi, 11)
        xs, ys = np.meshgrid(posx, posy)
        zeros = np.zeros(xs.size)
        actions = self.get_actions()
        n_act = len(actions)

        i = 1
        if n_plots == 1:
            actions = [actions[len(actions)/2]]
        for a in actions:
            if n_plots > 1:
                plt.subplot(1, n_act, i)
            a_col = np.ones(xs.size) * a
            states = np.vstack((xs.flat, zeros, ys.flat, zeros, a_col)).T
            rlframe.draw_Qplot(xs, ys, states)
            plt.xlabel("s")
            plt.ylabel("angle")
            plt.title("action: " + str(a))
            i += 1
        plt.suptitle("Max Q")
