import numpy as np
import random
from ..base import RLModel
import matplotlib.pyplot as plt


class nXOR(RLModel):

    ndim = 1
    State= None

    def __init__(self, nDim):
        RLModel.__init__(self)
        self.n_state = self.ndim = nDim
        self.n_action = 1
        self.State = np.array([[0,1]]) if self.ndim ==1 else np.hstack((np.random.uniform(0,0.1,nDim),np.random.uniform(1,0.1,nDim)))
        self.nnNI = nDim+1
        self._actions = np.array([0, 1])

    def init(self, start=None):
        if start is not None:
            return start
        return self.State[:,0]

    def get_random_action(self):
        return float(random.randint(0,1)) #np.random.randint(2)

    def next(self,s,a) :
        if a == 0:
            return s
        else :
            if s==self.State[:,0]:
                return self.State[:,1]
            else :
                return self.State[:,0]

    def get_reward(self,s,s1,a):
        return 1 if s1 == self.State[:,1] else 0

    def get_state_range(self):
        #return np.kron(np.ones((1,self.ndim+1)), np.array([[0,1]]).T) 
        return np.tile(np.array([[0,1]]).T, (1,self.ndim+1))

    def get_actions(self):
        return self._actions

    def get_action_index(self, action):
        return np.where(self._actions == action)[0][0]

    def plot_Q(self, rlframe, n_plots=-1):
        xpos = np.array([0,1])
        acts = np.array([0,1])
        xs, ys = np.meshgrid(xpos,acts)
        states = np.vstack((xs.flat,ys.flat)).T
        rlframe.draw_Qplot(xs, ys, states)
        plt.xlabel("s")
        plt.ylabel("a")
        plt.title(self.Qtitle(rlframe))
