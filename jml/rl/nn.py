""" Reinforcement learning experimental framework with Neural Networks

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

"""
import numpy as np
#import random
from ..regress.nnetq import NeuralNetQ
from base import RLFrame
import matplotlib.pyplot as plt


class RLtrainNN(RLFrame):
    """ Reinforcement learning framework with Neural Network
    """

    def make(self, *args, **params):
        """ create neural net function approximation 
            
            parameters
            ----------
            n_units     the number of units (list)
                        input, hidden units (one or more), and output
        """
        if len(args) != 1:
            raise TypeError("make(): wrong number of arguments")
        n_units = args[0]
        self.n_outputs = n_units[-1]

        strange = params.pop('strange', True)
        if strange:
            stRange = self.model.get_state_range()
        else:
            stRange = None
        self._func = NeuralNetQ(n_units, stRange)

    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.setHunit(args[0])

    def train(self, N=None, **rlparams):
        """
            parameters
            ----------
            Lambda      float
                        regularization coefficient
            explore     boolean
                        to check the mean, std, min and max of data
                        * when make, strange should be set to False
            Gamma       float
                        discounting factor
            nReps       int
                        the number of episodes to run
            nSteps      int
                        the number of steps for each episode
            optim       String
                        optimization option
            nIterations int
                        max. number of iterations for scg
            wPrecision  int
                        min. weight precision to stop scg
            fPrecision  int
                        min. function error precision to stop scg
            showplot    boolean
                        show online plot of learning
            logact      boolean
                        an option for storing actions played
            disc_act_learning   boolean
                        multiple Q output learning for each discrete action 
            n_batch     int
                        the number of batch training episodes
        """
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, n_batch = \
                    RLFrame.train(self, N, **rlparams)
               

    ######################################################################

        # for NN regularization param
        lmbd = rlparams.pop("Lambda", 0.0)

        # check value range
        explore = rlparams.pop("explore", False)

        # optimization option
        optim = rlparams.pop('optim', 'scg')

        # SCG params
        niter= rlparams.pop("nIterations",1000)
        wprecision = rlparams.pop("wPrecision",1e-10)
        fprecision = rlparams.pop("fPrecision",1e-10)

        # log action
        blog_act = rlparams.pop("logact", False)

        # experience replay
        exp_replay = rlparams.pop('expReplay', 0)

        ### Variables for plotting later
        ftrace = []
        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': []}
        #rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        self.search_start = -1
     
        # for sigma update
        #n_old = 0
        # debug: print Q
        #print self._func.use(np.array([[0, 100, 0,0]]))


        #f = open('test.txt','a+') 
        if exp_replay > 0:
            replay_X = None  # exp replay
            replay_R = None  # exp replay
            replay_Qn = None  # exp replay
            replay_Q = None  # exp replay
            replay_A = None  # exp replay
            replay_AI = None  # exp replay

        for reps  in xrange(nReps):

            batch_container = {}
            steps = []
            for i_batch in xrange(n_batch):
                ## Collect N[reps] samples, each being s, a, r, s1, a1

                # exp replay
                if exp_replay > 0 and reps % exp_replay == 0 and\
                   replay_R is not None and i_batch == 0:
                    smplX, smplR, smplQn = replay_X, replay_R, replay_Qn
                    smplQ, smplA, smplAI = replay_Q, replay_A, replay_AI
                    #print "[", reps, "] replaying...", np.sum(smplR)
                else:
                    smplX, smplR, smplQn, smplQ, smplA, smplAI, orgX =\
                        self.get_samples(N[reps], epsilon)
                    if self.disc_action_learning:
                        smplQ = smplQ[xrange(len(smplR)), smplAI.flat]

                    # chk non-zero rewards  # only for boat ##REMOVE ME 
                    #if np.sum(smplR[:-1]) != 0:
                    #    sum_R = np.sum(smplR)
                    #    if sum_R < -10: sum_R = -10
                    #    smplR[-1] = sum_R
                    #    smplR[:-1] = 0
                #rsum.append(np.sum(smplR))
                #rmean.append(np.mean(smplR))
                step = len(smplR)
                steps.append(step)
                    
                if exp_replay > 0 and np.sum(smplR) > 0:
                    if replay_R is None or np.sum(replay_R) <= np.sum(smplR):
                        replay_X, replay_R, replay_Qn = smplX, smplR, smplQn
                        replay_Q, replay_A, replay_AI = smplQ, smplA, smplAI


                if not 'X' in batch_container.keys():
                    batch_container['X'] = smplX
                    batch_container['R'] = smplR 
                    batch_container['Qn'] = smplQn
                    batch_container['Q'] = smplQ
                    batch_container['A'] = smplA
                    batch_container['AI'] = smplAI
                    batch_container['orgX'] = orgX
                else:
                    batch_container['X'] = np.vstack((batch_container['X'], smplX))
                    batch_container['R'] = np.vstack((batch_container['R'], smplR))
                    batch_container['Qn'] = np.vstack((batch_container['Qn'], smplQn))
                    batch_container['Q'] = np.vstack((batch_container['Q'], smplQ))
                    batch_container['A'] = np.vstack((batch_container['A'], smplA))
                    batch_container['AI'] = np.vstack((batch_container['AI'], smplAI))
                    batch_container['orgX'] = np.vstack((batch_container['orgX'], orgX))

                if i_batch == n_batch - 1:
                    smplX = batch_container['X']
                    smplR = batch_container['R']
                    smplQn = batch_container['Qn']
                    smplQ = batch_container['Q']
                    smplA = batch_container['A']
                    smplAI = batch_container['AI']
                    orgX = batch_container['orgX']
                    step = np.sum(steps)
                    
                rtrace['sum'].append(np.sum(smplR))
                rtrace['mean'].append(np.mean(smplR))
                rtrace['steps'].append(step)

            if self.disc_action_learning:
                n_a = self.model.get_action_len()
                if n_a > 0:
                    smplX = smplX[:, :-n_a]
            else:
                smplAI = None

            ## Update the Q neural network.
            self._func.train(smplX, smplQ, smplR, smplQn, smplAI,
                             niter=niter, Lambda=lmbd,
                             gamma=gamma, et_lambda=et_lambda,
                             ftracep=True, explore=explore,
                             optim=optim,
                             algo=algo,
                             wprecision=wprecision, fprecision=fprecision)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            #rtrace['sum'].append(np.mean(rsum)) #np.sum(smplR))
            #rtrace['mean'].append(rmean) #np.mean(smplR))
            #rtrace['steps'].append(np.mean(steps)) #len(smplR))

            self.model.search_act_store(smplR)

            if blog_act:
                rtrace['act'].append(smplA)

            epsilontrace.append(epsilon)
            if self._func:
                ftrace = ftrace + self._func.ftrace

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, orgX[:step+1, :],
                                 extonlineplot, n_extonlineplot, ftrace)

            if self.debug > 0 and reps % self.debug == self.debug-1:
                self.model.debug_progress(self, reps)

            if self.search_act == False and\
                self.search_act_cond == 'n-thres' and\
                self.model.check_search_cond(rtrace['mean']): #[(reps-5):]):
                if (self.search_act_stop):
                    return (rtrace, epsilontrace, ftrace)

                self.set_search_action(True, False)
                if self.search_start == -1:
                    self.search_start = reps

        #f.close()
        if bplot: plt.ioff()
        return (rtrace, epsilontrace, ftrace)
