""" online sparse Gaussian Process Temporal Difference learning

    online GPTD add input x_t only when feature space phi(x) cannot be approximated
    sufficiently well with combination of the images of exsiting states
                        by lemin

    refrences
    ---------
    [1] Yaakov Engel, Shie Mannor, and Ron Meir
        Bayesian Reinforcement Learning with Gaussian Process
        Temporal Difference Methods
        Journal of Machine Learning Research 2007
"""
import numpy as np
from base import RLFrame, convert_to_npmatrix
import matplotlib.pyplot as plt
#from ..svm.svmdata import SVMData
from jml.svm.svmdata import SVMData
from ..util.exp import Standardizer
import random
from copy import deepcopy as copy


class oGptdRegr():
    def __init__(self, **params):
        # kernel option parameters handling
        # default: Gaussian Kernel
        #self._kernel = params.pop('kernel', {'kernel': 'rbf', 'gamma': 1.})
        self.data = SVMData(None)
        self.data.set_kernel(params.pop('kernel', 'rbf'), **params)

        # init parameters
        self.init_params()

        stateRange = params.pop('stateRange', None)
        if stateRange is not None:
            self.stdX = Standardizer(stateRange)

    def init_params(self):
        # need to fill in the first run!
        self.D = []
        self.K_inv = None

        self.a = np.ones((1, 1))
        self.alpha = np.zeros((1, 1))
        self.C = np.zeros((1, 1))
        self.c = np.zeros((1, 1))
        self.d = 0.
        self.s = np.inf

    def init_kernel(self, x0):
        if len(self.D) > 0: 
            return
        if self.stdX:
            x0 = self.stdX.standardize(x0)
        self.add_data(x0)
        self.K_inv = self.data.kernelf(x0)

    def add_data(self, x):
        self.D.append(x)
        self.data.add_data(x, None)

    # update GPTD (recursive MC GPTD)
    def update(self, x, x1, r1, gamma, var, var1, v):
        if self.stdX:
            x = self.stdX.standardize(x)
            x1 = self.stdX.standardize(x1)
        if not isinstance(x, np.ndarray) or len(x.shape) == 1:
            x = np.asarray(x).reshape((1, -1))
        if not isinstance(x1, np.ndarray) or len(x1.shape) == 1:
            x1 = np.asarray(x1).reshape((1, -1))

        kx = self.data.kernelf(x).T
        kx1 = self.data.kernelf(x1).T
        kxx = self.data.kernelf(x, x)
        kxx1 = self.data.kernelf(x, x1).T
        kx1x1 = self.data.kernelf(x1, x1)
        t = self.data.n_samples + 1

        h_t = np.zeros(t)
        if t == 1:
            h_t[0] = 1
        else:
            h_t[-2:] = [1, -gamma]
        h_t = h_t.reshape((-1, 1))

        a_t = np.dot(self.K_inv, kx1)
        delta = kx1x1 - np.dot(kx1.T, a_t)

        dk_t = (kx - gamma * kx1)
        #dk_tt = float(kxx - 2 * gamma * kxx1 + gamma**2 * kx1x1)

        num = gamma * var
        a =  num / self.s 
        a_sq = num**2 / self.s

        self.d = a * self.d + r1 - float(np.dot(dk_t.T, self.alpha))

        #print "delta:", abs(delta), "v:", v
        if abs(delta) > v:
            K = np.zeros((self.K_inv.shape[0]+1, self.K_inv.shape[1]+1))
            K[:-1, :-1] = self.K_inv + np.dot(a_t, a_t.T) / delta
            K[:-1, -1, None] = -a_t / delta
            K[-1, :-1] = -a_t.T / delta
            K[-1, -1] = 1. / delta
            self.K_inv = K

            a_t = np.zeros((len(self.D)+1, 1))  # a_t / a_t-1 = self.a
            a_t[-1, 0] = 1
            h_t = np.vstack((self.a, -gamma))  # a_t-1
            dk_tt = float(np.dot(self.a.T, (kx - 2 * gamma * kx1))) + gamma**2 * kx1x1  # a_t-1

            c1 = a * np.vstack((self.c, [0.])) + h_t -\
                    np.vstack((np.dot(self.C, dk_t), [0.]))
            self.s = var + gamma ** 2 * var1 - a_sq + dk_tt -\
                    float(np.dot(dk_t.T, np.dot(self.C, dk_t))) +\
                    2 * a * np.dot(self.c.T, dk_t)
            self.c = c1
            self.alpha = np.vstack((self.alpha, [0])) # add a term later
            C1 = np.zeros((self.C.shape[0]+1, self.C.shape[1]+1))
            C1[:-1, :-1] = self.C
            self.C = C1 # add a term later

            #add data here!! - CHECK!!!
            #print "x1:", x1
            #print self.data.X
            self.add_data(x1)
            #print self.data.X
            #import pdb; pdb.set_trace()

        else:
            h_t = self.a - gamma * a_t
            dk_tt = np.dot(h_t.T, dk_t)
            c1 = a * self.c + h_t - np.dot(self.C, dk_t)
            self.s = var + gamma**2 * var1 + np.dot(dk_t.T, (c1 + a * self.c)) - a_sq
            self.c = c1

        self.a = a_t
        self.alpha += self.c / self.s * self.d
        self.C += 1./self.s * np.dot(self.c, self.c.T)

        return 

    def use(self, X):
        """ apply learned regression model
        Parameters
        ----------
        X       array, shape=[n_samples, n_dim]
                data set to apply a linear model for prediction

        Return
        ------
        Y       array
                prediction result
        """
        if self.stdX:
            X = self.stdX.standardize(X)
        if not isinstance(X, np.ndarray):
            X = np.asanyarray(X)
        # when nothing is learned
        if self.data.X is None:
            return np.zeros(X.shape[0])
        if isinstance(self.alpha, int):
            return self.data.kernelf(X)
        k = self.data.kernelf(X).T
        #if X.shape[0] == 1:
        #    k = k.flatten().reshape((-1, 1))
        return np.dot(self.alpha.T, k)


class GPTD(RLFrame):

    def __init__(self, model, **params):
        RLFrame.__init__(self, model, **params)
        self.type_fa = 'value'

    def make(self, *args, **params):
        # build a function approximation
        #self._func = oGptdRegr(**params)
        strange = params.pop('strange', True)
        if strange:
            stRange = self.model.get_state_range()[:,:-1]
        else:
            stRange = None
        params['stateRange'] = stRange

        self._G0 = oGptdRegr(**params)
        self._G1 = oGptdRegr(**params)
        """
        self._func = self._actor = GptdRegr(**params)
        self._critic = GptdRegr(**params)
        """

    def opi_greey_action(self, s, gamma, epsilon):
        actions = self.model.get_actions()
        n_actions = len(actions)
        if self._func.data.X is None:
            ai = np.random.randint(n_actions)
            qs = np.asarray([0] * n_actions)
        else:
            qs = []
            for tmp_a in actions:
                s1 = self.model.next(s, tmp_a)
                r1 = self.model.get_reward(s, s1, tmp_a)
                # assume state transition probability is 1 for all
                q = self._func.use(s1)
                if isinstance(q, np.ndarray):
                    q = float(q)
                qs.append(r1 + gamma * q)

            #if epsilon < 0.5:
            #    import pdb; pdb.set_trace()
            
            if random.uniform(0,1) < epsilon:  #epsilon greedy
                ai = np.random.randint(n_actions)
            else:
                #ai = np.argmax(qs)        
                maxq = np.max(qs)
                i_qs = np.where(qs == maxq)[0]
                if len(i_qs) == 0: raise ValueError("invalid Q values")
                ai = int(np.random.choice(i_qs))

        return qs[ai], actions[ai], ai, qs

    def train(self, N=None, **rlparams):
        """
            online updates as collecting samples,
            so it does not use get_samples()

            parameters
            ----------
            Lambda      float
                        regularization coefficient
            Gamma       float
                        discounting factor
            nReps       int
                        the number of episodes to run
            nSteps      int
                        the number of steps for each episode
            showplot    boolean
                        show online plot of learning
            logact      boolean
                        an option for storing actions played
            qlearn      boolean
                        an option for qlearn/sarsa
        """

        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, n_batch = \
                    RLFrame.train(self, N, **rlparams)

        ######################################################################

        # log action
        blog_act = rlparams.pop("logact", False)
        bqlearn = rlparams.pop('qlearn', False)
        # dV variance to be constant (no difference in other)
        self.sigma = rlparams.pop('sigma', 1.)
        # accuracy thershold - relevant to addition of data to kernel (lower, more data)
        v = rlparams.pop('accu_thresh', .1)
        # convergence threshold - when two FAs are close each other, it stops learning
        eta = rlparams.pop('conv_thresh', 1e-4)
            
        ftrace = []
        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': []}
        epsilontrace = []

        #nReps = 10000  # dbg
        for reps  in xrange(nReps):
            #print "Episode #", reps+1
            N_reps = N[reps]
            #N_reps = 10000  # dbg

            done = False
            ## Collect N_reps samples, each being s, a, r, s1, a1
            na = self.model.get_action_len()

            s = self.model.init()

            if reps == 0:
                self._G0.init_kernel(s)
            self._func = self._G0
            #q, a, ai, qs = self.epsilon_greedy(s, epsilon, qlearn=bqlearn)
            q, a, ai, qs = self.opi_greey_action(s, gamma, epsilon)

            ns = len(self.model.transform_state(s))
            X = np.zeros((N_reps, ns+na))
            X1 = np.zeros((N_reps, ns+na))
            R = np.zeros((N_reps, 1))
            A = np.zeros((N_reps, na))
            Q = np.zeros((N_reps, 1))
            Qn = np.zeros((N_reps, 1))

            #self._func.init_params()

            #turn = True  # True for critic learning, Faluse for actor
            steps = []
            step_cnt = 0
            
            for step in xrange(N_reps):

                if reps == 0 and step == 1:
                    self._G1.init_kernel(s)
                # collect a sample
                s1 = self.model.next(s, a)
                r1 = self.model.get_reward(s, s1, a)
                self._func = self._G0 if step % 2 == 0 else self._G1
                #q1, a1, ai1, qs = self.epsilon_greedy(s1, epsilon,
                #                                      qlearn=bqlearn)
                # OPI action selection
                q1, a1, ai1, qs = self.opi_greey_action(s1, gamma, epsilon)

                # update GPTD (recursive MC GPTD)  - CRITIC
                # two GPTD - how does critic affect actor?
                """
                if turn:
                    self._critic.update(s, s1, r1, gamma)
                    self._actor.D.append(s1)
                else:
                    self._actor.update(s, s1, r1, gamma)
                    self._actor.D.append(s1)
                """
                #self._func.update(s, s1, r1, gamma, self.sigma**2, self.sigma**2, v)
                if not done:
                    if step % 2 == 0:
                        self._G0.update(s, s1, r1, gamma, self.sigma**2, self.sigma**2, v)
                        g0 = self._G0.use(s1)
                    else:
                        self._G1.update(s, s1, r1, gamma, self.sigma**2, self.sigma**2, v)
                        g1 = self._G1.use(s1)

                # actor update - how to define probability of continuous state / actions?
                # OPI ? based on value and model, choose an action that maximizes
                # compute improvement from value approximation 

                # J_k+1 = T_mu^m J_k 

                """
                actions = self.model.get_actions()
                qs = []
                for tmp_a in actions:
                    s_n = self.model.next(s1, tmp_a)
                    # assume state transition probability is 1 for all
                    qs.append(r1 + gamma * self._func.use(s_n))
                    
                a1 = actions[np.argmax(qs)]
                """

                # trace
                if bqlearn:
                    maxq = np.max(qs)
                    TDerr = r1 + gamma * et_lambda * maxq - q
                else:
                    TDerr = r1 + gamma * et_lambda * q1 - q
                ftrace.append(TDerr)

                x = np.hstack((convert_to_npmatrix(
                                    self.model.transform_state(s)),
                                convert_to_npmatrix(
                                    self.model.transform_action(a))))

                x1 = np.hstack((convert_to_npmatrix(
                                    self.model.transform_state(s1)),
                                convert_to_npmatrix(
                                    self.model.transform_action(a1))))
                X[step,:], X1[step,:] = x[:, :], x1[:,:]
                R[step, 0], A[step, :] = r1, a
                Q[step, 0], Qn[step, 0] = q, q1

                step_cnt += 1
 
                if self.model.check_early_stop(s1, r1):
                    steps.append(step_cnt)
                    step_cnt = 0

                    s = self.model.init()
                    q1, a1, ai1, qs = self.opi_greey_action(s1, gamma, epsilon)
                    x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                   convert_to_npmatrix(self.model.transform_action(a))))
                    #break
                else:    
                    # next state
                    s, a, ai = s1, a1, ai1

                # check convergence
                #if reps > 0 and step > 50 and abs(g0 - g1) < eta:
                if reps > 0 and step > 10 and abs(g0 - g1) < eta:
                    done = True

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                           epsilonRate, epsilons, reps)

            epsilontrace.append(epsilon)
            rtrace['sum'].append(np.sum(R[:(step+1)]))
            rtrace['mean'].append(np.mean(R[:(step+1)]))
            if len(steps) == 0:
                steps.append(step+1)
            rtrace['steps'].append(np.mean(steps))
            if blog_act:
                rtrace['act'].append(A[:(step+1)])

            if (bplot and
                (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, X[:step+1, :-1],
                                 extonlineplot, n_extonlineplot, ftrace)

            #print "# basis:", self._func.data.X.shape[0]

            #if done: break

        if bplot: plt.ioff()

        return (rtrace, epsilontrace, ftrace)


if __name__ == '__main__':

    
    import sys
    gamma = float(sys.argv[1])
    sigma = float(sys.argv[2])
    eps_opt = sys.argv[3]
    eps = float(sys.argv[4])

    test_params = {'nReps':  500,
                   'nSteps': 500,
                   'Gamma':  0.9,
                   'et_lambda': 1,
                   "epsilonType": "fixed",
                   'finalEpsilon': 0.01,
                   'fixedEpsilon': 0.5,

                   'conv_threshold': 1e-4,
                   'sigma': sigma,

                   'showplot': False, #True,
                   'plot_interval': 10,
                   }

    test_params['epsilonType'] = eps_opt
    if eps_opt == 'fixed':
        test_params['fixedEpsilon'] = eps
    elif eps_opt == 'fixed':
        test_params['finalEpsilon'] = eps

    print test_params

    # test with marble
    from jml.rl.models.marble import Marble
    model = Marble()
    rlframe = GPTD(model)
    rlframe.make(kernel='rbf', gamma=gamma)
    rtrace, epstrace, ftrace = rlframe.train(**test_params)

    import jml.util.anal as anal
    fn = "ogptd_test" + "_v" + str(v) + "_gamma" + str(gamma) + "_sigma" + str(sigma) + "_eps-" + eps_opt + str(eps)
    anal.save(fn, note="1ist GPTD test with marble",
              params=test_params,
              rtrace=rtrace)
    # Turn this in to Actor-Critic with value function approximation not Q
