""" Reinforcement learning experimental framework

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

    late modified: 11/27/11
        - modified RLtrainSVR to use AOSVRQ

    history:
        11/27/11
        - modified RLtrainSVR to use AOSVRQ

        10/02/2011
        - 20111001 modification tested

        - 10/01/2011
          imported modified RGD
          modified RLtrain to RLFrame and other function approx to
          inherit this class (RLtrain can be aliased for backward compat)
"""
import numpy as np
import random 
import matplotlib.pyplot as plt
from neuralnet import NeuralNet
from RBFtile import RGD, RGDBatch
#from svr import SVRQ
from aosvr import AOSVRQ
from svmdata import SVMData
from skSVR import skSVR


def convert_to_npmatrix(a):
    am = np.array(a)
    if am.ndim==1: am.shape = (1,len(am))
    elif am.ndim==0: am.shape = (1,1)
    return am


class RLModel:
    """ virtual class for reinforce learning agent model """
    nnNI=0

    def __init__(self):
        pass
    def init(self):
        pass
    def getRandomAction(self):
        pass
    def getActionLen(self):
        return 1
    def getApproxAct(self,a):
        return a
    def next(self,s,a) :
        pass
    def getReward(self,s,s1,a):
        pass
    def getStateRange(self):
        pass
    def getActions(self):
        pass
    def drawTrajectory(self, smplX):
        pass


class RLFrame:
    """ Reinforcement learning framework
        
        Parameters
        ----------

        model:  RLModel
            The reinforcement learning problem to perform an experiment

        _func: 
            Function approximation class that has train() and use()

        _updact_wpre: float
            The parameter for searching action, wprecision

        _updact_fpre: float
            The parameter for searching action, fprecision

        _updact_fpre: int
            The parameter for searching action, the number of iteration

        Methods
        -------

        Attributes
        ----------

        Notes
        -----
    """

    def __init__(self, model, **params):
        self.model = model 
        self.set_search_action(False, **params)
        self._func = None
    
    def set_search_action(self, enable=True, **params):
        """ set search action parameters. """

        self.search_act = enable
        self._updact_wpre = params.pop('wpre', 1e-8)
        self._updact_fpre = params.pop('wpre', 1e-8)
        self._updact_niter= params.pop('niter', 10000)

    ######################################################################
    # Interface for sample collection and epsilon policy handling
    ######################################################################

    def get_eps_policy(self, nReps, **rlparams):
        """ return epsilon policy based on parameters. """

        epsilonType = rlparams.pop("epsilonType", 'expDecrease') 
        epsilonSlope = rlparams.pop("epsilonSlope", 6)
        epsilonStart = rlparams.pop("epsilonStart", 0)
        epsilonEnd = rlparams.pop("epsilonEnd", -1)
        fixedEpsilon = rlparams.pop("fixedEpsilon", 0.5)
        finalEpsilon = rlparams.pop("finalEpsilon", 0.001)
        epsilons = None
        epsilon = 1
        epsilonRate = 1

        if epsilonType == 'fixed': 
            epsilon = fixedEpsilon
        elif epsilonType == '-tanh':
            if epsilonEnd < 0 or epsilonEnd > nReps: 
                epsilonEnd = nReps
            sigmoidRange = epsilonEnd - epsilonStart

            step= (2. * epsilonSlope + 1.) / sigmoidRange
            epsx = list(np.arange(-epsilonSlope, epsilonSlope+step, step))
            epsilons = [1] * epsilonStart + list((-np.tanh(epsx) + 1) / 2)\
                        + [0] * (nReps - epsilonEnd)
        else: # 'expDecrease'
            epsilonRate = np.exp(np.log(finalEpsilon) / nReps)
            #print "epsilon decay rate = %d" % epsilonRate

        return (epsilonType, epsilonRate, epsilons, epsilon)

    def get_cur_epsilon(self, eps, epsType, epsRate, epsilons, reps):
        """ return computed current epsilon """

        if epsType=='expDecrease': 
            return eps * epsRate
        elif epsType=='-tanh':
            return  epsilons[reps]
        elif epsType =='fixed':
            return eps

    def epsilonGreedy(self, s, epsilon):
        """ apply epsilon greedy with given epsilon
            searching action using backpropagation is executed
            if configured
        """

        a = self.model.getRandomAction()
        am = convert_to_npmatrix(a)
        sm = convert_to_npmatrix(s)

        if random.uniform(0,1) < epsilon :
            Q = self._func.use(np.hstack((sm,am)))[0,0]
        else:
            #if self.__bSearchAct and epsilon < 0.3: # start cont search when 
            # Another Parameter to search when is good point to switch to finer control?
            # TRADE-OFF : speed-up vs rewards
            if self.search_act:
                # search the best action 
                Q = None
                #for i in xrange(3): # test three times with different random start pos
                if True: # to shorten time - run once
                    a = self._func.updateAction(
                                    sm,
                                    convert_to_npmatrix(self.model.getRandomAction()),
                                    self._updact_wpre,
                                    self._updact_fpre,
                                    self._updact_niter)
                    a = self.model.getApproxAct(a)
                    am = convert_to_npmatrix(a)
                    q = self._func.use(np.hstack((sm,am)))
                    # TODO: CHECK discretization works better??? 
                    #   GUESS: depending on the TASK: rough control works better for simple problem?
                    if Q==None or q > Q:
                        Q = q 
                        reta= a
                a = reta 
            else:  # remove loop 20110107
                actions = self.model.getActions()
                inputs = np.hstack((np.tile(s,(len(actions),1)),np.array([actions]).T))
                qs = self._func.use(inputs)
                i = np.argmax(qs)
                a = actions[i]
                Q = qs[i,0]

        return (Q, a)

    def getSamples(self, numSamples, epsilon):
        """ collecting samples by running the model with epsilong greedy. """

        X = np.zeros((numSamples, self.model.nnNI))
        R = np.zeros((numSamples, 1))
        Q = np.zeros((numSamples, 1))
        Y = np.zeros((numSamples, 1))
        A = np.zeros((numSamples, self.model.getActionLen()))

        s = self.model.init()
        q, a = self.epsilonGreedy(s, epsilon)

        for step in range(numSamples):
            s1 = self.model.next(s, a) 
            r1 = self.model.getReward(s, s1, a)
            q1, a1 = self.epsilonGreedy(s1, epsilon)

            # Collect
            X[step, 0:len(s)] = s
            X[step, len(s):] = a
            R[step, 0], Q[step, 0],Y[step, 0] = r1, q1, q
            A[step, :] = a
            
            # next state
            s, a, q = s1, a1, q1

        return (X, R, Q, Y, A)

    ######################################################################
    # Function approximation management
    ######################################################################

    def make(self, *args, **params):
        """ interface for creating function approximation """
        pass

    def pretrain(self, *args, **params):
        """ interface for pre-trainingfunction approximation """
        pass

    ######################################################################
    # train reinforcment learning
    ######################################################################

    def train(self, N=None, **params):
        """ interface for training function approximation """
        pass

    ######################################################################
    # plotting functions
    ######################################################################

    def online_plot(self, rtrace, epsilontrace, smplX, ftrace=None):
        plt.figure(1)
        if ftrace is None:
            npltRow=1
            npltCol=3
        else:
            npltRow=2
            npltCol=2
        plotidx = 1
        plt.clf()
        plt.subplot(npltRow,npltCol,plotidx)
        plt.plot(rtrace,'o-') 
        plt.xlabel("# rep") 
        plt.ylabel("rewards")
        plotidx += 1
        plt.subplot(npltRow,npltCol,plotidx)
        plt.plot(epsilontrace)
        plt.xlabel("# rep") 
        plt.ylabel("epsilon")

        if ftrace is not None:
            plotidx += 1
            plt.subplot(npltRow,npltCol,plotidx)
            plt.plot(ftrace)
            plt.xlabel("# rep") 
            plt.ylabel("TD error")

        # searching action plot
        #plt.subplot(npltRow,npltCol,4)
        #plt.plot()
        #plt.xlabel("actions") 
        #plt.ylabel("Q")

        # test trajectory
        plotidx += 1
        plt.subplot(npltRow,npltCol,plotidx)
        self.model.drawTrajectory(smplX)
        #if reps==nReps-1:
        #    plt.show()
        #else:

        plt.draw()        

    def draw_Qplot(self, xs, ys, st_inputs):
        """ draw contour plot of max Q values for state input """

        qs = self._func.use(st_inputs)
        qsmax = np.max(qs,axis=1).reshape(xs.shape)
        cs = plt.contourf(xs, ys, qsmax)
        plt.colorbar(cs)


class RLtrainNN(RLFrame):
    """ Reinforcement learning framework with Neural Network
    """

    def make(self, *args, **params):
        """ create neural net function approximation 
            
            parameters
            ----------
            n_units     the number of units (list)
                        input, hidden units (one or more), and output
        """
        if len(args) != 1:
            raise TypeError("make(): wrong number of arguments")
        n_units = args[0]
        self._func = NeuralNet('TD', n_units)

    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.setHunit(args[0])

    def train(self, N=None, **rlparams):

        # RL params
        lmbd = rlparams.pop("lambda", 0.0)
        gamma = rlparams.pop("gamma", 0.9)
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)

        if N==None: N=[nSteps] * nReps

        # SCG params
        niter= rlparams.pop("nIterations",1000)
        wprecision = rlparams.pop("wPrecision",1e-10)
        fprecision = rlparams.pop("fPrecision",1e-10)

        # plot option
        bplot = rlparams.pop("showplot", False)

        ### Initial epsilon is 1, for fully random action selection
        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)
       
        ### Variables for plotting later
        ftrace = []
        rtrace = []
        epsilontrace = []
        wtrace = []

        if bplot: plt.ion()

        #f = open('test.txt','a+') 
        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX,smplR, smplQ, smplY, smplA = self.getSamples(N[reps],epsilon)

            ## Update the Q neural network.
            if self._func:
                self._func.ftracep=True
                self._func.setTrainParams(nIterations=niter, lmb=lmbd, gamma=gamma, 
                                    wprecision=wprecision, fprecision=fprecision)
                self._func.train(smplX, smplY, smplR, smplQ)
            elif self.__svr:
                ### TODO:: How to make a Kernel for TD and how to conver the data for PyML?
                self.__svr.train()

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)
            if self._func:
                ftrace = ftrace + self._func.ftrace
                wtrace.append(self._func.getWeights()[1])

            #for sx in xrange(10):
            #    s = [sx]
            #    actions = self.model.getActions()
            #    inputs = np.hstack((np.tile(s,(len(actions),1)),np.array([actions]).T))
            #    qs = self._func.use(inputs)
            #    i = np.argmax(qs)
            #    a = actions[i]
            #    Q = qs[i,0]
            #    print ":::",s, qs.T, a
            #print "------------------------------------------"

            if bplot: self.online_plot(rtrace, epsilontrace, smplX, ftrace)

        #f.close()
        if bplot: plt.ioff()
        return (rtrace, epsilontrace, ftrace, wtrace)


class RLtrainRGD(RLFrame):
    """ Reinforcement learning framework with Radial Basis Function
    """
    def make(self, *args, **params):
        """ create restricted gradient descent function approximation 
            
            parameters
            ----------
            n_units     the number of units (list - triplet)
                        input, hidden units (centers), and output
        """
        if len(args) != 1:
            raise TypeError("make(): wrong number of arguments")
        n_units = args[0]
        batch = params.pop('batch', True)
        if batch:
            self._func = RGDBatch(n_units[0], n_units[1], n_units[2], **params)
        else:
            self._func = RGD(n_units[0], n_units[1], n_units[2], **params)

    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.setWeights(args[0])

    def train(self, N=None, **rlparams):

        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)

        if N==None: N=[nSteps]*nReps

        # plot option
        bplot = rlparams.pop("showplot", False)

        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA = self.getSamples(N[reps],epsilon)

            ## Update the Q neural network.
            # DEBUG with standardizing C #########################################
            self._func.train(smplX, smplY, smplR, smplQ, stdC=(reps==0), **rlparams)
            ######################################################################

            if False:
                print np.hstack((smplX, smplR, smplQ, smplY))
                print "----------------------------------------------------------"
                """ debug codes """
                sm = np.arange(11)
                am = np.array([-1,0,1])
                #sm = np.array([0,1])
                #am = np.array([0,1])
                s, a = np.meshgrid(sm,am)

                Q = self._func.use(np.vstack((s.flat,a.flat)).T)
                #print np.hstack((np.vstack((s.flat,a.flat)).T,Q))
                print np.vstack((np.array([np.nan, -1, 0, 1]), 
                                np.hstack((s[0].reshape((11,1)),Q.reshape((11,3))))))
                print "----------------------------------------------------------"
                """"""

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            #print "[",reps,"] ntiles: ", self._func.ntiles
            if bplot: self.online_plot(rtrace, epsilontrace, smplX)

        if bplot: plt.ioff()
        #self.online_plot(rtrace, epsilontrace, smplX)
        #plt.savefig('final.png')

        #print "--------------------------------------"
        #print self._func.W
        #print self._func.C

        return (rtrace, epsilontrace)


class RLtrainSVR(RLFrame):
    """ Reinforcement learning framework with Support Vector Machines

    """
    def make(self, *args, **params):
        if len(args) > 3:
            raise TypeError("make(): wrong number of arguments, pass gamma, C, eps in order")
        elif len(args) == 3:
            self._func = AOSVRQ(gamma=args[0], C=args[1], eps=args[2])
        else:
            self._func = AOSVRQ()

    def pretrain_SVs(self, X, A):
        """ collecting samples by running the model with epsilong greedy. """
        n_samples = SV.shape[0]

        R = np.zeros((n_samples, 1))

        for step in range(n_samples):
            s = X[step]
            a = A[step]

            # test action on model and retrieve reward for Q value estimation
            s1 = self.model.next(s, a) 
            r1 = self.model.getReward(s, s1, a)

            # Collect
            R[step, 0] = r1
            
        return R


    def pretrain(self, *args):
        """ support vector transfer 
            
            transfer the parameters like C, eps, tol
            along with support vectors and theta weights
        """
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        master = args[0]

        # do not transfer weights since it is quite relevant to domain
        # instead, transfer only support vectors,
        # which are considered as meaningful moves
        # for this, set weights to zeros and initiate the sv's as R
        # so, pretrained SV's are set to Remaining Set (R) and 
        # R matrix does not need to be set
        # again do not transfer bias b 
        if True:
            #self._func.theta = np.zeros(len(master.S)) #master.theta[master.S]

            # general approach (SV instance transfer)
            SV = master.data.X[master.S]
            X = SV[:self.model.ndim]  # state
            A = SV[self.model.ndim:]  # action

            R = self.pretrain_SVs(X, A)
            # ignore 2nd arg, assume Q is equivalent to R initially
            self._func.train(X, Q, R, R, **rlparams)
            
        else:
            self._func.theta = master.theta[master.S]
            self._func.b = master.b
            self._func.S = range(len(master.S))
            self._func.Rmat = master.Rmat

            # transfer support vectors
            self._func.data = SVMData(master.data.X[master.S],
                                      master.data.label[master.S])
            self._func.data.set_kernel(master.data.kernel,
                                       gamma=master.data.gamma,
                                       coef=master.data.coef0,
                                       degree=master.data.degree)
        self.tol = master.tol
        self.eps = master.eps
        self.C = master.C



    def train(self, N=None, **rlparams):
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)

        if N==None: N=[nSteps]*nReps

        # plot option
        bplot = rlparams.pop("showplot", False)

        # AOSVR option
        max_sample = rlparams.pop('maxsample', None)

        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA = self.getSamples(N[reps],epsilon)

            ## Update the Q neural network.
            #import time
            #cur = time.time()
            self._func.train(smplX, smplY, smplR, smplQ, 
                                maxsample=max_sample, **rlparams)
            #print time.time() - cur
            #print ">>", self._func.data.n_samples, len(self._func.R)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            if bplot:
                self.online_plot(rtrace, epsilontrace, smplX)

                # qplot for marble test (# SV check)
                plt.figure(2)
                plt.clf()
                xpos = np.linspace(0,10,11)
                acts = np.linspace(-1,1,3)
                xs, ys = np.meshgrid(xpos,acts)
                states = np.vstack((xs.flat,ys.flat)).T
                self.draw_Qplot(xs, ys, states)
                # TODO: plot SV points
                plt.plot(self._func.data.X[self._func.S, 0], self._func.data.X[self._func.S, 1], 'kx')
                plt.xlabel("x")
                plt.ylabel("a")
                plt.xlim([-0.5, 10.5])
                plt.ylim([-1.5, 1.5])
                plt.title("Max Q")
                plt.draw()


        return (rtrace, epsilontrace)

# TODO for transfer analysis
#   check which samples are transfered 
#   and which samples are remained/removed in the end
#   --- how theta values are changed?

#  for transfer in XOR
#   transfering only SV's might be better since theta is predicting the 
#   state changes
#   but by transfering only SV's, it can reduce the possibility of neg. transfer
#   but, how to utilize SV's on RL training is critical point to think about

class RLtrainskSVR(RLFrame):
    """ Reinforcement learning framework with Support Vector Machines

    """
    def make(self, *args, **params):
        if len(args) > 4:
            raise TypeError("make(): wrong number of arguments, pass gamma, C, eps in order")
        elif len(args) == 4:
            self._func = skSVR(rlgamma=args[0], C=args[1], epsilon=args[2], 
                                kernel='rbf', gamma=args[3])
        else:
            self._func = skSVR()

    def train(self, N=None, **rlparams):
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)

        if N==None: N=[nSteps]*nReps

        # plot option
        bplot = rlparams.pop("showplot", False)

        # AOSVR option
        max_sample = rlparams.pop('maxsample', None)

        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA = self.getSamples(N[reps],epsilon)

            ## Update the Q neural network.
            self._func.train(smplX, smplY, smplR, smplQ, 
                                maxsample=max_sample, **rlparams)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            if bplot:
                self.online_plot(rtrace, epsilontrace, smplX)

            """
                # qplot for marble test (# SV check)
                plt.figure(2)
                plt.clf()
                xpos = np.linspace(0,10,11)
                acts = np.linspace(-1,1,3)
                xs, ys = np.meshgrid(xpos,acts)
                states = np.vstack((xs.flat,ys.flat)).T
                self.draw_Qplot(xs, ys, states)
                # TODO: plot SV points
                plt.plot(self._func.data.X[self._func.S, 0], self._func.data.X[self._func.S, 1], 'kx')
                plt.xlabel("x")
                plt.ylabel("a")
                plt.xlim([-0.5, 10.5])
                plt.ylim([-1.5, 1.5])
                plt.title("Max Q")
                plt.draw()
            """


        return (rtrace, epsilontrace)


