""" Reinforcement learning experimental framework with Neural Networks

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

"""
import numpy as np
import random 
import matplotlib.pyplot as plt
from skSVR import skSVR
from base import RLFrame


class RLtrainskSVR(RLFrame):
    """ Reinforcement learning framework with Support Vector Machines

    """
    def make(self, *args, **params):
        if len(args) > 4:
            raise TypeError("make(): wrong number of arguments, pass gamma, C, eps in order")
        elif len(args) == 4:
            self._func = skSVR(rlgamma=args[0], C=args[1], epsilon=args[2], 
                                kernel='rbf', gamma=args[3])
        else:
            self._func = skSVR()

    def train(self, N=None, **rlparams):
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)

        if N==None: N=[nSteps]*nReps

        # plot option
        bplot = rlparams.pop("showplot", False)

        # AOSVR option
        max_sample = rlparams.pop('maxsample', None)

        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA = self.getSamples(N[reps],epsilon)

            ## Update the Q neural network.
            self._func.train(smplX, smplY, smplR, smplQ, 
                                maxsample=max_sample, **rlparams)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            if bplot:
                self.online_plot(rtrace, epsilontrace, smplX)

            """
                # qplot for marble test (# SV check)
                plt.figure(2)
                plt.clf()
                xpos = np.linspace(0,10,11)
                acts = np.linspace(-1,1,3)
                xs, ys = np.meshgrid(xpos,acts)
                states = np.vstack((xs.flat,ys.flat)).T
                self.draw_Qplot(xs, ys, states)
                # TODO: plot SV points
                plt.plot(self._func.data.X[self._func.S, 0], self._func.data.X[self._func.S, 1], 'kx')
                plt.xlabel("x")
                plt.ylabel("a")
                plt.xlim([-0.5, 10.5])
                plt.ylim([-1.5, 1.5])
                plt.title("Max Q")
                plt.draw()
            """


        return (rtrace, epsilontrace)


