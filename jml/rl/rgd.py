""" Reinforcement learning experimental framework with RGD

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       [1] A. da Motta Salles Barreto, C.W. Anderson
           Restricted Gradient-Descent Algorithm for Value-Function Approximation in Reinforcement Learning
           Artificial Intelligence, 2007

                            by lemin (Minwoo Jake Lee)

    ### NOT COMPLETE: need more test!!! ######################################

"""
import numpy as np
import matplotlib.pyplot as plt
from ..regress.rbf import RGD, RGDBatch
from base import RLFrame


class RLtrainRGD(RLFrame):
    """ Reinforcement learning framework with Radial Basis Function
    """
    def make(self, *args, **params):
        """ create restricted gradient descent function approximation 
            
            parameters
            ----------
            n_units     the number of units (list - triplet)
                        input, hidden units (centers), and output
        """
        if len(args) != 1:
            raise TypeError("make(): wrong number of arguments")
        n_units = args[0]
        batch = params.pop('batch', True)
        if batch:
            self._func = RGDBatch(n_units[0], n_units[1], n_units[2], **params)
        else:
            self._func = RGD(n_units[0], n_units[1], n_units[2], **params)

    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.setWeights(args[0])

    def train(self, N=None, **rlparams):
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, _ = \
                    RLFrame.train(self, N, **rlparams)
        # TODO: gamma has been passed now. update aosvr interface

        rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA, smplAI, orgX =\
                self.get_samples(N[reps],epsilon)

            if self.disc_action_learning:
                smplQ = smplQ[xrange(len(smplR)), smplAI.flat]

            ## Update the Q neural network.
            # DEBUG with standardizing C #########################################
            self._func.train(smplX, smplY, smplR, smplQ, stdC=(reps==0), **rlparams)
            ######################################################################

            if False:
                print np.hstack((smplX, smplR, smplQ, smplY))
                print "----------------------------------------------------------"
                """ debug codes """
                sm = np.arange(11)
                am = np.array([-1,0,1])
                #sm = np.array([0,1])
                #am = np.array([0,1])
                s, a = np.meshgrid(sm,am)

                Q = self._func.use(np.vstack((s.flat,a.flat)).T)
                #print np.hstack((np.vstack((s.flat,a.flat)).T,Q))
                print np.vstack((np.array([np.nan, -1, 0, 1]), 
                                np.hstack((s[0].reshape((11,1)),Q.reshape((11,3))))))
                print "----------------------------------------------------------"
                """"""

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace.append(np.mean(smplR))
            epsilontrace.append(epsilon)

            #print "[",reps,"] ntiles: ", self._func.ntiles
            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, smplX[:step+1, :],
                                 extonlineplot, n_extonlineplot, ftrace)

        if bplot: plt.ioff()
        #self.online_plot(rtrace, epsilontrace, smplX)
        #plt.savefig('final.png')

        #print "--------------------------------------"
        #print self._func.W
        #print self._func.C

        return (rtrace, epsilontrace)
