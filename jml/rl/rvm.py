""" Reinforcement learning experimental framework with RVM

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, support vector regression, and
    relevant vector machines. 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

    ### NOT COMPLETE: need more test!!! ######################################
"""
import numpy as np
import matplotlib.pyplot as plt
from ..rvm.rvmq import RVMQ
from base import RLFrame
from copy import deepcopy as copy
import collections

debug = False  # options to store trajectories for debugging/analysis

class RLtrainRVM(RLFrame):
    """ Reinforcement learning framework with Support Vector Machines

    """

    def make(self, *args, **params):
        if len(args) > 1:
            raise TypeError("make(): wrong number of arguments, pass gamma")

        strange = params.pop('strange', True)
        if strange:
            stRange = self.model.get_state_range()
        else:
            stRange = None

        if len(args) == 1:
            self._func = RVMQ(gamma=args[0], stateRange=stRange)
        else:
            self._func = RVMQ(stateRange=stRange)

# TODO:
#    def pretrain_SVs(self, X, A):
#        """ collecting samples by running the model with epsilong greedy. """
#        n_samples = X.shape[0]
#
#        R = np.zeros((n_samples, 1))
#
#        for step in range(n_samples):
#            s = X[step]
#            a = A[step]
#
#            # test action on model and retrieve reward for Q value estimation
#            s1 = self.model.next(s, a) 
#            r1 = self.model.get_reward(s, s1, a)
#
#            # Collect
#            R[step, 0] = r1
#            
#        return R
#
#
#    def pretrain(self, *args):
#        """ support vector transfer 
#            
#            transfer the parameters like C, eps, tol
#            along with support vectors and theta weights
#        """
#        master = args[0]
#        self.make(master.gamma, master.eps, kernel=master.kernel_opt)
#
#        # do not transfer weights since it is quite relevant to domain
#        # instead, transfer only support vectors,
#        # which are considered as meaningful moves
#        # for this, set weights to zeros and initiate the sv's as R
#        # so, pretrained SV's are set to Remaining Set (R) and 
#        # R matrix does not need to be set
#        # again do not transfer bias b 
#        if True:
#            #self._func.theta = np.zeros(len(master.S)) #master.theta[master.S]
#
#            # general approach (SV instance transfer)
#            SV = np.asarray(master.get_SVs())
#            X = SV[:self.model.ndim]  # state
#            A = SV[self.model.ndim:]  # action
#
#            R = self.pretrain_SVs(X, A)
#            # ignore 2nd arg, assume Q is equivalent to R initially
#            self._func.train(SV, R, R, R)
#            
#        """
#        else:
#            self._func.theta = master.theta[master.S]
#            self._func.b = master.b
#            self._func.S = range(len(master.S))
#            self._func.Rmat = master.Rmat
#
#            # transfer support vectors
#            self._func.data = SVMData(master.data.X[master.S],
#                                      master.data.label[master.S])
#            self._func.data.set_kernel(master.data.kernel,
#                                       gamma=master.data.gamma,
#                                       coef=master.data.coef0,
#                                       degree=master.data.degree)
#        """

    def train(self, N=None, **rlparams):
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, _ = \
                    RLFrame.train(self, N, **rlparams)
        # gamma has been passed now. update aosvr gamma
        self._func.set_gamma(gamma)

        rollback_coeff = rlparams.pop('rollback_coeff', 0.05)
        b_earlyStop = rlparams.pop('earlyStop', False)

        # log action
        blog_act = rlparams.pop("logact", False)

        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': [], 'weights': []}
        epsilontrace = []

        if bplot: plt.ion()

        # multiple dimensional output for discrete action learning
        if self.disc_action_learning:
            self._func.set_target_dim(len(self.model.get_actions()))

        btrace_samples = rlparams.pop('sample_trace', 0)
        if btrace_samples > 0:
            to_store = {'smplX': [],
                        'smplR': [],
                        'smplQ': [],
                        'smplY': [],
                        'smplA': [],
                        'RVi': []}
        btrace_conf = rlparams.pop('param_trace', True)
        if btrace_conf:
            beta_trace = []
            alpha_trace = []
            used_trace = []
            s_trace = []
            q_trace = []

        RV_X = None
        RV_T = None
        init_rvs = None
        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQ, smplY, smplA, smplAI, orgX =\
                self.get_samples(N[reps],epsilon)
            step = len(smplR)

            if RV_X is not None:
                #print smplX.shape
                #smplX = np.vstack((RV_X, smplX))
                #print "after:", smplX.shape
                #smplR = np.vstack((RV_R, smplR))
                #smplQ = np.vstack((RV_Q, smplQ))
                #smplY = np.vstack((RV_Y, smplY))
                n_initRVs = len(RV_X)
                init_rvs = {'X': RV_X,
                            'Y': RV_Y,
                            'R': RV_R,
                            'Q': RV_Q,
                            'alpha': copy(self._func.rvmAlpha),
                            'beta': self._func.rvmBeta,
                            'gamma': copy(self._func.rvmGamma)}


            #import time
            #cur = time.time()
            bkrvm = copy(self._func)
            err = self._func.train(smplX, smplY, smplR, smplQ,
                             smplAI if self.disc_action_learning else None, 
                             rvs=init_rvs, **rlparams)
            maxerr = np.sqrt(np.sum((np.max(smplX) - np.mean(smplX)) ** 2))
            if err > maxerr * rollback_coeff:
                self._func = copy(bkrvm)
                #print "error is ", err, "> ", maxerr*rollback_coeff, "recovering rvm"
            else:
                if init_rvs is not None:
                    rv_idx = np.asarray(self._func.rv_idx)
                    old_i = rv_idx[rv_idx < n_initRVs]
                    new_i = rv_idx[rv_idx >= n_initRVs] - n_initRVs

                    RV_X = np.vstack((RV_X[old_i, :], smplX[new_i, :]))
                    RV_Y = np.vstack((RV_Y[old_i, :], smplY[new_i, :]))
                    RV_R = np.vstack((RV_R[old_i, :], smplR[new_i, :]))
                    RV_Q = np.vstack((RV_Q[old_i, :], smplQ[new_i, :]))
                else:
                    RV_X = copy(smplX[self._func.rv_idx, :])
                    RV_Y = copy(smplY[self._func.rv_idx, :])
                    RV_R = copy(smplR[self._func.rv_idx, :])
                    RV_Q = copy(smplQ[self._func.rv_idx, :])

            if btrace_conf:
                beta_trace.append(self._func._beta)
                alpha_trace.append(self._func._alpha)
                used_trace.append(self._func._used)
                s_trace.append(self._func._s)
                q_trace.append(self._func._q)

            if btrace_samples > 0 and ((reps+1) % btrace_samples == 0):
                to_store['smplX'].append(smplX)
                to_store['smplR'].append(smplR)
                to_store['smplQ'].append(smplQ)
                to_store['smplY'].append(smplY)
                to_store['smplA'].append(smplA)
                to_store['RVi'].append(self._func.rv_idx)

            #print time.time() - cur
            #print ">>", self._func.data.n_samples, len(self._func.R)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace['weights'].append(copy(self._func._mu))  # for fRVMQ weight tracing
            rtrace['sum'].append(np.sum(smplR))
            rtrace['mean'].append(np.mean(smplR))
            if isinstance(self._step_trace, collections.Iterable):
                rtrace['steps'].append(np.mean(self._step_trace))
            else:
                rtrace['steps'].append(len(smplR))
            epsilontrace.append(epsilon)
            if blog_act:
                rtrace['act'].append(smplA)

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, smplX[:step+1, :],
                                 extonlineplot, n_extonlineplot)

        if bplot: plt.ioff()

        if btrace_samples > 0:
            rtrace['samples'] = to_store

        if btrace_conf:
            rtrace['betas'] = beta_trace
            rtrace['alphas'] = alpha_trace
            rtrace['used'] = used_trace
            rtrace['Ss'] = s_trace
            rtrace['Qs'] = q_trace

        return (rtrace, epsilontrace, None)

    def get_RVs(self):
        return self._func.get_RVs()
