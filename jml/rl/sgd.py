""" stochastic gradient descent reinforcement learning experimental framework
    for GQ(GTD, GTD2, TDC) or RVM

    unlike other function approximations, SGD is implemented here
    --not separately in jml.regression 

    TODO: RLsgdGQ and RLsgdRVM can be combined with different upadte call.

    
    references:
       [1] Maei, H. R. (2011)
           Gradient Temporal-Difference Learning Algorithms

                            by lemin (Minwoo Jake Lee)

"""
import numpy as np
from ..regress.nnetq import NeuralNetQ
from ..rvm.rvmq import oRVMQ
from base import RLFrame, convert_to_npmatrix
import matplotlib.pyplot as plt


class RLsgdGQ(RLFrame):
    """ Reinforcement learning framework for Greedy-GQ
            name changed from RLtrainSGD
    """
    def make(self, *args, **params):
        """ create neural net function approximation 
            
            parameters
            ----------
            n_units     the number of units (list)
                        input, hidden units (one or more), and output
        """
        if len(args) != 1:
            raise TypeError("make(): wrong number of arguments")
        n_units = args[0]
        self.n_layers = len(n_units) - 1
        self.n_outputs = n_units[-1]
        self.n_inputs = n_units[0]

        strange = params.pop('strange', True)
        if strange:
            stRange = self.model.get_state_range()
        else:
            stRange = None
        self._func = NeuralNetQ(n_units, stRange)
        
    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.setHunit(args[0])

    def train(self, N=None, **rlparams):
        """
            SGD updates weight as collecting samples,
            so it does not use get_samples()

            parameters
            ----------
            Lambda      float
                        regularization coefficient
            explore     boolean
                        to check the mean, std, min and max of data
                        * when make, strange should be set to False
            Gamma       float
                        discounting factor
            nReps       int
                        the number of episodes to run
            nSteps      int
                        the number of steps for each episode
            optim       String
                        optimization option
            nIterations int
                        max. number of iterations for scg
            wPrecision  int
                        min. weight precision to stop scg
            fPrecision  int
                        min. function error precision to stop scg
            showplot    boolean
                        show online plot of learning
            logact      boolean
                        an option for storing actions played
            disc_act_learning   boolean
                        multiple Q output learning for each discrete action 
            n_batch     int
                        the number of batch training episodes

            alpha       learning rate (step size)
            beta        learning rate (step size for psi)

            algo        algorithm selection (GTD2 / TDC)
        """
        if self.n_layers > 2:
            raise ValueError("too many layers!")

        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, n_batch = \
                    RLFrame.train(self, N, **rlparams)

        ######################################################################

        # log action
        blog_act = rlparams.pop("logact", False)

        # learning rate (stepsize)
        if self.n_layers == 2:
            alpha = rlparams.pop("alpha", [0.1, 0.1])
            beta = rlparams.pop("beta", [0.1, 0.1])
        else:
            alpha = rlparams.pop("alpha", 0.1)
            beta = rlparams.pop("beta", 0.1)

        # experience replay is not possible with SGD
        exp_replay = rlparams.pop('expReplay', 0)

        # update option (GTD2/TDC)
        algo = rlparams.pop('algo', 'GTD2')

        ### Variables for plotting later
        ftrace = []
        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': []}
        #rtrace = []
        epsilontrace = []

        ## trace for tdc 
        cvtrace = None # correction term (v) trace
        cwtrace = None # correction term (w) trace
        gvtrace = None # gradient trace
        gwtrace = None # gradient trace
        tdptrace = [] # TD approx trace

        if bplot: plt.ion()

        if exp_replay > 0:
            replay_X = None  # exp replay
            replay_X1 = None  # exp replay
            replay_R = None  # exp replay
            replay_Qn = None  # exp replay
            replay_Q = None  # exp replay
            replay_A = None  # exp replay

        if self.n_layers == 2:
            psi_v = np.zeros(self._func._W[0].shape)
            psi_w = np.zeros(self._func._W[1].shape)
            nh = self._func._W[0].shape[1]
        else:
            psi_w = np.zeros(self._func._W[0].shape)
            
        for reps  in xrange(nReps):

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            na = self.model.get_action_len()

            s = self.model.init()
            q, a, ai, qs = self.epsilon_greedy(s, epsilon, qlearn=True)

            ns = len(self.model.transform_state(s))
            X = np.zeros((N[reps], ns+na))
            X1 = np.zeros((N[reps], ns+na))
            R = np.zeros((N[reps], 1))
            A = np.zeros((N[reps], na))
            Q = np.zeros((N[reps], 1))
            Qn = np.zeros((N[reps], 1))

            for step in range(N[reps]):
                if exp_replay > 0 and reps % exp_replay == 0 and\
                   replay_R is not None:
                    x = convert_to_npmatrix(replay_X[step, :])
                    x1 = convert_to_npmatrix(replay_X1[step, :])
                    s, s1 = x[0, :ns], x1[0, :ns]
                    r1, a = replay_R[step, 0], replay_A[step, :]
                    q, q1 = replay_Q[step, 0], replay_Qn[step, 0]
                else:
                    s1 = self.model.next(s, a) 
                    r1 = self.model.get_reward(s, s1, a)
                    q1, a1, ai1, qs = self.epsilon_greedy(s1, epsilon, qlearn=True)

                    x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                   convert_to_npmatrix(self.model.transform_action(a))))
                    x1 = np.hstack((convert_to_npmatrix(self.model.transform_state(s1)), 
                                   convert_to_npmatrix(self.model.transform_action(a1))))
                X[step,:], X1[step,:] = x[:, :], x1[:,:]
                R[step, 0], A[step, :] = r1, a
                Q[step, 0], Qn[step, 0] = q, q1

                #################################################################
                # updating weights
                # importance weighting for Q learning is not considered

                # instead of using train call self._func.train()
                # directly update the weights here

                # create nnetgtd - that utilize silver(2012)
                if self.n_layers == 2:
                    _, Z = self._func.use(x, retZ=True)
                    _, Z1 = self._func.use(x1, retZ=True)

                    Zc = self._func.add_ones(Z[1])
                    Z1c = self._func.add_ones(Z1[1])

                if False: #self._func.stdX:
                    xs = self._func.stdX.standardize(x)
                    xs1 = self._func.stdX.standardize(x1)
                else:
                    xs, xs1 = x, x1
                Xc = self._func.add_ones(xs)
                X1c = self._func.add_ones(xs1)

                maxq = np.max(qs)
                ############ CHECK _OVERFLOW HERE!
                import warnings
                with warnings.catch_warnings(record=True) as warn:
                    TDerr = r1 + gamma * et_lambda * maxq - q
                    ftrace.append(TDerr)

                    if len(warn) == 1:
                        wrnmsg = ''.join(["[",str(step), ",", str(reps),"]",
                                         str(r1), "+", str(gamma), "*",
                                         str(et_lambda), "*", str(q1),
                                         "-", str(q)])
                        warnings.warn(Warning(wrnmsg))
                ###############################################################
                def proj(dwt): return dwt

                if self.n_layers == 2:
                    w = self._func._W[1][1:, :] # linear layer weight

                    dZ = 1 - Z[1] ** 2
                    dZ1 = 1 - Z1[1] ** 2

                    phi_v = np.dot(Xc.T, w.T * dZ)
                    phi_v1 = np.dot(X1c.T, w.T * dZ1)
                    phi_w = Zc.T #np.sum(Zc,0) - single sample = do not need sum
                    phi_w1 = Z1c.T #np.sum(Z1c,0)
                    
                    h_k = np.zeros(psi_v.shape)
                    for vi in xrange(nh):
                        tilde_phi_v = -2 * Z[1][:, vi] * np.dot(Xc, psi_v[:, vi]) * phi_v[:, vi]
                        h_k[:, vi] = (TDerr - np.dot(phi_v[:, vi].T, psi_v[:, vi])) * tilde_phi_v
                    ###  simplify the operation by using flattened vector
                    #tilde_phi_v = -2 * Z * np.dot(X1, psi_v) * phi_v
                    #h_k = (TDerr - np.dot(phi_v.flat, psi_v.flat)) * tilde_phi_v

                    if algo=='GTD2':
                        grad_v = alpha[0] * ((phi_v - gamma * phi_v1) *\
                                    np.dot(phi_v.flat, psi_v.flat) - h_k)
                        grad_w = alpha[1] * ((phi_w - gamma * phi_w1) *\
                                    np.dot(phi_w, psi_w))
                    elif algo=='TDC':
                        grad_v = alpha[0] * (TDerr * phi_v\
                                 - gamma * np.dot(phi_v1, np.dot(phi_v.T, psi_v))\
                                 - h_k)
                        grad_w = alpha[1] * (TDerr * phi_w\
                                 - gamma * np.dot(phi_w1, np.dot(phi_w.T, psi_w)))
                        corr_v = - gamma * np.dot(phi_v1, np.dot(phi_v.T, psi_v))\
                                 - h_k
                        corr_w = - gamma * np.dot(phi_w1, np.dot(phi_w.T, psi_w))
                    v_new = proj(self._func._W[0] +\
                                 grad_v.reshape(self._func._W[0].shape))
                    w_new = proj(self._func._W[1] +\
                                 grad_w.reshape(self._func._W[1].shape))
                    self._func.unpack(np.hstack((v_new.flat, w_new.flat)))

                    if self.btrace:
                        TDgrad_v = TDerr * phi_v
                        TDgrad_w = TDerr * phi_w
                        if cvtrace is None: cvtrace = corr_v.flat
                        else: cvtrace = np.vstack((cvtrace, corr_v.flat))
                        if cwtrace is None: cwtrace = corr_w.flat
                        else: cwtrace = np.vstack((cwtrace, corr_w.flat))
                        if gvtrace is None: gvtrace = TDgrad_v.flat
                        else: gvtrace = np.vstack((gvtrace, TDgrad_v.flat))
                        if gwtrace is None: gwtrace = TDgrad_w.flat
                        else: gwtrace = np.vstack((gwtrace, TDgrad_w.flat))
                        tdptrace.append(np.dot(phi_w.flat, psi_w.flat))

                    for vi in xrange(nh):
                        psi_v[:, vi] = psi_v[:, vi] +\
                                beta[0] * (TDerr - np.dot(phi_v[:, vi].T, psi_v[:, vi])) * phi_v[:, vi]
                    ###  simplify the operation by using flattened vector
                    #psi_v = psi_v + beta_v * (TDerr - np.dot(phi_v.T.flat, psi_v.T.flat)) * phi_v
                    psi_w = psi_w + beta[1] * (TDerr - np.dot(phi_w.T, psi_w)) * phi_w

                else:
                    phi, phin = Xc, X1c

                    if algo=='GTD2':
                        grad_w= alpha * ((phi - gamma * phin) *\
                                              np.dot(phi, psi_w))
                    elif algo=='TDC':
                        grad_w = alpha * (TDerr * phi - gamma * phin *\
                                             np.dot(phi.flat, psi_w.flat))
                        corr_w = - gamma * phin * np.dot(phi.flat, psi_w.flat)
                    w_new = proj(self._func._W[0] + \
                                 grad_w.reshape(self._func._W[0].shape))
                    self._func.unpack(w_new.flat)

                    psi_grad = beta * (TDerr - np.dot(phi.flat, psi_w.flat)) * phi
                    psi_w = psi_w + psi_grad.reshape(psi_w.shape)

                    if self.btrace:
                        if cwtrace is None: cwtrace = corr_w.flat
                        else: cwtrace = np.vstack((cwtrace, corr_w.flat))
                        if gwtrace is None: gwtrace = (TDerr * phi_w).flat
                        else: gwtrace = np.vstack((gwtrace, (TDerr * phi_w).flat))
                        tdptrace.append(np.dot(phi_w.flat, psi_w.flat))


                if self.model.check_early_stop(s1, r1):
                    
                    if self._early_stop:
                        break

                    # restart
                    s = self.model.init()
                    q, a, ai, qs = self.epsilon_greedy(s, epsilon)
                    x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                   convert_to_npmatrix(self.model.transform_action(a))))
                else:
                    # next state
                    s, a, ai = s1, a1, ai1

                # >>>> q is using old value.. not reflecting updated weights
                q = self._func.use(x)

            if exp_replay > 0 and np.sum(R[:step+1,0]) > 0:
                if replay_R is None or np.sum(replay_R) <= np.sum(R[:step+1]):
                    replay_X, replay_X1, replay_R = X[:step+1,:], X1[:step+1,:], R[:step+1,:]
                    replay_Q, replay_Qn, replay_A = Q[:step+1,:], Qn[:step+1,:], A[:step+1,:]

            # after sample collection, decrease stddev for gaussian search
            if self.search_act_gauusian:
                self.model.decrease_action_stddev()

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace['sum'].append(np.sum(R[:(step+1)]))
            rtrace['mean'].append(np.mean(R[:(step+1)]))
            rtrace['steps'].append(len(R[:(step+1)]))

            self.model.search_act_store(R[:(step+1)])

            if blog_act:
                rtrace['act'].append(A[:(step+1)])

            epsilontrace.append(epsilon)

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, X[:step+1, :],
                                 extonlineplot, n_extonlineplot, ftrace)

            if self.debug > 0 and reps % self.debug == self.debug-1:
                self.model.debug_progress(self, reps)

        if bplot: plt.ioff()

        if self.btrace:
            self.trace = {'corrv':cvtrace, 'corrw':cwtrace,
                          'gradv':gvtrace, 'gradw':gwtrace, 'tdp':tdptrace}

        return (rtrace, epsilontrace, None)


class RLtrainSGD(RLsgdGQ):
    """ for backward compatibility"""
    pass


class RLsgdRVM(RLFrame):
    """ Reinforcement learning framework for oRVM-RL
    """

    def make(self, *args, **params):
        """ create neural net function approximation 
            
            parameters
            ----------
            n_units     the number of units (list)
                        input, hidden units (one or more), and output
        """
        if len(args) > 1:
            raise TypeError("make(): wrong number of arguments")

        strange = params.pop('strange', True)
        if strange:
            stRange = self.model.get_state_range()
        else:
            stRange = None

        if self.model.disc_act_learning:
            M = len(self.model.get_actions())
        else:
            M = 1
        if len(args) == 1:
            self._func = oRVMQ(gamma=args[0], stateRange=stRange, targetDim=M)
        else:
            self._func = oRVMQ(stateRange=stRange, targetDim=M)
        
    def train(self, N=None, **rlparams):
        """
            SGD updates weight as collecting samples,
            so it does not use get_samples()

            parameters
            ----------
            Lambda      float
                        regularization coefficient
            explore     boolean
                        to check the mean, std, min and max of data
                        * when make, strange should be set to False
            Gamma       float
                        discounting factor
            nReps       int
                        the number of episodes to run
            nSteps      int
                        the number of steps for each episode
            optim       String
                        optimization option
            nIterations int
                        max. number of iterations for scg
            wPrecision  int
                        min. weight precision to stop scg
            fPrecision  int
                        min. function error precision to stop scg
            showplot    boolean
                        show online plot of learning
            logact      boolean
                        an option for storing actions played
            disc_act_learning   boolean
                        multiple Q output learning for each discrete action 
            n_batch     int
                        the number of batch training episodes

            alpha       learning rate (step size)
            beta        learning rate (step size for psi)

            algo        algorithm selection (GTD2 / TDC)
        """
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, n_batch = \
                    RLFrame.train(self, N, **rlparams)

        ######################################################################

        # log action
        blog_act = rlparams.pop("logact", False)

        # experience replay is not possible with SGD
        exp_replay = rlparams.pop('expReplay', 0)
        uniform_exp_replay = rlparams.pop('uniformExpReplay', 0)  # size of exp replay window
        bqlearn = rlparams.pop('qlearn', False)

        dup_check = rlparams.pop('dupcheck', 0)
        if dup_check > 0 and not 'dup_update' in dir(self._func):
            dup_check = 0

        ### Variables for plotting later
        ftrace = []
        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': []}
        #rtrace = []
        epsilontrace = []

        btrace_conf = rlparams.pop('param_trace', True)
        if btrace_conf:
            beta_trace = []
            alpha_trace = []
            used_trace = []
            s_trace = []
            q_trace = []

        if bplot: plt.ion()

        if exp_replay > 0:
            replay_X = None  # exp replay
            replay_X1 = None  # exp replay
            replay_R = None  # exp replay
            replay_Qn = None  # exp replay
            replay_Q = None  # exp replay
            replay_A = None  # exp replay
            replay_AI = None  # exp replay
        if uniform_exp_replay  > 0:
            ue_replay = []

        n_a = len(self.model.get_actions())
        na = self.model.get_action_len()  #dimension of actions

        if self.disc_action_learning:
            self._func.set_target_dim(n_a)

        for reps  in xrange(nReps):

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            s = self.model.init()
            q, a, ai, qs = self.epsilon_greedy(s, epsilon, qlearn=bqlearn)

            ns = len(self.model.transform_state(s))
            if self.disc_action_learning:
                X = np.zeros((N[reps], ns))
                Q = np.zeros((N[reps], n_a))
                #Qn = np.zeros((N[reps], n_a))
                X1 = np.zeros((N[reps], ns))
            else:
                if self.disc_action_indicator:
                    X = np.zeros((N[reps], ns+n_a))
                    X1 = np.zeros((N[reps], ns+n_a))
                else:
                    X = np.zeros((N[reps], ns+na))
                    X1 = np.zeros((N[reps], ns+na))
                Q = np.zeros((N[reps], 1))
            Qn = np.zeros((N[reps], 1))  # store only on-policy Q (SARSA)
            R = np.zeros((N[reps], 1))
            A = np.zeros((N[reps], na))
            AI = np.zeros((N[reps], 1), int)

            self._func.dup_update = dup_check

            for step in range(N[reps]):
                if exp_replay > 0 and reps % exp_replay == 0 and\
                   replay_R is not None:
                    x = convert_to_npmatrix(replay_X[step, :])
                    x1 = convert_to_npmatrix(replay_X1[step, :])
                    s, s1 = x[0, :ns], x1[0, :ns]
                    r1, a, ai = replay_R[step, 0], replay_A[step, :], replay_AI[step, :]
                    q, q1 = replay_Q[step, 0], replay_Qn[step, 0]
                else:
                    s1 = self.model.next(s, a) 
                    r1 = self.model.get_reward(s, s1, a)
                    q1, a1, ai1, qs1 = self.epsilon_greedy(s1, epsilon, qlearn=bqlearn)

                    if self.disc_action_learning:
                        x = convert_to_npmatrix(self.model.transform_state(s))
                        x1 = convert_to_npmatrix(self.model.transform_state(s1))
                    elif self.disc_action_indicator:
                        tmp = np.zeros(n_a)
                        tmp[ai] = 1
                        x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                       convert_to_npmatrix(tmp)))
                        tmp1 = np.zeros(n_a)
                        tmp1[ai1] = 1
                        x1 = np.hstack((convert_to_npmatrix(self.model.transform_state(s1)), 
                                       convert_to_npmatrix(tmp1)))
                    else:
                        x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                       convert_to_npmatrix(self.model.transform_action(a))))
                        x1 = np.hstack((convert_to_npmatrix(self.model.transform_state(s1)), 
                                       convert_to_npmatrix(self.model.transform_action(a1))))

                X[step,:], X1[step,:] = x[:, :], x1[:,:]
                R[step, 0], A[step, :], AI[step, :] = r1, a, ai
                if self.disc_action_learning:
                    Q[step, :], Qn[step, 0] = qs.flat, q1  #qs1.flat
                    # for q learning
                    #Q[step, :], Qn[step, 0] = qs.flat, max(qs1)  #qs1.flat
                else:
                    Q[step, 0], Qn[step, 0] = q, q1

                # TRY TO STORE ONLY POSITIVE SAMPLES:
                #     DO NOT ADD TO REPLAY BUFFER WHEN target is zero
                if uniform_exp_replay > 0 and not np.allclose(q1 + r1, 0):

                    if len(ue_replay) >= uniform_exp_replay:
                        ue_replay.pop(0)  # remove oldest
                    ue_replay.append([X[step:step+1,:], #Q[step:step+1,:],
                                      R[step:step+1,:], #Qn[step:step+1,:],
                                      AI[step:step+1,:] if self.disc_action_learning else None,
                                      1 if step == N[reps]-1 else 0])
                    
                    n_ue = len(ue_replay)
                    # uniformly pick a sample
                    while True:
                        rand_idx = np.random.randint(n_ue)
                        if ue_replay[rand_idx][-1] == 0:
                            break

                    if rand_idx == n_ue-1:
                        r_q, r_q1 = q, q1
                    else:
                        #train_data = ue_replay[rand_idx]
                        # TODO: update target with current!!!
                        r_q = self._func.use(ue_replay[rand_idx][0])
                        #r_s1 = self.model.next(ue_replay[rand_idx][0][0, :ns],
                        #                       ue_replay[rand_idx][0][0, ns:])
                        #r_q1, _, _, _ = self.epsilon_greedy(r_s1, epsilon, qlearn=bqlearn)
                        r_q1 = self._func.use(ue_replay[rand_idx+1][0])
                    train_data = [ue_replay[rand_idx][0], r_q,
                                  ue_replay[rand_idx][1], r_q1,
                                  ue_replay[rand_idx][2]]
                else:
                    train_data = [X[step:step+1,:], Q[step:step+1,:],
                                  R[step:step+1,:], Qn[step:step+1,:],
                                  AI[step:step+1,:] if self.disc_action_learning else None]
                
                #################################################################
                # online train oRVMQ
                ll = self._func.update(*train_data, **rlparams)  # buffer option
                ftrace.extend(ll)

                if self.model.check_early_stop(s1, r1):
                    if self._early_stop:
                        break

                    # restart
                    s = self.model.init()
                    q, a, ai, qs = self.epsilon_greedy(s, epsilon)
                    x = np.hstack((convert_to_npmatrix(self.model.transform_state(s)), 
                                   convert_to_npmatrix(self.model.transform_action(a))))
                else:
                    # next state
                    s, a, ai = s1, a1, ai1

                # >>>> q is using old value.. not reflecting updated weights
                # MULTIDIM SUPPORT
                if self.disc_action_learning:
                    q = self._func.use(x).flat[ai]
                    qs = self._func.use(x)
                else:
                    q = self._func.use(x)

            if exp_replay > 0 and np.sum(R[:step+1,0]) > 0:
                if replay_R is None or np.sum(replay_R) <= np.sum(R[:step+1]):
                    replay_X, replay_X1, replay_R = X[:step+1,:], X1[:step+1,:], R[:step+1,:]
                    replay_Q, replay_Qn, replay_A = Q[:step+1,:], Qn[:step+1,:], A[:step+1,:]
                    replay_AI = AI[:step+1,:]

            # after sample collection, decrease stddev for gaussian search
            if self.search_act_gauusian:
                self.model.decrease_action_stddev()

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace['sum'].append(np.sum(R[:(step+1)]))
            rtrace['mean'].append(np.mean(R[:(step+1)]))
            rtrace['steps'].append(len(R[:(step+1)]))

            if btrace_conf:
                beta_trace.append(self._func.rvm_beta)
                alpha_trace.append(self._func.rvm_alpha)
                used_trace.append(self._func.rvm_used)
                s_trace.append(self._func.rvm_s)
                q_trace.append(self._func.rvm_q)

            self.model.search_act_store(R[:(step+1)])

            if blog_act:
                rtrace['act'].append(A[:(step+1)])

            epsilontrace.append(epsilon)

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, X[:step+1, :],
                                 extonlineplot, n_extonlineplot, ftrace)

            if self.debug > 0 and reps % self.debug == self.debug-1:
                self.model.debug_progress(self, reps)

        if bplot: plt.ioff()

        if btrace_conf:
            rtrace['betas'] = beta_trace
            rtrace['alphas'] = alpha_trace
            rtrace['used'] = used_trace
            rtrace['Ss'] = s_trace
            rtrace['Qs'] = q_trace

        return (rtrace, epsilontrace, ftrace)

    def get_RVs(self):
        return self._func.get_RVs()
