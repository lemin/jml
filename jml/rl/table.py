""" Reinforcement learning experimental framework with Neural Networks

    With implemented framework, one can run reinforcement learning
    with function approximation of Q(s,a).
    RLModel provides interfaces for general reinforcement learning model.
    One can inherit this class for specific task or experiment.

    Currently supported function approximations are neural networks, 
    radial basis function, and support vector regression (not yet). 
    
    references:
       Sutton and Barto (1998)
         http://webdocs.cs.ualberta.ca/~sutton/book/the-book.html

                            by lemin (Minwoo Jake Lee)

"""
import numpy as np
import matplotlib.pyplot as plt
from base import RLFrame


class TableFun():
    def __init__(self, n_input, actions, initQ=0.):
        self.Q = {}
        self._initQ = initQ
        self._n_input = n_input
        self._n_actions = len(actions)
        self._actions = np.asarray(actions).reshape(self._n_actions, -1)
        self._d_state = self._n_input - self._actions.shape[1]
    
    def train(self, X, Q, R, Qn, A, gamma, alpha):
        #import pdb; pdb.set_trace()
        for step in xrange(len(X)):
            #s = X[step, :-n_a]
            #a = A[step]
            key = tuple(X[step,:])
            if not self.Q.has_key(key):
                self.Q[key] = self._initQ

            self.Q[key] += alpha * (R[step,0] + gamma * Qn[step,0] - self.Q[key])
            #oldkey = key

    def use(self, s):
        if not isinstance(s, np.ndarray):
            s = np.asarray(s)

        keys = None
        n_samples = 1
        if len(s.shape) > 1:
            if s.shape[1] == self._n_input: #s.shape[0] > 1 and 
                keys = [tuple(s[i, :]) for i in xrange(s.shape[0])]
            n_samples = s.shape[0]
        else:
            # check size
            if s.size == self._n_input:
                # transform s to key
                key = tuple(s.flatten())
                if not self.Q.has_key(key):
                    self.Q[key] = self._initQ
                # return Q
                return self.Q[key]
            else:
                s = s.reshape((self._d_state, -1))

        if keys is None:
            inputs = np.hstack((np.repeat(s, self._n_actions, axis=0),
                                np.tile(self._actions, (s.shape[0],1))))
            keys = [tuple(inputs[i, :]) for i in xrange(inputs.shape[0])]

        if keys is not None:
            ret = np.zeros((len(keys), 1))
            for i, k in enumerate(keys):
                if not self.Q.has_key(k):
                    self.Q[k] = self._initQ
                ret[i, 0] = self.Q[k]
            if n_samples > 1:
                ret = ret.reshape((n_samples, -1))
            return ret


class RLtrainTBL(RLFrame):
    """ Reinforcement learning framework with tabular approximation
    """

    def make(self, *args, **params):
        """ create dictionary for tabular function approximation
        """
        init_q = params.pop('initQ', 0)
        self._func = TableFun(self.model.nnNI,
                              self.model.get_actions(),
                              init_q)

    def pretrain(self, *args):
        if len(args) != 1:
            raise TypeError("pretrain(): wrong number of arguments")
        self._func.Q = args[0]

    def train(self, N=None, **rlparams):
        """
            parameters
            ----------
            Gamma       float
                        discounting factor
            Alpha       float
                        learning rate
            nReps       int
                        the number of episodes to run
            nSteps      int
                        the number of steps for each episode
            showplot    boolean
                        show online plot of learning
            logact      boolean
                        an option for storing actions played
        """
        gamma, et_lambda, nReps, nSteps, N, algo,\
        epsilonType, epsilonRate, epsilons, epsilon,\
        bplot, plot_intvl, extonlineplot, n_extonlineplot, _ = \
                    RLFrame.train(self, N, **rlparams)

        self.disc_action_learning = True

        # RL params
        alpha = rlparams.pop("Alpha", .5)

        if N==None: N=[nSteps] * nReps

        # log action
        blog_act = rlparams.pop("logact", False)

        ### Initial epsilon is 1, for fully random action selection
        #epsilonType, epsilonRate, epsilons, epsilon =\
        #            self.get_eps_policy(nReps, **rlparams)
       
        ### Variables for plotting later
        rtrace = {'mean': [], 'sum': [], 'steps': [], 'act': []}
        #rtrace = []
        epsilontrace = []

        if bplot: plt.ion()

        self.search_start = -1
        #f = open('test.txt','a+') 
        for reps  in xrange(nReps):
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplQn, smplQ, smplA, smplAI, orgX =\
                self.get_samples(N[reps], epsilon)
            if self.disc_action_learning:
                smplQ = smplQ[xrange(len(smplR)), smplAI.flat]

            # train Q table 
            self._func.train(smplX, smplQ, smplR, smplQn, smplA,gamma, alpha)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            rtrace['sum'].append(np.sum(smplR))
            rtrace['mean'].append(np.mean(smplR))
            rtrace['steps'].append(len(smplR))

            self.model.search_act_store(smplR)

            if blog_act:
                rtrace['act'].append(smplA)

            epsilontrace.append(epsilon)

            if (bplot and (reps % plot_intvl == plot_intvl-1 or reps == nReps-1)) or\
               (self.pdf is not None and reps == nReps-1):
                self.online_plot(rtrace, epsilontrace, orgX[:step+1, :],
                                 extonlineplot, n_extonlineplot, None)

        #f.close()
        if bplot: plt.ioff()

        return (rtrace, epsilontrace, None)
