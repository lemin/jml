""" online Relevant Vector Machine (based on ref[3])
        modified version of RVM to train online
                by Jake Lee (lemin)

 References:
 [3] Michael E. Tipping and A. C. Faul.
     "Fast marginal likelihood maximisation for sparse Bayesian models."
     In C. M. Bishop and B. J. Frey (Eds.),
     Proceedings of the Ninth International Workshop
     on Artificial Intelligence and Statistics, Key West, FL, Jan 3-6 (2003)

 [0] Michael E. Tipping and Alex Smola,
     "Sparse Bayesian Learning and the Relevance Vector Machine".
     Journal of Machine Learning Research 1: 211?244. (2001)
 [1] Bishop C.M, Pattern Recognition and Machine Learning, 
     Springer, New York, 2006

 [2] Vladimir Vapnik. "The Nature of Statistical Learning Theory."
     Springer-Verlag, 1995

 [4] David P. Wipf and Srikantan Nagarajan,
     "A New View of Automatic Relevance Determination,"
     In J.C. Platt, D. Koller, Y. Singer, and S. Roweis, editors,
     Advances in Neural Information Processing Systems 20, MIT Press, 2008.
 [5] David P. Wipf and Srikantan Nagarajan,
     "Sparse Estimation Using General Likelihoods and Non-Factorial Priors,"
     In Advances in Neural Information Processing Systems 22, 2009.
"""
import numpy as np
#from ..svm.svmdata import SVMData
from jml.svm.svmdata import SVMData
from scipy.linalg import cholesky as chol, inv, det, LinAlgError  # for uppper/lower option
from copy import deepcopy as copy
from jml.util.exp import Standardizer
from itertools import product


EST, ADD, DEL, TERM, RETRY, SKIP = [0, 1, 2, 3, 10, 11]
STRACT = ["EST", "ADD", "DEL", "TERM"]

class oRVM():
    """ online sequential sparse Bayesian Learning Algorithm

        Attributes
        ----------
        _alpha      array
                    hyperparameter alpha
        _beta       array
                    inverse variance of target distribution (sigma^-2)
        _mu         array
                    mean of the weight distribution (Guassian)
        _Sigma      array
                    covariance of the weight distribution
        data        SVMData
                    SVMData to store training data and label with kernel info

        _used       list
                    currently used basis
        rv_idx      list
                    relevance vector index
        _gamma      array
                    well-determinedness: how well w_i is determined by the data
        _s          array
                    sparsity: what extent does the basis overlaps with the other
        _q          array
                    quality: a measure of alignment btn T and y_{-i}
        _w          array
                    scaled weight for inference

        Methods
        -------
        train(self, X, T, **params)
            using online_train(.) train relevance vector regression

        online_train(self, x, t, **params)
            single sample update

        use(self, X, T)
            apply the trained RVM (RMR)

        References
        ----------
        Michael E. Tipping and A. C. Faul.
            "Fast marginal likelihood maximisation for sparse Bayesian models."
            In C. M. Bishop and B. J. Frey (Eds.),
            Proceedings of the Ninth International Workshop
            on Artificial Intelligence and Statistics, Key West, FL, Jan 3-6 (2003)
    """

    def __init__(self):
        self._alpha = None
        self._beta = None
        self._mu = None
        self._Sigma = None
        self.data = None

        self._used = []
        self._ibuff = []
        self.rv_idx = None
        self._gamma = None
        self._s = None
        self._q = None
        self._w = None
        self.Phi = np.array([]).reshape((0, 0))

        self.dup_update = 0

        self.stdX = None

    def set_basis(self, x, t, **params):

        # kernel setting
        if self.data is None:
            self.data = SVMData(x, t.astype(float))
        else:
            self.data.set_data(x)
            self.data.set_label(t)

        self.data.set_kernel(params.pop('kernel', 'rbf'), **params)
        self.Phi = self.data.kernelf(x)   # the active basis only 

        # normalize basis
        #self.scales = np.sqrt(np.sum(self.Phi ** 2, 0))
        #self.scales[self.scales == 0] = 1.

        #self.Phi[...] = self.Phi / self.scales

        #print "set basis: ", self.Phi.shape

    def inc_phi(self, Phi, x):  #, scales):
        M = Phi.shape[0]

        phi_m = self.data.kernelf(x)[0]
        phi_mm = self.data.kernelf(x, x)[0]
        # normalize basis
        #scale_m = np.sqrt(np.sum(phi_m ** 2, 0))
        #phi_m = phi_m / scales
        #phi_mm = phi_mm / scale_m
        #scales = np.append(self.scales, scale_m)

        PHI = np.zeros((M+1, M+1))
        PHI[:-1, :-1] = Phi
        PHI[-1, :-1] = phi_m
        PHI[:-1, -1] = phi_m
        PHI[-1, -1] = phi_mm
        return PHI#, scales

    def add_basis(self, x, t):
        # increment kernel matrix
        self.Phi = self.inc_phi(self.Phi, x)
        #self.Phi, self.scales = self.inc_phi(self.Phi, x, self.scales)
        #print "add_basis......"
        # add datat to kernel data
        self.data.add_data(x.flat, t)  # x.flat since single input

    def del_basis(self, i):
        self.data.remove_data(i)
        self.Phi = np.delete(np.delete(self.Phi, i, 0), i, 1)
        #self.scales.pop(i)

    def train(self, X, T, **params):
        self.rv_idx = []
        self.nrv_idx = []

        llike = []
        if 'verbose' in params.keys():
            verbose = params['verbose']
        bshow = params.pop('online_plot', False)
        if bshow:
            import matplotlib.pyplot as plt
            plt.ion()
            plt.figure(1)
            ys = []
        for i in xrange(X.shape[0]):
            print "\n\n ------------------------------------", i, "-------------------\n\n"
            if verbose > 1: print "Data >>>>>>>>>>>>>>>>>>>>>>> ", i, self.rv_idx
            if np.any(np.isnan(T)):
                chgidx = np.where(~np.isnan(T[i]))[0]
            else:
                chgidx = -1
            ll = self.online_train(X[i:i+1, :], T[i:i+1],
                                   chgidx=chgidx,**params)

            ### instead of using index translation, 
            ### convert rv_idx to read data index 

            RVXrows = self.data.X[self._used].view([('', self.data.X.dtype)] * X.shape[1])
            Xrows = X.view([('', X.dtype)] * X.shape[1])
            self.rv_idx = np.where(np.in1d(Xrows, RVXrows))[0]

            llike.extend(ll)
            """
            if verbose > 1: print "added result:", added, moved, deled

            tmp_del = []
            to_ibdrop = []
            # delete
            #for di in sorted(deled)[::-1]:
            #    print " RV: deleted", di
            #    tmp_del.append(self.rv_idx.pop(di))
            sorted_i = np.argsort(deled)
            for s_i in sorted_i[::-1]:
                if verbose > 1:
                    print " RV: deleted", deled[s_i], dropped[s_i]
                v = self.rv_idx.pop(deled[s_i])
                if len(dropped) > 0 and not dropped[s_i]:
                    tmp_del.append(v)

            # move from non-RVs to RVs
            for mi, loc in moved:
                self.rv_idx.insert(loc, self.nrv_idx[mi])
                to_ibdrop.append(mi)

            # add
            if added == 1:
                if verbose > 1: print " RV: added", i
                self.rv_idx.append(i)
            elif added == -1:
                self.nrv_idx.append(i)

            # move from RVs to non-RVS
            self.nrv_idx.extend(tmp_del[::-1])

            # combine moved drop and ib_drop
            to_ibdrop.extend(ibdrp)
            # move after addition for right ordering
            #for mi, loc in moved[::-1]:
            #    self.nrv_idx.pop(mi)
            for mi in sorted(to_ibdrop)[::-1]:
                self.nrv_idx.pop(mi)

            #print self._used, self._ibuff
            #print self.rv_idx, self.nrv_idx
            """

            if bshow:
                y = self.use(X[:i+1, :])
                ys.append(y)

                plt.clf()
                plt.plot(X[:i+1, :], T[:i+1], 'bo');
                plt.plot(X[self.rv_idx], T[self.rv_idx], 'ro');
                for y in ys[:-1]:
                    plt.plot(X[:len(y), :], y, 'r-', alpha=0.2);
                plt.plot(X[:i+1, :], ys[-1], 'r-');
                plt.draw()
        if bshow:
            plt.ioff()

        return llike


    def online_train(self, x, t, **params):
        """ train RVM

            Parameters
            ==========
            x       array
                    training data (one sample)
            t       array
                    training target (one sample)
            tol     float
                    tolerence for termination condition (term if dlogML < tol)
            minDeltaLogAlpha    float
                    minimum delta log alpha (for term condition)
            minDeltaLogBeta     float
                    minimum delta log beta (for recompute parameters)
            alpha_lim   list
                    min and max values for alpha
            ltrace  boolean
                    an option for recording log marginal likelihood
            etrace  boolean
                    an option for recording squared error
            betaUpdate  list
                    a pair of values of start and frequence
                    the frequency-based beta update start at the start
            fixedBeta   boolean
                    do not update beta if True

            *** kernel options
            kernel  string
                    type of kernel (rbf/linear/poly/sigmoid)
            gamma   float
                    a coefficient on product term
            coef    float
                    an additional coefficent for poly and sigmoid
            degree  float
                    degree parameter for polynomial kernel
            errorf  function
                    function that returns error and sqaured error
                    from current basis and mean

            nbuff   int
                    the size of buffer for non-RV's for accurate fit
        """

        ###########################################################
        # options and initialization
        ###########################################################

        tol = params.pop('tol', 1e-10)
        mdlog_alpha = params.pop('minDeltaLogAlpha', 1e-3)
        mdog_beta = params.pop('minDeltaLogBeta', 1e-3)

        a_min, a_max = params.pop('alpha_lim', [np.finfo(float).eps, 1e10])
        max_step = params.pop('maxstep', 500)
        
        bltrace = params.pop('ltrace', True)
        betrace = params.pop('etrace', False)

        # beta_update default: start at 10 every 5 steps (from SparseBayes)
        beta_update = params.pop('betaUpdate', [10, 5])
        fix_beta = params.pop('fixedBeta', False)

        align_test = params.pop('alignTest', True)
        alignMax = params.pop('alignMax', 0.999)
        verbose = params.pop('verbose', 0)

        errorf = params.pop('errorf', None)

        # nbuff
        n_buff = params.pop('nbuff', 0)

        p = x.shape[1]
        M = self.Phi.shape[0]
        N = M + 1
        Mall = N

        if self.stdX:
            x = self.stdX.standardize(x)

        # MULTIDIM SUPPORT
        # set multidimensional option
        if len(t.shape) > 1 and t.shape[1] > 1:
            md_target = True
            chg_idx = params.pop('chgidx', -1) #np.where(t != 0)[0]
        else:
            md_target = False

        if M == 0:
            self.set_basis(x, t, **params)
            PHI = copy(self.Phi)
            T = t
        else:
            PHI = self.inc_phi(self.Phi, x)
            #PHI, scales = self.inc_phi(self.Phi, x, self.scales)

            # UPDATE LABEL WITH NEW APPROXIMATION!!!
            # MULTIDIM SUPPORT 
            if md_target:
                valid = ~np.isnan(self.data.label)
                #self.data.label[valid] = self.use(self.data.X, stdX=False)[valid]
                self.data.label[valid] = self.use(self.data.X)[valid] #, stdX=False)[valid]
            else:
                tmp = copy(self.data.label)
                self.data.label[...] = self.use(self.data.X).flatten() #, stdX=False).flatten()
                np.set_printoptions(precision=4, suppress=True)

            # check dupilcates
            if self.dup_update > 0:
                dup_i = np.where((self.data.X==x).all(axis=1))[0]
                n_dups = len(dup_i)
                if n_dups > 0:
                    if self.dup_update == 2:  # mean
                        mean_dup = (np.sum(self.data.label[dup_i]) + t) / n_dups
                        self.data.label[dup_i] = mean_dup
                    else: # 1 for simple update with recent target
                        self.data.label[dup_i] = t
            if len(t.shape) == 1:
                T = np.append(self.data.label, t)
            else:
                T = np.vstack((self.data.label, t))

        valid_t_idx = np.arange(T.shape[0])
        if verbose > 2:
            print "------------PHI--------------------------"
            print PHI
            print "-----------------------------------------"

        #normalize basis
        scales = np.sqrt(np.sum(PHI ** 2, 0))
        scales[scales == 0] = 1.
        PHI[...] = PHI / scales

        if verbose > 1:
            np.set_printoptions(precision=4, suppress=True)

        # to check changes before stochastic update
        old_X = copy(self.data.X[self._used])

        ###########################################################
        # Inner function definitions
        ###########################################################
        if errorf is None:
            def errorf(T, Phi, mu):
                # MULTIDIM SUPPORT
                if md_target:
                    err = T - np.dot(Phi, np.nan_to_num(mu))
                    E = np.diag(np.dot(err.T, err))
                else:
                    err = T - np.dot(Phi, mu)
                    E = np.dot(err.flat, err.flat)
                return err, E


        def compute_params(used, PHI_Phi):
            """ inner function to compute statisticsinner function to compute statistics
                used in step 3 and 10
            """
            Phi = PHI[:, used]

            A = beta * np.dot(Phi.T, Phi) + np.diag(alpha[used])  # (12)
            bcomp_err = False
            if np.any(np.isinf(A)):
                U = np.ones(A.shape) * np.inf
                Sigma = np.zeros((A.shape[0], A.shape[0]))
                bcomp_err = True
            else:
                try:
                    U = chol(A)
                    Ui = inv(U)
                    Sigma = np.dot(Ui, Ui.T)
                except LinAlgError, e:
                    print e
                    U = np.ones(A.shape) * np.inf
                    Sigma = np.zeros((A.shape[0], A.shape[0]))
                    bcomp_err = True

            # For RL discrete output, store mu and recover nans!
            if md_target:
                nan_idx = np.all(np.isnan(T), 0)
                #mu = beta * np.dot(Sigma, np.dot(Phi.T, np.nan_to_num(T)))  # (13)
                mu = beta * np.dot(Sigma, PHI_T[used])  # (13)
                mu[:, nan_idx] = np.nan
            else:
                #mu = beta * np.dot(Sigma, np.dot(Phi.T, T))  # (13)
                mu = beta * np.dot(Sigma, PHI_T[used])  # (13)

            if bcomp_err:
                mu[:] = 0.  # prevent generating NaN's for use()
                S = np.ones(Phi.shape[0]) * np.nan
                Q = np.ones(T.shape) * np.nan
                s = copy(S)
                q = copy(S)
            else:
                """
                # common term in (24) and (25)
                Z = beta * PHI.T - (beta ** 2) *\
                    np.dot(PHI.T, np.dot(Phi, np.dot(Sigma, Phi.T)))
                S = np.diag(np.dot(Z, PHI))  # (24)
                if md_target:
                    Q = np.dot(Z, np.nan_to_num(T))      # (25)
                    Q[np.isnan(T)] = np.nan
                else:
                    Q = np.dot(Z, T)      # (25)
                """
                S = beta - np.sum(beta * np.dot(PHI_Phi, Ui) ** 2, 1)
                Q = beta * (PHI_T - np.dot(PHI_Phi, mu))

                denom = 1 - S[used] / alpha[used]
                # (23) for s and q
                s = copy(S)
                q = copy(Q)
                s[used] = S[used] / denom
                #MULTIDIM SUPPORT
                if md_target:
                    q[used] = Q[used] / denom.reshape((-1, 1))
                else:
                    q[used] = Q[used] / denom 

            # log marginal likelihood
            # 3.86 [1] | log (15) [0]
            #   ln p(t|a, b) = 1/2 * M ln a + 1/2 * N ln b
            #                   - E(w) - 1/2 * ln |A| - 1/2 * N ln(2pi)
            #   where
            #       E(w) = 1/2 * b * ||t - phi w||^2 + 1/2 * a w^T w
            #
            #   and log determinant 
            #       from det(A) = det(V)^2 = product(diag(V))^2)
            #       log |A| = 2 * sum(log(diag(V)))
            #
            #   here the constant term with pi is ignored
            musq = mu ** 2
            err, E = errorf(T, Phi, mu)

            log_det = np.sum(np.log(np.diag(U)))
            ll = .5 * (np.sum(np.log(alpha[used])) + N * np.log(beta)\
                    - beta * E - np.dot(alpha[used], musq))\
                 - log_det
            # MLE ref: http://pages.uoregon.edu/aarong/teaching/G4075_Outline/node13.html

            return mu, Sigma, s, q, ll, S, Q, E

        def update_base(i, act, new_alpha, S, Q, s, q, PHI_Phi):
            """ update bases based on theta
                contains step 6, 7, and 8
            """
            Phi = PHI[:, used]
            phi = PHI[:, i]

            ###########################################################
            # 6. if theta_i > 0 and alpha_i < inf, reestimate alpha_i
            if act == EST:

                if verbose > 1:
                    print "REESTIMATE: ", i

                j = used.index(i)

                # (33) for new Sigma
                delta_alpha = new_alpha - alpha[i]
                kappa = 1. / (Sigma[j, j] + 1./ delta_alpha)
                s_j = Sigma[:, j, None]
                n_Sigma = Sigma - kappa * np.dot(s_j, s_j.T)

                # (34) for new mu
                if md_target:
                    #MULTIDIM SUPPORT
                    mu_j = np.tile(np.nan_to_num(mu[j]), (mu.shape[0], 1))
                    n_mu = mu - kappa * mu_j * s_j
                else:
                    n_mu = mu - kappa * mu[j] * s_j.flat

                #term = (beta * np.dot(np.dot(PHI.T, Phi), s_j)).flatten()
                term = (beta * np.dot(PHI_Phi, s_j)).flatten()
                S = S + kappa * term ** 2      # (35)
                #MULTIDIM SUPPORT
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (Q.shape[0], 1))
                    Q = Q + kappa * mu_j * term.reshape((-1, 1))   # (36)
                else:
                    Q = Q + kappa * mu[j] * term   # (36)

                alpha[i] = new_alpha

            ###########################################################
            # 7. if theta_i > 0 and alpha_i = infi, add phi_i
            elif act == ADD:

                if verbose > 1:
                    print "APPEND: ", i
                used.append(i)

                # append PHI' * Phi
                PHI_phi = np.dot(PHI.T, phi.reshape((-1, 1)))
                PHI_Phi = np.hstack((PHI_Phi, PHI_phi))

                term = np.dot(Sigma, np.dot(Phi.T, phi)) * beta
                term = term.reshape((-1, 1))  # to column vector
                # (28) for new Sigma
                #   s_ii for Sigma_ii and s_i for Sigma_i
                #s_ii = 1. / (new_alpha + S[i])
                s_ii = 1. / (abs(new_alpha) + abs(S[i]))
                s_i = -s_ii * term
                M = len(used)
                n_Sigma = np.zeros((M, M))
                #n_Sigma[:-1, :-1] = Sigma + np.outer(-s_i, term)
                n_Sigma[:-1, :-1] = Sigma + np.dot(-s_i, term.T)
                n_Sigma[:-1, -1] = s_i.flat
                n_Sigma[-1, :-1] = s_i.flat
                n_Sigma[-1, -1] = s_ii

                # (29) for new mu
                mu_i = s_ii * Q[i]
                # MULTIDIM SUPPORT
                if md_target:
                    nan_mu_i = np.nan_to_num(mu_i)
                    mu_i_tile = np.tile(nan_mu_i, (mu.shape[0], 1))
                    n_mu = np.vstack((mu - mu_i_tile * term.reshape((-1, 1)), mu_i))
                else:
                    n_mu = np.append(mu - mu_i * term.flat, mu_i)

                e_i = phi - np.dot(Phi, term.flatten())
                term1 = beta * np.dot(PHI.T, e_i)
                S = S - s_ii * term1 ** 2  # (30)

                #MULTIDIM SUPPORT
                if md_target:
                    Q = Q - np.tile(nan_mu_i, (Q.shape[0], 1)) * term1.reshape((-1, 1))
                else:
                    Q = Q - mu_i * term1       # (31)

                alpha[i] = new_alpha

            ###########################################################
            # 8. if theta_i <= 0 and alpha_i < infi, delete phi_i
            elif act == DEL:

                if verbose > 1:
                    print "DELETE: ", i

                j = used.index(i)
                alpha[i] = a_max
                used.remove(i)

                # delete i (jth column) from PHI_Phi
                PHI_Phi = np.delete(PHI_Phi, j, 1)

                # (38)
                s_j = Sigma[:, j, None]
                s_jj = Sigma[j, j]
                s_j_s = s_j.T / s_jj
                #n_Sigma = Sigma - 1./ s_jj * np.dot(s_j, s_j.T)
                n_Sigma = Sigma - np.dot(s_j, s_j_s)
                n_Sigma = np.delete(n_Sigma, j, 0)
                n_Sigma = np.delete(n_Sigma, j, 1)

                # (39)
                if md_target:
                    # MULTIDIM SUPPORT
                    mu_j = np.tile(np.nan_to_num(mu[j]), (mu.shape[0], 1))
                    n_mu = mu - mu_j * s_j_s[0].reshape((mu.shape[0], 1))
                else:
                    mu_j = mu[j]
                    n_mu = mu - mu_j * s_j_s[0]
                n_mu = np.delete(n_mu, j, 0)

                term = (beta * np.dot(np.dot(PHI.T, Phi), s_j))
                S = S +  1. / s_jj * term.flatten() ** 2
                #MULTIDIM SUPPORT
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (Q.shape[0], 1))
                else:
                    term = term.flatten()
                Q = Q +  mu_j / s_jj * term

            if verbose > 1:
                print "USED:", used

            denom = 1 - S[used] / alpha[used]
            # (23) for s and q
            s = copy(S)
            q = copy(Q)
            s[used] = S[used] / denom
            #MULTIDIM SUPPORT
            if md_target:
                q[used] = Q[used] / denom.reshape((-1, 1))
            else:
                q[used] = Q[used] / denom 

            err, E = errorf(T, PHI[:, used], n_mu)

            return n_mu, n_Sigma, S, Q, s, q, E, PHI_Phi

        ###########################################################
        # Algorithm  [3] Section 4
        ###########################################################

        A_MAX = 1000
        B_MAX = 1000000. #/ np.var(T)
        
        if len(self._used) > 0:
            #
            # recover parameters
            #
            used = copy(self._used)
            if n_buff == 0:
                alpha = np.append(self._alpha * (scales[used]**2), a_max)
            else:
                alpha = copy(self._alpha)
                alpha[used] = self._alpha[used] * (scales[used]**2)
                alpha = np.append(alpha, a_max)
            alpha = copy(self._alpha)
            alpha[used] = self._alpha[used] * (scales[used]**2)
            alpha = np.append(alpha, a_max)
            beta = self._beta
            mu = self._mu
            Sigma = self._Sigma
            gamma = self._gamma  
            s = self._s
            q = self._q
        else:
            # 1. initiailze beta = sigma^-2 to some sensible value
            beta = params.pop('beta', 100.)  #10. / np.var(T))

            # 2. initialize alpha_i with a single basis vector phi_i
            # single basis phi
            #used = [0]  # initial used basis index (only the bias is used)

            alpha = np.ones(Mall) * a_max  # set to a_max for free basis
            # max projection with the target
            ### MULTIDIM SUPPORT
            if md_target:
                if chg_idx >= 0:
                    # use chg_idx for the first time only
                    used = [np.nanargmax(np.abs(np.dot(PHI.T, T[:, chg_idx])))]
                else:
                    used = [np.nanargmax(np.mean(np.abs(np.dot(PHI.T, T)), 1))]
            else:
                used = [np.argmax(np.abs(np.dot(PHI.T, T)))]

            phib = PHI[:, used]
            if md_target and chg_idx >= 0:
                phibT = np.dot(phib.T, np.nan_to_num(T[:, chg_idx]))
            else:
                phibT = np.dot(phib.T, T)
            norm_phi = np.dot(phib.T, phib)
            norm_phiT = np.dot(phibT.flat, phibT.flat)  # MULTIDIM OUTPUT (flat)
            a_i = norm_phi / (norm_phiT / norm_phi - 1 / beta)
            if a_i < 0:
                print "warning: there no relevant basis function at init"
                alpha[used] = A_MAX
            else:
                alpha[used] = a_i
            gamma = np.array([1])

        # precompute for fast computation
        PHI_Phi = np.dot(PHI.T, PHI[:, used])
        PHI_T = np.dot(PHI.T, T)
                 
        # for alignment check on basis addition
        aligned_in = []     # existing basis
        aligned_out = []    # basis index that is similar to ones in aligned_in

        prev_ci = 0
        # 3. compute Sigma and mu along with sm and qm for all M bases phi_m
        mu, Sigma, s, q, logML, S, Q, E = compute_params(used, PHI_Phi)
        if bltrace: llike = [logML]
        if betrace: etrace = [E]
        for step in xrange(max_step):
            # 5. Compute theta_i
            ### MULTIDIM EXTENSION
            if md_target:
                # not to lose precision, square first
                if chg_idx >= 0:
                    q_sq = np.nanmean(q**2, 1)
                else:
                    q_sq = np.mean(q**2,1)
            else:
                q_sq = q ** 2
            theta = q_sq - s

            valid_idx = np.where(theta > tol)[0]
            est_idx = np.intersect1d(valid_idx, used)
            add_idx = np.setdiff1d(valid_idx, est_idx) 
            if align_test:
                add_idx = np.setdiff1d(add_idx, aligned_out)  # already checked
            if len(used) > 1:
                del_idx = np.intersect1d(np.where(theta <= tol)[0], used)
            else:
                del_idx = np.array([])

            # 4. Select a candiate vector phi_i
            #   max increase in marginal likelihood (w/ more computation) - App. A

            # from App. A
            dL = np.zeros(Mall) 
            ### MULTIDIM EXTENSION
            if md_target:
                # not to lose precision, square first
                if chg_idx >= 0:
                    Q_sq = np.nanmean(Q**2, 1)
                else:
                    Q_sq = np.mean(Q**2, 1)
            else:
                Q_sq = Q ** 2

            if len(est_idx):
                new_a = (s[est_idx] ** 2) / theta[est_idx]
                delta = (1. / new_a - 1. / alpha[est_idx])
                dL[est_idx] = ((delta * Q_sq[est_idx])\
                              / (delta * S[est_idx] + 1)\
                              - np.log(1 + S[est_idx] * delta)) / 2

            if len(add_idx):
                quot = Q_sq[add_idx] / S[add_idx]
                dL[add_idx] = (quot - 1 - np.log(quot)) / 2

            if len(del_idx):
                dL[del_idx] = -(q_sq[del_idx] / (s[del_idx] + alpha[del_idx])\
                                -np.log(1 + s[del_idx] / alpha[del_idx])) / 2
            
            # priority on deletion
            if len(del_idx) > 0:
                dL[add_idx] = 0
                dL[est_idx] = 0

            ci = np.argmax(dL)
            if ci in add_idx:
                act = ADD
            elif ci in del_idx:
                act = DEL
            else:
                act = EST
            if verbose > 1:
                print "selected:", ci, STRACT[act]

            # 11. check convergence
            if act == DEL:
                new_alpha = a_max
                logda = 0
            else:
                new_alpha = s[ci] ** 2 / theta[ci]
                logda = np.abs(np.log(new_alpha) - np.log(alpha[ci]))
            dlogML = np.max(dL)
            if dlogML <= 0 or \
              (act == EST and logda < mdlog_alpha and len(del_idx) == 0):
                act = TERM

            # if the new basis is too close to existing one,
            # do not add
            if align_test:
                if act == ADD:
                    p = np.dot(PHI[:, ci], PHI[:, used])
                    aligned_idx = np.where(p > alignMax)[0]
                    n_aligned = len(aligned_idx)
                    if n_aligned > 0:
                        act = SKIP
                        aligned_out.extend([ci] * n_aligned)
                        aligned_in.extend(np.asarray(used)[aligned_idx])
                elif act == DEL:
                    if ci in aligned_in:
                        aligned_idx = np.where(np.asarray(aligned_in) == ci)[0]
                        for di in reversed(aligned_idx):
                            del aligned_in[di]
                            del aligned_out[di]

            if act < TERM:
                mu, Sigma, S, Q, s, q, E, PHI_Phi = update_base(ci, act,
                                                       new_alpha, S, Q, s, q,
                                                       PHI_Phi)
            M = len(used)
            gamma = np.ones(M) - alpha[used] * np.diag(Sigma)  # [0](17)
            # 9. update beta

            if not fix_beta and\
               (step < beta_update[0] or step % beta_update[1] == 4):
                # work-around zero-noise issue from SparseBayes
                old_beta = beta
                # MULTIDIM SUPPORT
                if md_target:
                    if np.mean(E) == 0: 
                        beta = B_MAX
                    else:
                        if chg_idx >= 0:
                            beta = min((N - np.sum(gamma)) / np.nanmean(E), B_MAX)
                            #beta = min((N - np.sum(gamma)) / E[chg_idx], B_MAX)
                        else:
                            beta = min((N - np.sum(gamma)) / np.mean(E), B_MAX)
                else:
                    beta = min((N - np.sum(gamma)) / np.mean(E), B_MAX)

            # 10. recompute/update Sigma, mu and s & q
            dlogb = np.log(beta) - np.log(old_beta)
            if abs(dlogb) > mdog_beta:  # from SparseBayes
                mu, Sigma, s, q, logML, S, Q, E = compute_params(used, PHI_Phi)
                dlogML = 0.
                act = RETRY

            logML += dlogML
            if bltrace: llike.append(logML)
            if betrace: etrace.append(E)

            if verbose and step % 10 == 0:
                print step, "> gamma: ", np.sum(gamma), "M=", M, "beta=", beta

            if verbose > 2:
                print "-------------Update Summary-------------"
                print "alpha:", alpha
                print "beta:", beta
                print "S:", S
                print "Q:", Q
                print "s:", s
                print "q:", q
                print "mu:", mu
                print "Sigma:"
                print Sigma
                print "logML:", logML
                #print "dL: ", dL
                print "used:", used
                print "E:", E
                print "----------------------------------------"


            if act == TERM: 
                if verbose:
                    print "dlogML:", dlogML, " stops at", step+1, "steps."
                break

        if verbose and step == max_step-1:
            print "reached the max step", max_step

        ### Store the parameters
        if n_buff == 0:
            self._alpha = alpha[used] / (scales[used] ** 2)
        else:
            self._alpha = alpha
            self._alpha[used] = alpha[used] / (scales[used] ** 2)
        self._beta = beta
        old_mu = self._mu
        self._mu = mu
        self._Sigma = Sigma
        self._gamma = gamma  
        self._s = s
        self._q = q
        # MULTIDIM SUPPORT
        if md_target:
            self._w = mu / scales[used].reshape((-1, 1))
            # nan-handling
            self._w = np.nan_to_num(self._w)
        else:
            self._w = mu / scales[used]

        old_used = self._used 
        old_ibuff = copy(self._ibuff)
        if verbose > 1:
            print "------------------------------------------------------"
            print  "used: ", used , "\told: ", old_used, "size:", PHI.shape[0]
            print " old_ibuff>: ",  self._ibuff


        #before indexing for Debug
        #b4_res = np.dot(PHI[:, used], mu)[used]
        #b4_Phi = copy(self.Phi)
        #b4_PHI_used = PHI[:, used]

        if len(old_used) > 0:
            deled = list(np.setdiff1d(old_used, used))
            added = list(np.setdiff1d(used, old_used))
            if n_buff == 0:  # for buffered case, treat later
                if len(deled) > 0 and PHI.shape[0]-1 in added:
                    del_i = deled.pop(0)
                    used[np.argmax(used)] = del_i
                    self.data.X[del_i, :] = x
                    self.data.label[del_i] = t
                    recoverPHI = PHI * scales
                    self.Phi[del_i, :] = recoverPHI[-1, :-1]
                    self.Phi[:, del_i] = recoverPHI[:-1, -1]
                    self.Phi[del_i, del_i] = recoverPHI[-1, -1]
                    added.remove(PHI.shape[0]-1) 
                    badded = 1

                for deli in sorted(deled)[::-1]:
                    if verbose: print "dbg>>> deleting ", deli
                    self.del_basis(deli)
                    used = self.dec_list(used, deli)
                    if verbose: print "dbg>>> used:", used

            if PHI.shape[0]-1 in added:
                # new input is a basis
                if verbose: print "dbg>>> adding new input as a basis"
                self.add_basis(x, t)
                badded = 1
                added.remove(PHI.shape[0]-1)
            else:
                # new input is not a basis
                badded = 0

            if len(added) > 0:
                # basis selected from a candidate buffer
                for ai in added:
                    self._ibuff.remove(ai)
                    # do not need to call add_basis b/c already there

                if not badded:
                    # buffer have a room for one or more now 
                    self.add_buf(n_buff, PHI, used, x, t, dL=dL)
                    badded = -1

            # buffered learning. to consider as a candidate
            if n_buff > 0:
                del_list = []
                # deleted RV as a candidate
                tmp_deled = []
                for deli in sorted(deled)[::-1]:
                    tmp_deled.append(old_used.index(deli))
                    del_list.append(self.add_buf(n_buff, PHI, used, deli, dL=dL))
                # to consider as a candidate
                if not badded:
                    del_list.append(self.add_buf(n_buff, PHI, used, x, t, dL=dL))

                # remove del_list from buffer
                # and rearrange used index
                del_list = filter(lambda x: x >= 0, del_list) # remove all -1's
                if verbose > 1: print "    DBG:del_list ", del_list
                for di in sorted(del_list)[::-1]:
                    self.del_basis(di)
                    self._alpha = np.delete(self._alpha, di)
                    # update used
                    used = self.dec_list(used, di)
                    # update ibuff
                    self._ibuff = self.dec_list(self._ibuff, di)
                    ## update deleted index - not necessary? CHECK THIS
                    ####deled = self.dec_list(deled, di)

                if verbose > 1:
                    print " _ibuff>: ",  self._ibuff

        c = params.pop('mu_c', 1.)
        if old_mu is not None:
            #print old_X
            #print self.data.X[used]
            old_i = []
            new_i = []
            new_X = self.data.X[used]
            # find matching pairs
            for i in xrange(old_X.shape[0]):
                n_i = np.where((new_X == old_X[i]).all(axis=1))[0]
                if len(n_i) == 1:
                    new_i.append(n_i[0])
                    old_i.append(i)
                
            #print old_i, new_i
            #print old_mu
            #print self._mu#, new_i
            self._mu[new_i] = c * self._mu[new_i] + (1 - c) * old_mu[old_i]

            newadd = np.setdiff1d(range(len(self._mu)), new_i)
        else:
            newadd = used
        #print "newadd: ", newadd
        self._mu[newadd] *= c

        # after indexing
        #after_res = np.dot(self.Phi[:, used], self._w)[used]
        self._used = used

        return llike

    def dec_list(self, lst, di):
        return [lst[i] if lst[i]<di else lst[i]-1 for i in xrange(len(lst))]

    def add_i_to_buff(self, PHI, used, i, min_dist):
        to_del = i
        for j in self._ibuff:
            j_min = min(PHI[j, used])
            if min_dist > j_min:
                to_del = j

        if to_del != i:
            self._ibuff.remove(to_del)
            self._ibuff.append(i)

        return to_del

    # with dL
    def add_i_to_buff_dL(self, PHI, used, i, dL):
        dL[i]
        j = np.argmin(dL[self._ibuff])
        to_del = self._ibuff[j]
        if dL[i] < dL[to_del]:
            to_del = i
        if to_del != i:
            self._ibuff.remove(to_del)
            self._ibuff.append(i)
        return to_del

    def add_buf(self, nbuff, PHI, used, *args, **params):
        """ add a candidate with index to data  (with one int args[0])
            or (x, t) that is not in data (x for args[0], t for args[1])
        """
        if len(args) == 1 and isinstance(args[0], int):
            i = args[0]
        elif len(args) == 2:
            i = self.data.n_samples
            x = args[0]
            t = args[1]
        else:
            raise ValueError("add_buf: Wrong ", len(args)," arguments!") 

        if len(self._ibuff) < nbuff:
            self._ibuff.append(i)
            if len(args) == 2:
                self.add_basis(x, t)
        else:
            if 'dL' in params.keys():
                dL = params.pop('dL', None)
                to_del = self.add_i_to_buff_dL(PHI, used, i, dL)
            else:
                min_dist = min(PHI[i, used])
                to_del = self.add_i_to_buff(PHI, used, i, min_dist)

            #
            # DO NOT delete here. 
            # for index post-processing, return index-to-delete
            if len(args) == 1:
                # i is already in data, do not need to add
                # but to_del needs to be deleted
                # delete to_del from both Phi and data
                return to_del
            elif to_del != i:
                # in case of adding i, it needs to update both Phi and data
                self.add_basis(x, t)
                return to_del
            else:
                return -2
        return -1

    def use(self, x, **params):
        """
            parameters
            ==========
            x           array 
                        new data input to predict a target
            with_var    boolean
                        return variance if True

            return
            ======
            pred    mean of the predicted target distribution (Gaussian) 
            var     variance of the target distribution
        """
        if len(x.shape) == 1:
            x = x.reshape((-1, 1))

        #bstd = params.pop('stdX', True)  # for target update in online_train
        #if bstd and self.stdX:
        if self.stdX:
            xs = self.stdX.standardize(x)
        else:
            xs = x

        with_var = params.pop('with_var', False)

        # MULTIDIM SUPPORT
        if len(self._w.shape) > 1 and self._w.shape[1] > 1:
            pred = np.zeros((len(x), self._w.shape[1]))
        else:
            pred = np.zeros(len(x))
        if with_var:
            var = np.zeros(len(x))
        for i in xrange(len(x)):
            if False:
                phi_x = np.append(1., self.data.kernelf(xs[i:i+1, :]))
            else:
                if 'xs' not in locals():
                    import pdb; pdb.set_trace()
                phi_x = self.data.kernelf(xs[i:i+1, :])[0]

            pred[i] = np.dot(phi_x[self._used], self._w)  # [0]((21)
            if with_var:  # [0](22)
                var[i] = 1 / self._beta + np.dot(phi_x.T, np.dot(self._Sigma, phi_x))

        if with_var:
            return pred, var
        else:
            return pred

    def score(self, X, T):
        """ compute root-mean-square-error """
        pred = self.use(X)
        return np.sqrt(np.mean((T - pred) ** 2))

    def get_alpha(self):
        return self._alpha[self._used]
        
    def get_beta(self):
        return self._beta
        
    def get_gamma(self):
        return self._gamma

    def get_used(self):
        return self._used

    def get_s(self):
        return self._s

    def get_q(self):
        return self._q
        
    rvm_alpha = property(get_alpha)
    rvm_beta = property(get_beta)
    rvm_gamma = property(get_gamma)
    rvm_used = property(get_used)
    rvm_s = property(get_s)
    rvm_q = property(get_q)


class omRVM():
    """ multidimensional RVM 
            simple extension to multidimensional output as 
            a collection of m oRVM's

            oRVM supports multidimensional output but it shares
            the beta and Sigma for all the dimension.
            This doesn't work with RVM that does not share the output
            at the same time. omRVM will be useful when each output is
            independent.
    """
    
    def __init__(self, M=1):
        self._rvms = []
        self._dim = M

        for i in xrange(M):
            self._rvms.append(oRVM())
        
    def train(self, X, T, **params):
        llike = []
        for i, rvm in enumerate(self._rvms):
            nanidx = ~np.isnan(T[:, i])
            llike.append(rvm.train(X[nanidx], T[nanidx, i], **params))
        return llike

    def online_train(self, x, t, **params):
        ll = []
        for i, rvm in enumerate(self._rvms):
            if not np.isnan(t[:, i]):
                ll.extend(rvm.online_train(x, t[:, i], **params))
        return ll

    def use(self, X, **params):
        return np.hstack(([rvm.use(X, **params).reshape((-1, 1)) if rvm.data is not None
                           else np.zeros((len(X), 1)) for rvm in self._rvms]))

    def get_alpha(self):
        return [rvm._alpha[rvm._used] if rvm._alpha is not None else None
                for rvm in self._rvms]
        
    def get_beta(self):
        return [rvm._beta for rvm in self._rvms]
        
    def get_gamma(self):
        return [rvm._gamma for rvm in self._rvms]

    def get_used(self):
        return [rvm._used for rvm in self._rvms]

    def get_s(self):
        return [rvm._s for rvm in self._rvms]

    def get_q(self):
        return [rvm._q for rvm in self._rvms]

    def get_gamma(self):
        return [rvm._gamma for rvm in self._rvms]

    rvm_alpha = property(get_alpha)
    rvm_beta = property(get_beta)
    rvm_gamma = property(get_gamma)
    rvm_used = property(get_used)
    rvm_s = property(get_s)
    rvm_q = property(get_q)


if __name__ == '__main__':

    errorf = None
    if True:
        X = np.linspace(-10, 10, 101)
        T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3 #+ np.random.rand()
        X = X.reshape((-1,1)) 

        # for test
        x = X

        #gamma = 6.
        #gamma = 16.
        gamma = 2 
        tol = 1e-3
    elif False:
        X = np.linspace(-10, 10, 101).reshape((-1, 1))
        T1 = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3 #+ np.random.rand()
        T2 = 0.314 * X ** 2 + 0.278 * X + 10.3 #+ np.random.rand()

        T = np.hstack((T1, T2))
        gamma = 2 
        tol = 1e-3

        x = X
    elif False:
        """
        X = np.array([[-1.2],
                      [-1.],
                      [-1.2],
                      [-1.],
                      [-1.2]
                     ])
        T = np.array([[np.nan, np.nan, 1.],
                      [0., np.nan, np.nan],  
                      [np.nan, np.nan, 1.031],  
                      [0., np.nan, np.nan],  
                      [np.nan, 0, np.nan]
                     ])
        """
        from numpy import nan
        X = np.array([[ 0.],
                      [ 1.],
                      [ 0.],
                      [ 0.],
                      [ 1.],
                      [ 2.],
                      [ 2.],
                      [ 1.],
                      [ 1.],
                      [ 0.],
                      [-1.],
                      [ 0.],
                      [ 1.],
                      [ 2.],
                      [ 1.],
                      [ 2.],
                      [ 2.],
                      [ 1.],
                      [ 2.],
                      [ 2.],
                      [ 2.],
                      [ 1.],
                      [ 0.]])
        T = np.array([[ nan,  nan,   0.],
                      [  1.,  nan,  nan],
                      [ nan,   1.,  nan],
                      [ nan,  nan,   0.],
                      [ nan,  nan,   0.],
                      [ nan,  nan,   0.],
                      [ 0.012,    nan,    nan],
                      [   nan,  0.002,    nan],
                      [  1.,  nan,  nan],
                      [ 0.019,    nan,    nan],
                      [ nan,  nan,   1.],
                      [ nan,  nan,  -0.],
                      [   nan,    nan,  0.003],
                      [ 0.001,    nan,    nan],
                      [   nan,    nan, -0.005],
                      [   nan,    nan, -0.004],
                      [ -0.,  nan,  nan],
                      [   nan,    nan, -0.002],
                      [   nan,    nan, -0.001],
                      [ nan,   0.,  nan],
                      [  0.,  nan,  nan],
                      [  1.,  nan,  nan],
                      [ nan,   1.,  nan]])
        
        gamma = .5
        tol = 1e-3

        x = X

        def errorf(T, Phi, mu):
            Y = np.dot(Phi, np.nan_to_num(mu))
            Y[np.isnan(T)] = 0.  # put to zero!
            err = np.zeros(T.shape)
            ai = np.where(~np.isnan(T[-1]))[0]
            err = np.nan_to_num(T) - Y
            E = np.diag(np.dot(err.T, err))
            #if np.any(np.isnan(E)):
            #    import pdb; pdb.set_trace()
            return err, E

        rvr = omRVM(3)
        ltrace = rvr.train(X, T, kernel='rbf', gamma=1./gamma, tol=tol,
                        nbuff= 25,
                        verbose=0, online_plot=True)

        y = rvr.use(x)
        np.set_printoptions(precision=3, suppress=True)
        print y
        print T-y

        import sys
        sys.exit(0)
    else:
        data = np.array([
            [0.000000, 0.349486],
            [0.111111, 0.830839],
            [0.222222, 1.007332],
            [0.333333, 0.971507],
            [0.444444, 0.133066],
            [0.555556, 0.166823],
            [0.666667, -0.848307],
            [0.777778, -0.445686],
            [0.888889, -0.563567],
            [1.000000, 0.261502],
            ])

        X = data[:, :1]
        T = data[:, 1]
        
        x = np.linspace(0, 1, 50)  #.reshape((-1, 1))

        #gamma = 0.0025
        gamma = 0.05
        tol = 1e-8

    
    
    rvr = oRVM()
    ltrace = rvr.train(X, T, kernel='rbf', gamma=1./gamma, tol=tol,
                       nbuff= 25,
                       errorf=errorf,
                       verbose=0, online_plot=True)

    #print rvr._alpha
    print len(rvr.rv_idx), "RVs:"
    print rvr.rv_idx

    import matplotlib.pyplot as plt
    fig = plt.figure()
    fig.add_subplot(121)

    plt.plot(X, T, 'bo');
    plt.plot(X[rvr.rv_idx], T[rvr.rv_idx], 'ro');

    y = rvr.use(x)

    plt.plot(x, y, 'r-');

    fig.add_subplot(122)

    plt.plot(ltrace)
    plt.xlabel("steps")
    plt.ylabel("log marginal likelihood")

    plt.show()

    np.set_printoptions(precision=3, suppress=True)
    print T
    print y
