""" Relevant Vector Machine (based on ref[3])
    
                by Jake Lee (lemin)

 References:
 [3] Michael E. Tipping and A. C. Faul.
     "Fast marginal likelihood maximisation for sparse Bayesian models."
     In C. M. Bishop and B. J. Frey (Eds.),
     Proceedings of the Ninth International Workshop
     on Artificial Intelligence and Statistics, Key West, FL, Jan 3-6 (2003)

 [0] Michael E. Tipping and Alex Smola,
     "Sparse Bayesian Learning and the Relevance Vector Machine".
     Journal of Machine Learning Research 1: 211?244. (2001)
 [1] Bishop C.M, Pattern Recognition and Machine Learning, 
     Springer, New York, 2006

 [2] Vladimir Vapnik. "The Nature of Statistical Learning Theory."
     Springer-Verlag, 1995

 [4] David P. Wipf and Srikantan Nagarajan,
     "A New View of Automatic Relevance Determination,"
     In J.C. Platt, D. Koller, Y. Singer, and S. Roweis, editors,
     Advances in Neural Information Processing Systems 20, MIT Press, 2008.
 [5] David P. Wipf and Srikantan Nagarajan,
     "Sparse Estimation Using General Likelihoods and Non-Factorial Priors,"
     In Advances in Neural Information Processing Systems 22, 2009.
"""
import numpy as np
from jml.svm.svmdata import SVMData
from scipy.linalg import cholesky as chol, inv, det, LinAlgError
from copy import deepcopy as copy


EST, ADD, DEL, TERM, RETRY, SKIP, NOOP = [0, 1, 2, 3, 10, 11, 255]
STRACT = {0:"EST", 1:"ADD", 2:"DEL", 3:"TERM", 10:"RETRY", 11:"SKIP", 255:"NOOP"}
#EPS = np.finfo(float).eps


def get_eigval(cov, vec):
    # sum of Sigma V
    sigma_v = np.diag(np.dot(cov, vec))
    eigval = sigma_v / np.diag(vec)
    return eigval

class RVM():
    """ sequential sparse Bayesian Learning Algorithm

        Attributes
        ----------
        _alpha      array
                    hyperparameter alpha
        _beta       array
                    inverse variance of target distribution (sigma^-2)
        _mu         array
                    mean of the weight distribution (Guassian)
        _Sigma      array
                    covariance of the weight distribution
        data        SVMData
                    SVMData to store training data and label with kernel info

        _used       list
                    currently used basis
        rv_idx      list
                    relevance vector index
        _gamma      array
                    well-determinedness: how well w_i is determined by the data
        _s          array
                    sparsity: what extent does the basis overlaps with the other
        _q          array
                    quality: a measure of alignment btn T and y_{-i}
        _w          array
                    scaled weight for inference

        Methods
        -------
        train(self, X, T, **params)
            train relevance vector regression

        use(self, X, T)
            apply the trained RVM (RMR)

        References
        ----------
        Michael E. Tipping and Alex Smola,
            "Sparse Bayesian Learning and the Relevance Vector Machine".
            Journal of Machine Learning Research 1: 211-244. (2001)
    """

    def __init__(self):
        self._alpha = None
        self._beta = None
        self._mu = None
        self._Sigma = None
        self.data = None

        self._used = None
        self.rv_idx = None
        self._gamma = None
        self._s = None
        self._q = None
        self._w = None

        self.dup_update = 0
        self.stdX = None

        # eigen test (pilot)
        self.eig_test = False

    def train(self, X, T, **params):
        """ train RVM

            Parameters
            ==========
            X       array
                    training data
            T       array
                    training target
            tol     float
                    tolerence for termination condition (term if dlogML < tol)
            minDeltaLogAlpha    float
                    minimum delta log alpha (for term condition)
            minDeltaLogBeta     float
                    minimum delta log beta (for recompute parameters)
            alpha_lim   list
                    min and max values for alpha
            ltrace  boolean
                    an option for recording log marginal likelihood
            etrace  boolean
                    an option for recording squared error
            betaUpdate  list
                    a pair of values of start and frequence
                    the frequency-based beta update start at the start
            fixedBeta   boolean
                    do not update beta if True

            *** kernel options
            kernel  string
                    type of kernel (rbf/linear/poly/sigmoid)
            gamma   float
                    a coefficient on product term
            coef    float
                    an additional coefficent for poly and sigmoid
            degree  float
                    degree parameter for polynomial kernel
            errorf  function
                    function that returns error and sqaured error
                    from current basis and mean
        """

        ###########################################################
        # options and initialization
        ###########################################################

        tol = params.pop('tol', 1e-10)
        mdlog_alpha = params.pop('minDeltaLogAlpha', 1e-3)
        mdog_beta = params.pop('minDeltaLogBeta', 1e-3)

        a_min, a_max = params.pop('alpha_lim', [np.finfo(float).eps, 1e10])
        max_step = params.pop('maxstep', 500)
        
        bltrace = params.pop('ltrace', True)
        betrace = params.pop('etrace', False)

        # beta_update default: start at 10 every 5 steps (from SparseBayes)
        beta_update = params.pop('betaUpdate', [10, 5])
        fix_beta = params.pop('fixedBeta', False)

        align_test = params.pop('alignTest', True)
        alignMax = params.pop('alignMax', 0.999)
        verbose = params.pop('verbose', 0)

        errorf = params.pop('errorf', None)

        Xorg = copy(X)
        if self.stdX:
            X = self.stdX.standardize(X)
        """
        np.set_printoptions(precision=3, suppress=False)
        comp = np.hstack((Xorg, X, T.reshape((-1, 1))))
        ind =np.lexsort((comp[:, 0], comp[:, 1]))
        print comp[ind]
        #import pdb; pdb.set_trace()
        """

        init_rvs = params.pop('rvs', None)
        if init_rvs is not None:

            # 2015.4.16. to avoid duplicates
            # duplicates to be replaced by new target
            # (TODO: how to define duplicates for cts domain?)
            rvX = init_rvs['X']
            #print ">>>>> TRANSFERRED RVS: ", rvX.shape[0]
            # find intersection and update duplicates 
            RVXrows = rvX.view([('', rvX.dtype)] * rvX.shape[1])
            Xrows = X.view([('', X.dtype)] * X.shape[1])
            inter_i = np.where(np.in1d(RVXrows, Xrows))[0]
            if len(inter_i) > 0:
                inter_T = []
                for ii in inter_i:
                    dup_i = np.where((X==rvX[ii]).all(axis=1))[0]
                    inter_T.append(T[dup_i][0])

            """ Ref codes: 
                    2D to 1D handling
                        http://stackoverflow.com/questions/11903083/find-the-set-difference-between-two-large-arrays-matrices-in-python
                    intersection indices
                        http://stackoverflow.com/questions/11483863/python-intersection-indices-numpy-array
            """

            X = np.vstack((init_rvs['X'], X))
            # 2015.3.13 modify transferred target for stability
            new_T = self.use(init_rvs['X'], stdX=False)  # replace init_rvs['T']
            # to be consistent with new inputs when the targets are different
            if len(inter_i) > 0:
                new_T[inter_i] = np.asarray(inter_T).reshape(new_T[inter_i].shape)
            if len(T.shape) == 1:
                T = np.concatenate((new_T.flat, T))
            else:
                T = np.vstack((new_T, T))

            self._used = range(len(rvX))

        N, p = X.shape

        # MULTIDIM SUPPORT
        # set multidimensional option
        if len(T.shape) > 1 and T.shape[1] > 1:
            md_target = True
            chg_idx = params.pop('chgidx', -1)
        else:
            md_target = False
            T = T.reshape((-1, 1))

        # kernel setting
        if self.data is None:
            self.data = SVMData(X, T.astype(float))
        else:
            self.data.set_data(X)
            self.data.set_label(T)

        self.data.set_kernel(params.pop('kernel', 'rbf'), **params)
        kernelf_ = self.data.kernelf

        # choice of initial basis to ones
        if False:  # unlike [0], [3] does not contain additional 1s column
            Mall = N + 1
            PHI = np.ones((N, Mall))  
            PHI[:, 1:] = kernelf_(X)   # basis for future use
        else:
            Mall = N
            PHI = kernelf_(X)   # basis for future use

        #normalize basis
        self.scales = np.sqrt(np.sum(PHI ** 2, 0))
        self.scales[self.scales == 0] = 1.

        PHI[...] = PHI / self.scales

        if verbose > 1:
            np.set_printoptions(precision=4, suppress=True)

        old_X = copy(self.data.X[self._used])
                
        ###########################################################
        # Inner function definitions
        ###########################################################
        if errorf is None:
            def errorf(Phi, mu):
                # MULTIDIM SUPPORT
                if md_target:
                    err = T - np.dot(Phi, np.nan_to_num(mu))
                    E = np.diag(np.dot(err.T, err))
                else:
                    err = T - np.dot(Phi, mu)
                    E = np.dot(err.flat, err.flat)
                return err, E


        def compute_params(used, PHI_Phi):
            """ inner function to compute statisticsinner function to compute statistics
                used in step 3 and 10
            """

            Phi = PHI[:, used]
            # rounding error causes non-positive-definite error
            #A = beta * np.dot(Phi.T, Phi) + np.diag(alpha[used])  # (12)
            A = np.dot(beta * Phi.T, Phi) + np.diag(alpha[used])  # (12)
            # to handle zero variance input
            bcomp_err = False
            if beta == np.inf or np.any(np.isinf(A)):
                U = np.ones(A.shape) * np.inf
                #Ui = np.zeros(A.shape)
                Sigma = np.zeros((A.shape[0], A.shape[0]))
                bcomp_err = True
            else:
                try:
                    U = chol(A)
                    Ui = inv(U)
                    Sigma = np.dot(Ui, Ui.T)
                except LinAlgError, e:
                    print e
                    U = np.ones(A.shape) * np.inf
                    Sigma = np.zeros((A.shape[0], A.shape[0]))
                    bcomp_err = True
                    """
                    print used
                    print alpha[used]
                    print A
                    import pdb; pdb.set_trace()
                    """

            # For RL discrete output, store mu and recover nans!
            if md_target:
                nan_idx = np.all(np.isnan(T), 0)
                #mu = beta * np.dot(Sigma, np.dot(Phi.T, np.nan_to_num(T)))  # (13)
                mu = beta * np.dot(Sigma, PHI_T[used])  # (13)
                mu[:, nan_idx] = np.nan
            else:
                #mu = beta * np.dot(Sigma, np.dot(Phi.T, T))  # (13)
                mu = beta * np.dot(Sigma, PHI_T[used])  # (13)

                """ check this later
                """
                if self.eig_test:
                    #eigval = get_eigval(Sigma, beta * np.dot(Phi.T, T))
                    eigval = get_eigval(Sigma, beta * Phi[used].T)
                    print "eigvals:"
                    print eigval
                    print "vars:",
                    print np.diag(Sigma)
                    #import pdb; pdb.set_trace()

            # find a way to compute s and q ----!!!!!!!!!!
            #C = beta * np.eye() + 
            
            #if np.any(np.isinf(A)):

            if bcomp_err:  # np.any(np.isinf(A)):
                mu[:] = 0.  # prevent generating NaN's for use()
                S = np.ones(Phi.shape) * np.nan
                Q = copy(S)
                s = copy(S)
                q = copy(S)
            else:
                """ This ways is straightforward from the paper
                    but, not computationally efficient
                    now new version will use pre-computed array
                print "\t----------------------------------------------------" 
                print "\t\tCHECK_TIME: compute_params: Z",
                import time
                start = time.time()
                # common term in (24) and (25)
                #Z = beta * PHI.T - (beta ** 2) *\
                #    np.dot(PHI.T, np.dot(Phi, np.dot(Sigma, Phi.T)))
                #Z = beta * PHI.T - (beta ** 2) *\
                #    np.dot(np.dot(PHI.T, Phi), np.dot(Sigma, Phi.T))

                # For fast computation, avoid np.dot(NxN, NxN)
                Z = beta * PHI.T - (beta ** 2) *\
                    np.dot(PHI_Phi, np.dot(Sigma, Phi.T))  # use precomputed
                end = time.time()
                print end-start
                print "\t\tCHECK_TIME: compute_params: S", 
                start = time.time()
                S = np.diag(np.dot(Z, PHI))  # (24)
                end = time.time()
                print end-start
                print "\t----------------------------------------------------" 
                import pdb; pdb.set_trace()
                if md_target:
                    Q = np.dot(Z, np.nan_to_num(T))      # (25)
                    Q[np.isnan(T)] = np.nan
                else:
                    Q = np.dot(Z, T)      # (25)
                """
                # if PHI is normalized, diag(PHI' * PHI) = 1
                # only need diagonal ones so element-wise squared sum
                S = beta - np.sum(beta * np.dot(PHI_Phi, Ui) ** 2, 1)
                S = S.reshape((-1, 1))
                Q = beta * (PHI_T - np.dot(PHI_Phi, mu))
                Q = Q.reshape((-1, 1))

                denom = 1 - S[used] / alpha[used].reshape((-1, 1))
                # (23) for s and q
                s = copy(S)
                q = copy(Q)
                s[used] = S[used] / denom
                q[used] = Q[used] / denom


            # log marginal likelihood
            # 3.86 [1] | log (15) [0]
            #   ln p(t|a, b) = 1/2 * M ln a + 1/2 * N ln b
            #                   - E(w) - 1/2 * ln |A| - 1/2 * N ln(2pi)
            #   where
            #       E(w) = 1/2 * b * ||t - phi w||^2 + 1/2 * a w^T w
            #
            #   and log determinant 
            #       from det(A) = det(V)^2 = product(diag(V))^2)
            #       log |A| = 2 * sum(log(diag(V)))
            #
            #   here the constant term with pi is ignored
            musq = mu ** 2
            err, E = errorf(Phi, mu)
            #err = T - np.dot(PHI[:, used], mu)
            #E = np.dot(err.flat, err.flat)
            log_det = np.sum(np.log(np.diag(U)))
            ll = .5 * (np.sum(np.log(alpha[used])) + N * np.log(beta)\
                    - beta * E - np.dot(alpha[used], musq))\
                 - log_det

            return mu, Sigma, s, q, ll, S, Q, E

        def update_base(i, act, new_alpha, S, Q, s, q, PHI_Phi):
            """ update bases based on theta
                contains step 6, 7, and 8
            """
            Phi = PHI[:, used]
            phi = PHI[:, i, None]

            ###########################################################
            # 6. if theta_i > 0 and alpha_i < inf, reestimate alpha_i
            if act == EST:

                if verbose > 1:
                    print "REESTIMATE: ", i

                j = used.index(i)

                # (33) for new Sigma
                delta_alpha = new_alpha - alpha[i]
                kappa = 1. / (Sigma[j, j] + 1./ delta_alpha)
                s_j = Sigma[:, j, None]
                n_Sigma = Sigma - kappa * np.dot(s_j, s_j.T)

                # (34) for new mu
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (mu.shape[0], 1))
                    n_mu = mu - kappa * mu_j * s_j
                else:
                    n_mu = mu - kappa * mu[j] * s_j


                #term = (beta * np.dot(np.dot(PHI.T, Phi), s_j)).flatten()
                term = (beta * np.dot(PHI_Phi, s_j))
                S = S + kappa * term ** 2      # (35)
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (Q.shape[0], 1))
                    Q = Q + kappa * mu_j * term.reshape((-1, 1))   # (36)
                else:
                    Q = Q + kappa * mu[j] * term   # (36)

                alpha[i] = new_alpha

            ###########################################################
            # 7. if theta_i > 0 and alpha_i = infi, add phi_i
            elif act == ADD:

                if verbose > 1:
                    print "APPEND: ", i
                used.append(i)

                PHI_phi = np.dot(PHI.T, phi.reshape((-1, 1)))

                # append PHI' * Phi
                PHI_Phi = np.hstack((PHI_Phi, PHI_phi))

                term = np.dot(Sigma, np.dot(Phi.T, phi)) * beta
                term = term.reshape((-1, 1))  # to column vector
                # (28) for new Sigma
                #   s_ii for Sigma_ii and s_i for Sigma_i
                s_ii = 1. / (new_alpha + S[i])
                s_i = -s_ii * term
                M = len(used)
                n_Sigma = np.zeros((M, M))
                #n_Sigma[:-1, :-1] = Sigma + np.outer(-s_i, term)
                n_Sigma[:-1, :-1] = Sigma + np.dot(-s_i, term.T)
                n_Sigma[:-1, -1] = s_i.flat
                n_Sigma[-1, :-1] = s_i.flat
                n_Sigma[-1, -1] = s_ii

                # (29) for new mu
                mu_i = s_ii * Q[i]
                if md_target:
                    nan_mu_i = np.nan_to_num(mu_i)
                    mu_i_tile = np.tile(nan_mu_i, (mu.shape[0], 1))
                    n_mu = np.vstack((mu - mu_i_tile * term.reshape((-1, 1)), mu_i))
                else:
                    n_mu = np.vstack((mu - mu_i * term, mu_i))

                e_i = phi - np.dot(Phi, term)
                term1 = beta * np.dot(PHI.T, e_i)
                S = S - s_ii * term1 ** 2  # (30)
                if md_target:
                    Q = Q - np.tile(nan_mu_i, (Q.shape[0], 1)) * term1.reshape((-1, 1))
                else:
                    Q = Q - mu_i * term1       # (31)

                alpha[i] = new_alpha

            ###########################################################
            # 8. if theta_i <= 0 and alpha_i < infi, delete phi_i
            elif act == DEL:

                if verbose > 1:
                    print "DELETE: ", i

                j = used.index(i)
                alpha[i] = a_max
                used.remove(i)
                
                # delete i (jth column) from PHI_Phi
                PHI_Phi = np.delete(PHI_Phi, j, 1)

                # (38)
                s_j = Sigma[:, j, None]
                s_jj = Sigma[j, j]
                s_j_s = s_j / s_jj
                #n_Sigma = Sigma - 1./ s_jj * np.dot(s_j, s_j.T)
                n_Sigma = Sigma - np.dot(s_j_s, s_j.T)
                n_Sigma = np.delete(n_Sigma, j, 0)
                n_Sigma = np.delete(n_Sigma, j, 1)

                # (39)
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (mu.shape[0], 1))
                    n_mu = mu - mu_j * s_j_s[0].reshape((mu.shape[0], 1))
                else:
                    mu_j = mu[j]
                    n_mu = mu - mu_j * s_j_s
                n_mu = np.delete(n_mu, j, 0)

                term = (beta * np.dot(np.dot(PHI.T, Phi), s_j))
                S = S + 1. / s_jj * term ** 2
                if md_target:
                    mu_j = np.tile(np.nan_to_num(mu[j]), (Q.shape[0], 1))
                else:
                    term = term
                Q = Q + mu_j / s_jj * term

            if verbose > 1:
                print "USED:", used

            denom = 1 - S[used] / alpha[used].reshape((-1, 1))
            # (23) for s and q
            s = copy(S)
            q = copy(Q)
            s[used] = S[used] / denom
            if md_target:
                q[used] = Q[used] / denom
            else:
                q[used] = Q[used] / denom 

            err, E = errorf(PHI[:, used], n_mu)
            #err = T - np.dot(PHI[:, used], n_mu)
            #E = np.dot(err.T, err)

            #return action, logdelta, n_mu, n_Sigma, S, Q, s, q, E
            return n_mu, n_Sigma, S, Q, s, q, E, PHI_Phi

        ###########################################################
        # Algorithm  [3] Section 4
        ###########################################################

        A_MAX = 1000
        varT = np.var(T)
        B_MAX = 1000000. / np.var(T) if varT != 0 else np.inf
        # 1. initiailze beta = sigma^-2 to some sensible value
        if init_rvs is not None:
            # reuse previous beta
            beta = init_rvs['beta']
            if beta < 0:
                beta = 10.
        else:
            beta = params.pop('beta', 100.)  #10. / np.var(T))

        # 2. initialize alpha_i with a single basis vector phi_i
        # single basis phi
        #used = [0]  # initial used basis index (only the bias is used)

        alpha = np.ones(Mall) * a_max  # set to a_max for free basis
        if init_rvs is not None:
            used = range(len(init_rvs['X']))
            alpha[used] = init_rvs['alpha']
            gamma = init_rvs['gamma']
        else:
            # max projection with the target
            if md_target:
                if chg_idx >= 0:
                    # use chg_idx for the first time only
                    used = [np.nanargmax(np.abs(np.dot(PHI.T, T[:, chg_idx])))]
                else:
                    used = [np.nanargmax(np.mean(np.abs(np.dot(PHI.T, T)), 1))]
            else:
                used = [np.argmax(np.abs(np.dot(PHI.T, T)))]

            phib = PHI[:, used]
            if md_target and chg_idx >= 0:
                phibT = np.dot(phib.T, np.nan_to_num(T[:, chg_idx]))
            else:
                phibT = np.dot(phib.T, T)
            norm_phi = np.dot(phib.T, phib)
            norm_phiT = np.dot(phibT, phibT.T)
            a_i = norm_phi / (norm_phiT / norm_phi - 1 / beta)
            if a_i < 0:
                print "warning: there no relevant basis function at init"
                alpha[used] = A_MAX
            else:
                alpha[used] = a_i
            gamma = np.array([1])

        # PRECOMPUTE for fast computation
        # PHI * Phi (incrementally managed for fast computation)
        PHI_Phi = np.dot(PHI.T, PHI[:, used])
        PHI_T = np.dot(PHI.T, T)

        # for alignment check on basis addition
        aligned_in = []     # existing basis
        aligned_out = []    # basis index that is similar to ones in aligned_in

        prev_ci = 0
        # 3. compute Sigma and mu along with sm and qm for all M bases phi_m
        mu, Sigma, s, q, logML, S, Q, E = compute_params(used, PHI_Phi)
        if bltrace: llike = [logML]
        if betrace: etrace = [E]

        for step in xrange(max_step):
            # 5. Compute theta_i
            if md_target:
                # not to lose precision, square first
                if chg_idx >= 0:
                    q_sq = np.nanmean(q**2, 1)
                else:
                    q_sq = np.mean(q**2,1)
            else:
                q_sq = q ** 2
            theta = q_sq - s

            valid_idx = np.where(theta > 0)[0]
            est_idx = np.intersect1d(valid_idx, used)
            add_idx = np.setdiff1d(valid_idx, est_idx) 
            if align_test:
                add_idx = np.setdiff1d(add_idx, aligned_out)  # already checked
            if len(used) > 1:
                del_idx = np.intersect1d(np.where(theta <= 0)[0], used)
            else:
                del_idx = np.array([])

            # 4. Select a candiate vector phi_i
            #   max increase in marginal likelihood (w/ more computation) - App. A

            # from App. A
            dL = np.zeros(Mall) 
            if md_target:
                # not to lose precision, square first
                if chg_idx >= 0:
                    Q_sq = np.nanmean(Q**2, 1)
                else:
                    Q_sq = np.mean(Q**2, 1)
            else:
                Q_sq = Q ** 2
            #dL[used] = (Q_sq[used] - S[used])

            if len(est_idx):
                new_a = (s[est_idx] ** 2) / theta[est_idx]
                delta = (1. / new_a - 1. / alpha[est_idx])
                # for computational stability
                in_log = 1 + S[est_idx] * delta
                in_log[in_log <= 0] = 1.  # to log to be zero
                log_term = np.log(in_log)
                dL[est_idx] = ((delta * Q_sq[est_idx])\
                              / (delta * S[est_idx] + 1)\
                              - log_term) / 2
                #dL[in_log <= 0] = 0.

            if len(add_idx):
                quot = Q_sq[add_idx] / S[add_idx]
                # for computational stability
                quot[quot <= 0] = 1.  # to log to be zero
                log_quot = np.log(quot)
                dL[add_idx] = (quot - 1 - log_quot) / 2
                #dL[quot <= 0] = 0.

            if len(del_idx):
                dL[del_idx] = -(q_sq[del_idx] / (s[del_idx] + alpha[del_idx])\
                                -np.log(1 + s[del_idx] / alpha[del_idx])) / 2
            
            # priority on deletion
            if len(del_idx) > 0:
                dL[add_idx] = 0
                dL[est_idx] = 0

            ci = np.argmax(dL)
            if ci in add_idx:
                act = ADD
            elif ci in del_idx:
                act = DEL
            elif ci in est_idx:
                act = EST
            else:
                act = NOOP
            if verbose > 1:
                print "selected:", ci, STRACT[act]

            # 11. check convergence
            if act == DEL:
                new_alpha = a_max
                logda = 0
            else:
                new_alpha = s[ci] ** 2 / theta[ci]
                if act == NOOP:
                    logda = 0
                else:
                    logda = np.abs(np.log(new_alpha) - np.log(alpha[ci]))
            dlogML = np.max(dL)
            if act == NOOP or dlogML <= tol or \
               (act == EST and logda < mdlog_alpha and len(del_idx) == 0):
                act = TERM
                #if verbose:
                #    print "dlogML:", dlogML, " stops at", step+1, "steps."
                #break

            # if the new basis is too close to existing one,
            # do not add
            if align_test:
                if act == ADD:
                    p = np.dot(PHI[:, ci], PHI[:, used])
                    aligned_idx = np.where(p > alignMax)[0]
                    n_aligned = len(aligned_idx)
                    if n_aligned > 0:
                        act = SKIP
                        aligned_out.extend([ci] * n_aligned)
                        aligned_in.extend(np.asarray(used)[aligned_idx])
                        #if verbose > 1:
                        #    print "SKIP: ", p
                elif act == DEL:
                    if ci in aligned_in:
                        aligned_idx = np.where(np.asarray(aligned_in) == ci)[0]
                        for di in reversed(aligned_idx):
                            del aligned_in[di]
                            del aligned_out[di]

            if act < TERM:
                mu, Sigma, S, Q, s, q, E, PHI_Phi = update_base(ci, act,
                                                       new_alpha, S, Q, s, q,
                                                       PHI_Phi)
            M = len(used)

            # 9. update beta
            if not fix_beta and\
               (step < beta_update[0] or step % beta_update[1] == 4):
                # work-around zero-noise issue from SparseBayes
                old_beta = beta
                gamma = np.ones(M) - alpha[used] * np.diag(Sigma)  # [0](17)
                if md_target:
                    if np.mean(E) == 0: 
                        beta = B_MAX
                    else:
                        if chg_idx >= 0:
                            beta = min((N - np.sum(gamma)) / np.nanmean(E), B_MAX)
                            #beta = min((N - np.sum(gamma)) / E[chg_idx], B_MAX)
                        else:
                            beta = min((N - np.sum(gamma)) / np.mean(E), B_MAX)
                else:
                    beta = min((N - np.sum(gamma)) / E, B_MAX) if E != 0 else B_MAX

                # 10. recompute/update Sigma, mu and s & q
                # for computational stability
                if old_beta < 0 or beta < 0:
                    dlogb = 0.
                else:
                    dlogb = np.log(beta) - np.log(old_beta)
                if abs(dlogb) > mdog_beta:  # from SparseBayes
                    mu, Sigma, s, q, logML, S, Q, E = compute_params(used, PHI_Phi)
                    dlogML = 0.
                    if not np.isnan(logML):
                        act = RETRY

            logML += dlogML
            if bltrace: llike.append(logML)
            if betrace: etrace.append(E)

            if verbose and step % 10 == 0:
                print step, "> gamma: ", np.sum(gamma), "M=", M, "beta=", beta

            if verbose > 2:
                print "-------------Update Summary-------------"
                print "alpha:", alpha
                print "beta:", beta
                print "S:", S
                print "Q:", Q
                print "s:", s
                print "q:", q
                print "mu:", mu
                print "Sigma:"
                print Sigma
                print "logML:", logML
                #print "dL: ", dL
                print "used:", used
                print "E:", E
                print "----------------------------------------"

            if act == TERM: 
                if verbose:
                    print "dlogML:", dlogML, " stops at", step+1, "steps."
                break

        if verbose and step == max_step-1:
            print "reached the max step", max_step

        c = params.pop('mu_c', 1.)
        old_mu = self._mu

        ### Store the parameters
        self._used = used
        self._alpha = alpha
        self._alpha[used] /= (self.scales[used] ** 2)
        self._beta = beta
        self._mu = mu
        self._Sigma = Sigma
        self.rv_idx = used
        self._gamma = gamma  
        self._s = s
        self._q = q
        if md_target:
            self._w = mu / self.scales[used].reshape((-1, 1))
            # nan-handling
            self._w = np.nan_to_num(self._w)
        else:
            self._w = mu / self.scales[used].reshape((-1, 1))

        if init_rvs is not None:
            del_trrv = np.setdiff1d(range(len(init_rvs['X'])), used) 

            n_init = len(init_rvs['X'])
            self.dbg_trrvs = [n_init, n_init - len(del_trrv)]
        else:
            self.dbg_trrvs = [0, len(used)]
        
        if old_mu is not None:
            old_i = []
            new_i = []
            new_X = self.data.X[used]
            # find matching pairs
            for i in xrange(old_X.shape[0]):
                n_i = np.where((new_X == old_X[i]).all(axis=1))[0]
                if len(n_i) == 1:
                    new_i.append(n_i[0])
                    old_i.append(i)


            self._mu[new_i] = c * self._mu[new_i] + (1 - c) * old_mu[old_i]
            newadd = np.setdiff1d(range(len(used)), new_i)
        else:
            newadd = range(len(used))
        self._mu[newadd] *= c


        return llike

    def use(self, x, **params):
        """
            parameters
            ==========
            x           array 
                        new data input to predict a target
            with_var    boolean
                        return variance if True

            return
            ======
            pred    mean of the predicted target distribution (Gaussian) 
            var     variance of the target distribution
        """

        if len(x.shape) == 1:
            x = x.reshape((-1, 1))

        with_var = params.pop('with_var', False)

        bstd = params.pop('stdX', True)  # for target update in online_train
        if bstd and self.stdX:
            x = self.stdX.standardize(x)

        pred = np.zeros(len(x))
        if with_var:
            var = np.zeros(len(x))
        for i in xrange(len(x)):
            if False:
                phi_x = np.append(1., self.data.kernelf(x[i:i+1, :]))
            else:
                phi_x = self.data.kernelf(x[i:i+1, :])[0]
            phi = phi_x[self._used]
            pred[i] = np.dot(phi, self._w)  # [0]((21)
            if with_var:  # [0](22)
                var[i] = 1 / self._beta + np.dot(phi.T, np.dot(self._Sigma, phi))

        if with_var:
            return pred, var
        else:
            return pred

    def score(self, X, T):
        """ compute root-mean-square-error """
        pred = self.use(X)
        return np.sqrt(np.mean((T - pred) ** 2))

    def _get_alpha(self):
        return self._alpha[self._used]
        
    def _get_beta(self):
        return self._beta
        
    def _get_gamma(self):
        return self._gamma
        
    rvmAlpha = property(_get_alpha)
    rvmBeta = property(_get_beta)
    rvmGamma = property(_get_gamma)


if __name__ == '__main__':

    if True:
        X = np.linspace(-10, 10, 101)
        T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3 #+ np.random.rand()
        X = X.reshape((-1,1)) 

        # for test
        x = X

        #gamma = 6.
        #gamma = 16.
        gamma = 2 
        tol = 1e-3
    elif False: #True:
        """ Test with different outputs for the same input (avg?)
        """
        #X = np.array([0, 1, 0, 1, 2, 3, 0, 1, 3, 2, 0])
        #T = np.array([0, 0, 1, 1, 1, 2, 1, 2, 3, 1, 0])
        X = np.array([0, 1, 0, 1, 2, 3, 0, 3, 2, 0, 3, 3, 0, 0, 0, 0])
        T = np.array([1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1])
        X = X.reshape((-1,1)) 
        x= X
        gamma = 1 #0.01 
        tol = 1e-3
    else:
        data = np.array([
            [0.000000, 0.349486],
            [0.111111, 0.830839],
            [0.222222, 1.007332],
            [0.333333, 0.971507],
            [0.444444, 0.133066],
            [0.555556, 0.166823],
            [0.666667, -0.848307],
            [0.777778, -0.445686],
            [0.888889, -0.563567],
            [1.000000, 0.261502],
            ])

        X = data[:, :1]
        T = data[:, 1]
        
        x = np.linspace(0, 1, 50)  #.reshape((-1, 1))

        #gamma = 0.0025
        gamma = 0.05
        tol = 1e-8

    
    
    rvr = RVM()
    rvr.eig_test = False  #True
    ltrace = rvr.train(X, T, kernel='rbf', gamma=1./gamma, tol=tol, verbose=0)

    #print rvr._alpha
    print len(rvr.rv_idx), "RVs:"
    print rvr.rv_idx

    import matplotlib.pyplot as plt
    fig = plt.figure()
    fig.add_subplot(121)

    plt.plot(X, T, 'bo');
    plt.plot(X[rvr.rv_idx], T[rvr.rv_idx], 'ro');

    y = rvr.use(x)
    pres = np.hstack((X, T.reshape((-1, 1)), y.reshape((-1, 1))))
    ind = np.argsort((pres[:, 0]))
    print pres[ind]

    plt.plot(x, y, 'r-');

    fig.add_subplot(122)

    plt.plot(ltrace)
    plt.xlabel("steps")
    plt.ylabel("log marginal likelihood")

    plt.show()
