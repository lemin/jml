""" Relevant Vector Machines for RL

    first, adopted dlib and dlib-py to implement RVMQ
    but, now removed dependency to dlib and used jml.rvm

                                by lemin (Jake Lee)
    
    For reference, please take a look at
    [1] Vladimir Vapnik. "The Nature of Statistical Learning Theory."
        Springer-Verlag, 1995
    [2] Michael E. Tipping and Alex Smola, "Sparse Bayesian Learning and the Relevance Vector Machine".
        Journal of Machine Learning Research 1: 211?244. (2001)
    [3] Michael E. Tipping and A. C. Faul. "Fast marginal likelihood maximisation for sparse Bayesian models."
        In C. M. Bishop and B. J. Frey (Eds.), Proceedings of the Ninth International Workshop on Artificial Intelligence and Statistics, Key West, FL, Jan 3-6 (2003)
    [4] David P. Wipf and Srikantan Nagarajan, "A New View of Automatic Relevance Determination,"
        In J.C. Platt, D. Koller, Y. Singer, and S. Roweis, editors, Advances in Neural Information Processing Systems 20, MIT Press, 2008.
    [5] David P. Wipf and Srikantan Nagarajan, "Sparse Estimation Using General Likelihoods and Non-Factorial Priors,"
        In Advances in Neural Information Processing Systems 22, 2009.
    [6] Bishop C.M, Pattern Recognition and Machine Learning, 
        Springer, New York, 2006

    implementation references:
    - SparseBayes Matlab http://www.vectoranomaly.com/downloads/downloads.htm
    - dlib-py by Liam Childs: https://github.com/childsish/dlib-py
    - Dlib C++: http://dlib.net/
    - kernel machine library (KML): http://www.terborg.net/research/kml/reference/rvm.html
"""
from rvm import RVM
from orvm import oRVM, omRVM
import numpy as np
from ..util.exp import Standardizer


class RVMQ(RVM):
    """ Relevant Vector Regression for Reinforcement Learning
    """

    def __init__(self, **params):
        """ 
            Parameters
            ==========
            gamma   float
                    learning rate (different from gamma for RBF kernel)
                    (distinguished from RVM._gamma for well-determinedness
                     or kernel parameter gamma)
        """
        RVM.__init__(self)
        self.gamma = params.pop('gamma', 0.9)
        self._dim = 1

        stateRange = params.pop('stateRange', None)
        if stateRange is not None:
            self.stdX = Standardizer(stateRange)

    def set_gamma(self, gamma):
        self.gamma =  gamma

    def set_target_dim(self, dim):
        self._dim = dim

    def train(self, X, Y, R, Q, Ai, **params):
        """ reinforcement learning training with RVM

            Parameters
            ==========
            X, Y, R, Q  array
        """
        init_rvs = params.pop('rvs', None)
        T = R + self.gamma * Q
        self._dim = T.shape[1]
        
        if init_rvs is not None:
            init_rvs['T'] = init_rvs['R'] + self.gamma * init_rvs['Q']

        RVM.train(self,
                  X,
                  T.flatten() if self._dim == 1 else T,
                  rvs=init_rvs,
                  #errorf=None if Ai is None else errorf,
                  verbose=0,
                  #alignMax=0.7,
                  **params)

        return self.score(X, T)

    def use(self, X, **params):
        if self.data is None:
            return np.zeros((len(X), self._dim))
        return RVM.use(self, X, **params).reshape((-1, self._dim))

    def get_RVs(self):
        if self.rv_idx is not None and len(self.rv_idx) > 0:
            Xu = self.stdX.unstandardize(self.data.X)
            return Xu[self.rv_idx]
        return None


class oRVMQ(oRVM, omRVM):
    """ online Relevant Vector Regression for Reinforcement Learning
    """

    def __init__(self, **params):
        """ Parameters
            ==========
            gamma   float
                    learning rate (different from gamma for RBF kernel)
                    (distinguished from RVM._gamma for well-determinedness
                     or kernel parameter gamma)
        """
        self._tmp_dbg = False
        m = params.pop('targetDim', 1)
        self._dim = m
        if m == 1:
            oRVM.__init__(self)
            self._super = oRVM
        else:
            omRVM.__init__(self, m)
            self._super = omRVM
        self.gamma = params.pop('gamma', 0.9)

        stateRange = params.pop('stateRange', None)
        if stateRange is not None:
            self.stdX = Standardizer(stateRange)

        self._test_prevX = None
        self._test_prevT = None

    def set_gamma(self, gamma):
        self.gamma =  gamma

    def set_target_dim(self, dim):
        self._dim = dim

    def update(self, X, Y, R, Q, Ai, **params):
        """ single-sample reinforcement learning training with RVM

            Parameters
            ==========
            X, Y, R, Q  array
        """
        if self._dim > 1:
            N = X.shape[0]
            Ri = np.ones((N, self._dim)) * np.nan
            Ri[np.arange(N), Ai.flatten()] = 1
            R = Ri * R.reshape((-1, 1))
        T = R + self.gamma * Q

        self._dim = T.shape[1]

        if Ai is not None:
            def errorf(T, Phi, mu):
                Y = np.dot(Phi, mu)
                err = np.zeros(T.shape)
                ai = Ai.flatten()
                err[:, ai] = T[:, ai] - Y[:, ai]
                E = np.diag(np.dot(err.T, err))
                return err, E

        vbs = 0
        if self._dim == 1:
            ll = oRVM.online_train(self,
                            X,
                            T.flatten() if self._dim == 1 else T,
                            errorf=None if Ai is None else errorf,
                            verbose=vbs,
                            chgidx=-1 if Ai is None else int(Ai),
                            **params)
        else:
            ll = omRVM.online_train(self,
                            X,
                            T,
                            verbose=vbs,
                            **params)

        """
        print "________________________________________________________________"
        y = self.use(X)
        np.set_printoptions(precision=3, suppress=True, linewidth=150, threshold=np.nan)
        present = np.hstack((X, R, Q, T, y, T-y))
        ind =np.lexsort((present[:, 0], present[:, 1]))
        print present[ind, :], np.unique(present[:, 2])
        print "________________________________________________________________"
        """
 
        return ll

    def use(self, X, **params):
        if self._dim == 1:
            if self.data is None:
                return np.zeros((len(X), self._dim))
            return oRVM.use(self, X, **params).reshape((-1, self._dim))
        else:
            return omRVM.use(self, X, **params).reshape((-1, self._dim))

    def get_RVs(self):
        if self._dim == 1:
            if len(self._used) > 0:
                return self.stdX.unstandardize(self.data.X[self._used])
                #return self.data.X[self._used]
            return None
        else:
            return [rvm.stdX.unstandardize(rvm.data.X[rvm._used]) for rvm in self._rvms] 
            #return [rvm.data.X[rvm._used] for rvm in self._rvms] 

    ### selective inheritance
    def train(self, X, T, **params):
        return self._super.train(self, X, T, **params)        

    def online_train(self, x, t, **params):
        return self._super.online_train(self, X, T, **params)        

    def get_alpha(self):
        return self._super.get_alpha(self)
        
    def get_beta(self):
        return self._super.get_beta(self)
        
    def get_gamma(self):
        return self._super.get_gamma(self)

    def get_used(self):
        return self._super.get_used(self)

    def get_s(self):
        return self._super.get_s(self)

    def get_q(self):
        return self._super.get_q(self)

    rvm_alpha = property(get_alpha)
    rvm_beta = property(get_beta)
    rvm_gamma = property(get_gamma)
    rvm_used = property(get_used)
    rvm_s = property(get_s)
    rvm_q = property(get_q)
