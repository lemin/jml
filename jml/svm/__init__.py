"""SVM for regression and classification"""

__all__ = ["svm", "svr", "aosvr", "svmdata"]
