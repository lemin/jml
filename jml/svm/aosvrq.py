""" Online Support Vector Regression for Reinforcement Learning
    
    For reference, please take a look at

    ..  [1] Junshui Ma, James Theiler, and Simon Perkins,
        Accurate On-line Support Vector Regression,
        Nueral Computation, vol.15, no.11, pp.2683-2703, 2003

    ..  [2] Gert Cauwenberghs and Tomaso Poggio,
        Incremental and Decremental Support Vector Machine Learning,
        Advances in neural information processing systems, pp409-415, 2001

    ..  [3] Francesco Parrella
        Online Support Vector Regression,
        Thesis in Information Science, University of Genoa, Italy, 2007,
        http://onlinesvr.altervista.org/, accessed 17 Nov 2011

    ..  [4] Mario Martin,
        On-line Support Vector Machines for Function Approximation,
        Techn. report, Universitat Politecnica de Catalunya, 2002

                                by Jake Lee (lemin)

    history:
        4/13/12
        + split AOSVRQ from aosvr
"""
import numpy as np
from aosvr import AOSVR


class AOSVRQ(AOSVR):
    """ Accurate Online Support Vector Regression for Reinforcement Learning

        Parameters
        ==========
        gamma   float
                learning rate (different from gamma for RBF kernel)
                (distinguished from AOSVR.gamma for incremental learning)
    """
    def __init__(self, **params):
        AOSVR.__init__(self)
        self._gamma = params.pop('gamma', .9)

    def set_gamma(self, gamma):
        self._gamma =  gamma

    def train(self, X, Y, R, Q, **params):
        """ reinforcement learning training with AOSVR

            Parameters
            ==========
            X, Y, R, Q  array
        """
        n_samples, n_dim = X.shape
        self.tol = params.pop('tol', 1e-3)
        self.eps = params.pop('eps', 1e-3)
        self.C = params.pop('C', 0.1)

        T = R + self._gamma * Q
        self._dim = T.shape[1]
        self.batch_train(X, T.flat, **params)  # flatten T for dimensional compat

        # remove non-supporting data set for efficiency
        # an option for selecting forgetting or shrinking
        # ==> this way turned out to be way to slow - deprecated
        """
        max_sample = params.pop('maxsample', -1)  # default shrink only
        if max_sample == -1:
            self.shrink()
        else:
            import time
            cur = time.time()
            n_unlearn = self.data.n_samples - max_sample
            print n_unlearn, "...."
            if n_unlearn > 0:
                self.unlearn_N(n_unlearn)
            print "unlearn>> ", n_unlearn,">>", time.time() - cur
        """

    def use(self, X):
        if self.data is None:
            return np.zeros((len(X),1)) # return 0 as an prediction
        # reshape to match w/ rltrain
        return AOSVR.use(self, X).reshape((-1, self._dim))
