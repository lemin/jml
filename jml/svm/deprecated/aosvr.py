""" Online Support Vector Regression
    
    For reference, please take a look at

    ..  [1] Junshui Ma, James Theiler, and Simon Perkins,
        Accurate On-line Support Vector Regression,
        Nueral Computation, vol.15, no.11, pp.2683-2703, 2003

    ..  [2] Gert Cauwenberghs and Tomaso Poggio,
        Incremental and Decremental Support Vector Machine Learning,
        Advances in neural information processing systems, pp409-415, 2001

    ..  [3] Francesco Parrella
        Online Support Vector Regression,
        Thesis in Information Science, University of Genoa, Italy, 2007,
        http://onlinesvr.altervista.org/, accessed 17 Nov 2011

    ..  [4] Mario Martin,
        On-line Support Vector Machines for Function Approximation,
        Techn. report, Universitat Politecnica de Catalunya, 2002

                                by Jake Lee (lemin)

    example usage:

        X = np.linspace(-10, 10, 100)
        T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3
        X = X.reshape((-1,1))

        svr = AOSVR(eps=eps, C=C)
        svr.batch_train(X, T, kernel='rbf', gamma=gamma)
        Y = svr.use(X)
 
    history:
        12/06/11
        - batch_train pops kernel options and do not pass it to online_train
          when it does not initiate a R matrix
        - remove equality in case 0
        - use() assumes only one data sample. fix it to handle multiple data samples
        + add an option for kernel matrix creation for efficiency of small data

        12/05/11
        - update theta for new data in the middle of processing
        - error in beta1 computation for add_Rmat

        12/04/11
        - fix index error after modification on 12/01

        12/01/11
        + modify to follow Parrella's structure
          (compute gamma for all data
           add the new data in the beginning for gamma_c computation ...)

        11/27/11
        + AOSVRQ for reinforcement learning added

        11/26/11
        + shriking and unlearning are added

        11/24/11
        + first built AOSVR

    last modified: 12/07/11
        - in add_Rmat() and remove_Rmat(), 
          when gamma is zero, do not add/substract a update term
        - in minimum variation calculation, avoid divide-by-zero

    TODO:
        - modify libsvm to support online SVR (Asa's advice)
        - check unlearning
        
        --------- old logs --------------------------------------------
        V. still some thetas are out of box constraint
            ==> Parrella stabilizes after training

        *. Parrella uses stabilization to make all optimization samples satisfy KKT conditions
        V. double-check my unlearning codes 
        V. make stabilization code for learning
        V. check why test in learning results in errors! current PDB break
            V. checking min variation codes differ but it does not affect on the result
            V. check the update routinge and check how much it differs
               and then find out what the cause is 
        2. test if stabilization work (possible infinite loop- add optoins? )
            V. with linear, stabilization do not require
            0. check unlearn code later...
        V. test with rbf kernel

        V. logging on top 
"""
import numpy as np
from copy import deepcopy as copy
from svr import SVR
from svmdata import SVMData
#import matplotlib.pyplot as plt
import sys
import pdb
debug = 0

MACHINE_EPS=1e-8

class AOSVR():
    """ Accurate Online Support Vector Regression

        Parameters
        ----------

        Attributes
        ----------
        alpha, alpha_   array  shape=(n_samples)
            Lagrangian multipliers
        F   array, shape=(n_samples)
            error cache
        b   float
            bias
        data    SVMData
            training data
        I0a, I0b, I0, I1, I2, I3    list
            index sets based on alpha and alpha_ conditions

        S,E,R    list
            index list for support vectors, error data, and remainders

        Methods
        -------
        batch_train(self, X, T, **params)
            train support vector regression with batch of data

        online_train(self, xc, yc, **params)
            online (incremental) support vector regression

        use(self, X, return_kernel=False):
            apply the trained SVR

        score(self, X, T)
            compute RMSE of the trained SVR

        add_Rmat(self, beta, gamma, add_idx)
        remove_Rmat(self, k)
            update R matrix (withouting using the costly matrix inverse)

        update_index_after_data_removal(self, idx)
        remove_data_from_R(self, idx)
            utitily function for handling index sets (S, E, R)

        shrink(self, eps=1e-8)
            shrinking by removing insignificant data samples

        unlearn(self, idx)
            decremental learning (removal of a sample by index)

        stabilize(self)
            check trained model and see if any sample violates KKT conditions
            if so, stabilize the model (by relearning it after decrementing)
            
        get_n_samples_in_KKT(self):
        check_KKT_conditions(self, idx):
            utility function for checking KKT conditions

        References
        ----------
        Ma et al. Accurate On-line Support Vector Regression.
            http://www.mitpressjournals.org/doi/abs/10.1162/089976603322385117
    """
    def __init__(self, **params):
        #self.alpha = np.zeros(0)
        #self.alpha_ = np.zeros(0)
        self.theta = np.zeros(0)
        self.b = 0
        self.data = None
        self.Rmat = None
        self.S = []
        self.E = []
        self.R = []

        self.tol = params.pop('tol', 1e-3)
        self.eps = params.pop('eps', 1e-3)
        self.C = params.pop('C', 0.1)

        # theta history for shrinking
        self.theta_history = None


    def batch_train(self, X, T, **params):
        """ train AOSVR with batch data using online_train
            
            Two initialization methods are provided
            one is initiating with a batch SVR solution (option 'svr')
            the other is with two samples (eq. 3.14 in Ma. et al)

            without initiation, online_train create an initial Rmat 
            with eq. 3.11 in Ma. et al.

            Parameters
            ==========
            init_R      boolean
                        initiate R matrix before starting batch learning
            init_R_size int
                        the number of samples for initial R matrix
            init_R_opt  string (svr/two)
                        options for initial R matrix creation (Batch SVR/Two sample)
            init_R_only boolean
                        an option if it proceeds learning or not 
            stabilize   boolean
                        an option for stabilization (Parrella) after training
                        
            kernel      string
                        select a kernel (linear/rbf/polynomial/sigmoid)

            ...         pass kernel parameters as stated in SVMData.set_kernel
            maxsample   int
                        the number of samples to maintain (monitoring window size)
        """
        
        n_samples, n_dim = X.shape

        init_Rmat = params.pop('init_R', False)
        init_Rmat_opt = params.pop('init_R_opt', 'two')
        init_Rmat_size = params.pop('init_R_size', 2)
        init_Rmat_only = params.pop('init_R_only', False)
        bstabilize = params.pop('stabilize', False)

        max_sample = params.pop('maxsample', None)

        if init_Rmat:  
            kernel = params.pop('kernel', 'linear')

        if init_Rmat and init_Rmat_opt == 'svr':

            if debug >= 1: print "initiating R matrix with a batch SVR"

            svr = SVR()
            svr.train(X[:init_Rmat_size], T[:init_Rmat_size])
            # set R, theta, and b
            self.b = svr.b
            self.theta = svr.alpha - svr.alpha_
            # kernel matrix of SV's
            svI = np.logical_and(abs(self.theta) > 0, abs(self.theta) < self.C)
            if np.sum(svI) == 0:
                print "no support vector is found!"
                sys.exit(1)
            eI = abs(self.theta) == self.C
            #rI = abs(self.theta) == 0
            n_sv = np.sum(svI)
            SVs = svr.data.X[svI]
            Qss = svr.data.kernelf(SVs, SVs)
            
            R = np.hstack((np.ones((n_sv, 1)), Qss))
            R = np.vstack((np.ones((1, n_sv+1)), R))
            R[0, 0] = 0
            self.Rmat = np.linalg.inv(R)

            # set S and data
            dataI = svI + eI
            self.data = SVMData(X[dataI], T[dataI])
            self.data.set_kernel(kernel, **params)
            self.S = list(np.where(svI)[0])
            self.E = list(np.where(eI)[0])
            #self.R = list(np.where(rI)[0])
            
            start_sample = init_Rmat_size

        elif init_Rmat and init_Rmat_opt == 'two':

            if debug >= 1: print "initiating R matrix with two samples"
            # start from scratch 
            self.data = SVMData(X[:2], T[:2])
            self.data.set_kernel(kernel, **params)

            self.theta = np.zeros(2)
            k11, k12 = self.data.kernelf(0)
            tmp = (T[0] - T[1] - 2 * self.eps) / 2 * (k11 - k12)
            self.theta[0] = max(0, min(self.C, tmp))
            self.theta[1] = -self.theta[0]
            self.b = (T[0] + T[1]) / 2.

            # update set R, S, E
            for i in xrange(2):
                if self.theta[i] == 0:
                    self.R.append(i)
                elif abs(self.theta[i]) == self.C:
                    self.E.append(i)
                elif abs(self.theta[i]) > 0 and abs(self.theta[i]) < self.C:
                    self.S.append(i)

            ls = len(self.S)
            if ls > 0:
                SVs = self.data.X[self.S]
                Qss = self.data.kernelf(SVs, SVs)
                R = np.hstack((np.ones((ls, 1)), Qss))
                R = np.vstack((np.ones((1, ls+1)), R))
                R[0, 0] = 0
                self.Rmat = np.linalg.inv(R)

            start_sample = 2

        else:
            start_sample = 0

        if init_Rmat_only:
            return

        #import time
        #cur = time.time()
        for i in xrange(start_sample, n_samples):
            if debug >= 1:
                print "sample ", i, "----------------------------"
                print "New DATA:", X[i]
                print "New LABEL:", T[i]

            """
            ## redundancy reduction -- move this part to AOSVRQ (TODO)
            if self.data is not None and self.data.X.shape[0] > 0:
                # matching rows except Q values
                matched_row = np.where(np.all(X[i][:-1] == self.data.X[:, :-1], 1))[0]
                if len(matched_row) > 0:
                    if np.all(X[i, -1] == self.data.X[matched_row, -1]):
                        # repeated data, skip
                        #print "repeating data, skip"
                        continue
                    else:
                        # conflicting data, relearning
                        #print "relearning conflicting data"
                        self.unlearn(matched_row[0])

            print "starting online training", i
            """
            self.online_train(X[i], T[i], **params)

            #for i in self.R:
            #    self.unlearn(i)  # remove non-SV's first

            if max_sample is not None and self.data.n_samples > max_sample:
                self.unlearn(0)  # remove oldest
                ### TODO: check unlearning right away is better 
                ###       or unlearning batch of several samples is better
                ### TODO  to prevent, removing significant SV's leaving remainders
                ###       after online_train, remove R members first
        #print "batch_train w/ maxsample>> ", max_sample,">>", time.time() - cur
        #print "redundancy removal >>", time.time() - cur

        if bstabilize:
            self.stabilize()

    def online_train(self, xc, yc, **params):
        """ online (incremental) learning for SVR
            
            Parameters
            ==========
            kernel_matrix   boolean
                            an option to use a kernel matrix cache
            unlearn         int
                            default(-1) is for learn,
                            others (>=0) for unlearning the specific sample

            References
            ==========
        """

        use_kernel_matrix = params.pop('kernel_matrix', False)
        dec_learn_idx = params.pop('unlearn', -1)
        if dec_learn_idx != -1:
            dec_learn = True
        else:
            dec_learn = False

            if self.data is None:
                kernel = params.pop('kernel', 'linear')
                self.data = SVMData(xc, yc)
                self.data.set_kernel(kernel, **params)
            else:
                self.data.add_data(xc, yc)
            self.theta = np.append(self.theta, 0.)

        if use_kernel_matrix:
            fx, Q = self.use(self.data.X, True)
        else:
            fx = self.use(self.data.X)
        fx = fx.flat
        hx = fx - self.data.label
        if debug >= 2:
            print hx

        if dec_learn:
            theta_c = self.theta[dec_learn_idx]
        else:
            theta_c = 0
        if use_kernel_matrix:
            Qc = Q[:, -1:]
        else:
            Qc = self.data.kernelf(self.data.n_samples - 1).reshape((-1, 1))
        hx_c = hx[-1]

        if dec_learn:
            q = np.sign(hx_c)
        else:
            q = np.sign(-hx_c)

            if abs(hx_c) < self.eps:
                if debug >= 1: print "case 0: in the margin. into R"
                self.R.append(self.data.n_samples-1)
                return

        loop_cnt = 0
        while True: ## check KKT condition of xc
            loop_cnt += 1
            if loop_cnt > (self.data.n_samples + 1) * 100:
                print "samples are examined more than 100 times - infinite loop ?"
                sys.exit(1)

            hx_c = hx[-1] # reset after update
            if dec_learn:
                q = np.sign(hx_c)
            else:
                q = np.sign(-hx_c)

            #N = self.E + self.R
            if len(self.S) > 0:
                Qsc = Qc[self.S]
                Qsc1 = np.vstack(([1], Qsc))
                beta = np.dot(-self.Rmat, Qsc1)

                if use_kernel_matrix:
                    Qs = np.hstack((np.ones((Q.shape[0], 1)), Q[:, self.S]))
                else:
                    ### TODO: FASTER!
                    Qs = np.asarray(map(lambda i: self.data.kernelf(i), self.S)).T
                    Qs = np.hstack((np.ones((self.data.n_samples, 1)), Qs))
                gamma = Qc + np.dot(Qs, beta)
                gamma_c = gamma[-1, 0]
            else:
                beta = np.inf
                gamma = np.ones((self.data.n_samples, 1))
                gamma_c = 1
                    

            ###############################
            # bookkeeping procedure
            Ls = []
            Le = []
            Lr = []

            # check the new sample xc
            if dec_learn:  # no case 1
                Lc1 = np.inf
            else:
                if debug >= 2:
                    print "hx_c:" , hx_c, "gamma_c : ", gamma_c
                if gamma_c <= 0:
                    Lc1 = np.inf
                else:
                    Lc1 = (-hx_c - q * self.eps) / gamma_c
            if dec_learn:  # |theta_c| > 0 --> 0
                Lc2 = -theta_c
            else:
                if len(self.S) > 0:
                    Lc2 = q * self.C - theta_c
                else:
                    # when a support vector is taken out, skip this case
                    Lc2 = np.inf

            # check each sample xi in the set S
            for i, si in enumerate(self.S):#xrange(len(self.S)):
                if False:
                    if q * beta[i+1, 0] > 0 and self.theta[si] >= 0\
                        and self.theta[si] < self.C:
                        Ls.append((self.C - self.theta[si]) / beta[i+1, 0])

                    elif q * beta[i+1, 0] > 0 and self.theta[si] < 0\
                        and self.theta[si] >= -self.C:
                        Ls.append(-self.theta[si] / beta[i+1, 0])

                    elif q * beta[i+1, 0] < 0 and self.theta[si] > 0\
                        and self.theta[si] <= self.C:
                        Ls.append(-self.theta[si] / beta[i+1, 0])

                    elif q * beta[i+1, 0] < 0 and self.theta[si] <= 0\
                        and self.theta[si] > -self.C:
                        Ls.append((-self.C - self.theta[si]) / beta[i+1, 0])
                    else:
                        Ls.append((np.inf))
                else:
                    if abs(beta[i+1]) < MACHINE_EPS: # == 0:
                        Ls.append(np.inf)
                    elif q * beta[i+1, 0] > 0:
                        if hx[si] > 0:
                            if self.theta[si] < -self.C:
                                Ls.append((-self.theta[si] - self.C) / beta[i+1, 0])
                            elif self.theta[si] <= 0:
                                Ls.append(-self.theta[si] / beta[i+1, 0])
                            else:
                                Ls.append(np.inf)
                        else:
                            if self.theta[si] < 0:
                                Ls.append(-self.theta[si] / beta[i+1, 0])
                            elif self.theta[si] < self.C:
                                Ls.append((-self.theta[si] + self.C) / beta[i+1, 0])
                            else:
                                Ls.append(np.inf)
                                
                    else:
                        if hx[si] > 0:
                            if self.theta[si] > 0:
                                Ls.append(-self.theta[si] / beta[i+1, 0])
                            elif self.theta[si] >= -self.C:
                                Ls.append((-self.theta[si] - self.C) / beta[i+1, 0])
                            else:
                                Ls.append(np.inf)
                        else:
                            if self.theta[si] > self.C:
                                Ls.append((-self.theta[si] + self.C) / beta[i+1, 0])
                            elif self.theta[si] >= 0:
                                Ls.append(-self.theta[si] / beta[i+1, 0])
                            else:
                                Ls.append(np.inf)

            # check each sample xi in the set E
            gi = 0
            for gi, i in enumerate(self.E):
                if False:
                    Le.append((-hx[i] - np.sign(q * gamma[i, 0]) * self.eps) / gamma[i, 0]) # (gamma_4_all)
                else:
                    if abs(gamma[i, 0]) < MACHINE_EPS: #== 0:
                        le_val = np.inf
                    elif q * gamma[i, 0] > 0:
                        if self.theta[i] > 0:
                            if hx[i] < -self.eps:
                                le_val = (-hx[i] - self.eps) / gamma[i, 0]
                            else:
                                le_val = np.inf
                        else:
                            if hx[i] < self.eps:
                                le_val = (-hx[i] + self.eps) / gamma[i, 0]
                            else:
                                le_val = np.inf
                    else:
                        if self.theta[i] > 0:
                            if hx[i] > -self.eps:
                                le_val = (-hx[i] - self.eps) / gamma[i, 0]
                            else:
                                le_val = np.inf
                        else:
                            if hx[i] > self.eps:
                                le_val = (-hx[i] + self.eps) / gamma[i, 0]
                            else:
                                le_val = np.inf
                        
                    Le.append(le_val) # mod from Parrella 

            # check each sample xi in the set R
            for gi, i in enumerate(self.R):
                if False:
                    Lr.append((-hx[i] - np.sign(q * gamma[i, 0]) * self.eps) / gamma[i, 0]) #(gamma_4_all) 
                else:
                    if abs(gamma[i, 0]) < MACHINE_EPS: # == 0:
                        lr_val = np.inf
                    elif q * gamma[i, 0] > 0:
                        if hx[i] < -self.eps - self.tol:
                            lr_val = (-hx[i] - self.eps) / gamma[i, 0]
                        elif hx[i] < self.eps + self.tol:
                            lr_val = (-hx[i] + self.eps) / gamma[i, 0]
                        else:
                            lr_val = np.inf
                    else:
                        if hx[i] > self.eps + self.tol:
                            lr_val = (-hx[i] + self.eps) / gamma[i, 0]
                        elif hx[i] > -self.eps - self.tol:
                            lr_val = (-hx[i] - self.eps) / gamma[i, 0]
                        else:
                            lr_val = np.inf
                    Lr.append(lr_val) # mod from Parrella
                
            Ls = np.abs(Ls)
            Le = np.abs(Le)
            Lr = np.abs(Lr)
            # nan value processing? should it prompt an error?
            #Ls[np.isnan(Ls)] = np.inf
            #Le[np.isnan(Le)] = np.inf
            #Lr[np.isnan(Lr)] = np.inf

            #####################################################################
            # DEBUG - To check validity
            if debug > 0:
                valid = True
                for i in self.S:
                    if self.theta[i] != 0 and hx[i] != 0 and\
                        np.sign(hx[i]) == np.sign(self.theta[i]):
                        print "wrong support set", i, "hx: ", hx[i], "theta:", self.theta[i]
                        valid = False

                for i in self.E:
                    if np.sign(self.theta[i] * hx[i]) > 0 or\
                        abs(hx[i]) < self.eps - self.tol: #0.000001:
                        print "wrong error set", i, "hx: ", hx[i]
                        valid = False

                for i in self.R:
                    if abs(hx[i]) > self.eps + self.tol: #0.000001:
                        print "wrong remaining set", i, "hx: ", hx[i]
                        valid = False

                if not valid and debug >= 2:
                    pdb.set_trace()
            #####################################################################
            
            if len(Ls) > 0:
                Ls_min, ls = min(Ls), np.argmin(Ls)
            else:
                Ls_min = np.inf
            if len(Le) > 0:
                Le_min, le = min(Le), np.argmin(Le)
            else:
                Le_min = np.inf
            if len(Lr) > 0:
                Lr_min, lr = min(Lr), np.argmin(Lr)
            else:
                Lr_min = np.inf

            if debug >=2 :
                print "Lc1: ", Lc1
                print "Lc2: ", Lc2
                print "Ls: ", Ls
                print "Le: ", Le
                print "Lr: ", Lr

            L = [abs(Lc1), abs(Lc2), abs(Ls_min), abs(Le_min), abs(Lr_min)]
            flag = np.argmin(L)
            dtheta_c = q * L[flag]
            flag += 1  # case number
            if flag == 3:
                l = self.S[ls]
                li = ls
            elif flag == 4:
                l = self.E[le]
                li = le
            elif flag == 5:
                l = self.R[lr]
                li = lr
            else:
                l = -1
                li = -1
            ############################ end of bookkeeping

            # Update theta, b and hx
            if len(self.S) > 0:
                si = self.S
                dtheta = beta.flat * dtheta_c
                self.theta[si] += dtheta[1:]
                self.b += dtheta[0]
                theta_c += dtheta_c
                self.theta[-1] = theta_c
                hx += gamma.flat * dtheta_c  # update h(x)
            else:
                self.b += dtheta_c
                hx += dtheta_c

            if debug >= 2:
                print "theta=", self.theta
                print "bias=", self.b

            # update sets S, R, and E
            if flag == 1 and not dec_learn: # case 1
                if debug >= 1: print "case 1: new data to S" 

                # add xc to data set and to S
                self.S.append(self.data.n_samples-1)
                # update R (3.13)
                self.add_Rmat(beta, gamma_c, self.data.n_samples - 1)

            elif flag == 2:  # case 2
                if debug >= 1: print "case 2: new data to E" 

                if dec_learn:
                    self.data.remove_data(dec_learn_idx)
                    self.update_index_after_data_removal(dec_learn_idx)
                else:
                    self.E.append(self.data.n_samples-1)
                    self.theta[-1] = np.sign(theta_c) * self.C

            elif flag == 3:  # case 3
                # bound box-constraint - parella
                abs_wl = abs(self.theta[l])
                if abs_wl < abs(self.C - abs_wl):
                    self.theta[l] = 0
                else:
                    self.theta[l] = np.sign(self.theta[l]) * self.C

                if self.theta[l] == 0:
                    self.R.append(l)
                    if debug >= 1: print "case 3a:", l, "in S to R"
                else:
                    self.E.append(l)
                    if debug >= 1: print "case 3b:", l, "in S to E"

                self.S.pop(li)
                # update R (3.12)
                self.remove_Rmat(li)

            elif flag == 4:  # case 4
                self.E.pop(li)
                if debug >= 1: print "case 4:", l, "in E to S"

            elif flag == 5:  # case 5
                self.R.pop(li)
                if debug >= 1: print "case 5:", l, "in R to S"

            if flag >= 4:
                # update R (3.13)
                # compute new gamma value using index l
                if use_kernel_matrix:
                    Ql = Q[l] #self.data.kernelf(l)
                else:
                    Ql = self.data.kernelf(l)
                Qsl1 = np.hstack((1, Ql[self.S]))

                if self.Rmat is None:
                    tmp_beta = beta
                    gamma_l = gamma[l]
                else:
                    tmp_beta = np.dot(-self.Rmat, Qsl1).reshape((-1, 1))
                    gamma_l = Ql[l] + np.dot(Qsl1, tmp_beta)
                    gamma[l] = gamma_l  ### Parrella's update gamma_l
                self.add_Rmat(tmp_beta, gamma_l, l)
                self.S.append(l)

                # parella set hx[l] to sign(hx[l]) * eps
                hx[l] = np.sign(hx[l]) * self.eps

            if flag <= 2:
                break

    def add_Rmat(self, beta, gamma, add_idx):
        """ increase R matrix based on added sample
            

            Parameters
            ==========
            beta    arraylike, shape=(n_svs + 1, 1)
            gamma   float
                    gamma value for the added sample
            add_idx int
                    index for the added sample
        """
        if self.Rmat is None:
            Qss = self.data.kernelf(add_idx, add_idx)
            # simple computation of 2d R matrix
            self.Rmat = np.array([[-Qss, 1.], [1., 0.]])
        else:
            n_samples, n_dim = self.Rmat.shape
            # append zero rows and columns
            self.Rmat = np.vstack((self.Rmat, np.zeros((1, n_dim))))
            self.Rmat = np.hstack((self.Rmat, np.zeros((n_samples + 1, 1))))

            beta1 = np.vstack((beta, 1))
            if abs(gamma) > self.tol:  # to prevent devide-by-zero
                self.Rmat += 1/gamma * np.dot(beta1, beta1.T)

    def remove_Rmat(self, k):
        """
            Remove k'th support vecture from R matrix

            Parameters
            ==========
            k   int
                ordinary number in set S to remove
        """
        if self.Rmat.shape[0] == 2 and k == 0:
            self.Rmat = None
            return
        k = k + 1 # first index (0) is for 1/0
        R_kk = self.Rmat[k, k]
        # delete k+1 rows and columns
        R_Ik = copy(self.Rmat)
        R_Ik = np.delete(R_Ik, k, 0)
        R_Ik = R_Ik[:, k:k+1]
        R_kI = copy(self.Rmat)
        R_kI = np.delete(R_kI, k, 1)
        R_kI = R_kI[k:k+1, :]
        self.Rmat = np.delete(self.Rmat, k, 0)
        self.Rmat = np.delete(self.Rmat, k, 1)

        
        # update R matrix
        if abs(R_kk) > self.tol:
            self.Rmat -= np.dot(R_Ik, R_kI) / R_kk
                
    def use(self, X, return_kernel=False):
        """ apply trained model on new data """

        ### TODO: for efficiency, check if I can reduce the number of calls
        if len(X.shape) == 1 or X.shape[0] == 1:
            Q = self.data.kernelf(X)
            pred = np.dot(Q, self.theta) + self.b
        else:
            # if batch of X is given, compute the kernel matrix
            # this use function should not be used with a large # of samples
            if return_kernel:
                Q = np.asarray(map(lambda x: self.data.kernelf(x), X))
                pred = np.dot(Q, self.theta) + self.b
            else:

                try: 
                    if tmp_global_cnt==0:
                        print "start counting"
                except NameError:
                    tmp_global_cnt=0
                ### TODO: FASTER!
                tmp_global_cnt += 1
                if tmp_global_cnt % 5 ==0:
                    print tmp_global_cnt
                if tmp_global_cnt == 100:
                    pdb.set_trace()
                pred = map(lambda x:
                        np.dot(self.data.kernelf(x), self.theta) + self.b, X)
                pred = np.asarray(pred)
                return pred

        if return_kernel:
            return pred, Q
        else:
            return pred

    def score(self, X, T):
        """ root mean square error """
        pred = self.use(X).flat
        return np.sqrt(np.mean((T - pred) ** 2))

    def update_index_after_data_removal(self, idx):
        if isinstance(idx, int):
            idx = [idx]
         # adjust R, S, E indices
        for ri in xrange(len(self.R)):
            n_del = 0
            for ti in idx:
                if self.R[ri] > ti:
                    n_del += 1
            self.R[ri] -= n_del
        for si in xrange(len(self.S)):
            n_del = 0
            for ti in idx:
                if self.S[si] > ti:
                    n_del += 1
            self.S[si] -= n_del
        for ei in xrange(len(self.E)):
            n_del = 0
            for ti in idx:
                if self.E[ei] > ti:
                    n_del += 1
            self.E[ei] -= n_del

        self.theta = np.delete(self.theta, idx)
        if self.data.n_samples == 0:
            self.b = 0
       
    def remove_data_from_R(self, idx):
        if isinstance(idx, int):
            idx = [idx]
        self.data.remove_data(idx)
        for ri in idx:
            self.R.remove(ri)

        self.update_index_after_data_removal(idx)

    def shrink(self, eps=1e-8):
        """ shrinking
            
            References
            ==========
            T. Joachims. Text Categorization with Support Vector Machine.
                http://www.cs.cornell.edu/People/tj/publications/joachims_98a.pdf

            Dong et al. A Fast SVM Training Algorithm
                http://www.springerlink.com/content/p9uh7yjmttuwdbcj/
        """

        if self.theta_history is None:
            self.theta_history = np.where(abs(self.theta) < eps)[0]
            return

        rem_idx = []
        for i in self.theta_history:
            if self.theta[i] < eps:
                if i in self.R:
                    rem_idx.append(i)
                elif debug >= 2:
                    print "skip data not in R"
        if len(rem_idx) > 0:
            if debug >= 1: print "removing ", len(rem_idx), "data..."
            self.remove_data_from_R(rem_idx)

        self.theta_history = np.where(abs(self.theta) < eps)[0]

    def unlearn(self, idx):
        """ decremental learning 

            direction of change q = sign(h(xc))
            no case 1
            case 2: theta_c changing from non-zero to zero

            Parameters
            ==========
            idx     int
                    the sample index to remove
        """
        # if data is in set R, remove it
        if idx in self.R:
            self.remove_data_from_R(idx)
            if debug > 1: print "unlearn", idx, "from R"
            return
        elif idx in self.S:
            rem_i = self.S.index(idx)
            self.S.pop(rem_i)
            self.remove_Rmat(rem_i)
            if debug > 1: print "unlearn", idx, "from S"
        elif idx in self.E:
            self.E.remove(idx)
            if debug > 1: print "unlearn", idx, "from E"

        # if not, gradually reduce the value of the coefficient to zero
        # while ensuring all the other samples in the training set continue to 
        # satisfy the KKT conditions.

        self.online_train(self.data.X[idx:idx+1], self.data.label[idx], unlearn=idx)

    def stabilize(self):
        """ correct the errors generated by floating point operations
            for the samples that do not satisfy KKT conditions,
            unlearn it and then re-train with it
        """

        while self.get_n_samples_in_KKT() != self.data.n_samples:
            sub_idx = 0
            for i in xrange(self.data.n_samples):
                cur_i = i - sub_idx
                if not self.check_KKT_conditions(cur_i):
                    if debug >= 1: print "stabilizing ", i
                    xc = self.data.X[cur_i]
                    yc = self.data.label[cur_i]
                    self.unlearn(cur_i)
                    self.online_train(xc, yc)
                    sub_idx += 1

    def get_n_samples_in_KKT(self):
        """ returns the number of samples that do not satisfy KKT conditions
        """
        fx = self.use(self.data.X).flat
        hx = fx - self.data.label
        abs_theta = np.abs(self.theta)
        abs_hx = np.abs(hx)

        sv_cond = np.logical_and(abs_theta > 0, abs_theta < self.C)
        sv_cond = np.logical_and(sv_cond, abs_hx <= self.eps + self.tol)
        err_cond = np.logical_and(abs_theta == self.C, hx > self.eps)
        nsv_cond = np.logical_and(abs_theta <= self.tol, hx < self.eps)
        return np.sum(sv_cond) + np.sum(err_cond) + np.sum(nsv_cond)

    def check_KKT_conditions(self, idx):
        """ check KKT condition of sample indexed by the parameter

            Parameters
            ==========
            idx     int
                    index for a sample to check KKT conditions
        """
        fx = self.use(self.data.X[idx]).flat
        hx = fx - self.data.label[idx]
        abs_theta = abs(self.theta[idx])
        abs_hx = abs(hx)

        if idx in self.S:
            if abs_theta > 0 and abs_theta < self.C and abs_hx == self.eps:
                return True
            else:
                return False
            
        if idx in self.E:
            if abs_theta == self.C and abs(hx) > self.eps:
                return True
            else:
                return False

        if idx in self.R:
            if abs_theta == 0 and abs_hx < self.eps:
                return True
            else:
                return False

    def leave_one_out(self):
        errors = []
        for cur_i in xrange(self.data.n_samples):
            svr_cv = copy(self)
            xc = svr_cv.data.X[cur_i]
            yc = svr_cv.data.label[cur_i]
            svr_cv.unlearn(cur_i)  # take the first one out
            error = svr_cv.score(xc, yc)
            if debug > 1:
                if cur_i in self.R: s="R"
                elif cur_i in self.S: s="S"
                else: s="E"
                print "[", xc,",", yc,"(",s,")]", svr_cv.use(xc), error
            errors.append(error)
            
            #self.online_train(xc, yc)  # readd to the last - diff. result
        return errors

    def unlearn_N(self, N=1, **params):
        """ remove N samples from AOSVR
        """
        bLOOCV = params.pop('LOOCV', False)

        if bLOOCV:
            ########################################################### 
            # after running LOOCV, remove N least significant samples
            ########################################################### 
            import time
            cur = time.time()
            print "starting LOOCV"
            errors = self.leave_one_out()
            print "leave one out>> ", time.time() - cur
            to_del = np.argsort(np.abs(errors))[:N]  # trim out N only
            to_del = np.sort(to_del)[::-1]  # delete from back
            cur = time.time()
            for i in to_del:
                self.unlearn(i)
            print "unlearn ", i, ">>", time.time() - cur
        else:
            ########################################################### 
            # As stated in AOSOR Paper: forget oldest sample
            # lemin modification - remove R first then forget
            ########################################################### 
            n_remain = N - len(self.R)
            for i in self.R:
                self.unlearn(i)

            if n_remain > 0:
                for i in xrange(n_remain):
                    self.unlearn(0)  # remove the oldest sample


"""
if __name__ == '__main__':

    test_params = False
    if False:
        X = np.linspace(0, 10, 101)
        T = X * 4 + 2# + np.random.normal(0, 0.3, len(X))
        X = X.reshape((-1, 1))
        svr = AOSVR(eps=0.2, C=0.5)
        n_samples = X.shape[0]

        # for shrinking test
        if True:
            #svr.batch_train(X[:n_samples], T[:n_samples], scratch=True)
            svr.batch_train(X[:n_samples], T[:n_samples])
            # *** the solution simply satisfies KKT conditions 
            #     and do not require stabilization
        else:
            n_div = 5
            n_part = n_samples / n_div
            n_cur = n_part
            for i in xrange(n_div):
                if i == 0:
                    svr.batch_train(X[:n_part], T[:n_part])
                else:
                    n_next = n_cur + n_part
                    svr.batch_train(X[n_cur:n_next], T[n_cur:n_next])
                    n_cur = n_next
                svr.shrink()
    else:
        if False: # simple sin # working param: C=1 gamma=30. eps=0.01
            X = np.linspace(0, 10, 101)
            T = np.sin(X)
        else:
            X = np.linspace(-10, 10, 101)
            T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3
        X = X.reshape((-1, 1))

        test_params = False
        test_C = [100.] #[1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1, 1e2]
        test_gamma = [10] #[1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1, 1e2]
        test_eps = [0.01] #[1e-2, 1e-1, .5, 1]

        scores = []
        params = []
        for C in test_C:
            for gamma in test_gamma:
                for eps in test_eps:
                    svr = AOSVR(eps=eps, C=C)
                    #svr.batch_train(X, T, scratch=True, kernel='rbf', gamma=gamma)
                    svr.batch_train(X, T, kernel='rbf', gamma=gamma)
                    print "==================================================="
                    print "C: ", C, "gamma: ", gamma, "eps: ", eps
                    scr = svr.score(X, T)
                    print "Score: ", scr
                    params.append((C, gamma, eps))
                    if np.isnan(scr):
                        scores.append(np.inf)
                    else:
                        scores.append(scr)

 
        if test_params:
            minidx = np.argmin(scores)
            tx = range(len(scores))
            print "best parameters:", params[minidx]
            plt.clf()
            plt.plot(tx, scores)
            plt.plot(tx, [scores[minidx]] * len(tx), 'r-')
            plt.xticks(tx, params, rotation=45)
            #print scores
            print "min: ", params[minidx], "score: ", scores[minidx]
            plt.show()

    if not test_params:
        if False:  # leave-one-out test
            #errors = svr.leave_one_out()
            svr.unlearn_N(5)

        if True:  # just plotting
            Y = svr.use(X)
            print "Score: ", svr.score(X, T)

            plt.clf()
            plt.plot(X, T, 'b-')
            plt.plot(X, Y, 'r-')
            plt.plot(X, Y-svr.eps, 'g--')
            plt.plot(X, Y+svr.eps, 'g--')
            plt.show()

        else:  # testing unlearn/relearn
 
            svr_org = copy(svr)

            plt.ion()
            xc = None
            print "theta:::", svr.theta
            print len(svr.theta)
            debug = 0
            for i in np.repeat(range(100), 2): #xrange(100):

                if xc is not None:
                    print "relearning", i
                    print "[", xc, yc, "]"
                    svr.online_train(xc, yc)
                    xc, yc = None, None
                else:
                    svr = copy(svr_org)
                    print "unlearning ... ", i
                    xc = svr.data.X[i]
                    yc = svr.data.X[i]
                    print "[", xc, yc, "]"
                    svr.unlearn(i)

                Y = svr.use(X)
                print "Score: ", svr.score(X, T)
                print "theta:::", svr.theta
                print len(svr.theta)
                #print Y
                
                plt.clf()
                plt.plot(X, T, 'b-')
                plt.plot(X, Y, 'r-')
                plt.plot(X, Y-svr.eps, 'g--')
                plt.plot(X, Y+svr.eps, 'g--')
                #plt.show()
                plt.draw()

                key = raw_input("press any key to continue...")

            
## TODO: still AOSVRQ is slow
##  V. remove oldest sample on training after learning new 
##    => even faster, but finding good number of samples is critical
##       since RL is different from time-series
##  2. online_learning code to weave
##  3. check removing methods
##      it seems that removing right after each iteration is not good choice
##      RL is different from time-series model
"""
