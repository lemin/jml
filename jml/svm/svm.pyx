""" Support Vector Machine
    Sequential Minimal Optimization
    For reference, please take a look at

    ..  [1] J.C. Platt, 
        Fast Training of Support Vector Machines Using 
        Sequential Minimal Optimization,
        Advances in Kernel Methods-Support Vector Learning.
        B. Scholkopf, C.J. C. Burges, and A.J. Smola, eds., 
        Cambridge, Mass.: MIT Press, 1999.

                                by Jake Lee (lemin)

    Contains:
        SVMData class for training data and kernel computation
        SVM class for classification

    example usage:

        X = np.vstack((np.random.normal(1, .1, (10,2)),
                        np.random.normal(2, .1, (10,2))))
        T = np.hstack(([-1] * 10, [1] * 10))

        svm = SVM()
        svm.train(X, T, kernel='poly')
        predicted, Y = svm.use(X)

    history:
        11/18/11
        - SVMData class moved out to a separate file

        11/06/11
        - first built SVM using Platt's SVM

    last modified: 11/29/11
        - fixed bug in index handling for i_nonzero_nonC
"""
import numpy as np
import random
from svmdata import SVMData
    

class SVM():
    """ John Platt's Sequential Minimal Optimization 
        
        Attributes
        ----------
        alpha   array, shape = (n_samples)
            Lagrangian multipliers
        w   array, shape = (n_dims)
            primary weight vector
        E   array, shape = (n_samples)
            error cache
        b   float
            bias
        data
            training data

        Methods
        -------
        take_step
            SMO joint optimization for selected two indices

        examine_example
            second heuristic for an index i1 with given first choice of i2, 

        smo_optimf
            first heuristic for an index i2

        train
            train support vector regression with SMO

        use
            apply the trained SVR

        score
            compute RMSE of the trained SVR

        References
        ----------
        J.C. Platt, Fast Training of Support Vector Machines Using 
        Sequential Minimal Optimization,
            http://research.microsoft.com/apps/pubs/?id=68391
    """

    def __init__(self):
        self.alpha = np.zeros(0)
        self.w = np.zeros(0)
        self.E = np.zeros(0)  # error cache
        self.b = 0
        self.data = None

    #def decision_func(self, idx):
    #    return np.sum(self.alpha * self.data.label *\
    #                    self.data.kernelf(idx)) - self.b

    #def objective_func(self, x):
    #    pass

    def take_step(self, i1, i2, alph2, y2, E2):
        """
            Precondition: data & alpha is preset based on data

            Parameters
            ----------
            i1, i2  int
                    index for first and second example
            alph2, y2, E2   float
                    additional i2 params, alpha2, label y2, and error
        """

        if i1 == i2:
            return 0

        alph1 = self.alpha[i1]
        y1 = self.data.label[i1]
        if alph1 > 0 and alph1 < self.C:
            E1 = self.E[i1]
        else:
            #E1 = self.decision_func(i1) - y1
            E1, _ = self.use(self.data.X[i1]) - y1

        s = y1 * y2

        if y1 == y2:
            L = max(0, alph2 + alph1 - self.C)
            H = min(self.C, alph2 + alph1)
        else:
            L = max(0, alph2 - alph1)
            H = min(self.C, self.C + alph2 - alph1)
    
        if L == H:
            return 0

        k11 = self.data.kernelf(i1, i1)
        k12 = self.data.kernelf(i1, i2)
        k22 = self.data.kernelf(i2, i2)
        eta = 2 * k12 - k11 - k22
        if eta < 0:
            a2 = alph2 - y2 * (E1 - E2) / eta
            if a2 < L: a2 = L
            elif a2 > H: a2 = H
        else:
            ### REVIEW OBJ FUNCTION at a2=L
            c1 = eta / 2
            c2 = y2 * (E1 - E2) - eta * alph2
            Lobj = c1 * L * L + c2 * L
            Hobj = c1 * H * H + c2 * H

            if Lobj > Hobj + self.eps:
                a2 = L
            elif Lobj < Hobj - self.eps:
                a2 = H
            else:
                a2 = alph2

        if a2 < 1e-8:
            a2 = 0
        elif a2 > self.C - 1e-8:
            a2 = self.C
        if abs(a2 - alph2) < self.eps * (a2 + alph2 + self.eps):
            #print a2, "-", alph2, "=", a2 - alph2
            return 0

        a1 = alph1 + s * (alph2 - a2)
        if a1 < 0:
            a2 += s * a1
            a1 = 0
        elif a1 > self.C:
            a2 += s * (a1 - self.C)
            a1 = self.C 

        # Update threshold to reflect change in Lagrange multipliers
        b1 = self.b + E1 + y1 * (a1 - alph1) * k11 + y2 * (a2 - alph2) * k12
        b2 = self.b + E2 + y1 * (a1 - alph1) * k12 + y2 * (a2 - alph2) * k22
        if a1 > 0 and a1 < self.C:
            bnew = b1
        elif a2 > 0 and a2 < self.C:
            bnew = b2
        else:
            bnew = (b1 + b2) / 2

        delta_b = bnew - self.b
        self.b = bnew

        t1 = y1 * (a1 - alph1)
        t2 = y2 * (a2 - alph2)

        # update weight vector to reflect change in a1 and a2 if SVM is linear
        if self.data.kernel == 'linear':
            self.w = self.w + t1 * self.data.X[i1] + t2 * self.data.X[i2]
            # TODO: for more efficienty treat sparse/binary vector differently
                
        # update error cache using new Lagrange multipliers
        self.E = self.E + t1 * self.data.kernelf(i1)\
                    + t2 * self.data.kernelf(i2) + delta_b
        ## TODO : kernel - func btn i1 and i2 ||| kernelf - func btn i1 and X

        # store a1 and a2 into alpha array
        self.alpha[i1] = a1
        self.alpha[i2] = a2
        return 1

    def examine_example(self, i2):
        alph2 = self.alpha[i2]
        y2 = self.data.label[i2]
        if alph2 > 0 and alph2 < self.C:
            E2 = self.E[i2]
        else:
            #E2 = self.decision_func(i2) - y2
            E2, _ = self.use(self.data.X[i2]) - y2
        r2 = E2 * y2

        if (r2 < -self.tol and alph2 < self.C) or (r2 > self.tol and alph2 > 0):
            #i_nonzero_nonC = np.where(self.alpha[np.where(self.alpha < self.C)] > 0)[0]
            i_nonzero_nonC = np.where(np.logical_and(self.alpha > 0, self.alpha < self.C))[0]
            n_nonzero_nonC = len(i_nonzero_nonC)
            if n_nonzero_nonC  > 1:
                # second choice heuristic
                i1 = np.argmax(abs(self.E - E2))
                if self.take_step(i1, i2, alph2, y2, E2) == 1:
                    return 1

            randi = random.sample(xrange(n_nonzero_nonC), n_nonzero_nonC)
            for ri in randi:
                i1 = i_nonzero_nonC[ri]
                if self.take_step(i1, i2, alph2, y2, E2) == 1:
                    return 1

            randi = random.sample(xrange(self.data.n_samples), self.data.n_samples)
            for i1 in randi:
                if self.take_step(i1, i2, alph2, y2, E2) == 1:
                    return 1
        return 0

    def smo_optimf(self, n_samples):
        num_changed = 0
        examine_all = True

        while num_changed > 0 or examine_all:
            num_changed = 0
            if examine_all:
                for i2 in xrange(n_samples):
                    num_changed += self.examine_example(i2)
            else:
                #i_nonzero_nonC = np.where(self.alpha[np.where(self.alpha < self.C)] > 0)[0]
                i_nonzero_nonC = np.where(np.logical_and(self.alpha > 0, self.alpha < self.C))[0]
                for i2 in i_nonzero_nonC:
                    num_changed += self.examine_example(i2)
            if examine_all:
                examine_all = False
            elif num_changed == 0:
                examine_all = True
       

    def train(self, X, T, **params):
        n_samples, n_dim = X.shape
        self.alpha = np.zeros(n_samples)
        self.w = np.zeros(n_dim)
        self.E = np.zeros(n_samples)  # error cache

        self.data = SVMData(X, T)

        self.tol = params.pop('tol', 1e-3)
        self.eps = params.pop('eps', 1e-3)
        self.C = params.pop('C', 0.1)

        self.data.set_kernel(params.pop('kernel', 'linear'), **params)  ##did not pass params

        self.smo_optimf(n_samples)

    def use(self, X):

        
        if self.data.kernel == 'linear':
            pred = np.dot(X, self.w) - self.b
        else:
            # to avoid memory error loop through each examples
            if len(X.shape) < 2:
                X = X.reshape((1, -1))
            pred = np.zeros(len(X))
            for i in xrange(len(X)):
                pred[i] = np.dot(self.data.kernelf(X[i]), self.alpha)
            pred -= self.b

        return np.sign(pred), pred


    def score(self, X, T):
        pred, _ = self.use(X)
        return np.sqrt(np.mean((T - pred) ** 2))


if __name__ == '__main__':

    X = np.vstack((np.random.normal(1, .1, (10,2)),
                    np.random.normal(2, .1, (10,2))))
    T = np.hstack(([-1] * 10, [1] * 10))

    if False:
        ### Simplified SMO
        # TODO - not working: fix later
        a, b, f = smo(X, T)
        print a, b
        print f(X)

    if True:
        ### SMO (John Platt)
        svm = SVM()
        svm.train(X, T, kernel='linear')
        predicted, Y = svm.use(X)
        print svm.alpha
        print predicted#, Y
        print svm.score(X, T)
