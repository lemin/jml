""" SVM data and kernel 

                                by Jake Lee (lemin)

    Contains:
        SVMData class for training data and kernel computation

    example usage:

        data = SVMData(X, T)
        data.set_kernel('rbf', gamma=1.)
        ...
        # kernel function
        data.kernelf(x, y)  # K(x, y)
        data.kernelf(x)     # K(x, X*)
        data.kernelf(i, j)  # K(Xi, Xj)
        data.kernelf(i)     # K(Xi, X*)
        

    history:
        11/06/11
        - first built w/ Platt's SMO

    last modified: 11/18/11
        - SVMData class moved out to a separate file from smo.py
"""
import numpy as np
from copy import deepcopy as copy

class SVMData():
    """ Data/Label class

        Parameters
        ----------
        X   array, shape=(n_samples, n_dim)
            training data
        T   array, shape=(n_samples)
            training label

        Attributes
        ----------

        Methods
        -------
    """
    def __init__(self, X, T=None):
        self.set_data(X)
        if T is not None:
            self.set_label(T)
        self.kernel = 'linear'

    def set_label(self, T):
        if not isinstance(T, np.ndarray):
            T = np.asarray([T])
        self.label = copy(T)

    def set_data(self, X):
        if X is None:
            self.X = None
            self.n_samples = self.n_dims = 0
            return
        if len(X.shape) == 1:
            X.shape = 1, -1
        self.X = copy(X)
        self.n_samples, self.n_dims = self.X.shape

    def add_data(self, x, y):
        if self.X is None:
            self.X = np.asarray(x)
            if len(self.X.shape) == 1:
                self.X = self.X.reshape((1, -1))
        else:
            self.X = np.vstack((self.X, x))
        # MULTIDIM SUPPORT
        if y is not None:
            if len(y.shape) > 1 and y.shape[1] > 1:
                self.label = np.vstack((self.label, y))
            else:
                self.label = np.hstack((self.label, y))
        self.n_samples, self.n_dims = self.X.shape
        if self.kernel == 'rbf':
            if isinstance(x, np.ndarray) and len(x.shape) == 2:
                x = x.flatten()
            if self.Xsquare is None:
                self.Xsquare = np.asarray([np.dot(x, x)])
            else:
                self.Xsquare = np.append(self.Xsquare, np.dot(x, x))

    def remove_data(self, i):
        if self.X is None: return
        self.X = np.delete(self.X, i, 0)
        # MULTIDIM SUPPORT
        if len(self.label.shape) > 1:
            self.label = np.delete(self.label, i, 0)
        else:
            self.label = np.delete(self.label, i)
        self.n_samples, self.n_dims = self.X.shape
        if self.kernel == 'rbf':
            self.Xsquare = np.delete(self.Xsquare, i)

    def set_kernel(self, k, **params):
        self.kernel = k
        self.gamma = params.pop('gamma', 1.)
        self.coef0 = params.pop('coef', 1.)
        self.degree = params.pop('degree', 2)
        self.dims = params.pop('dim', [1, 1])
        if self.X is None:
            self.Xsquare = None
            self.Xsquare1 = None
            self.Xsquare2 = None
            return
        if k == 'rbf':
            self.Xsquare = np.asarray(map(lambda x: np.dot(x, x), self.X))
        elif k == 'rbfpair':
            self.Xsquare1 = np.asarray(map(lambda x: np.dot(x, x), self.X[:, :self.dims[0]]))
            self.Xsquare2 = np.asarray(map(lambda x: np.dot(x, x), self.X[:, self.dims[0]+1:]))

    def kernelf(self, x, y=None):
        """ compute kernel

            Parameters
            ==========
            x   int / array-like shape = (n_dim)
            y   int / array-like shape = (n_dim) / None
        """

        if isinstance(x, int):
            i1, i2 = x, y
            if i2 is None:
                dot_prod = np.dot(self.X[i1], self.X.T)
                if self.kernel == 'rbf':
                    square_term = self.Xsquare[i1] + self.Xsquare
            else:
                dot_prod = np.dot(self.X[i1], self.X[i2])
                if self.kernel == 'rbf':
                    square_term = self.Xsquare[i1] + self.Xsquare[i2]
        else:
            if not isinstance(x, np.ndarray):
                x = np.asarray(x)
            if y is None:
                y = self.X
            elif not isinstance(y, np.ndarray):
                y = np.asarray(y)
            dot_prod = np.dot(x, y.T)
            if self.kernel == 'rbf':
                if len(x.shape) == 1:
                    x.shape = 1, -1
                x2 = np.asarray(map(lambda t: np.dot(t, t), x))
                y2 = np.asarray(map(lambda t: np.dot(t, t), y))
                square_term = np.repeat(x2.reshape((-1, 1)), len(y2), axis=1) +\
                              np.repeat(y2.reshape((1, -1)), len(x2), axis=0)
                # NOTE: repeat is faster than tile!
                #square_term = np.tile(x2, (1, len(y2))) +\
                #              np.tile(y2, (len(x2), 1))

        if self.kernel == 'linear':
            return dot_prod
        elif self.kernel == 'rbf':
            #print "rbf:", x, y, np.exp(-self.gamma * (square_term - 2 * dot_prod))
            return np.exp(-self.gamma * (square_term - 2 * dot_prod))
        elif self.kernel == 'poly':
            return (self.gamma * dot_prod + self.coef0) ** self.degree
        elif self.kernel == 'sigmoid':
            return np.tanh(self.gamma * dot_prod + self.coef0)
        elif self.kernel == 'rbfpair':
            self.Xsquare = self.Xsquare1
            self.kernel = 'rbf'
            if len(x.shape) == 1:
                x.shape = 1, len(x)
            if len(y.shape) == 1:
                y.shape = 1, len(y)
            k1 = self.kernelf(x[:, :self.dims[0]], y[:, :self.dims[0]])
            #print "k1", x[:, :self.dims[0]], y[:, :self.dims[0]], k1
            self.Xsquare = self.Xsquare2
            k2 = self.kernelf(x[:, self.dims[0]:], y[:, self.dims[0]:])
            #print "k2", x[:, self.dims[0]:], y[:, self.dims[0]:], k2
            self.kernel = 'rbfpair'
            return k1 * k2
        ## TODO: precomputed kernel processing
        ## TODO: check how multiple i2 handling is done in libsvm
