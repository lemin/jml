""" Support Vector Regression
    Sequential Minimal Optimization for regression
    For reference, please take a look at

    ..  [1] S.K. Shevade, S.S. Keerthi, C. Bhattacharyya, and K.R..K. Murphy,
        Improvements to the SMO Algorithm for SVM Regression,
        IEEE Transactions on Neural Networks, vol.11, no.5, pp.1188-1193, 2000
    ..  [2] A.J. Smola and B. Scholkopf,
        A tutorial on support vector regression,
        Statistics and computing, vol.14, no.3, pp.199-222, 2004
    ..  [3] G.W. Flake and S. Lawrence,
        Efficient SVM regression training with SMO,
        Machine Learning, vol.46, no.1, pp.271-290, 2002
    ..  [4] J.C. Platt,
        Fast Training of Support Vector Machines Using 
        Sequential Minimal Optimization,
        Advances in Kernel Methods-Support Vector Learning.
        B. Scholkopf, C.J. C. Burges, and A.J. Smola, eds.,
        Cambridge, Mass.: MIT Press, 1999.

                                by Jake Lee (lemin)

    example usage:

        X = np.linspace(-10, 10, 100)
        T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3
        X = X.reshape((-1,1))

        svr = SVR()
        svr.train(X, T, eps=1e-3, C=1e3,
                    kernel='rbf', gamma=10.5, coef=0)
        #svr.train(X, T, eps=1e-2, kernel='poly', degree=2, coef=1)
        #svr.train(X, T, eps=1e-1, kernel='linear')
        Y = svr.use(X)

    history:
        11/11/11
        - first built SVR using SMO

    last modified: 11/11/11
        - first built SVR using SMO

    TODO:
        - *** check weights are bounded in box constraints properly
        - combine SMO optim
        - inherit SMO for SVM and SVR
            see Keerthi's paper for classification
        - nu-SVM and MF-SVM
        - SVR-SCG
"""
import numpy as np
import random
from svmdata import SVMData
import matplotlib.pyplot as plt
from copy import deepcopy as copy


class SVR():
    """ Shevade et al.'s modified version of SMO for SVR

        Attributes
        ----------
        alpha, alpha_   array  shape=(n_samples)
            Lagrangian multipliers
        F   array, shape=(n_samples)
            error cache
        b   float
            bias
        data    SVMData
            training data
        I0a, I0b, I0, I1, I2, I3    list
            index sets based on alpha and alpha_ conditions

        Methods
        -------
        take_step
            SMO joint optimization for selected two indices

        examine_example(self, i2):
            second heuristic for an index i1 with given first choice of i2, 

        train(self, X, T, **params):
            train support vector regression with SMO

        use(self, X, add_b=True):
            apply the trained SVR

        score(self, X, T):
            compute RMSE of the trained SVR

        References
        ----------
        Shevade et al. Improvements to the SMO Algorithm for SVM Regression
            http://citeseer.ist.psu.edu/viewdoc/summary?doi=10.1.1.146.375
    """

    def __init__(self):
        self.alpha = np.zeros(0)
        self.alpha_ = np.zeros(0)
        self.F = np.zeros(0)  # F-cache
        self.b = 0
        self.data = None

        self.I0a = []
        self.I0b = []
        self.I0 = []
        self.I1 = []
        self.I2 = []
        self.I3 = []

    def take_step(self, i1, i2, alph2, alph2_, F2):
        """
            Precondition: data & alpha is preset based on data

            Parameters
            ----------
            i1, i2  int
                    indices for first and second example
        """

        if i1 == i2:
            return 0

        alph1 = self.alpha[i1]
        alph1_ = self.alpha_[i1]
        F1 = self.F[i1]

        a2old, a2old_ = alph2, alph2_
        a1old, a1old_ = alph1, alph1_

        k11 = self.data.kernelf(i1, i1)
        k12 = self.data.kernelf(i1, i2)
        k22 = self.data.kernelf(i2, i2)
        eta = k11 + k22 -2. * k12
        gamma = alph1 - alph1_ + alph2 - alph2_

        case1 = case2 = case3 = case4 = True
        finished = False
        deltaphi = F1 - F2
        large_enough_change = False

        while not finished:
            # This loop is passed at most three times
            # Case variables needed to avoid attempting amall changes twice

            if case1 and\
                (alph1 > 0 or (alph1_ == 0 and deltaphi > 0)) and\
                (alph2 > 0 or (alph2_ == 0 and deltaphi < 0)):
                L = max(0, gamma - self.C)
                H = min(self.C, gamma)

                if L < H:
                    if eta <= 0:
                        LObj = -L * deltaphi
                        HObj = -H * deltaphi
                        if LObj > HObj:
                            a2 = L
                        else:
                            a2 = H
                    else:
                        a2 = alph2 - (deltaphi / eta)
                        a2 = min(a2, H)
                        a2 = max(L, a2)
                    a1 = alph1 - (a2 - alph2)
                    # update alph1, alph2
                    if abs(a1 - alph1) > self.eps * (a1 + alph1 + self.eps) or\
                       abs(a2 - alph2) > self.eps * (a2 + alph2 + self.eps):
                        large_enough_change = True
                        alph2 = a2
                        alph1 = a1
                else:
                    finished = True
                case1 = False

            elif case2 and\
                (alph1 > 0 or (alph1_ == 0 and deltaphi > 2 * self.eps)) and\
                (alph2_ > 0 or (alph2 == 0 and deltaphi > 2 * self.eps)):
                L = max(0, -gamma)
                H = min(self.C, self.C - gamma)

                if L < H:
                    if eta <= 0:
                        LObj = L * (-2. * self.eps + deltaphi)
                        HObj = H * (-2. * self.eps + deltaphi)
                        if LObj > HObj:
                            a2 = L
                        else:
                            a2 = H
                    else:
                        a2 = alph2_ + ((deltaphi - 2 * self.eps) / eta)
                        a2 = min(a2, H)
                        a2 = max(L, a2)
                    a1 = alph1 + (a2 - alph2_)
                    # update alph1, alph2_
                    if abs(a1 - alph1) > self.eps * (a1 + alph1 + self.eps) or\
                       abs(a2 - alph2_) > self.eps * (a2 + alph2_ + self.eps):
                        large_enough_change = True
                        alph2_ = a2
                        alph1 = a1
                else:
                    finished = True
                case2 = False

            elif case3 and\
                (alph1_ > 0 or (alph1 == 0 and deltaphi < -2 * self.eps)) and\
                (alph2 > 0 or (alph2_ == 0 and deltaphi < -2 * self.eps)):
                L = max(0, gamma)
                H = min(self.C, gamma + self.C)

                if L < H:
                    if eta <= 0:
                        LObj = -L * (2 * self.eps + deltaphi)
                        HObj = -H * (2 * self.eps + deltaphi)
                        if LObj > HObj:
                            a2 = L
                        else:
                            a2 = H
                    else:
                        a2 = alph2 - ((deltaphi + 2 * self.eps) / eta)
                        a2 = min(a2, H)
                        a2 = max(L, a2)
                    a1 = alph1_ + (a2 - alph2)
                    # update alph1_, alph2
                    if abs(a1 - alph1_) > self.eps * (a1 + alph1_ + self.eps) or\
                       abs(a2 - alph2) > self.eps * (a2 + alph2 + self.eps):
                        large_enough_change = True
                        alph2 = a2
                        alph1_ = a1
                else:
                    finished = True
                case3 = False

            elif case4 and\
                (alph1_ > 0 or (alph1 == 0 and deltaphi < 0)) and\
                (alph2_ > 0 or (alph2 == 0 and deltaphi > 0)):
                L = max(0, -gamma - self.C)
                H = min(self.C, -gamma)

                if L < H:
                    if eta <= 0:
                        LObj = L * deltaphi
                        HObj = H * deltaphi
                        if LObj > HObj:
                            a2 = L
                        else:
                            a2 = H
                    else:
                        a2 = alph2_ + (deltaphi / eta)
                        a2 = min(a2, H)
                        a2 = max(L, a2)
                    a1 = alph1_ - (a2 - alph2_)
                    # update alph1_, alph2_
                    if abs(a1 - alph1_) > self.eps * (a1 + alph1_ + self.eps) or\
                       abs(a2 - alph2_) > self.eps * (a2 + alph2_ + self.eps):
                        large_enough_change = True
                        alph2_ = a2
                        alph1_ = a1
                else:
                    finished = True
                case4 = False
            else:
                finished = True

            # update deltaphi
            #deltaphi = F1 -F2 - (eta * ((alph2 - alph2_) - (a2old - a2old_)))
            deltaphi += eta * ((alph2 - alph2_) - (a2old - a2old_))

        if not large_enough_change: 
            return 0

        # Update F-cache for I0 - {i1,i2}
        t1 = (a1old - a1old_) - (alph1 - alph1_)
        t2 = (a2old - a2old_) - (alph2 - alph2_)
        if len(self.I0) > 0:
            # update error cache except for i1 and i2
            upd_idx = list(set(self.I0).difference([i1, i2]))
            self.F[upd_idx] += (t1 * self.data.kernelf(i1)\
                                + t2 * self.data.kernelf(i2))[upd_idx]
        # update F-cache for {i1, i2}
        self.F[i1] += t1 * k11 + t2 * k12
        self.F[i2] += t1 * k12 + t2 * k22


        # Store the changes in alpha and alpha_ array
        self.alpha[i1] = alph1
        self.alpha[i2] = alph2
        self.alpha_[i1] = alph1_
        self.alpha_[i2] = alph2_

        # Update I0, I1, I2, I3
        for a, a_, i in [ (alph1, alph1_, i1), (alph2, alph2_, i2)]:
            i0a = i0b = False
            if (a > 0 and a < self.C): i0a = True
            if (a_ > 0 and a_ < self.C): i0b = True

            if i0a:
                self.I0a.append(i)
            elif i in self.I0a:
                self.I0a.remove(i)

            if i0b:
                self.I0b.append(i)
            elif i in self.I0b:
                self.I0b.remove(i)
                
            if i0a or i0b:
                self.I0.append(i)
            elif i in self.I0:
                self.I0.remove(i) 

            if a == 0 and a_ == 0:
                self.I1.append(i)
            elif i in self.I1:
                self.I1.remove(i)

            if a == 0 and a_ == self.C:
                self.I2.append(i)
            elif i in self.I2:
                self.I2.remove(i)

            if a == self.C and a_ == 0:
                self.I3.append(i)
            elif i in self.I3:
                self.I3.remove(i)

        ###################################################
        # compute (i_low, b_low) and (i_up, b_up)
        # using only i1, i2, and indices in I0

        #recompute the bias
        self.b_low = -np.inf
        self.b_up = np.inf
        for i in self.I0:
            if i in self.I0b\
                and self.F[i] + self.eps > self.b_low:
                self.b_low = self.F[i] + self.eps
                self.i_low = i
            elif i in self.I0a\
                and self.F[i] - self.eps > self.b_low:
                self.b_low = self.F[i] - self.eps
                self.i_low = i
            elif i in self.I0a\
                and self.F[i] - self.eps < self.b_up:
                self.b_up = self.F[i] - self.eps
                self.i_up = i
            elif i in self.I0b\
                and self.F[i] + self.eps < self.b_up:
                self.b_up = self.F[i] + self.eps
                self.i_up = i

        for i in [i1, i2]:
            if i in self.I2\
                and self.F[i] + self.eps > self.b_low:
                self.b_low = self.F[i] + self.eps
                self.i_low = i
            elif i in self.I1\
                and self.F[i] - self.eps > self.b_low:
                self.b_low = self.F[i] - self.eps
                self.i_low = i
            elif i in self.I3\
                and self.F[i] - self.eps < self.b_up:
                self.b_up = self.F[i] - self.eps
                self.i_up = i
            elif i in self.I1\
                and self.F[i] + self.eps < self.b_up:
                self.b_up = self.F[i] + self.eps
                self.i_up = i
     
        return 1

    def examine_example(self, i2):
        alph2 = self.alpha[i2]
        alph2_ = self.alpha_[i2]
        if i2 in self.I0:
            F2 = self.F[i2]
        else:
            F2 = self.data.label[i2] - self.use(self.data.X[i2:i2+1,:], False)
            if isinstance(F2, np.ndarray):
                F2 = F2[0]
            self.F[i2] = F2
            # update (b_low, i_low) or (b_up, i_up) using (F2, i2)...
            if i2 in self.I1:
                if F2 + self.eps < self.b_up:
                    self.b_up = F2 + self.eps
                    self.i_up = i2
                elif F2 - self.eps > self.b_low:
                    self.b_low = F2 - self.eps
                    self.i_low = i2
            elif i2 in self.I2 and F2 + self.eps > self.b_low:
                self.b_low = F2 + self.eps
                self.i_low = i2
            elif i2 in self.I3 and F2 - self.eps < self.b_up:
                self.b_up = F2 - self.eps
                self.i_up = i2

        # Check optimality using current b_low and b_up and, 
        # if violated, find an index i1 to do joint optim with i2
        optimality = 1
        blow_chk_ = self.b_low - (F2 - self.eps)
        bup_chk_ = (F2 - self.eps) - self.b_up
        blow_chk = self.b_low - (F2 + self.eps)
        bup_chk = (F2 + self.eps) - self.b_up
        if i2 in self.I0a:
            if blow_chk_ > 2 * self.tol:
                optimality = 0
                i1 = self.i_low
                if bup_chk_ > blow_chk_:
                    i1 = self.i_up
            elif bup_chk_ > 2 * self.tol:
                optimality = 0
                i1 = self.i_up
                if blow_chk_ > bup_chk_:
                    i1 = self.i_low

        elif i2 in self.I0b:
            if blow_chk > 2 * self.tol:
                optimality = 0
                i1 = self.i_low
                if bup_chk > blow_chk:
                    i1 = self.i_up
            elif bup_chk > 2 * self.tol:
                optimality = 0
                i1 = self.i_up
                if blow_chk > bup_chk:
                    i1 = self.i_low

        elif i2 in self.I1:
            if blow_chk > 2 * self.tol:
                optimality = 0
                i1 = self.i_low
                if bup_chk > blow_chk:
                    i1 = self.i_up
            elif bup_chk_ > 2 * self.tol:
                optimality = 0
                i1 = self.i_up
                if blow_chk_ > bup_chk_:
                    i1 = self.i_low

        elif i2 in self.I2:
            if bup_chk > 2 * self.tol:
                optimality = 0
                i1 = self.i_up

        elif i2 in self.I3:
            if blow_chk_ > 2 * self.tol:
                optimality = 0
                i1 = self.i_low
            
        if optimality == 1:
            return 0

        if self.take_step(i1, i2, alph2, alph2_, F2) == 1:
            return 1
        else:
            return 0

    def smo_optimf(self, n_samples):
        self.I1 = range(n_samples)

        # choose i randomly
        i = random.sample(self.I1, 1)[0]

        # maintains bound for b
        self.b_up = self.data.label[i] + self.eps
        self.b_low = self.data.label[i] - self.eps
        self.i_up = self.i_low = i

        num_changed = 0
        examine_all = True

        while num_changed > 0 or examine_all:
            num_changed = 0
            if examine_all:
                for i2 in xrange(n_samples):
                    num_changed += self.examine_example(i2)
            else:
                if True:
                    # Modification-2 loop
                    inner_loop_success = 1
                    while True:
                        i2 = self.i_low
                        alph2 = self.alpha[i2]
                        alph2_ = self.alpha_[i2]
                        F2 = self.F[i2]
                        i1 = self.i_up
                        inner_loop_success = self.take_step(i1, i2, alph2, alph2_, F2)
                        num_changed += inner_loop_success

                        if self.b_up > self.b_low - 2 * self.tol or\
                            inner_loop_success == 0:
                            num_changed = 0
                            break
                else:
                    # Modification-1 loop
                    for i2 in self.I0:
                        num_changed += self.examine_example(i2)
                        # check if optimality on I0 is attained...
                        if self.b_up > self.b_low - 2 * self.tol:
                            num_changed = 0
                            break

            if examine_all:
                examine_all = False
            elif num_changed == 0:
                examine_all = True

        # final choice of b for inference
        self.b = (self.b_up + self.b_low) / 2.

    def train(self, X, T, **params):
        n_samples, n_dim = X.shape
        self.alpha = np.zeros(n_samples)
        self.alpha_ = np.zeros(n_samples)
        self.F = np.zeros(n_samples)  # error cache

        if self.data is None:
            self.data = SVMData(X, T)
        else:
            self.data.set_data(X)
            self.data.set_label(T)

        self.tol = params.pop('tol', 1e-3)
        self.eps = params.pop('eps', 1e-3)
        self.C = params.pop('C', 0.1)

        self.data.set_kernel(params.pop('kernel', 'linear'), **params)

        self.smo_optimf(n_samples)

    def use(self, X, add_b=True):
        alpha = self.alpha - self.alpha_

        if self.data.kernel == 'linear':
            w = np.dot(alpha, self.data.X)
            pred = np.dot(X, w)
        else:
            # to avoid memory error loop through each examples
            pred = np.zeros(len(X))
            for i in xrange(len(X)):
                pred[i] = np.dot(self.data.kernelf(X[i:i+1, :]), alpha)
                # can make it faster by handling sparse alpha
            #pred = np.dot(alpha, self.data.kernelf(self.data.X, self.data.X))

        return pred + self.b if add_b else pred

    def score(self, X, T):
        pred = self.use(X)
        return np.sqrt(np.mean((T - pred) ** 2))

    def set_weights(self, alpha, b):
        self.alpha, self.alpha_ = alpha
        self.b = b


class SVRQ(SVR):
    """ Support Vector Regression for Reinforcement Learning

        parameters
        ----------
        _gamma  float
            learning rate (different from gamma for RBF kernel)

        PROBLEM::
            1. initial value selection when SVR is not trained
                no kernel data is given. computation is impossible
            2. when new data comes, would it be good? 
                using previous learned alpha as starting point 
                can be or cannot be helpful. 
                Q: is alpha data dependent? 
    """
    def __init__(self, **params):
        SVR.__init__(self)
        self._gamma = params.pop('gamma', .9)

        self.alpha = np.zeros(0)
        self.alpha_ = np.zeros(0)
        self.F = np.zeros(0)  # error cache

    def train(self, X, Y, R, Q, **params):
        n_samples, n_dim = X.shape
        tmp_init = False
        if len(self.alpha) > 0:
            tmp_alpha = copy(self.alpha)
            tmp_alpha_ = copy(self.alpha)
            tmp_F = copy(self.alpha)
            tmp_init = True
        self.alpha = np.zeros(n_samples)
        self.alpha_ = np.zeros(n_samples)
        self.F = np.zeros(n_samples)  # error cache
        if tmp_init:
            self.alpha[:len(tmp_alpha)] = tmp_alpha
            self.alpha_[:len(tmp_alpha)] = tmp_alpha_
            self.F[:len(tmp_alpha)] = tmp_F

        self.tol = params.pop('tol', 1e-3)
        self.eps = params.pop('eps', 1e-3)
        self.C = params.pop('C', 0.1)

        if self.data is None:
            self.data = SVMData(X)
        else:
            self.data.set_data(X)
        self.data.set_kernel(params.pop('kernel', 'linear'), **params)

        Y = self.use(X).reshape((-1,1))  # reshape to match w/ rltrain
        T = R + self._gamma * Q - Y
        self.data.set_label(T.flatten())

        self.smo_optimf(n_samples)

    def use(self, X, add_b=True):
        if self.data is None:
            return np.zeros((len(X),1)) # return 0 as an prediction (ask to Chuck)
        # reshape to match w/ rltrain
        return SVR.use(self, X, add_b).reshape((-1, 1))


if __name__ == '__main__':
    
    if True:
        X = np.linspace(0, 10, 100)
        T = X * 4 + 2# + np.random.normal(0, 0.3, len(X))
        X = X.reshape((-1,1))
        mode = 'linear'
    else:
        X = np.linspace(-10, 10, 100)
        T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3
        #T = 2.0 * np.sin(X)# + 0.314 * X ** 2 + 0.278 * X + 10.3
        X = X.reshape((-1,1))
        mode = 'nonlinear'

    plt.ion()
    for i in xrange(1):
        svr = SVR()
        if mode == 'linear':
            svr.train(X, T, eps=1e-10)
        else:
            svr.train(X, T, eps=1e-3, C=1e3,
                        kernel='rbf', gamma=10.5, coef=0)
            #svr.train(X, T, eps=1e-2, kernel='poly', degree=1, coef=1)
        Y = svr.use(X)

        print svr.score(X, T)
        #print np.sum(svr.alpha>0)
        #print np.sum(svr.alpha_>0)

        plt.clf()
        plt.plot(X, T, 'b-')
        plt.plot(X, Y, 'r-')
        plt.draw()
     
        key = raw_input("press any key to continue...")

    # TODO:
    #   V. add kernels at least rbf, polynormial, and sigmoid
    #   V. compare the codes with pseudo-code
    #   +   currently the outcome is not stable
    #       find out the reason
    #   4. RL with SVR (with any working version)
    #   5. multidimensional expansion
    #   6. pretraining
    #
    #   +. looking libsvm, use Platt's SMO as it is for regression 

    # PLAN:
    #   1. create SMO, SMOMOD classes
    #   2. make SVM and SVR classes that inherits SMO/SMOMOD
    #   3. final interface will be SVM/SVR/nuSVM/nuSVR
    #   4. optimization will be combined
    #       by adding simple interior point methods, mean field theory


    ##
