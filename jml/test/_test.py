import numpy as np 
from jml.svm.aosvr import AOSVR
import matplotlib.pyplot as plt


if __name__ == '__main__':

    test_params = False
    if False:
        X = np.linspace(0, 10, 101)
        T = X * 4 + 2# + np.random.normal(0, 0.3, len(X))
        X = X.reshape((-1, 1))
        svr = AOSVR(eps=0.2, C=0.5)
        n_samples = X.shape[0]

        # for shrinking test
        if True:
            #svr.batch_train(X[:n_samples], T[:n_samples], scratch=True)
            svr.batch_train(X[:n_samples], T[:n_samples])
            # *** the solution simply satisfies KKT conditions 
            #     and do not require stabilization
        else:
            n_div = 5
            n_part = n_samples / n_div
            n_cur = n_part
            for i in xrange(n_div):
                if i == 0:
                    svr.batch_train(X[:n_part], T[:n_part])
                else:
                    n_next = n_cur + n_part
                    svr.batch_train(X[n_cur:n_next], T[n_cur:n_next])
                    n_cur = n_next
                svr.shrink()
    else:
        if False: # simple sin # working param: C=1 gamma=30. eps=0.01
            X = np.linspace(0, 10, 101)
            T = np.sin(X)
        else:
            X = np.linspace(-10, 10, 101)
            T = 10 * np.sin(X) + 0.314 * X ** 2 + 0.278 * X + 10.3
        X = X.reshape((-1, 1))

        test_params = False
        test_C = [100.] #[1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1, 1e2]
        test_gamma = [10] #[1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1, 1e2]
        test_eps = [5.01] #[1e-2, 1e-1, .5, 1]

        scores = []
        params = []
        for C in test_C:
            for gamma in test_gamma:
                for eps in test_eps:
                    svr = AOSVR(eps=eps, C=C)
                    #svr.batch_train(X, T, scratch=True, kernel='rbf', gamma=gamma)
                    svr.batch_train(X, T, kernel='rbf', gamma=gamma)
                    print "==================================================="
                    print "C: ", C, "gamma: ", gamma, "eps: ", eps
                    scr = svr.score(X, T)
                    print "Score: ", scr

                    print "SV's = ", len(svr.S)
                    params.append((C, gamma, eps))
                    if np.isnan(scr):
                        scores.append(np.inf)
                    else:
                        scores.append(scr)

 
        if test_params:
            minidx = np.argmin(scores)
            tx = range(len(scores))
            print "best parameters:", params[minidx]
            plt.clf()
            plt.plot(tx, scores)
            plt.plot(tx, [scores[minidx]] * len(tx), 'r-')
            plt.xticks(tx, params, rotation=45)
            #print scores
            print "min: ", params[minidx], "score: ", scores[minidx]
            plt.show()

    if not test_params:
        if False:  # leave-one-out test
            #errors = svr.leave_one_out()
            svr.unlearn_N(5)

        if True:  # just plotting
            Y = svr.use(X)
            print "Score: ", svr.score(X, T)

            plt.clf()
            plt.plot(X, T, 'b-')
            plt.plot(X, Y, 'r-')
            plt.plot(X, Y-svr.eps, 'g--')
            plt.plot(X, Y+svr.eps, 'g--')
            plt.show()


