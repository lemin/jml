import numpy as np
import matplotlib.pyplot as plt
from classify import LDA, QDA, LogisticRegression, NonlinearLogisticRegression
from util.exp import dnorm
from mpl_toolkits.mplot3d import Axes3D
import pdb


##################################################################
# 1D Plots
##################################################################
# 1 dimensional plot for visualization
def plot1d(model, train, test, disc):

    n_plots = 6
    ylim = (min(model.classes)-.5, max(model.classes)+.5)
    xlim = (np.min(test[0]), np.max(test[0]))
    
    plotidx = 0
    # training data
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    X, T = train
    for c in model.classes:
        ci = np.where(T==c)
        plt.plot(X[ci], T[ci], 'o')
    plt.ylabel("Train Class")
    plt.ylim(ylim)
    plt.xlim(xlim)
    
    X, T = test
    Xs = model.standardizeX(X)
    px_c = np.hstack(map(lambda p: dnorm(Xs, p[0], p[1]), model._prob))
    # p(x | c=k)
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    plt.plot(X, px_c)
    plt.ylabel("$p(x|$Class$=k)$")

    # p(x)
    prior = map(lambda p: p[2], model._prob)
    px = np.sum(np.asarray(px_c * prior), axis=1)
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    plt.plot(X, px)
    plt.ylabel("$p(x)$")

    # p(c=k | x)
    pc_x = px_c * prior / px.reshape((-1, 1))
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    plt.plot(X, pc_x)
    plt.ylabel("$p($class$=k|x)$")
    plt.ylim([-.2, 1.2])

    # disc functions
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    plt.plot(X, disc)
    plt.ylabel("$\delta_k$")
        
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    for c in model.classes:
        ci = np.where(T==c)
        plt.plot(X[ci], T[ci], '-', linewidth=3)
    plt.ylabel("predicted Class")
    plt.ylim(ylim)
    plt.xlim(xlim)


# 1 dimensional plot for visualization of logitic regression
def plot1d_logistic(model, train, test, Y):

    n_plots = 3
    ylim = (min(model.classes)-.5, max(model.classes)+.5)
    xlim = (np.min(test[0]), np.max(test[0]))
    
    plotidx = 0
    # training data
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    X, T = train
    for c in model.classes:
        ci = np.where(T==c)
        plt.plot(X[ci], T[ci], 'o')
    plt.ylabel("Train Class")
    plt.ylim(ylim)
    plt.xlim(xlim)
    
    X, T = test
    # p(c=k | x)
    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    plt.plot(X, Y)
    plt.ylabel("$p($class$=k|x)$")
    plt.ylim([-.2, 1.2])

    plotidx += 1
    plt.subplot(n_plots, 1, plotidx)
    for c in model.classes:
        ci = np.where(T==c)
        plt.plot(X[ci], T[ci], '-', linewidth=3)
    plt.ylabel("predicted Class")
    plt.ylim(ylim)
    plt.xlim(xlim)


##################################################################
# 2D Plots
##################################################################
# 2 dimensional plot for visualization
def plot2d(fig, model, train, test, disc):

    n_plots = 3
    ylim = (min(model.classes)-.5, max(model.classes)+.5)
    xlim = (np.min(test[0]), np.max(test[0]))
    colors = ['b', 'r', 'g', 'c', 'm', 'y', 'k']
    
    plotidx = 0
    # training data
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    X, T = train
    for c in model.classes:
        ci = np.where(T==c)
        ax.scatter(X[ci,0], X[ci,1], T[ci], 'o', c=colors[c])
    ax.set_zlabel("Train Class")
    #plt.ylim(ylim)
    #plt.xlim(xlim)
    
    Xx, Xy, T = test
    X = np.vstack((Xx.flat, Xy.flat)).T
    Xs = model.standardizeX(X)
    px_c = np.hstack(map(lambda p: dnorm(Xs, p[0], p[1]), model._prob))
    # p(x | c=k)
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    for ci in xrange(px_c.shape[1]):
        ax.plot_surface(Xx, Xy, px_c[:, ci].reshape(Xx.shape), color=colors[ci])
    ax.set_zlabel("$p(x|$Class$=k)$")

    # p(x)
    prior = map(lambda p: p[2], model._prob)
    px = np.sum(np.asarray(px_c * prior), axis=1)
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    ax.plot_surface(Xx, Xy, px.reshape(Xx.shape))
    ax.set_zlabel("$p(x)$")

    # p(c=k | x)
    pc_x = px_c * prior / px.reshape((-1, 1))
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    for ci in xrange(pc_x.shape[1]):
        ax.plot_surface(Xx, Xy, pc_x[:, ci].reshape(Xx.shape), color=colors[ci])
    ax.set_zlabel("$p($class$=k|x)$")
    #plt.ylim([-.2, 1.2])

    # disc functions
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    for ci in xrange(disc.shape[1]):
        ax.plot_surface(Xx, Xy, disc[:, ci].reshape(Xx.shape), color=colors[ci])
    ax.set_zlabel("$\delta_k$")
        
    plotidx += 1
    ax = fig.add_subplot(n_plots, 2, plotidx, projection='3d')
    ax.contourf(Xx, Xy, T.reshape(Xx.shape))
    ax.set_zlabel("predicted Class")
    #plt.ylim(ylim)
    #plt.xlim(xlim)


# 2 dimensional plot for visualization of logitic regression
def plot2d_logistic(fig, model, train, test, Y):

    n_plots = 3
    ylim = (min(model.classes)-.5, max(model.classes)+.5)
    xlim = (np.min(test[0]), np.max(test[0]))
    colors = ['b', 'r', 'g', 'c', 'm', 'y', 'k']
    
    plotidx = 0
    # training data
    plotidx += 1
    ax = fig.add_subplot(n_plots, 1, plotidx, projection='3d')
    X, T = train
    for c in model.classes:
        ci = np.where(T==c)
        ax.scatter(X[ci,0], X[ci,1], T[ci], 'o', c=colors[c])
    ax.set_zlabel("Train Class")
    #plt.ylim(ylim)
    #plt.xlim(xlim)
    
    Xx, Xy, T = test
    # p(c=k | x)
    plotidx += 1
    ax = fig.add_subplot(n_plots, 1, plotidx, projection='3d')
    for ci in xrange(Y.shape[1]):
        ax.plot_surface(Xx, Xy, Y[:, ci].reshape(Xx.shape), color=colors[ci])
    ax.set_zlabel("$p($class$=k|x)$")
    #plt.ylim([-.2, 1.2])

    plotidx += 1
    ax = fig.add_subplot(n_plots, 1, plotidx, projection='3d')
    ax.contourf(Xx, Xy, T.reshape(Xx.shape))
    plt.ylabel("predicted Class")
    ax.set_zlabel("predicted Class")
    #plt.ylim(ylim)
    #plt.xlim(xlim)


##################################################################
# 1-Dimension 
##################################################################
X = np.hstack((np.random.normal(1, .1, 10),
                np.random.normal(2, .1, 10),
                np.random.normal(3, .1, 10))).reshape((-1, 1))
T = np.hstack(([1] * 10, [2] * 10, [3] * 10))
Xtest = np.linspace(0, 4, 100).reshape((-1, 1))

if True:
    # QDA
    qda = QDA()
    qda.train(X, T)
    Y, disc = qda.use(Xtest, stodisc=True)
    plt.figure(1)
    plot1d(qda, (X, T), (Xtest, Y), disc)

    # LDA
    lda = LDA()
    lda.train(X, T)
    Y, disc = lda.use(Xtest, stodisc=True)
    plt.figure(2)
    plot1d(lda, (X, T), (Xtest, Y), disc)

    # Linear Logistic Regression
    llr = LogisticRegression()
    llr.train(X, T)
    predicted, Y = llr.use(Xtest)
    plt.figure(3)
    plot1d_logistic(llr, (X, T), (Xtest, predicted), Y)

    # Non-Linear Logistic Regression
    ni = 1
    nh = 3
    no = 2
    nlr = NonlinearLogisticRegression([ni, nh, no])
    nlr.train(X, T, niter=10000)
    predicted, Y = nlr.use(Xtest)
    plt.figure(4)
    plot1d_logistic(nlr, (X, T), (Xtest, predicted), Y)
    plt.show()

##################################################################
# 2-Dimension
##################################################################
X = np.vstack((np.random.normal(1, .1, (10,2)),
                np.random.normal(2, .1, (10,2)),
                np.random.normal(3, .1, (10,2))))
T = np.hstack(([1] * 10, [2] * 10, [3] * 10))
tmp = np.linspace(0, 4, 100).reshape((-1, 1))
Xtestx, Xtesty = np.meshgrid(tmp, tmp)
Xtest = np.vstack((Xtestx.flat, Xtesty.flat)).T

if False:
    # QDA
    qda = QDA()
    qda.train(X, T)
    Y, disc = qda.use(Xtest, stodisc=True)
    fig = plt.figure(5)
    plot2d(fig, qda, (X, T), (Xtestx, Xtesty, Y.reshape(Xtestx.shape)), disc)

    # LDA
    lda = LDA()
    lda.train(X, T)
    Y, disc = lda.use(Xtest, stodisc=True)
    fig = plt.figure(6)
    plot2d(fig, lda, (X, T), (Xtestx, Xtesty, Y.reshape(Xtestx.shape)), disc)

    # Linear Logistic Regression
    llr = LogisticRegression()
    llr.train(X, T)
    predicted, Y = llr.use(Xtest)
    fig = plt.figure(7)
    plot2d_logistic(fig, llr, (X, T), (Xtestx, Xtesty, predicted.reshape(Xtestx.shape)), Y)

    # Non-Linear Logistic Regression
    ni = 2
    nh = 5
    no = 2
    nlr = NonlinearLogisticRegression([ni, nh, no])
    nlr.train(X, T, niter=10000)
    predicted, Y = nlr.use(Xtest)
    fig = plt.figure(8)
    plot2d_logistic(fig, nlr, (X, T), (Xtestx, Xtesty, predicted.reshape(Xtestx.shape)), Y)
    plt.show()
