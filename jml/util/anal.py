""" This is a collection of store/load/analyze functions
    this can be a general purpose analysis tools, but
    currently we only consider reinforcement learning test results.

    storing and loading test results
        - npz do not store cython objects (need to have warning)
        - include file-naming rule
        - exp notes storing
    analysis tools include 
        - plotting (boxplot/trend w. variance)
        - short text summary (for short glace)
        - table presentation

                                    by lemin (Minwoo Jake Lee)

    Currently anal have
        store(fname, **options) for store results into npz
        load(fname) for load npz
        plot(fname)
        boxplot(fname)
        short(fname)
        table(fname)

    late modified: Mar 21, 2015
"""
import numpy as np
from exp import get_numbered_fn, moving_avg
import matplotlib.pyplot as plt
import collections
import time


# save experimental results in npz
#
# @fname    filename to store
# @note     short note about an experiment
# @params   parameters to be used
# @results  experiment results in dictionary
def save(fname, note, params, **results):
    """ A function for storing results in a specified npz
        Usage:
            store('results.npz', rlframe=RLframe, rtrace=rtrace)
    """
    if ".npz" not in fname:
        fname = fname + ".npz"
    fn = get_numbered_fn(fname, ext=".npz")
    if 'rlframe' in results.keys():
        print "Storing reinforcement learning results to ", fn
    else:
        print "Storing to ", fn, "..."

    print "\t", note
    print "\t", params 
    np.savez(fn, note=note, params=params, **results)


# load experimental results
#
# @fname    filename to load
def load(fname, verbose=True):
    """ A function for loading results in a specified npz
        Usage:
            load('results.npz')
    """

    note = None
    params = None
    if ".npz" not in fname:
        fname = fname + ".npz"
    results = np.load(fname)
    if 'note' in results.keys():
        note = results['note'].item()
    if 'params' in results.keys():
        params = results['params'].item()

    if 'rtrace' in results.keys():
        rtrace = results['rtrace'].item()
    else:
        rtrace = None
    if 'rlframe' in results.keys():
        if verbose:
            print "loading reinforcement learning results from ", fname

        rlframe = results['rlframe'].item()
        return results, note, params, rlframe, rtrace
    else:
        if verbose:
            print "loading from ", fname, "..."
        return results, note, params, None, rtrace


# plot results stored in a file
#
# @fname    filename to load
# @opt      list of plots (avgR/sumR/steps/ other keys)
def plot(fname, fig=None, key=None, opt=['mean'], subplot=True, **pltparams):
    """ A function to plot results
        Usage:
            plot('results.npz', key='rtrace')
    """
    results, note, params, rlframe, rtrace = load(fname)
    print note
    print params
    if key is None:
        to_read = results
    else:
        to_read = results[key].item()

    new_fig = pltparams.pop("newFig", True)
    smooth_q = pltparams.pop("smooth", -1)

    if new_fig and fig is None and not subplot:
        fig = plt.figure(figsize=(5 * len(opt), 5))

    nplots = len(opt)
    for i, k in enumerate(opt):
        if subplot:
            fig.add_subplot(1, nplots, i+1)
        elif new_fig:
            plt.figure()
        if k in to_read.keys():
            if smooth_q > 0:
                plt.plot(moving_avg(to_read[k], smooth_q), linewidth=3)
            else:
                plt.plot(to_read[k], linewidth=3)
            plt.ylabel(k)
        # TODO: add options for xlabel, legend or other styles


def plots(fnames, fig=None, key=None, opt=['mean'], subplot=True, **pltparams):
    """ A function to plot results with variance
        Usage:
            plot(['r1,npz', 'r2.npz'], key='rtrace')
    """
    new_fig = pltparams.pop("newFig", True)
    smooth_q = pltparams.pop("smooth", -1)

    if new_fig and fig is None and not subplot:
        fig = plt.figure()

    # reads
    if not isinstance(fnames, collections.Iterable):
        fnames = [fnames]
    to_read = []
    for fn in fnames:
        results, note, params, rlframe, rtrace = load(fn)
        if key is None:
            to_read.append(results)
        else:
            to_read.append(results[key].item())

    nplots = len(opt)
    for i, k in enumerate(opt):
        if subplot:
            fig.add_subplot(1, nplots, i+1)
        elif new_fig:
            plt.figure()

        print k
        if k in to_read[0].keys():
            alllist = [np.array(res[k], dtype=np.float) for res in to_read
                       if k in res.keys()]
            mean = np.mean(alllist, 0)
            std = np.std(alllist, 0)
            if smooth_q > 0:
                plt.plot(moving_avg(mean, smooth_q), linewidth=3)
                plt.fill_between(range(len(mean)),
                                 moving_avg(mean-std, smooth_q),
                                 moving_avg(mean+std, smooth_q),
                                 alpha=0.5)
            else:
                plt.plot(mean, linewidth=3)
                plt.fill_between(range(len(mean)), mean-std, mean+std,
                                 alpha=0.5)
            plt.ylabel(k)


# short result summary
#   currently, print first 10% and last 10% avg rewards and total steps
# @fname    filename to load
def short(fname, header=False):
    try:
        results, note, params, rlframe, rtrace = load(fname, False)
        if note is not None:
            print note

        if rtrace is not None:
            steps = rtrace['steps']
            rewards = rtrace['sum']
            portion = int(len(rewards) * .1)
            first, last = np.mean(rewards[:portion]), np.mean(rewards[-portion:])
            fs, ls = np.mean(steps[:portion]), np.mean(steps[-portion:])

            # header
            if header:
                print "%30s\t%23s\t%23s\tdec/inc" %\
                    ("filename", "first (R / steps)", "last (R / steps)")
            if last > first or ls < fs:
                print '\033[91m',
            else:
                print '\033[0m',
            print "%30s\t%10.4f / %10.4f\t%10.4f / %10.4f\t%10.4f / %10.4f" %\
                (fname[-30:], first, fs, last, ls, last-first, ls-fs)
            if last > first or ls < fs:
                print '\033[0m',
    except Exception as e:
        print "Error:", e.message


class Timer():
    """ Experimental timer to check elpased time

        usage
        =====
            timer = Timer()
            timer.start("time taken for test:")
            ...
            timer.end()
    """
    def __init__(self):
        self.start_time = 0
        self.str_msg = ""

    def start(self, str_msg):
        self.str_msg = str_msg
        #print "CHECK_TIME: ", str_msg,
        self.start_time = time.time()

    def end(self):
        end = time.time()
        print self.str_msg, end - self.start_time
