""" parameter test tools
                                    by lemin (Minwoo Jake Lee)

    late modified: Apr 5, 2015
"""
#import sys
#import random
import json
import os
import string
from copy import deepcopy as copy
from datetime import datetime
from subprocess import Popen, PIPE, call, check_output
from itertools import imap, chain, product

from ..rl.models import *
from ..rl import *
import anal
from exp import get_numbered_fn
#from __future__ import division
#prog = eval(prog)


#
#  display progress of an experiment
#
#   @id     randomly assigned id for an experiment
#
def display_progress(id):

    def parse_progress(str_in):
        prog, cur, elapsed = str_in.split()
        prog, nreps = prog.split('/')
        prog, nreps = int(prog), int(nreps)
        cur = float(cur)
        elapsed = float(elapsed)

        unit_elapse = elapsed / prog
        remain = nreps - prog
        time_take = unit_elapse * remain
        finish = datetime.fromtimestamp(cur + time_take).strftime("%m/%d %H:%M")

        return prog, nreps, time_take, finish

    if "/tmp/.progress_" in id:
        # id contains the whole file path
        filen = str(id)
    else:
        filen = ''.join(["/tmp/.progress_", str(id)])

    if "sshmaster" in id:
        # master filename for ssh: /tmp/.progress_sshmaster_...
        with open(filen, "r") as f:
            for line in f:
                machine, user, path = line.split()
                if path != "None":
                    try:
                        proc = Popen(["ssh", "%s@%s" % (user, machine),
                                      "tail -1 %s" % path], stdout=PIPE)
                        s_progress = proc.stdout.readline()
                        prog, nreps, time_take, finish = parse_progress(s_progress)
                        
                        print machine, "\t",
                        print "%d / %d\t%f s\t%s" % (prog, nreps, time_take, finish)
                    except ValueError:
                        print "%s FAILED" % (path)
    else:
        proc = Popen(["tail", "-1", filen], stdout=PIPE)
        out, err = proc.communicate() 
        s_progress = out.splitlines()

        prog, nreps, time_take, finish = parse_progress(s_progress[-1])

        print "current step: %d / %d" % (prog, nreps)
        print "estiamted remaining time: ", time_take, "s"
        print "experiment will be done at ", finish


#####################################################
# Parameter test (with multiple runs) #
#   What to test
#       list of parameter / list of modules
#
#   How to test
#       multiple machines via ssh/MPI/CUDA/Hadoop
#
#   When to present
#       in the beginning/on progressing/in the end
#
#   how to present
#       screen / file (json) / repository
#
#   what to present
#       test summary / test result / estimated time to complete / errors

# parameter test
#

class ParamTest:

    def __init__(self, testfn="test.json"):

        self.testfn = '/'.join([os.getcwd(), testfn])
        self.ssh_tester = "~/lib/python/jml/util/testjson.py"

        # read test configuration
        with open(testfn) as f:
            test_conf = json.load(f)

        self.test_name = test_conf['TestName']
        self.test_note = test_conf['Note']
        if 'repeat_opt' in test_conf.keys():
            # repeat option: individual or serial
            #    individual treats each test as a separate test
            #    serial tests the same one in one machine
            self.repeat_opt = test_conf['repeat_opt']
        else:
            self.repeat_opt = "individual"
            
        if 'repeat' in test_conf.keys():
            self.n_repeat = test_conf['repeat']
        else:
            self.n_repeat = 1

        if 'method' in test_conf.keys():
            self.test_method = test_conf['method']
        else:
            self.test_method = 'serial'

        self.b_store = test_conf['b_store']
        # list of machines
        self.machines = {}
        for cmd in test_conf['machines']:
            if "cmd:" in cmd:
                #proc = os.popen(cmd[4:])
                #lines = proc.readlines()
                lines = check_output(cmd[4:], shell=True).strip().split('\n')

                #self.machines.extend(map(string.strip, lines))
                #s_machines, s_cores = zip(*map(string.split, lines))
                list_machines = map(string.split, lines)
            else:
                # parse format ["machine_name #cores", ...]
                #s_machines, s_cores = zip(*cmd)
                list_machines = map(string.split, cmd)
            #self.machines.extend(s_machines)

            # use dictionary comprehension
            self.machines.update({k: int(v) for k,v in list_machines})

        test_params_common = test_conf['test_params_template']
        self.tests = []
        if test_conf.has_key('search_params_template'):
            search_params_common = test_conf['search_params_template']
        else:
            search_params_common = None

        for test in test_conf['tests']:

            # environmental model
            env  = test['env']
            self.env = env['name']
            if self.env.lower() == 'Acrobot'.lower():
                model = acrobot.Acrobot(**env['params'])
            elif self.env.lower() == 'boat'.lower():
                model = boat.Boat()
            elif self.env.lower() == 'bicycle'.lower():
                model = bicycle.Bicycle(**env['params'])
            elif self.env.lower() == 'marble'.lower():
                model = marble.Marble(**env['params'])
            elif self.env.lower() == 'DMarble'.lower():
                model = marble.DMarble(**env['params'])
            elif self.env.lower() == 'mtcar'.lower():
                model = mtcar.MtCar(**env['params'])
            elif self.env.lower() == 'multist'.lower():
                model = multist.MultiState(**env['params'])
            elif self.env.lower() == 'octopus'.lower():
                model = octopus.Octopus(**env['params'])
            elif self.env.lower() == 'octarm'.lower():
                model = octopus.OctArm(**env['params'])
            elif self.env.lower() == 'cartpole'.lower():
                model = pendulum.RlCartPole(**env['params'])
            elif self.env.lower() == 'xor'.lower():
                model = xor.nXOR(**env['params'])

            for k, v in env['attr'].iteritems():
                setattr(model, k, v)
                
            # test parameters
            test_params = copy(test_params_common)
            test_params.update(test['test_params'])

            # search parameters
            if search_params_common is None:
                if test.has_key('search_params'):
                    search_params = test['search_params']
                else:
                    search_params = {}
            else:
                search_params = copy(search_params_common)
                if test.has_key('search_params'):
                    search_params.update(test['search_params'])
                search_params.pop("__exp__", None)

            # algorithm model
            algo = test['algo']
            self.algo = algo['name']
            if self.algo.lower() == 'nn'.lower():
                rlframe = nn.RLtrainNN(model)
                n_units = [model.nnNI]
                n_units.extend(algo['params']['n_units'])
                rlframe.make(n_units)
            elif self.algo.lower() == 'rvm'.lower():
                rlframe = rvm.RLtrainRVM(model)
                rlframe.make(test_params['Gamma'])
            elif self.algo.lower() == 'orvm'.lower():
                rlframe = sgd.RLsgdRVM(model)
                rlframe.make(test_params['Gamma'])
            elif self.algo.lower() == 'svm'.lower():
                rlframe = aosvr.RltrainSVR()
            elif self.algo.lower() == 'gq'.lower():
                rlframe = sgd.RLsgdGQ()
            elif self.algo.lower() == 'rgd'.lower():
                rlframe = rgd.RltrainRGD()
            elif self.algo.lower() == 'table'.lower():
                rlframe = table.RltrainTBL()

            for k, v in algo['attr'].iteritems():
                setattr(rlframe, k, v)

            if 'progress_filen' in algo['attr'].keys():
                progress_fn = algo['attr']['progress_filen']# + "-" + str(test_i)
            else:
                progress_fn = ""


            if test_conf['b_store'] == 1:
                path = test['store_path']
                optstr = test['fn_optstr']
                dirpath = os.path.expanduser(path)
                dirpath = dirpath[:dirpath.rfind('/')]
                if not os.path.exists(dirpath):
                    os.makedirs(dirpath)
            else:
                path = None
                optstr = None

            if len(search_params) > 0:
                s_prms = product(*search_params.values())
                s_keys = search_params.keys()
                for s_i in s_prms:
                    str_params = ""
                    for s_k, s_v in zip(s_keys, s_i):
                        test_params[s_k] = s_v
                        str_params += ("_" + s_k + str(s_v))
                    

                    test_info = copy((self.algo, self.env,
                                 rlframe, model, test_params,
                                 path, optstr + str_params,
                                 progress_fn + str_params))

                    if self.n_repeat > 1 and self.repeat_opt == 'individual':
                        self.tests.extend([test_info] * self.n_repeat)
                    else:
                        self.tests.append(test_info)
            else:
                test_info = copy((self.algo, self.env,
                             rlframe, model, test_params,
                             path, optstr, progress_fn))
                if self.n_repeat > 1 and self.repeat_opt == 'individual':
                    self.tests.extend([test_info] * self.n_repeat)
                else:
                    self.tests.append(test_info)

    def sort_machine_list(self):
        s_machines = " ".join(self.machines.keys())
        cmd_rup = "rup -s "
        cmd_sort = " | awk '{print $1\" \" $6}' | sort -n -k2"

        #proc = os.popen(''.join([cmd_rup, s_machines, cmd_sort]))
        proc = check_output(''.join([cmd_rup, s_machines, cmd_sort]),
                            shell=True).strip().split('\n')

        #read = proc.readlines()
        #print read
        # leaving 2 extra core, count how many cores it can use
        sorted_machpairs = [[m, self.machines[m] - (round(float(l))+2)]\
                            for m, l in imap(string.split, proc)]
                            #for m, l in imap(string.split, read)]

        ## allowing duplicate runs in one machines 
        ## as the amount of the countedx available cores
        #  limit max runs in a machine to be 4
        sorted_machines = [[m] * min(int(r) if r > 0 else 0, 4)\
                           for m, r in sorted_machpairs]
        self.sorted_machines = list(chain(*sorted_machines))

    def test_ssh(self):
        """ test in multiple machines with ssh"""
        if len(self.tests) > len(self.sorted_machines):
            print "not enough machines (", len(self.sorted_machines), ")",
            print " for ", len(self.tests), " tests"
            return

        master_progressf = "/tmp/.progress_sshmaster_" +  self.testfn.split('/')[-1]
        with open(master_progressf, "w") as f:
            #subprocesses = []
            for i, test in enumerate(self.tests):

                # when running in other machines, local machine does not have a progress log
                # for better presentation, main progress log will store machines and filenames

                machine = self.sorted_machines[i]
                if len(test[-1]) > 0:
                    prog_fn = ''.join([test[-1], "-", str(i)])
                else:
                    prog_fn = ""
                print ">>>>> ", prog_fn, "<<<<<"

                proc = Popen(["whoami"], stdout=PIPE)
                s_user = proc.stdout.readline().strip()

                f.write("%s %s %s\n" % (machine, s_user,
                             prog_fn if len(prog_fn) > 0 else "NONE"))

                cmd = ' '.join(["nice -n 19", self.ssh_tester, self.testfn, str(i)])
                print "running test", i, "in ", machine, "..."
                ssh_cmd = ''.join(["ssh ", s_user, "@", machine, " ", cmd, "&"])
                #print ssh_cmd
                call(ssh_cmd, shell=True)
                #import pdb; pdb.set_trace()
                #ssh_proc = Popen(["ssh ", ''.join([s_user, "@", machine]),
                #                  ''.join([" \"", cmd, "\""])],
                #                  stdout=PIPE, stderr=PIPE)
                #subprocesses.append(ssh_proc)


    def test_one(self, algo, env, rlframe, model,
                 test_params, path, optstr, progf, index=-1):
        if index != -1:
            progf += ("-" + str(index))
            rlframe.progress_filen = progf
        print ">>>>> ", progf, "<<<<<"
        print "testing... ", algo, env, datetime.now().time()
        n_repeat = 1
        if self.repeat_opt == 'serial':
            print "\trepeating ", self.n_repeat, " times"
            n_repeat = self.n_repeat
        print self.test_note

        #############################################################
        ### serial repetition option is added
        ### now, it needs to update progress for serial reps

        for rep in range(n_repeat):
            print "\ttest #", rep
            rtrace, epstrace, _ = rlframe.train(**test_params)

            if path is not None:
                fnsto = path + "_".join([algo, env, optstr])
                anal.save(fnsto, note=self.test_note,
                          params=test_params,
                          rtrace=rtrace,
                          rlframe=rlframe,
                          json=self.testfn
                          )
            print "\tDone."
        print "_______________________________________________"

    def test_pp(self):
        """ parallel test """
        pass

    def test_serial(self):
        """ local test """
        for algo, env, rlframe, model, test_params,\
            path, optstr, progf in self.tests:
            self.test_one(algo, env, rlframe, model, test_params,
                          path, optstr, progf)

    def test_hadoop(self):
        pass

    def test(self):

        if self.test_method == 'serial':
            self.test_serial()
        elif self.test_method == 'ssh':
            self.sort_machine_list()
            self.test_ssh()
        elif self.test_method == 'mpi':
            # read machine pool
            self.test_pp()
        elif self.test_method == 'hadoop':
            self.test_hadoop()

"""
    def write_test_results():
    def write_test_progress():
"""

if __name__ == '__main__':
    tester = ParamTest("test.json")
