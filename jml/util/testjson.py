#!/bin/env python
""" read and run parameter test one experiment in local machine

    NOTE:
    this python script is designed for ssh workhorse
    thus, if the json method is set to 'ssh', 
    it need to change the configuration.

                            by lemin (jake lee)
"""
import sys
from jml.util.paramtest import ParamTest

def usage():
    print "Usage:"
    print "\tparamtest.py test_conf_file [index]"
    sys.exit(1)

n_args = len(sys.argv)
index = -1
if n_args == 2:
    filen = sys.argv[1]
elif n_args == 3:
    filen = sys.argv[1]
    index = int(sys.argv[2])
else:
    usage()

tester = ParamTest(sys.argv[1])
tester.test_method = 'serial'
tester.test_one(*tester.tests[index], index=index)
