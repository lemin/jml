#! /usr/bin/env python
#
from distutils.core import setup, Extension
from Cython.Distutils import build_ext 
from sys import version
if version < '2.2.3':
    from distutils.dist import DistributionMetadata
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None


DISTNAME = 'jml'
VERSION = '0.80a'
DESCRIPTION = "python modules for machine learning and reinforcement learning"
AUTHOR = "Minwoo Jake Lee"
EMAIL = "lemin@cs.colostate.edu"
URL = ""
pkg = ['jml', 'jml.classify', 'jml.regress', 'jml.svm', 'jml.util',
        'jml.rvm', 'jml.rl', 'jml.rl.models' ]
ext_pkg = 'jml'
ext = [
    Extension('svm.svmdata', ['jml/svm/svmdata.pyx']),
    Extension('svm.aosvr', ['jml/svm/aosvr.pyx']),
    Extension('svm.svr', ['jml/svm/svr.pyx']),
    Extension('svm.svm', ['jml/svm/svm.pyx']),
    Extension('rvm.rvm', ['jml/rvm/rvm.pyx']),
    Extension('rvm.orvm', ['jml/rvm/orvm.pyx']),
    Extension('regress.nnet', ['jml/regress/nnet.pyx']),
    Extension('util.grad', ['jml/util/grad.pyx']),
          ]

setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      author=AUTHOR,
      author_email=EMAIL,
      packages=pkg,
      cmdclass = {'build_ext': build_ext},
      ext_package=ext_pkg,
      ext_modules=ext,
      classifiers=[
          'Intended Audience :: Science/Research',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: Python Software Foundation License',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Programming Language :: C',
          'Topic :: Software Development',
          'Topic :: Scientific/Engineering'
          ]
     )
